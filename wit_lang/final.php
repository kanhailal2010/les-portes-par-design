<?php
$data['english'] = array(
	'pickup'				=>	'Pick up ordered product from les portes shop',
	'preferred'			=>	'Provide preferred transport and account number and we will ship it to you',
	'carrier'			=>	'Arrange shipping of orders from your carrier',
	'pickOrder'			=>	'For Pick up of Orders',
	'pickupDate'			=>	'Desired Pickup date',
	'preferTrans'			=>	'Preferred Transport',
	'accNo'				=>	'Account No',
	'totalCost'			=>	'Total Cost',
	'shippingCost'		=>	'Shipping Cost',
	'cancel'				=>	'Cancel',
	'clickToPay'			=>	'Click to Pay'
	);

$data['french'] = array(
	'pickup'				=>	'Ramasser la commande',
	'preferred'			=>	'Compagnie de transport préféré et no. du compte',
	'carrier'			=>	'Organiser livraison utilisant votre transport',
	'pickOrder'			=>	'Pour le ramassage des commandes',
	'pickupDate'			=>	'Date de ramassage préféré',
	'preferTrans'			=>	'Preferred Transport',
	'accNo'				=>	'Numero de compte',
	'totalCost'			=>	'Coût Total',
	'shippingCost'		=>	'Coût du fret',
	'cancel'				=>	'Supprimer',
	'clickToPay'			=>	'Appuyer pour payer'
	);
?>

