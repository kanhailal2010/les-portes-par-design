<?php
$data['english'] = array(
		'signin'				=>	'Sign In',
		'newuser'			=>	'New User',
		'createNew'			=> 	'Create a New Account',
		'loginHere'			=> 	'Login Here',
		'email'				=> 	'Email',
		'password'			=> 	'Password',
		'forgot'				=> 	'Forgot Password'
);

$data['french'] = array(
		'signin'				=>	'Ouvrir une session',
		'newuser'			=>	'Nouveau utilisateur',
		'createNew'			=> 	'Creer un nouveau compte',
		'loginHere'			=> 	'Connecter ici',
		'email'				=> 	'Courriel',
		'password'			=> 	'Mot de passe',
		'forgot'				=> 	' Mot de passe oublié'
);
?>

