<?php
$data['english'] = array(
		'shoppingCart'		=>	'Shopping Cart',
		'clearCart'			=> 	'Clear Cart',
		'profile'			=> 	'Profile',
		'dimension'			=> 	'Dimensions',
		'quantity'			=> 	'Quantity',
		'price'				=> 	'Price',
		'total'				=> 	'Total',
		'subTotal'				=> 	'Sub Total',
		'createQuote'			=>	'Create Quote',
		'noOfHoles'			=>	'No of Holes',
		
		'step'				=>	'Step',
		'profileType'			=>	'Profile Type',
		'profileFinish'		=>	'Profile Finish',
		'insertType'			=>	'Insert Type',
		'doorDimension'		=>	'Door Dimension',
		'orientation'			=>	'Orientation & Drilling holes',
		
		'doorPreview'			=>	'Door Preview',
		'doyouhinge'			=>	'Do you require Hinge holes',
		'doyouhandle'			=>	'Do you require Handle drilling holes',
		'yes'				=>	'Yes',
		'no'					=>	'No',
		'ins1'				=>	'Please Select Profile Type',
		'ins2'				=>	'Please Select Profile Insert Type',
		'ins3'				=>	'Please Select Profile Sub Insert Type if Available and add to cart.',
		'ins4'				=>	'Please Select Width X Height and Quantity of the door and add to cart.',
		'addToCart'			=>	'Add to Cart'
		);

$data['french'] = array(
		'shoppingCart'		=>	'Le Panier',
		'clearCart'			=> 	'Effacez le Panier',
		'profile'			=> 	'Profil',
		'dimension'			=> 	'Dimensions',
		'quantity'			=> 	'Quantité ',
		'price'				=> 	'Prix',
		'total'				=> 	'Total',
		'subTotal'			=> 	'Sous Total',
		'createQuote'		=>	'Creer un devis',
		'noOfHoles'			=>	'numeros des trous',
		
		'step'				=>	'Etape',
		'profileType'		=>	'Type de profil',
		'profileFinish'		=>	'Finitions de profil',
		'insertType'		=>	'Type d\'insert',
		'doorDimension'		=>	'Dimensions de la porte',
		'orientation'		=>	'Orientation et trous de perçages',
		
		'doorPreview'			=>	'Prévisualisationde la porte',
		'doyouhinge'			=>	'Avez-vous besoindes trouspour les pentures',
		'doyouhandle'			=>	'Avez-vous besoinde percer des trouspour les poignée',
		'yes'				=>	'Oui',
		'no'					=>	'Non',
		'ins1'				=>	'Sélectionner le type deprofil',
		'ins2'				=>	'Sélectionner le Finile profil',
		'ins3'				=>	'Sélectionner le type d’insert',
		'ins4'				=>	'Sélectionnez Largeur &nbsp; x &nbsp; Hauteuret la quantité des portes et ajouter au panier',
		'addToCart'			=>	'Ajoutez au panier'		
		);
?>

