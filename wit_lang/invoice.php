<?php
$data['english'] = array(
		'invoice'			=>	'Invoice',
		'invGenerated'		=>	'Invoice Generated',
		'shoppingCart'		=> 	'Shopping Cart',
		'profile'			=> 	'Profile',
		'dimension'			=> 	'Dimensions',
		'quantity'			=> 	'Quantity',
		'price'				=> 	'Price',
		'total'				=> 	'Total',
		'subTotal'			=> 	'Sub Total',
		'GST'				=> 	'GST',
		'QST'				=> 	'QST',
		'backToConfigurator'	=> 	'Go Back to Configurator',
		'checkout'			=> 	'Place Order and Checkout',
		'download'			=>	'Download and Print'
);

$data['french'] = array(
		'invoice'			=>	'Facture',
		'invGenerated'		=>	'Facture généré',
		'shoppingCart'		=> 	'Le Panier',
		'profile'			=> 	'Profil',
		'dimension'			=> 	'Dimensions',
		'quantity'			=> 	'Quantité ',
		'price'				=> 	'Prix',
		'total'				=> 	'Total',
		'subTotal'			=> 	'Sous Total',
		'GST'				=> 	'TPS',
		'QST'				=> 	'TVQ',
		'backToConfigurator'	=> 	'Retourner au configurateur',
		'checkout'			=> 	'Placer Commande et Payer',
		'download'			=>	'Téléchargez et imprimez'
);
?>

