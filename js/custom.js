$(function(){
	
	$('.dimension-calculation').bind('blur change',function(){
		var mm			= 25.4000508; // (2.54000508*10)
		var element		= $(this);
		var targetVal 	= $('.'+element.attr('id'));
		var targetText 	= $('.mm-'+element.attr('id'));
		targetVal.val(element.val());
		//console.log("ele val "+element.val()+" final "+(element.val()*mm));
		var calc	= Math.round(element.val()*mm);
		//console.log("the cal "+calc);
		targetText.text(calc);
	});
	$('.dimension-calculation').change();
	
	$('.door-calculation').bind('blur change',function(){
		var element = $(this);
		var target	=	$('.'+element.attr('id'));
		priceCalculation(element);
		target.html(element.val());
		target.val(element.val());
	});
	
});


// to send calculation request only once
request_sent = 0;
function priceCalculation(object)
{
	var obj = $(object);
	//console.log("current object "+obj.attr('name')+" val("+obj.val()+")");
	if(obj.val()!='' && request_sent==0)
	{
		showOverlay();
		//$.ajaxSetup({async: false});
		$.post(BASEPATH+'door-ajax-call.php',
			{
				action			: 'calculate_price',
				height			: $('#door-height').val(),
				width			: $('#door-width').val(),
				ptype			: $('#door-ptype').val(),
				itype			: $('#door-itype').val(),
				pfinish			: $('#door-pfinish').val(),
				sitype			: $('#door-sub_insert').val(),
				quantity		: $('#door-quantity').val(),
				hinge_holes		: $('#door-holes').val(),
				handle_holes	: $('#door-handle-holes').val()
				
			},
			function(data){
			//alert(data+ "the request sent "+request_sent);
			//console.log(data+ "the request sent async false "+request_sent);
				var r 		= JSON.parse(data);
				//console.log(r.debug);
				if(r.status==1)
				{
					$('#result,.result').html(r.price);
					$('#product_price,.product_price').val(r.price);
					
					var total 	= $('#door-quantity').val()*r.price;
						total	= total.toFixed(2);
					$('#total-price,.total-price').html(total);
					$('.temp_cart').fadeIn();
					hideOverlay();
					
					// set calculation request again to zero , so that we can send request again
					request_sent=0;
				}
				else
				{
					// set calculation request again to zero , so that we can send request again
					request_sent=0;
					//instructions('please complete first three steps '+r.field);
					hideOverlay();
				}
			//console.log("the request sent after $.post "+request_sent);
		});	
		//$.ajaxSetup({async: true});
	}
	request_sent++;
}

// calculate total price of cart
function totalPriceCalculation()
{
	$.post(BASEPATH+'door-ajax-call.php',
			{
				action	: 'calculate_total'
			},
			function(data){
				var r 		= JSON.parse(data);
				if(r.status==1)
				{
					$("#cartTotal").text(r.result.total);
				}
			});
}

// *****************************************************************************
// ============================= COMMON FUNCTION ===============================
// *****************************************************************************

// for debugging
$(function(){
	$('body').append('<div id="debug-log"></div>');
	$('#debug-log').css({ position: 'fixed', top: 12, left: '10%', width: '80%' });
});

//function to scroll to any div with id
function goToByScroll(id){
	  // Remove "link" from the ID
	id = id.replace("link", "");
	  // Scroll
	$('html,body').animate({
		scrollTop: $("#"+id).offset().top},
		'slow');
}
// javascript equivalent for php's in_array();
function inArray(needle, haystack) {
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i] == needle) return true;
    }
    return false;
}	

// *****************************************************************************
// ========================== DOOR PREVIEW FUNCTIONS ===========================
// *****************************************************************************

function previewStep(object,stepNo)
{
	var obj		= $(object);
	var element = obj.find('img');
	var clone	= element.clone();
	var target	= $('.preview_step_'+stepNo);
	var src		= element.attr('src');

	if(stepNo==1)
	{
		src		= obj.attr('preview');
		target.append('<img src="'+src+'">');//find('img').attr('src',src);
	}

	if(stepNo>=2)
	target.html(clone);
	
	target.fadeIn();
	if(stepNo==1) $('.preview_default').hide();
	
	instructions(stepNo);
	show_progress();
	// show the price preview block when "insert type" is selected
	if(stepNo==3)
	$('.show_price').fadeIn();
}
// show progress in progress bar
function show_progress()
{
	var p	= $('.progress .bar');
	var amount	= 0;
	if($('#window-step_1 .scroll .element').hasClass('active'))
	amount += 30;
	if($('#window-step_2 .scroll .element').hasClass('active'))
	amount += 30;
	if($('#window-step_3 .scroll .element').hasClass('active'))
	amount += 32;
	if($('#window-sub_step_3 .scroll .element').hasClass('active'))
	amount += 8;
	p.animate({width: amount+"%"});
}

function instructions(step)
{
	$('[class^="preview_instructions"]').hide();
	$('.preview_instructions'+(step+1)).show();
}
// *****************************************************************************
// =============================== STEPS FUNCTIONS =============================
// *****************************************************************************

function add_finish(profileType)
{
	var target = $('[rel="chrome"]');
	if(profileType=='AF003')
		target.show();
	else
		target.hide();
	
}

function selectProfileType(object)
{
	var obj		= $(object);
	var ptype 	= obj.attr('rel');
	var plang	= obj.attr('lang');
	
	obj.parent().find('.active').removeClass('active');
	obj.addClass('active');
	
	add_finish(ptype);
	hide_inserts(ptype);
	
	$('.finish-s-container').slideDown();
	$('#door-ptype').val(ptype);
	$('.door-ptype').text(ptype);
	$('#door-ptype-lang').val(plang);
	
	previewStep(obj,1);
	goToByScroll('step_1_heading');
	
	priceCalculation($('#door-ptype'));
}


function selectProfileFinish(object)
{
	var obj		= $(object);
	var pftype 	= obj.attr('rel');
	var pflang 	= obj.attr('lang');
	
	obj.parent().find('.active').removeClass('active');
	obj.addClass('active');
	
	$('.insert-s-container').slideDown();
	$('#door-pfinish').val(pftype);
	$('.door-pfinish').text(pftype);
	$('#door-pfinish-lang').val(pflang);
	
	previewStep(obj,2);
	goToByScroll('step_2_heading');
	
	priceCalculation($('#door-pfinish'));
}


	// for storing Insert Type Id's whose sub insert types have been loaded.
	var fetched	= new Array();
	
	// for storing the deattached_sub_insert elments (sub insert types) 
	var deattached_sub_insert		= new Array();
	var subElementIndex	= 0;
	var previousIndex	= -1;
	

// function that runs on Step 3
/* 
					====> When clicked on an element(Insert Type) for the first Time <====
=> Add class "fetched" to the clicked element.
- So that "Sub Insert Types" for the elements(Inset Types) will not be fetched again for the elemnts(Insert Types) having class "fetched". 
=> Add an attribute "index" whose value equals to the "value of variable subElementIndex" if this clicked element doesn't have class "fetched".
- Value of the variable "subElementIndex" will increment each time when clicked on an element(Insert Type) not having class "fetched"
=> Deattach currently displayed elements(sub Insert Types) if exist from the window and store them into the array "deattached_sub_insert" with array key equal to the value of the variable "subElementIndex".
=> Fetch sub Insert Types and display them in the window.

					====> When clicked on an element(Insert Type) for the Second Time <====
=> Fetch the value of the attribute "index"
- So that we can re-populate the deattached_sub_insert elements stored in the array "deattached_sub_insert" with array key equal to the value of the attribute index.


============
the variable "previousIndex" should be updated in the last
*/
function selectInsertType(object)
{
	var obj		= $(object);
	var itype 	= obj.attr('rel');
	var ilang 	= obj.attr('lang');
	
	obj.parent().find('.active').removeClass('active');
	obj.addClass('active');
	
	// hide sub insert type 
	var scrollObj = $('#window-sub_step_3 .win_scroll');
	var elements = $(".element.sub_ins",scrollObj);

	// if already fetched sub insert types 
	if(obj.hasClass('fetched'))
	{
		deattached_sub_insert[previousIndex] = elements.detach();
		scrollObj.css({left: 20}).append(deattached_sub_insert[obj.attr('index')]);
		goToByScroll('step_3_heading');
	}
	
	// only fetch if not already fetched from database
	if(!obj.hasClass('fetched'))
	{	
		// assign class "fetched"
		obj.addClass('fetched').attr('index',subElementIndex);
	
		$.ajaxSetup({async: false});
		$.post('door-ajax-call.php',{ action : 'fetch_sub_inserts',insert_id : obj.attr('id') },function(data){
			//alert(data);
			var r 		= JSON.parse(data);
			if(r.status==1)
			{
				//console.log(r.result);
				var folder	= r.folder;
				// set the width of the scroller
				scrollObj.css({width: ((obj.width()+4)*r.result.length),left: 20});
				$.each(r.result,function(){
					var t = $(this);
					//console.log(t.attr('door_sub_id'));
					scrollObj.append('<div class="element sub_ins" onclick="selectSubInsertType(this)" rel="'+t.attr('door_sub_id')+'" lang="'+t.attr('door_sub_title')+'" ><img class="vertical_orientation" src="'+folder+'thumbnail/'+t.attr('door_sub_image')+'" >'+t.attr('door_sub_title')+'</div>');
				});
				$('#window-sub_step_3 , .dimension-s-container').slideDown();
			}
			else if(r.status==0)
			{
				scrollObj.append('<div class="element sub_ins empty-element">No inserts types found for '+obj.text()+"</div>");
				$('.empty-element').css('width',((obj.width()+4)*noOfScrollingElement));
				$('#window-sub_step_3 , .dimension-s-container').slideDown();
			}
			
		// show step 5
		$('.orientation-s-container').slideDown();
				
		calculatePosition(scrollObj.parent(),noOfScrollingElement);
		});
		
		// to align the elements 
		$('.element',scrollObj).css({'float':'left'});
		
		goToByScroll('step_3_heading');
		
		$.ajaxSetup({async: true});
			
			// to prevent deattaching when element clicked for the first time
			if(previousIndex>=0) 
			deattached_sub_insert[previousIndex] = elements.detach();
			
		// increment the value of the variable (key of the array "deattached_sub_insert")
		subElementIndex++;
	}
	
	// increment to indicate that the scrolling Position need to be changed 
	resetscroll++;
	//console.log("resetscroll activated resetscroll value = "+resetscroll);
		
	$('#door-itype').val(itype);
	$('.door-itype').text(itype);
	$('#door-itype-lang').val(ilang);
		
	previewStep(obj,3);
	previousIndex = obj.attr('index');
	
	priceCalculation($('#door-itype'));
}

function selectSubInsertType(object)
{
	var obj				= $(object);
	var subInsert		= obj.attr('rel');
	var subInsertlang	= obj.attr('lang');
	
	obj.parent().find('.active').removeClass('active');
	obj.addClass('active');

	$('.orientation-s-container').slideDown();	
	$('#door-sub_insert').val(subInsert);
	$('.door-sub_insert').text(subInsert);
	$('#door-sub_insert-lang').val(subInsertlang);
	
	previewStep(obj,3);
	goToByScroll('window-sub_step_3');
}

	
function changeCurrentQuantity(object,type)
{
	var element 	= $(this);
	var target		= $('#door-quantity');
	var quantity	= target.val();
	//console.log("the target id is "+target.attr('id'));
	if(quantity>0)
	{
		if(type=='--')	{if(quantity>1) quantity--;}
		if(type=='++')	quantity++;

		target.val(quantity);
		target.blur();
	}
	//console.log(quantity);
}

// Step 5 Orientation
function changeOrientaion(object,orientation)
{
	var element		= $(object);
	var ori_element	= $('.orientation');
	var target		= $('#door-orientation');
	var scroll		= $('#window-sub_step_3 .win_scroll');
	ori_element.removeClass('btn-warning').addClass('btn-link');
	element.removeClass('btn-link').addClass('btn-warning');
	target.val(orientation);
	var ele 		= scroll.find('.element');
	$.each(ele,function(){
		var t		= $(this).find('img');
		//var ver		= t.attr('src');
		//var hor 	= t.attr('src2');
		//console.log(t.attr('src'));
		//t.attr('src',hor); 
		//t.attr('src2',ver);
		t.removeClass('vertical_orientation').removeClass('horizontal_orientation').addClass(orientation+'_orientation');
		
		if($(this).hasClass('active'))
		$(this).click();
	});
	//ALTER TABLE  `door_sub_insert` ADD  `door_horizontal_sub_image` VARCHAR( 60 ) NOT NULL AFTER  `door_sub_image`
}
// 
function requireHoles(object,bool)
{
	var obj		= $(object);
	var target 	= obj.parent().parent().find('.holes');
	var inp 	= target.find('input');
	var span	= target.next();
	
	if(bool=='yes')
	{
		target.fadeIn();
		span.hide();
	}
	else // Update the count to zero and update the cart
	{
		inp.val(0);
		priceCalculation(inp);
		target.fadeOut();
		span.show();
	}
}

// hide Inserts Type when AF006 profile is selected
function hide_inserts(profileType)
{
	var hidden = new Array();
	//console.log(profileType);
	
	var scroll	= $('#window-step_3 .win_scroll');
	var ele		= scroll.find('.element');
//	console.log("length of elements "+ele.length);
	
	// all elements with class equal to "clicked profile Type"
	var target = scroll.find('.'+profileType);
	
	// hide all by default
	ele.css('display','none');
	//console.log("no element with class "+profileType+" is "+target.length);
	
	if(target.length!=0)
	target.css({display:'block'});
	if(target.length==0)
	ele.css({display:'block'});
	
	// reset scroll position
	scroll.css({left: 20});
}


// reset configurator step by step
function resetStepByStep(stepNo)
{
	
}
// *****************************************************************************
// ===============================  CART FUNCTIONS =============================
// *****************************************************************************

$(function(){	
	$('#add_to_cart').click(function(){
	
		showOverlay();
		$.post(BASEPATH+'door-ajax-call.php',
		{
			add_to_cart			: 'Add to the cart',
			profile_type		: $('#door-ptype').val(),
			profile_finish		: $('#door-pfinish').val(),
			insert_type			: $('#door-itype').val(),
			sub_insert			: $('#door-sub_insert').val(),
			profile_finish_lang	: $('#door-pfinish-lang').val(),
			insert_type_lang	: $('#door-itype-lang').val(),
			sub_insert_lang		: $('#door-sub_insert-lang').val(),
			height				: $('#door-height').val(),
			width				: $('#door-width').val(),
			quantity			: $('#door-quantity').val(),
			price				: $('#product_price').val(),
			hinge_holes			: $('#door-holes').val(),
			handle_holes		: $('#door-handle-holes').val(),
			orientation			: $('#door-orientation').val()
		},
		function(data){
			
			//alert(data);
			$('#shopping-cart').fadeIn();
			var r = JSON.parse(data);
			
			var appendText = '<tr><th width="12"> &nbsp; </th><th> '+ r.profile_type +' </th><td> Finish: '+ r.profile_finish +' <br> Insert: '+r.insert_type+((r.sub_insert!='') ? '/' : '')+ r.sub_insert+'</td><td>'+r.width+' x '+r.height+'</td><td> '+r.quantity+'</td><td> $'+r.price+'</td><th> $'+r.sub_total+'</th><td> <i class="icon-remove" onclick="removeFromCart(this,\''+r.cart_id+'\')" ></i> </td></tr>';
			
			$('#shopping-cart').append(appendText);
			
			totalPriceCalculation();
			
			hideOverlay();
			goToByScroll('shopping-cart');
		});
	});
});
	
function removeFromCart(object,cartId)
{
	showOverlay();
	var obj		= $(object);
	$.post(BASEPATH+'door-ajax-call.php',
	{
		remove_from_cart	: cartId
	},
	function(data){
	var res = JSON.parse(data);
		if(res.status)
		{
			obj.parent().parent().remove();
			totalPriceCalculation();
			hideOverlay();
		}				
	});
}
	
function emptyCart(object)
{
	showOverlay();
	var obj		= $(object);
	$.post(BASEPATH+'door-ajax-call.php',
	{
		empty_cart	: 'empty_cart_contents'
	},
	function(data){
	var res = JSON.parse(data);
		if(res.status)
		{
			obj.parent().parent().parent().slideUp();
			obj.parent().parent().parent().find('tbody').remove();
			totalPriceCalculation();
			hideOverlay();
		}				
	});				
}