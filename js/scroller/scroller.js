
noOfScrollingElement	=	4; // no of elements to scroll at once
elementSpacing			=	8; // space between elements in px.
resetscroll 			=	0; // to reset scroller position

$(function(){

	(function( $ ){

	  $.fn.winscroller = function( options ) {  

		// Create some defaults, extending them with any options that were provided
		var o = $.extend({
			next			:	'.nextscroll',	// id for the Next Navigation Button
			prev			:	'.prevscroll',	// id for the Previous Navigation Button
			scrollElements	:	noOfScrollingElement	// no of elements to scroll when navigating
			},options);
			

		var win			= $(this); // the object for the window scroller.
		var scroller	= $('.win_scroll',this); // the scrolling div.
		
		var element 				= $('.element',this);
		var elementWidth			= element.width()+elementSpacing;
		var scrollWidth				= (elementWidth*o.scrollElements);
		var scrollPosition			= 0;
		var maxScrollPosition		= Math.floor( (elementWidth*element.length) / (elementWidth*o.scrollElements));
		var nextButton				= $(o.next,this);
		var prevButton				= $(o.prev,this);
		
		var scrollerWidth			= ((elementWidth*element.length));
		//calculatePosition(win,o.scrollElements);
		scroller.css('width',scrollerWidth);
		
		
		//Debug scroller
		//win.parent().append('<div class="'+win.attr('id')+'"></div>');
		//$('.'+win.attr('id')+'').html("element width: ("+elementWidth+") element count ("+element.length+") max scroll ("+maxScrollPosition+") scroll width ("+(elementWidth*element.length)+") \n element width*length ("+(elementWidth*o.scrollElements)+") need to scroll ("+Math.floor( (elementWidth*element.length) / (elementWidth*o.scrollElements))+") times");

		
		prevButton.click(function(){
		
			// reset the scrolling position when new contents are added to slider
			if(resetscroll>0) { scrollPosition = 0; resetScroller(); /* console.log(" \n resetting scroller \n"); */ }
		
		var ret				= calculatePosition(win,o.scrollElements);
		scrollWidth			= ret[0];
		//console.log("Back ==> maxScrollPosition "+ret[1]+"\n scrollWidth "+ret[0]+" \n No of Elements "+ret[2]+" \n Current Scroll Position "+scrollPosition);
			if(scrollPosition>0){
				$(this).parent().find('.win_scroll').animate({left: '+='+scrollWidth});
				scrollPosition--;
			}
		});
		
		nextButton.click(function(){
		var ret				= calculatePosition(win,o.scrollElements);
		scrollWidth			= ret[0];
		maxScrollPosition	= ret[1];
		var noOfElement		= ret[2];
		
		// reset the scrolling position when new contents are added to slider
		if(resetscroll>0) { scrollPosition = 0; resetScroller(); /* console.log(" \n resetting scroller \n"); */ }
		
		//console.log("Next ==> maxScrollPosition "+ret[1]+"\n scrollWidth "+ret[0]+" \n No of Elements "+ret[2]+" \n Current Scroll Position "+scrollPosition);
		
			if(scrollPosition<maxScrollPosition && noOfElement > o.scrollElements )
			{
				$(this).parent().find('.win_scroll').animate({left: '-='+scrollWidth});
				scrollPosition++;
			}
		});

	};  
	  
	})( jQuery );

});

// calculate the maxscrollWidth and width of scroller
function calculatePosition(object,scrollElements)
{
	var ret				= new Array();
	var element 		= $('.element',object).filter(':visible');
	var newObjWidth		= element.width()+elementSpacing;
	var scroller		= $('.win_scroll',object);
	var scrollerWidth	= (newObjWidth*((element.length <=scrollElements) ? (scrollElements+1) : element.length ));
	
	// if the current element has class "fetched(fetched get assigned to the element when selectInsertType() is called)" then set the scroll
	// increase the width of the scroller according to the no of elements in it.
	scroller.css('width',scrollerWidth); 
	var scrollWidth = newObjWidth*scrollElements;
	ret[0]	=	scrollWidth;
	//ret[1]	=	Math.floor( (newObjWidth*element.length) / (newObjWidth*scrollElements));
	//((element.length-offset) > 0) ? (element.length-offset) : 0; // max Scroll Position
	
	// max scroll equals to no_of_elements_in_scroller/no_of_scrolling_elements_variable 
	// minus(-) 1 if mod of element.length by no_of_scrolling_elements equals 0;
	// minus(-) 0 if mod of element.length by no_of_scrolling_elements is not equal to 0;
	ret[1]	=	Math.floor(element.length/noOfScrollingElement) - (((element.length%noOfScrollingElement)==0) ? 1 : 0);
	ret[2]	=	element.length;	// number of elements
	//console.log("\n max length "+Math.floor(element.length/noOfScrollingElement)+"\n the mod "+element.length%noOfScrollingElement);
	//console.log("scroll width ("+ret[0]+") maxscroll ("+ret[1]+") elements ("+ret[2]+")");
	return ret;
}

// to reset the scrolling position when new elements append to the scroller
function resetScroller()
{
	resetscroll = 0;
}

$(function(){
	// add scroller
	$('#window-step_1').winscroller();
	$('#window-step_2').winscroller();
	$('#window-step_3').winscroller();
	$('#window-sub_step_3').winscroller();
});