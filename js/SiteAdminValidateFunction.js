function jQueryPopupView(SelType,SelId,SType,UserType){
	jQuery('#BidErrSuccDiv').attr('class','');  jQuery('#BidErrSuccDiv').html('');
	jQuery('#'+SelType+'_Div').trigger('click');
	if(parseInt(SelId)>0 && SType!=''){
	  jQuery('#SelId').val(SelId);jQuery('#SelType').val(SelType);
	}
	if(SelType=='ChangeClas'){
		jQuery('#ClassId').val(SelId);
	}else if(SelType=='DeleteClas'){
		jQuery('#ClassIds').val(SelId);
	}
}
function GoCaptcha(site_path)
{
  $('#cvbvcb').html('<img src=\''+SiteHttpPath+'images/loading.gif\'/>');
  $.ajax({
	type:"GET",
	url:SiteHttpPath+'ajax_captcha.php',
	dataType:"html",
	success: function(r){
		$('#cvbvcb').html(r);}
	});
}
////
var FileId=1;var BErrCss='';
function FormValidateFunction(ValiOption,FieldId,ErrText)
{
	var AErrCss='box2';	
	if(BErrCss=='')
		BErrCss='box2';
		
	if(ErrText=='')
		ErrText='This field is required';		
	if(ErrCss=='')
		ErrCss='box2';

	switch(ValiOption)
	{
		case 'Text':
			if(jQuery(FieldId).val() == ''){
				jQuery(FieldId+'Msg').show();
				jQuery(FieldId+'Msg').html(ErrText);
				jQuery(FieldId).attr('class',ErrCss);
				jQuery(FieldId).focus();
				 return false;
			}else{
				jQuery(FieldId+'Msg').hide();
				jQuery(FieldId+'Msg').html('');
				jQuery(FieldId).attr('class',BErrCss);
				return true;
			}	
			break;
		case 'CheckBox':
			var selector_checked = jQuery("input[@id="+FieldId+"]:checked").length;
			if (selector_checked == 0)
			{
				jQuery(FieldId+'Msg').show();
				jQuery(FieldId+'Msg').html(ErrText);
				jQuery('#'+FieldId).attr('class',ErrCss);
				jQuery('#'+FieldId).focus();
				 return false;
			}
			else{
				jQuery(FieldId+'Msg').hide();
				jQuery(FieldId+'Msg').html('');
				jQuery('#'+FieldId).attr('class','');
				return true;
			}	
			break;
		case 'OnlyNumber':
			if(jQuery(FieldId).val() == ''){
				jQuery(FieldId+'Msg').show();
				jQuery(FieldId+'Msg').html(ErrText);
				jQuery(FieldId).attr('class',ErrCss);
				jQuery(FieldId).focus();
				 return false;
			}else{
				if(isNaN(jQuery(FieldId).val())){
					jQuery(FieldId+'Msg').show();
					jQuery(FieldId+'Msg').html(ErrText);
					jQuery(FieldId).attr('class',ErrCss);
					jQuery(FieldId).focus();
					 return false;
				}
				else{
					jQuery(FieldId+'Msg').hide();
					jQuery(FieldId+'Msg').html(ErrText);
					jQuery(FieldId).attr('class',BErrCss);
					return true;
				}
			}	
		break;
		case 'Image':
			if(jQuery(FieldId).val()!='')
			{
				var FileName=jQuery(FieldId).val();
				var NFName=FileName.split('.');
				var FileExtension=(NFName.pop()).toLowerCase();
				if(jQuery.inArray(FileExtension,AllowExtension) == -1){
					jQuery(FieldId+'Msg').show();
					jQuery(FieldId+'Msg').html(ErrText);
					jQuery(FieldId).attr('class',ErrCss);
					jQuery(FieldId).focus();
					 return false;
				}else{
					jQuery(FieldId+'Msg').hide();
					jQuery(FieldId+'Msg').html('');
					jQuery(FieldId).attr('class','compound_file');
					return true;
				}
			}else{
					jQuery(FieldId+'Msg').hide();
					jQuery(FieldId+'Msg').html('');
					jQuery(FieldId).attr('class','compound_file');
					return true;
				}
		break;
		case 'EMail':			
			var UserMail=jQuery(FieldId).val();
			var RegxMail = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
			if(!RegxMail.test(UserMail)){ 
				jQuery(FieldId+'Msg').show();
				jQuery(FieldId+'Msg').html(ErrText);
				jQuery(FieldId).attr('class',ErrCss);
				jQuery(FieldId).focus();
				 return false;
			}else{
				jQuery(FieldId+'Msg').hide();
				jQuery(FieldId+'Msg').html('');
				jQuery(FieldId).attr('class',BErrCss);
				return true;
			}
			break;
		case 'UserName':			
			var UserName=jQuery(FieldId).val();
			var RegxName = /^[\w\d\_\.]{4,}$/;
			if(!RegxName.test(UserName)){ 
				jQuery(FieldId+'Msg').show();
				jQuery(FieldId+'Msg').html(ErrText);
				jQuery(FieldId).attr('class',ErrCss);
				jQuery(FieldId).focus();
				 return false;
			}else{
				jQuery(FieldId+'Msg').hide();
				jQuery(FieldId+'Msg').html('');
				jQuery(FieldId).attr('class','');
				return true;
			}
			break;
		case 'Money':
			if(jQuery(FieldId).val()!='')
			{
				var MoneyVal=jQuery(FieldId).val();
				
				var price = MoneyVal.replace(/[^\d\.]/g, "" ); // zap all but digits and period
				var price = parseFloat(price); // convert it to a number
				
			}
			if ( isNaN( price ) ) price = 0;
				jQuery(FieldId).val(price);
			return true;
			break;
		case 'TinyMce':
			if(CKEDITOR.instances[FieldId].getData() == ''){
				jQuery('#'+FieldId+'Msg').show();
				jQuery('#'+FieldId+'Msg').html(ErrText);
				jQuery('#'+FieldId).attr('class',ErrCss);
				CKEDITOR.instances[FieldId].focus();
				return false;
			}
			else{
				jQuery('#'+FieldId+'Msg').hide();
				jQuery('#'+FieldId+'Msg').html('');
				jQuery('#'+FieldId).attr('class','');
				return true;
			}	
			break;
		case 'URL':
			var Redir=jQuery(FieldId).val();
			if(Redir.match(/^(ht|f)tps?:\/\/[a-z0-9-\.]+\.[a-z]{2,4}\/?([^\s<>\#%"\,\{\}\\|\\\^\[\]`]+)?$/)){
				jQuery(FieldId+'Msg').hide();
				jQuery(FieldId+'Msg').html('');
				jQuery(FieldId).attr('class','');
				return true;
			}else{
				jQuery(FieldId+'Msg').show();
				jQuery(FieldId+'Msg').html(ErrText);
				jQuery(FieldId).attr('class',ErrCss);
				jQuery(FieldId).focus();
				 return false;
			}	
			break;
		default:		
			break;
	}		
}
function GoBack(mgmtName)
{
	window.location.href= mgmtName;
}
function AllowOnlyNumbers(evt)
{
 	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	 return true;
}
function AllowOnlyCurrency(evt)
{
 	
	var charCode = (evt.which) ? evt.which : event.keyCode;
	if (charCode==46 || charCode==44)
	{	return true;}
	else if(charCode > 31 && (charCode < 48 || charCode > 57))
	{	return false;alert(charCode);	}
	 return true;
}
function addFormField() {
	//if(document.getElementById("id").value < 5){
	var id = document.getElementById("id").value;
	
	
	jQuery("#MoreFiles").append("<div class='spacer'></div><div class='AdmFleft' style='width:220px; padding-right:5px;'><div id='row"+id+"'><input type='file' size='20' name='ProImage[]'  style='width:180px;' id='PackImg_" + id + "'><div class='AdmFleft'><a href='#' onClick='removeFormField(\"#row"+id+"\"); return false;'>Remove</a></div></div></div>");
	id = (id-1) + 2;
	document.getElementById('id').value = id;
	/*}
	else
		alert('Limit Reached');*/
	
}
function addFormField1() {
	//if(document.getElementById("id").value < 5){
	var id = document.getElementById("id1").value;
	
	
	jQuery("#MoreFiles").append("<div class='spacer'></div><div class='AdmFleft' style='width:220px; padding-right:5px;'><div id='row"+id+"'><input type='file' size='20' name='Portfolio_image[]'  style='width:180px;' id='PackImg_" + id + "'><div class='AdmFleft'><a href='#' onClick='removeFormField1(\"#row"+id+"\"); return false;'>Remove</a></div></div></div>");
	id = (id-1) + 2;
	document.getElementById('id1').value = id;
	/*}
	else
		alert('Limit Reached');*/
	
}
function removeFormField1(id) {
	if(document.getElementById("id1").value >1)
	document.getElementById("id1").value = document.getElementById("id1").value-1;
	jQuery(id).remove();
}
function removeFormField(id) {
	if(document.getElementById("id").value >1)
	document.getElementById("id").value = document.getElementById("id").value-1;
	jQuery(id).remove();
}
////// Login Page////
function GoToLogValidate(ValiType)
{	
//	ErrObject.ErrCss='loginboxerr';	var FlagIns=true;
	
//	ErrObject.BErrCss='loginbox';ErrObject.MainErrDiv='#MainErrDivMsg';ErrObject.InnerErrDiv='#ErrDiv';
	ErrCss='box2';var FlagIns=true;
	
	FlagIns=FormValidateFunction('Text','#LogMail','Enter Email');
	if(FlagIns){	
		FlagIns=FormValidateFunction('EMail','#LogMail','Enter vaild Email');
	}
	if(FlagIns){	
		FlagIns=FormValidateFunction('Text','#LogPass','Enter Password');
	}
	if(FlagIns){	
		document.forms[FormName469].submit();
	}
	else{	return false;	}
}
