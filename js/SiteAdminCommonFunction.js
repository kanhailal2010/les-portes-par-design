function AdminCommonFunction(Action)
{ 
	switch(Action)
	{ 
		case 'Delete':
		case 'Active':
		case 'InActive':
		case 'Suspended':
		case 'Cancelled':
		case 'Pending':
		case 'InProgress':
		case 'Paid':
		case 'Completed':
		case 'Shipped':
		case 'Delivered':
		
		len=FormObj.elements.length;flag=false;var i=0;
			
			for(i=0; i<len; i++)
			{	
			
				if (FormObj.elements[i].name=='ConId[]')
				{
					if(FormObj.elements[i].checked)
					{	flag=true; break;	}
					else
					{	flag=false;		}
				}	
			} 
			if(flag)
			{	 
				if(Action=='Active'){	Action='activate';	}	
				else if(Action=='Suspended'){	Action='Suspend';	}
				if(confirm("Are you sure, you want to "+Action+" this "+MgmtName+"?"))
				{	
					if(Action=='Suspend'){	Action='Suspended';	}	
					if(Action=='activate'){	Action='Active';	}	
			
					FormObj.Action_Type.value =Action;
					FormObj.submit();
				}
				else{	return false;	}
			}
			else
			{
				alert('Please Select Atleast one Checkbox For This Action');
				return false;
			}
			break;
		case 'Select':
			len=FormObj.elements.length;
			var i=0;
			for(i=0; i<len; i++) 
				if (FormObj.elements[i].name=='ConId[]')
					FormObj.elements[i].checked=1;
			break;   
		case 'UnSelect':
			len=FormObj.elements.length;
			var i=0;
			for(i=0; i<len; i++)
			if (FormObj.elements[i].name=='ConId[]')
			   FormObj.elements[i].checked=0;		
			break;
		default:		
			break;
	}		
}
function SingleRowActiveDeleteInactive(Action,CatIdent)
{
	if(confirm("Are you sure to "+Action+" this "+MgmtName+" Details?"))
	{
		len=FormObj.elements.length;
		var i=0;
		for(i=0; i<len; i++)
		if (FormObj.elements[i].name=='ConId[]')
		   FormObj.elements[i].checked=0;	
		   
		FormObj.ConSId.value	= CatIdent;
		FormObj.Action_Type.value	= Action;
		FormObj.submit();
	}
	else{	return false;	}	
}

function AdminDeleteFunction(CatIdent)
{ 
	if(confirm("Are you sure to Delete this "+MgmtName+" Details?"))
	{
		len=FormObj.elements.length;
		var i=0;
		for(i=0; i<len; i++)
		if (FormObj.elements[i].name=='ConId[]')
		   FormObj.elements[i].checked=0;	
		   
		FormObj.ConSId.value	= CatIdent;
		FormObj.Action_Type.value	= "Delete";
		FormObj.submit();
	}
	else{	return false;	}	
}
////View Management In Hide && Show Start////
function GoToViewFunction(TypeVal,ShowDiv,HideDiv){ 
	    jQuery('.ViewDet').hide();
		jQuery('#ResIdDiv_'+TypeVal).show();
	    jQuery('#'+ShowDiv+'_'+TypeVal).show();
		jQuery('#'+HideDiv+'_'+TypeVal).hide();	  
}
function GoToHideFunction(UserIdS,ShowDiv,HideDiv){	 
	    jQuery('.ViewDet').hide();
		jQuery('#ResIdDiv_'+UserIdS).hide();
	    jQuery('#'+HideDiv+'_'+UserIdS).hide();
		jQuery('#'+ShowDiv+'_'+UserIdS).show();	 
}
////View Management In Hide && Show  End////

function GoBackRedirct(RedirURL)
{
	window.location.href= RedirURL;
}

function GoBackOpenPopupWindow(RedirURL)
{
   window.open(RedirURL,'Advertisment','width=700,height=500,scrollbars=yes');
}

function GoBackOpenPopupWindow(RedirURL)
{
   window.open(RedirURL,'Advertisment','width=700,height=500,scrollbars=yes');
}
function AdminImageFunction(SelId,SelType,CLogType)
{
	var AjacRes=jQuery.ajax({
		url:'admin_json_header.php',
		type: "POST",
		dataType: 'json',
		data: {SelId:SelId,SelType:SelType,CLogType:CLogType},
		error:function (xhr){
			alert('Server Error');
		},
		success: function(JSonRes){
			if(JSonRes.ajax_res=="true"){
				jQuery('.ActClass').hide();
				jQuery('.InActClass').show();
				jQuery('#Remove_'+SelId).hide();
				jQuery('#SaveData_'+SelId).show();
				if(JSonRes.redir_Status=='true'){
					var locationObj = window.location;
					window.location.href= 'banner_mgmt.php?act_type='+SelType;		
				}
				else{
					jQuery('.NewMsgDiv').html('<div class="Msg_error"><div class="AdmFleft">'+JSonRes.ajax_msg+'</div><div class="AdmFRight"><a class="close-msg" href="javascript:;" onclick="TriggerClick()"><img src="images/close-button.png" alt="" /></a></div></div>');
				}
			}
			else{
				jQuery('.NewMsgDiv').html('<div class="Msg_error"><div class="AdmFleft">'+JSonRes.ajax_msg+'</div><div class="AdmFRight"><a class="close-msg" href="javascript:;" onclick="TriggerClick()"><img src="images/close-button.png" alt="" /></a></div></div>');
			}
		}
	});
}