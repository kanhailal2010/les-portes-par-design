
$(document).ready(function(){


    /* DESCRIPTION */
    $("#iconDescription").click(function() {
      var classBouton = $("#iconDescription").attr("class");
      
      if (classBouton == 'statut1'){
        $(this).addClass('statut2').removeClass('statut1');
        $('#descriptionProjet').animate({
          left : 0
        }, 300);
        $('#menu').animate({
          left :132
        }, 300);
      } else {
        $(this).addClass('statut1').removeClass('statut2');
        $('#descriptionProjet').animate({
          left : -295
        }, 300);
        $('#menu').animate({
          left : 0
        }, 300);
      }
    });




    /* NEXT - PREV PROJECT */

    $('.nextProject').css('width', '0');
    $('.previousProject').css('width', '0');
    $('#menu').css('width', '101px');

    $('#iconPrev').mouseenter(function() {
      $('.previousProject').css('width', '101px');
      $('#menu').css('width', '202px');
    });

    $('#iconPrev').mouseout(function() {
      $('.previousProject').css('width', '0');
      $('#menu').css('width', '101px');
    });

    $('#iconNext').mouseenter(function() {
      $('.nextProject').css('width', '101px');
      $('#menu').css('width', '202px');
    });

    $('#iconNext').mouseout(function() {
      $('.nextProject').css('width', '0');
      $('#menu').css('width', '101px');
    });


});