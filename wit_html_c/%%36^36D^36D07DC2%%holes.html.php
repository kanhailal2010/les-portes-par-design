<?php /* Smarty version 2.6.3, created on 2013-05-30 05:43:57
         compiled from holes.html */ ?>
 
<script type="text/javascript">
	var Profile='<?php echo $this->_tpl_vars['Arr']; ?>
';
 	var ProfileId='<?php echo $_SESSION['ProfileId']; ?>
'; 
	var DrillHSta='<?php echo $this->_tpl_vars['Arr']['DrillHoleStatus']; ?>
';
</script>
<?php echo '
<script type="text/javascript">  
jQuery(document).ready(function(){
	if(Profile!=\'\'){
		jQuery(\'#www\').hide(); jQuery(\'#errormsg\').hide();
		jQuery(\'#cont\').show(); 
	}
	if(DrillHSta==\'Yes\'){
 		jQuery(\'#TotDH\').show(); 
	}
	else if(DrillHSta==\'No\'){   
 		jQuery(\'#TotDH\').hide(); 
	}
}); 
function GoEditVaildate(){	
	ErrCss=\'txt_con1\'; BErrCss=\'txt_con1\'; var FlagIns=true;
	
	if(ProfileId!=3){
		if(FlagIns) {	ErrCss=\'Seltop\'; BErrCss=\'Seltop\'; 
			FlagIns=FormValidateFunction(\'Text\',\'#Model\',\'Please select Model\');
		}ErrCss=\'txt_con1\'; BErrCss=\'txt_con1\'; 
		if(FlagIns) {	
			FlagIns=FormValidateFunction(\'Text\',\'#HoleA\',\'Please enter Length\');
		}  
		if(FlagIns) {	
			FlagIns=FormValidateFunction(\'Text\',\'#LengthB\',\'Please enter Height\');
		} 
		if(FlagIns) {	
			FlagIns=FormValidateFunction(\'Text\',\'#WidthC\',\'Please enter C\');
		}  
		if(FlagIns) {	
			FlagIns=FormValidateFunction(\'Text\',\'#WidthD\',\'Please enter D\');
		} 
		if(FlagIns) {
			var Sqft=(jQuery(\'#HoleA\').val()*jQuery(\'#LengthB\').val())/144;
			if(Sqft < 2){	
				jQuery("#FailMsg").empty();
				jQuery(\'#FailMsg\').html(\'<div class="notification hideit failure" style=" width:350px;">Please enter the Values A and B become minimum 2 Sqft</div><div class="spacer"></div>\');	
				jQuery("#FailMsg").fadeOut(10000);
				 return false;
			}
		}
	}
	if(ProfileId==4){ 
		/*if(FlagIns) {	
			FlagIns=FormValidateFunction(\'Text\',\'#TotDrillHoles\',\'Please enter Total Drilling Holes\'); 
		} */	
		//ErrCss=\'radio_class\'; BErrCss=\'radio_class\'; var FlagIns=true;
		if(jQuery(\'input:radio[name=DrillHoleStatus]:checked\').val()==\'Yes\'){	
			if(FlagIns){	
				FlagIns=FormValidateFunction(\'Text\',\'#TotDrillHoles\',\'Please enter Total Drilling Holes\'); 
			} 
 		}
	}
	if(FlagIns) {	
		FlagIns=FormValidateFunction(\'Text\',\'#Qty\',\'Please enter Total Qty\'); 
	} 
	
	if(FlagIns){	
 		document.forms[FormName1].submit();
	}
}  
function GoToJSONAjaxFunction(){
	jQuery(\'#www\').hide(); jQuery(\'#errormsg\').hide();
	jQuery(\'#cont\').show(); 
}   
function ShowErrortoSelect(){   
 	jQuery(\'#errormsg\').show(); 
}  
function ShowDrillHole(type){
	if(type==\'Yes\'){   
 		jQuery(\'#TotDH\').show(); 
	}
	else{   
 		jQuery(\'#TotDH\').hide(); 
	}
}  
function ShowHingeQty(type){
	if(type==\'Yes\'){   
 		jQuery(\'#HingeQty\').show(); 
	}
	else{   
 		jQuery(\'#HingeQty\').hide(); 
	}
} 
</script>
'; ?>

<div class="Content">
	<div class="connetentin">
		  <div class="common_titlesm">
			<div class="common_titles">
				<div class="title_lft"></div>
				<div class="title_mid"><?php echo $this->_tpl_vars['PageTitle']; ?>
</div>
			</div>
		</div>
		  <div class="clearxy height10"></div>
		  <div class="productmain">
			  <div class="product">
				<form action="" name="post_form1" method="post">
				 <div id="sub5">
						<div class="configurator_main">
						<?php if ($_SESSION['ProfileId'] == 1 || $_SESSION['ProfileId'] == 2): ?>
							<!--<div class="instut"><p>Select Extra Hinge Holes here...</p></div>-->
						<?php elseif ($_SESSION['ProfileId'] == 4): ?>
							<div class="instut"><p>Enter Quantity here...</p></div>
 						<?php else: ?>
							<div class="instut"><p>Enter frame length here...</p></div>
						<?php endif; ?>
						<div class="clearxy height10"></div>
						<?php if ($_SESSION['ProfileId'] == 1 || $_SESSION['ProfileId'] == 2): ?>
						<div class="image_witharrow"> <img src="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
images/hinge_pos.png" alt="Hinge Position" /> </div>
						
						<div class="frm_p1">Select Model :</div>
						<div class="frm_p2">
						<select name="Model" id="Model" class="Seltop">
						<option value="">Select Model</option> 
							<option value="Model W" <?php if ($this->_tpl_vars['Arr']['Model'] == 'Model W'): ?>selected="selected"<?php endif; ?>>Model W</option>
							<option value="Model X" <?php if ($this->_tpl_vars['Arr']['Model'] == 'Model X'): ?>selected="selected"<?php endif; ?>>Model X</option>
							<option value="Model Y" <?php if ($this->_tpl_vars['Arr']['Model'] == 'Model Y'): ?>selected="selected"<?php endif; ?>>Model Y</option>
							<option value="Model Z" <?php if ($this->_tpl_vars['Arr']['Model'] == 'Model Z'): ?>selected="selected"<?php endif; ?>>Model Z</option>
 						</select> 
							<br /><span id="ModelMsg" class="error"></span>
						</div>
						<div class="clearxy height10"></div>
							<?php if ($this->_tpl_vars['Arr']['HingeHoleSta'] == 'Yes'): ?>
								<div class="frm_p1">Holes (35mm) :</div>
								<div class="frm_p2">
									<input type="radio" name="HoleStatus" value="Yes" class="check" <?php if ($this->_tpl_vars['Arr']['HoleStatus'] == 'Yes'): ?>checked="checked"<?php endif; ?>/>&nbsp;<div class="check_text">Yes</div>
									<input type="radio" name="HoleStatus" value="No"  class="check" <?php if ($this->_tpl_vars['Arr']['HoleStatus'] == 'No'): ?>checked="checked"<?php endif; ?>/> &nbsp; <div class="check_text">No</div>
								</div>
								<div class="clearxy height10"></div>
							<?php endif; ?>
						<?php endif; ?>
						
						<div class="location">
						<div class="frm_p1_new">Length (A)</div>
						<div class="frm_p2">
						<input name="HoleA" id="HoleA" type="text" value="<?php if ($this->_tpl_vars['Arr']['HoleA'] != '0'):  echo $this->_tpl_vars['Arr']['HoleA'];  endif; ?>" class="txt_con1" onkeypress="return AllowOnlyCurrency(event);"/> in
							<br /><span id="HoleAMsg" class="error"></span>
						</div>
						</div>
						<div class="location">
						<div class="frm_p1_new">Height (B)</div>
						<div class="frm_p2">
						<input name="LengthB" id="LengthB" type="text" value="<?php if ($this->_tpl_vars['Arr']['LengthB'] != '0'):  echo $this->_tpl_vars['Arr']['LengthB'];  endif; ?>" class="txt_con1" onkeypress="return AllowOnlyCurrency(event);"/> in
							<br /><span id="LengthBMsg" class="error"></span>
						</div>
						</div>
 							<?php if ($this->_tpl_vars['Arr']['HingeHoleSta'] == 'Yes' || $_SESSION['ProfileId'] == 4): ?>
								<div class="location">
								<div class="frm_p1_new">C</div>
								<div class="frm_p2">
								<input name="WidthC" id="WidthC" type="text" value="<?php if ($this->_tpl_vars['Arr']['WidthC'] != '0'):  echo $this->_tpl_vars['Arr']['WidthC'];  endif; ?>" class="txt_con1" onkeypress="return AllowOnlyCurrency(event);"/> in
									<br /><span id="WidthCMsg" class="error"></span>
								</div>
								</div>
								<div class="location">
								<div class="frm_p1_new">D</div>
								<div class="frm_p2" style="width:230px;">
								<input name="WidthD" id="WidthD" type="text" value="<?php if ($this->_tpl_vars['Arr']['WidthD'] != '0'):  echo $this->_tpl_vars['Arr']['WidthD'];  endif; ?>" class="txt_con1" onkeypress="return AllowOnlyCurrency(event);"/> in
									<br /><span id="WidthDMsg" class="error"></span>
								</div>
								</div>
							<?php endif; ?><div class="clearxy height10"></div><div id="FailMsg"></div> <div class="clearxy height10"></div>
					<?php if ($_SESSION['ProfileId'] == 3): ?>
						<div class="frm_p1">Length (Running ft) :</div>
						<div class="frm_p2">
						<input name="Length" id="Length" type="text" value="<?php if ($this->_tpl_vars['Arr']['Length'] != '0'):  echo $this->_tpl_vars['Arr']['Length'];  endif; ?>" class="txt_con1" onkeypress="return AllowOnlyNumbers(event);"/>
							<br /><span id="LengthMsg" class="error"></span>
						</div>
						<div class="clearxy height10"></div>
					<?php endif; ?>
						<div class="clearxy height10"></div>
					<?php if ($_SESSION['ProfileId'] == 4): ?>
						<div class="frm_p1">Do you require drilling holes? </div>
						<div class="frm_p2" style="margin:8px 0 0 0;">
				<input type="radio" name="DrillHoleStatus" value="Yes" class="check" <?php if ($this->_tpl_vars['Arr']['DrillHoleStatus'] == 'Yes'): ?>checked="checked"<?php endif; ?> onclick="ShowDrillHole('Yes');"/>&nbsp;<div class="check_text" style="width:55px; float:left;" >Yes</div>
				<input type="radio" name="DrillHoleStatus" value="No"  class="check" <?php if ($this->_tpl_vars['Arr']['DrillHoleStatus'] == 'No'): ?>checked="checked"<?php endif; ?> onclick="ShowDrillHole('No');"/> &nbsp; <div class="check_text">No</div> </div> 
					<div class="clearxy height10"></div> 
					 <div id="TotDH">
						<div class="frm_p1">Total Drilling holes :</div>
						<div class="frm_p2">
						<input name="TotDrillHoles" id="TotDrillHoles" type="text" value="<?php if ($this->_tpl_vars['Arr']['TotDrillHoles'] != '0'):  echo $this->_tpl_vars['Arr']['TotDrillHoles'];  endif; ?>" class="txt_con1" onkeypress="return AllowOnlyNumbers(event);"/>
							<br /><span id="TotDrillHolesMsg" class="error"></span>
					<div class="clearxy height10"></div>   
						</div>
					</div>
						<div class="clearxy height10"></div> 
					<div style="font-weight:bolder; padding-left:160px;">�12% surcharge will be added with final Quote price for Glass Only Profile�</div>
					<?php endif; ?>
						<div class="frm_p1">Total Qty :</div>
						<div class="frm_p2">
						<input name="Qty" id="Qty" type="text" value="<?php if ($this->_tpl_vars['Arr']['Qty'] != '0'):  echo $this->_tpl_vars['Arr']['Qty'];  endif; ?>" class="txt_con1" onkeypress="return AllowOnlyNumbers(event);"/>
							<br /><span id="QtyMsg" class="error"></span>
						</div>
						<div class="clearxy height10"></div>
						<div class="frm_p1">Delivery Order  :</div>
						<div class="frm_p2">
				<input type="radio" name="OrderType" value="Normal" class="check" <?php if ($this->_tpl_vars['Arr']['OrderType'] == 'Normal'): ?>checked="checked"<?php endif; ?>/>&nbsp;<div class="check_text">
						Normal Order</div>
				<input type="radio" name="OrderType" value="Rush"  class="check" <?php if ($this->_tpl_vars['Arr']['OrderType'] == 'Rush'): ?>checked="checked"<?php endif; ?>/> &nbsp; <div class="check_text">Rush Order</div> </div>
					<div class="clearxy"></div> 
					<div style="font-weight:bolder; padding-left:160px;">�NORMAL Order: 10-15 Days after order placed.    RUSH Order: Added 10% and delivered 5-10 Days after order placed�</div>
					<div class="clearxy height10"></div> 
						<div class="clearxy height10"></div> 
 						<div class="frm_p1">Comments :</div>
						<div class="frm_p2" style="height:150px;">
						<textarea name="Comments" id="Comments" cols="60" rows="50" style="height:100px;"/><?php echo $this->_tpl_vars['Arr']['Comments']; ?>
</textarea> 
						</div> 
					<div class="clearxy height10"></div>	
						<div class="button_next11">
	<?php if ($_SESSION['ProfileId'] == 3 || $_SESSION['ProfileId'] == 4): ?>
	<input name="" type="button" class="button_next1a" value="Back" onClick="GoBackRedirct('<?php echo $this->_tpl_vars['SiteMainPath']; ?>
finish.html');"/>
	<?php else: ?>
	<input name="" type="button" class="button_next1a" value="Back" onClick="GoBackRedirct('<?php echo $this->_tpl_vars['SiteMainPath']; ?>
hingehole.html');"/>
	<?php endif; ?>
			<input name="Holes" type="button" class="button_next1ab" value="Next" onclick="GoEditVaildate();"/>
							</div>
						</div>
						</div> 
				</form>
				<div class="productbtm"></div>
	  </div>
		  </div>
		  <div class="clearxy"></div>
	</div>
</div>

<script type="text/javascript">
	var FormName1='post_form1';
	var FormObj=document.forms[FormName1];
</script>