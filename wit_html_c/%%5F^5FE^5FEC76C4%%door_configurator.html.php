<?php /* Smarty version 2.6.3, created on 2013-08-21 05:24:28
         compiled from door_configurator.html */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'trim', 'door_configurator.html', 44, false),)), $this); ?>

<!-- <link rel="stylesheet" href="<?php  echo BASEPATH ?>css/bootstrap.css"> -->
<link rel="stylesheet" href="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
css/door_configurator.css">
<link rel="stylesheet" href="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
js/scroller/scroller.css">


	

<div class="Content">
	<div class="connetentin">
		<div class="common_titlesm">
		<div class="common_titles">
			<div class="title_lft"></div>
			<div class="title_mid">Door Configurator</div>
		</div>
		</div>
			<div class="clearxy height10"></div>
			<div class="productmain">
				<div class="product">

				<!-- ============================== START =================================== -->
				<div style="display:none">
				<input type="text" name="ptype" id="door-ptype" value="" placeholder="profile type" class="door-calculation">
				<input type="text" name="pfinish" id="door-pfinish" value=""  placeholder="profile finish" class="door-calculation">
				<input type="text" name="pfinish" id="door-pfinish-lang" value=""  placeholder="profile finish in language">
				<input type="text" name="itype" id="door-itype" value="" placeholder="insert type" class="door-calculation">
				<input type="text" name="itype" id="door-itype-lang" value="" placeholder="insert type in language">
				<input type="text" name="sub_insert" id="door-sub_insert" value="" placeholder="sub insert type" class="door-calculation">
				<input type="text" name="sub_insert" id="door-sub_insert-lang" value="" placeholder="sub insert type in language">
				<input type="text" name="product_price" value="" placeholder="product price" id="product_price">
				</div>
				
		<table class="tdable">
			<tr> <td rowspan="12" width="10"> &nbsp;  </td> <td colspan="2" > &nbsp;  </td> </tr>
			<tr>
			<td width="620" style="vertical-align:top;" valign="top">
			<table width="620">
				<tr>
					<td colspan="10">
						<h3 id="step_1_heading"> <span class="step"><i class="icon-chevron-right"></i><?php echo $this->_tpl_vars['lang']['step']; ?>
 1</span> <?php echo $this->_tpl_vars['lang']['profileType']; ?>
 </h3>
						<div id="window-step_1">
							<div class="win_scroll">
								<?php if (count($_from = (array)$this->_tpl_vars['profile_factor'])):
    foreach ($_from as $this->_tpl_vars['pro']):
?>
									<div class="element" rel="<?php echo ((is_array($_tmp=$this->_tpl_vars['pro']['code'])) ? $this->_run_mod_handler('trim', true, $_tmp) : trim($_tmp)); ?>
" preview="<?php echo $this->_tpl_vars['SiteHttpPath'];   echo DOOR_FOLDER;   echo $this->_tpl_vars['pro']['image']; ?>
" onclick="selectProfileType(this)">
										<img src="<?php echo $this->_tpl_vars['SiteHttpPath'];   echo DOOR_FOLDER.'thumbnail/';   echo $this->_tpl_vars['pro']['image']; ?>
">
										<br/>
										<?php echo $this->_tpl_vars['pro']['name']; ?>

									</div>
								<?php endforeach; unset($_from); endif; ?>
							</div> <!-- end of .scroll -->
							<div class="prevscroll"> </div>
							<div class="nextscroll"> </div>
						</div> <!-- end of .window -->	
					</td>
				</tr>
				
				<tr>
					<td colspan="10">
						<h3 id="step_2_heading"> <span class="step"><i class="icon-chevron-right"></i><?php echo $this->_tpl_vars['lang']['step']; ?>
 2</span> <?php echo $this->_tpl_vars['lang']['profileFinish']; ?>
 </h3>
						<div id="window-step_2" class="finish-s-container">
							<div class="win_scroll">
								<?php if (count($_from = (array)$this->_tpl_vars['profile_finish'])):
    foreach ($_from as $this->_tpl_vars['pro']):
?>
									<?php   
										$avw = $this->get_template_vars(pro); //var_dump($avw);
										$langTitle = ($avw['profile_finish_title']);
									 ?>
									<div class="element" rel="<?php echo ((is_array($_tmp=$this->_tpl_vars['pro']['profile_finish_code'])) ? $this->_run_mod_handler('trim', true, $_tmp) : trim($_tmp)); ?>
" lang="<?php  echo $langTitle; ?>" onclick="selectProfileFinish(this)">
										<img src="<?php echo $this->_tpl_vars['SiteHttpPath'];   echo PROFILE_FINISH_FOLDER.'thumbnail/';   echo $this->_tpl_vars['pro']['profile_finish_image']; ?>
">
										<br/>
										<?php echo $this->_tpl_vars['pro']['profile_finish_title']; ?>

									</div>
								<?php endforeach; unset($_from); endif; ?>
							</div> <!-- end of .scroll -->
							<div class="prevscroll"> </div>
							<div class="nextscroll"> </div>
						</div> <!-- end of .window -->
					</td>
				</tr>
				
				<tr>
					<td colspan="10">
						<h3 id="step_3_heading"> <span class="step"><i class="icon-chevron-right"></i><?php echo $this->_tpl_vars['lang']['step']; ?>
 3 </span><?php echo $this->_tpl_vars['lang']['insertType']; ?>
 </h3>
						
						<div id="window-step_3" class="insert-s-container">
							<div class="win_scroll">
								<?php if (count($_from = (array)$this->_tpl_vars['profile_insert'])):
    foreach ($_from as $this->_tpl_vars['pro']):
?>
									<?php   
										$avw = $this->get_template_vars(pro); //var_dump($avw);
										$langTitle = ($avw['insert_title']);
										if($avw['insert_available_with']!='')
										{
											$avw = objectToArray( json_decode($avw['insert_available_with']));
											$avw = implode(' ',$avw);
										}
										else
										$avw = '';
									 ?>
									<div class="<?php echo $avw; ?> element" id="<?php echo $this->_tpl_vars['pro']['insert_id']; ?>
" rel="<?php echo ((is_array($_tmp=$this->_tpl_vars['pro']['insert_code'])) ? $this->_run_mod_handler('trim', true, $_tmp) : trim($_tmp)); ?>
" lang="<?php  echo $langTitle; ?>" onclick="selectInsertType(this)">
										<img src="<?php echo $this->_tpl_vars['SiteHttpPath'];   echo INSERT_FOLDER.'thumbnail/';   echo $this->_tpl_vars['pro']['insert_image']; ?>
">
										<br/>
										<?php echo $this->_tpl_vars['pro']['insert_title']; ?>

									</div>
								<?php endforeach; unset($_from); endif; ?>
							</div> <!-- end of .scroll -->
							<div class="prevscroll"> </div>
							<div class="nextscroll"> </div>
						</div> <!-- end of .window -->
						
							<div id="window-sub_step_3" class="sub-insert-s-container">
								<div class="win_scroll">
								</div> <!-- .scroll -->
								<div class="prevscroll"> </div>
								<div class="nextscroll"> </div>
							</div> <!-- .window-sub -->
					</td>
				</tr>
				
				<tr>
					<td colspan="10">
						<h3 id="step_4_heading"> <span class="step"><i class="icon-chevron-right"></i><?php echo $this->_tpl_vars['lang']['step']; ?>
 4</span> <?php echo $this->_tpl_vars['lang']['doorDimension']; ?>
 </h3>
						<table class="table dimension-s-container" width="100%">							
							<tr>
								<td rowspan="3" width="25" > &nbsp;  </td>
								<td> Width : </td>
								<td><input type="text" name="width" value="14" id="door-width" class="door-calculation dimension-calculation span1"></td>
								<th> = </th>
								<td> <span class="mm-door-width"> </span>  </td>
								<td> mm </td>
								<td> &nbsp; &nbsp; &nbsp;  </td>
								<td> Quantity </td>
							</tr>
							<tr>
								<td> Height : </td>
								<td><input type="text" name="height" value="24" id="door-height" class="door-calculation dimension-calculation span1"></td>
								<th> = </th>
								<td> <span class="mm-door-height"> </span>  </td>
								<td> mm  </td>
								<td> &nbsp; &nbsp; &nbsp;  </td>
								<td>
									<div class="input-prepend input-append">
									  <span class="add-on" onclick="changeCurrentQuantity(this,'--')">&minus;</span>
									  <input type="text" name="quantity" value="1" id="door-quantity" class="door-calculation door-qty span1 text-center">
									  <span class="add-on" onclick="changeCurrentQuantity(this,'++')">&#43;</span>
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				
				<tr>
					<td colspan="10">
						<h3 id="step_5_heading"> <span class="step"><i class="icon-chevron-right"></i><?php echo $this->_tpl_vars['lang']['step']; ?>
 5</span> <?php echo $this->_tpl_vars['lang']['orientation']; ?>
 </h3>
						<input type="hidden" name="orientation" id="door-orientation" value="vertical" class="">
						<table class="table orientation-s-container" width="100%">
							<tr>
								<td width="25"> &nbsp;  </td>
								<td> 
									<div class="orientation btn btn-warning" onclick="changeOrientaion(this,'vertical');" > Vertical </div> 
									<div class="orientation btn btn-link" onclick="changeOrientaion(this,'horizontal');"> Horizontal </div>  
								</td>
								<td colspan="2"> &nbsp;  </td>
							</tr>
							<tr>
								<td> &nbsp;  </td> 
								<td> <?php echo $this->_tpl_vars['lang']['doyouhinge']; ?>
 ?</td>
								<td width="20%"> 
									<div class="btn" onclick="requireHoles(this,'yes');" > <?php echo $this->_tpl_vars['lang']['yes']; ?>
 </div>
									<div class="btn" onclick="requireHoles(this,'no');"> <?php echo $this->_tpl_vars['lang']['no']; ?>
 </div>
								</td>
								<td width="30%"> 
									<div class="holes"><?php echo $this->_tpl_vars['lang']['noOfHoles']; ?>
 : 
									<input type="text" title="Two drilling holes free, additional drilling holes will be chargeable" class="span1 door-calculation" name="drilling_holes" id="door-holes"  value="" >
									</div>
									<span> <?php echo $this->_tpl_vars['lang']['noOfHoles']; ?>
 0 </span>
								</td>
							</tr>
							<tr>
								<td> &nbsp;  </td> 
								<td> <?php echo $this->_tpl_vars['lang']['doyouhandle']; ?>
 ?</td>
								<td> 
									<div class="btn" onclick="requireHoles(this,'yes');" > <?php echo $this->_tpl_vars['lang']['yes']; ?>
 </div>
									<div class="btn" onclick="requireHoles(this,'no');"> <?php echo $this->_tpl_vars['lang']['no']; ?>
 </div>
								</td>
								<td> 
									<div class="holes"><?php echo $this->_tpl_vars['lang']['noOfHoles']; ?>
 : 
									<input type="text" class="span1 door-calculation" name="handle_holes" id="door-handle-holes" value="" >
									</div>
									<span> <?php echo $this->_tpl_vars['lang']['noOfHoles']; ?>
 0 </span>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				
			</table>
			</td>
			<td> &nbsp;&nbsp; </td>
			<td valign="middle" width="250">
			
			<table id="preview_table" class="table" >
				<thead>
					<tr>
						<td class="alert alert-info" colspan="4" > 
							<!-- <div class="progress progress-striped active">
								<div class="bar"></div>
							</div> -->
							<div class="preview_instructions1"> <?php echo $this->_tpl_vars['lang']['ins1']; ?>
 </div>
							<div class="preview_instructions2 hidden"> <?php echo $this->_tpl_vars['lang']['ins2']; ?>
 </div>
							<div class="preview_instructions3 hidden"> <?php echo $this->_tpl_vars['lang']['ins3']; ?>
 </div>
							<div class="preview_instructions4 hidden"> <?php echo $this->_tpl_vars['lang']['ins4']; ?>
 </div>
						</td>
					</tr>
					<tr>
						<td class="preview_default" colspan="4" > <?php echo $this->_tpl_vars['lang']['doorPreview']; ?>
 </td>
					</tr>
				</thead>
				<tr>
					<td colspan="4" class="door_preview preview_step_1">
							
					</td>
				</tr>
				<tr>
					<td colspan="2" class="door_preview preview_step_2" width="50%">
						
					</td >
					<td  colspan="2" class="door_preview preview_step_3 preview_step_4" valign="middle">
						
					</td>
				</tr>
				
				<tfoot>
					<!--
					<tr class="door_preview show_price">
						<td colspan="4"> <div class="btn btn-warning" onclick="priceCalculation();"> Show Price </div> </td>
					</tr> -->
					<tr class="door_preview temp_cart">
						<td><?php echo $this->_tpl_vars['lang']['profile']; ?>
</td>
						<td>Qty</td>
						<td><?php echo $this->_tpl_vars['lang']['price']; ?>
</td>
						<td><?php echo $this->_tpl_vars['lang']['total']; ?>
</td>
					</tr>
					<tr class="door_preview temp_cart">
						<td class="door-ptype"></td>
						<td><div class="door-quantity">1</td>
						<td>
							<div class="result"><label>$</label></div>
						</td>
						<td><div id="total-price" class="product_price"></div></td>
					</tr>
					<tr class="door_preview temp_cart">
						<td colspan="2">&nbsp;</td>
						<td><?php echo $this->_tpl_vars['lang']['total']; ?>
</td>
						<td>
							<div class="total-price"></div>
						</td>
					</tr>
					<tr class="door_preview temp_cart">
						<td  colspan="4"> 
							<div class="btn btn-success btn-block" id="add_to_cart"> <?php echo $this->_tpl_vars['lang']['addToCart']; ?>
 </div> 
						</td>					
					</tr>
				</tfoot>
			</table>
			</td>
			</tr>
			</table>
			
			<br/><br/>
			
			<table class="table table-bordered" id="shopping-cart" style="<?php echo $this->_tpl_vars['style']; ?>
" > 
			<thead>
				<tr>
					<th colspan="2"> <?php echo $this->_tpl_vars['lang']['shoppingCart']; ?>
 </th>
					<td colspan="4"> &nbsp;  </td>
					<th colspan="2" onclick="emptyCart(this);"> <?php echo $this->_tpl_vars['lang']['clearCart']; ?>
 </th>
				</tr>
				<tr class="underline">
					<th width="12"> &nbsp; </th>
					<th> <?php echo $this->_tpl_vars['lang']['profile']; ?>
 </th>
					<th> &nbsp; </th>
					<th> <?php echo $this->_tpl_vars['lang']['dimension']; ?>
 </th>
					<th> <?php echo $this->_tpl_vars['lang']['quantity']; ?>
 </th>
					<th> <?php echo $this->_tpl_vars['lang']['price']; ?>
 </th>
					<th> <?php echo $this->_tpl_vars['lang']['total']; ?>
 </th>
					<th> &nbsp; </th>
				</tr>
			</thead>
			<tbody>
				<?php if (! empty ( $this->_tpl_vars['cartSession'] )): ?>
					<?php if (count($_from = (array)$this->_tpl_vars['cartSession'])):
    foreach ($_from as $this->_tpl_vars['c']):
?>
						<tr>
							<th width="12"> &nbsp; </th>
							<th> <?php echo $this->_tpl_vars['c']['profile_type']; ?>
 </th>
							<td> Finish: <?php echo $this->_tpl_vars['c']['profile_finish']; ?>
 <br> Insert: <?php echo $this->_tpl_vars['c']['insert_type'];  if ($this->_tpl_vars['c']['sub_insert'] != ''): ?>/<?php echo $this->_tpl_vars['c']['sub_insert']; ?>
 <?php endif; ?></td>
							<td> <?php echo $this->_tpl_vars['c']['width']; ?>
 x <?php echo $this->_tpl_vars['c']['height']; ?>
 </td>
							<td> <?php echo $this->_tpl_vars['c']['quantity']; ?>
 </td>
							<td> $<?php echo $this->_tpl_vars['c']['price']; ?>
  </td>
							<th> $<?php echo $this->_tpl_vars['c']['sub_total']; ?>
 <?php  $tot = '';  ?> </th>
							<td> <i class="icon-remove" onclick="removeFromCart(this,'<?php echo $this->_tpl_vars['c']['cart_id']; ?>
')" ></i> </td>
						</tr>
					<?php endforeach; unset($_from); endif; ?>
				<?php endif; ?>
			</tbody>
			<tfoot>
				<tr> 
					<td colspan="5"> &nbsp;  </td>
					<th> <?php echo $this->_tpl_vars['lang']['subTotal']; ?>
 </th>
					<th> $<span id="cartTotal"><?php echo $this->_tpl_vars['cartTotal']; ?>
 </span> </th>
				</tr>
				<tr> 
					<td colspan="6"> &nbsp;  </td>
					<td> <div class="btn btn-success btn-block" onclick="window.location='<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
invoice.html';" > <?php echo $this->_tpl_vars['lang']['createQuote']; ?>
 </div> </td>
				</tr>
			</tfoot>
			</table>
				<!-- ============================== END =================================== -->				
					<div class="productbtm"></div>
				</div> <!-- .product -->
			</div> <!-- .productmain -->
		<div class="clearxy"></div>
	</div> <!-- .connetentin -->
</div> <!-- .content -->



<script type="text/javascript"> BASEPATH="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
";</script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
js/scroller/scroller.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
js/koverlay/koverlay.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
js/custom.js"> </script>