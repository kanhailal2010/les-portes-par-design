<?php /* Smarty version 2.6.3, created on 2013-06-04 09:24:26
         compiled from checkout.html */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'stripslashes', 'checkout.html', 172, false),)), $this); ?>
 
<script type="text/javascript" src="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
js/ui.js"></script>
<link href="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
js/ui.css" rel="stylesheet"  />
<script type="text/javascript" src="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
js/time_picker.js"></script> 
<?php echo ' 
<script type="text/javascript">
jQuery(function(){jQuery(\'#availdate\').datetimepicker({dateFormat:\'yy-mm-dd\',minDate: new Date()});});
function showhide(show){
	if(show==\'Address\'){
		jQuery(\'#Carrier\').hide(); jQuery(\'#Account\').hide(); jQuery(\'#Address\').show();jQuery(\'#Amount\').show();
	} 
	else if(show==\'Account\'){
		jQuery(\'#Carrier\').hide(); jQuery(\'#Address\').hide(); jQuery(\'#Account\').show();jQuery(\'#Amount\').show();
	} 
	else if(show==\'Carrier\'){
		jQuery(\'#Address\').hide(); jQuery(\'#Account\').hide(); jQuery(\'#Amount\').hide();jQuery(\'#Carrier\').show();
	} 
}     
function GoToSignValidate(){	
	ErrCss=\'box2box_CC_Contact\'; BErrCss=\'box2box_CC_Contact\'; var FlagIns=true;
  	 
	if(jQuery(\'input:radio[name=ShipMethod]:checked\').val()==\'1\'){	
		if(FlagIns){	
			FlagIns=FormValidateFunction(\'Text\',\'#availdate\',\'Please select date\');
		} 
	}
	else if(jQuery(\'input:radio[name=ShipMethod]:checked\').val()==\'2\'){	
		if(FlagIns){	
			FlagIns=FormValidateFunction(\'Text\',\'#PTransport\',\'Please enter preferred transport\');
		} 
		if(FlagIns){	
			FlagIns=FormValidateFunction(\'Text\',\'#AccNo\',\'Please enter Account Number\');
		} 
	}
	if(FlagIns){	
 		document.forms[FormName1].submit();
		//return true;
	}
	else{	return false;	}
}   
</script>
'; ?>

<div class="Content">
				<div class="connetentin">
					  <div class="common_titlesm">
						<div class="common_titles">
							<div class="title_lft"></div>
							<div class="title_mid">Check Out</div>
						</div>
					</div>
					  <div class="clearxy height10"></div>
					  <div class="productmain">
						  <div class="product">
							<div class="configurator_main">
							<div class="dash_menuss">
									 <!-- ****************** MENUCONTENT START ************************** -->
									 <!-- ******************** MENU DASHBOARD START *********************** -->
					  <div id="menu" >
					<div id="icons" >
			   <div id="iconDescription" class="statut1" style=" width:200px; padding:16px 0 0 0; 
		font-weight:bold; font-family:Arial, Helvetica, sans-serif;
		text-align:center; font-size:15px; color:#FFFFFF; cursor:pointer;" >Dash Board <img src="images/iconDescription.png" alt="" style="margin-top:-4px; float:right; margin-right:10px;"/></div>
			</div>
					  </div>
	
				  <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['IncludeTpl1'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>	
									</div>
							    <div class="main_dash" >		
									<div class="dash_con_r">
 			<form method="post" name="post_form"> 
			<input type="hidden" name="QGId" value="<?php echo $this->_tpl_vars['ArrD']['QGId']; ?>
" /> 
			<?php if ($_GET['stype'] == ''): ?>
			<input type="hidden" name="PayAmt" value="<?php echo $this->_tpl_vars['ArrD']['PayAmt']; ?>
" /> 
			<?php else: ?>
			<input type="hidden" name="PayAmt" value="<?php echo $_GET['BAmt']; ?>
" /> 
			<input type="hidden" name="IPayAmt" value="<?php echo $_GET['BAmt']; ?>
" /> 
			<?php endif; ?>
                    <div class="profile_view"> 
						<div class="profile_view1">Quote Code</div>
						<div class="profile_view2">:</div>
						<div class="profile_view3"><?php echo $this->_tpl_vars['ArrD']['QuoteId']; ?>
</div>
					</div>
                    <div class="profile_view"> 
						<div class="profile_view1">Profile Type</div>
						<div class="profile_view2">:</div>
						<div class="profile_view3"><?php if ($this->_tpl_vars['ArrD']['ProfileId'] == 1): ?>Door Profile with Glass
								   <?php elseif ($this->_tpl_vars['ArrD']['ProfileId'] == 2): ?>Door Profile without Glass
								   <?php elseif ($this->_tpl_vars['ArrD']['ProfileId'] == 3): ?>Frame Only<?php else: ?>Glass Only<?php endif; ?></div>
					</div>
					<div class="profile_view"> 
						<div class="profile_view1">Qty</div>
						<div class="profile_view2">:</div>
						<div class="profile_view3"><?php echo $this->_tpl_vars['ArrD']['Qty']; ?>
</div>
					</div>
					<div class="profile_view"> 
						<div class="profile_view1">Order Type</div>
						<div class="profile_view2">:</div>
						<div class="profile_view3"><?php echo $this->_tpl_vars['ArrD']['OrderType']; ?>
 Order <?php if ($this->_tpl_vars['ArrD']['OrderType'] == 'Rush'): ?>(Includes Extra 10%)<?php endif; ?></div>
					</div>
					<div class="profile_view"> 
						<div class="profile_view1">Applied Taxes</div>
						<div class="profile_view2">:</div>
						<div class="profile_view3"><?php echo $this->_tpl_vars['Tax1']['Tax']; ?>
 - <?php echo $this->_tpl_vars['Tax1']['Percentage']; ?>
%, <?php echo $this->_tpl_vars['Tax2']['Tax']; ?>
 - <?php echo $this->_tpl_vars['Tax2']['Percentage']; ?>
%<?php if ($this->_tpl_vars['Tax3']['Percentage'] != '0.000'): ?>, <?php echo $this->_tpl_vars['Tax3']['Tax']; ?>
 - <?php echo $this->_tpl_vars['Tax3']['Percentage']; ?>
%<?php endif; ?></div>
					</div>
					<?php if ($_GET['stype'] == ''): ?>
					<div class="profile_view"> 
						<div class="profile_view1">Price</div>
						<div class="profile_view2">:</div>
						<div class="profile_view3">$  <?php echo $this->_tpl_vars['ArrD']['PayAmt']; ?>
</div>
					</div>
					<?php else: ?>
					<div class="profile_view"> 
						<div class="profile_view1">Price</div>
						<div class="profile_view2">:</div>
						<div class="profile_view3">$ <?php echo $_GET['BAmt']; ?>
</div>
					</div>
					<?php endif; ?>
                    <div class="profile_view"> 
						<div class="profile_view1">Payment Gateway</div>
						<div class="profile_view2">:</div>
						<div class="profile_view3"><img src="images/paypal_logo.jpg" /></div>
					</div>
					<?php if ($_GET['stype'] == ''): ?>
                    <div class="profile_view"> 
						<div class="profile_view1">Shipping Method</div>
						<div class="profile_view2">:</div>
						<div class="profile_view3">
							<div class="frm_p2" style="width:500px;">
							<input type="radio" name="ShipMethod" value="1" class="check" onclick="showhide('Address')" checked="checked"/>&nbsp;<div class="check_text" style="width:300px;">Pick up ordered product from les portes shop</div>  
							<div class="clearxy"></div>
							<input type="radio" name="ShipMethod" value="2"  class="check" onclick="showhide('Account')"/> &nbsp; <div class="check_text" style="width:470px;">Provide preferred transport and account number and we will ship it to you </div>  <div class="clearxy"></div>
							<input type="radio" name="ShipMethod" value="3"  class="check" onclick="showhide('Carrier')"/> &nbsp; <div class="check_text" style="width:300px;">Arrange shipping of orders from your carrier </div>
							</div>
						</div>
					</div>
					<div id="Address">
						<div class="profile_view"> 
							<div class="profile_view1">For Pick up of Orders</div>
							<div class="profile_view2">:</div>
							<div class="profile_view3" style="font-weight:bold;">4590, Henri-Bourassa West,Ville St-Laurent, Qu�bec, Canada, H4L 1A8.<div class="clearxy"></div><!--
	15 Nash Rd., Dollard Des Ormeaux, Quebec. H9B 2N9. 
	Toll free:     1-855-880-DOOR
	Local calls:    514-264-8884-->
	</div>
						</div>
 						<div class="profile_view"> 
							<div class="profile_view1">Desired Pickup date</div>
							<div class="profile_view2">:</div>
							<div class="profile_view3"> 
								 <input name="availdate" id="availdate" type="text" class="box2box_CC_Contact"/>
								 <br /><span id="availdateMsg" style="padding-left:10px;" class="error"></span></div>
						</div>
					</div>
					<div id="Account" style="display:none;">
 						<div class="profile_view"> 
							<div class="profile_view1">Preferred Transport</div>
							<div class="profile_view2">:</div>
							<div class="profile_view3"> 
								 <input name="PTransport" id="PTransport" type="text" class="box2box_CC_Contact" style="width:250px;"/>
								 <br /><span id="PTransportMsg" style="padding-left:10px;" class="error"></span></div>
						</div>
 						<div class="profile_view"> 
							<div class="profile_view1">Acount No</div>
							<div class="profile_view2">:</div>
							<div class="profile_view3"> 
								 <input name="AccNo" id="AccNo" type="text" class="box2box_CC_Contact" style="width:250px;"/>
								 <br /><span id="AccNoMsg" style="padding-left:10px;" class="error"></span></div>
						</div>
						<div class="profile_view"> 
							<div class="profile_view1">Shipping Address</div>
							<div class="profile_view2">:</div>
							<div class="profile_view3" style="font-weight:bold;"><?php echo ((is_array($_tmp=$this->_tpl_vars['Arr']['UShipAdds'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
, <?php echo ((is_array($_tmp=$this->_tpl_vars['Arr']['UShipCity'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
, <?php echo ((is_array($_tmp=$this->_tpl_vars['Arr']['state_name'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
, <?php echo ((is_array($_tmp=$this->_tpl_vars['Arr']['country_name'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
<div class="clearxy"></div>
	Phone:     <?php echo ((is_array($_tmp=$this->_tpl_vars['Arr']['UShipPhone'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
 
	</div>
						</div>
 					</div>
					<div id="Amount">
 						<div class="profile_view"> 
							<div class="profile_view1">Total Cost</div>
							<div class="profile_view2">:</div>
							<div class="profile_view3">$  <input name="IPayAmt" type="text" class="box2box_CC_Contact" value="<?php echo $this->_tpl_vars['ArrD']['PayAmt']; ?>
"/></div>  <div class="clearxy height10"></div>
							<!--<div class="profile_view3" style=" border:#000000 solid 1px; width:550px;font-weight:bold;">You can place the order without paying by entering 00.00 in the total cost field , otherwise you can edit the cost to pay partially or just pay as it is to pay in full.</div>--><div class="clearxy"></div>
						</div>
 					</div>
					<div id="Carrier" style="display:none;">
 						<div class="profile_view"> 
							<div class="profile_view1">Shipping Cost</div>
							<div class="profile_view2">:</div>
							<div class="profile_view3">$ <?php echo $this->_tpl_vars['ShipCost']; ?>
</div>
						</div>
 						<div class="profile_view"> 
							<div class="profile_view1">Total Cost</div>
							<div class="profile_view2">:</div>
							<div class="profile_view3"> $
								 <input name="SPayAmt" id="PayAmt" type="text" class="box2box_CC_Contact" value="<?php echo $this->_tpl_vars['TotalAmt']; ?>
" style="width:250px;"/>  </div><div class="clearxy height10"></div>
							<!--<div class="profile_view3" style=" border:#000000 solid 1px; width:550px;font-weight:bold;">You can place the order without paying by entering 00.00 in the total cost field , otherwise you can edit the cost to pay partially or just pay as it is to pay in full.<div class="clearxy"></div>
						</div>-->
						</div>
 					</div>
					<?php endif; ?>
                     <div class="clearxy height10"></div>
                    <div class="clearxy height10"></div>
                    <div class="main_in1_CC_dash1">
					   <div class="main_in1_CC_dash_in"> 
						
				<input type="hidden" name="UserAction" value="Renew" />
				<?php if ($_GET['stype'] == ''): ?>
				<input type="submit" value="Pay" name="Pay" class="btn1box" onclick="return GoToSignValidate();"/> 
				<?php else: ?>
				<input type="submit" value="Pay" name="Pay" class="btn1box"/> 
				<?php endif; ?></div>
						<div class="main_in1_CC_dash_in"><input type="reset" class="btn1box" value="Cancel" name="q" onClick="GoBackRedirct('<?php echo $this->_tpl_vars['SiteMainPath']; ?>
quotes.html');"></div>
						<div class="clearxy"></div>
					</div>
					</form>
					<div style="padding-left:100px;"><!-- PayPal Logo --><table border="0" cellpadding="10" cellspacing="0" align="center"><tr><td align="center"></td></tr><tr><td align="center"><a href="https://www.paypal.com/webapps/mpp/paypal-popup" title="How PayPal Works" onclick="javascript:window.open('https://www.paypal.com/webapps/mpp/paypal-popup','WIPaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700'); return false;"><img src="https://www.paypalobjects.com/webstatic/mktg/logo/AM_mc_vs_dc_ae.jpg" border="0" alt="PayPal Acceptance Mark"></a></td></tr></table><!-- PayPal Logo -->
					</div>
								</div>
								</div> 
							</div>
 						    <div class="productbtm"></div>
				  </div>
				      </div>
				      <div class="clearxy"></div>
			    </div>
	   </div>



<script type="text/javascript">
	var FormName1='post_form';
	var FormObj=document.forms[FormName1];
</script>