<?php /* Smarty version 2.6.3, created on 2013-08-17 11:32:10
         compiled from final.html */ ?>

<link rel="stylesheet" href="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
css/door_configurator.css">

<div class="Content">
	<div class="connetentin">
		<div class="common_titlesm">
		<div class="common_titles">
			<div class="title_lft"></div>
			<div class="title_mid">Payment Checkout </div>
		</div>
		</div>
			<div class="clearxy height10"></div>
			<div class="productmain">
				<div class="product">
				<br/><br/><br/>
				<form action="invoice.html?act_type=pay&invoice=<?php echo $this->_tpl_vars['invoice']['door_invoice_code']; ?>
" method="post" name="post_form" class="dash_con_r">
				<div class="profile_view"> 
						<div class="profile_view1">Payment Gateway</div>
						<div class="profile_view2">:</div>
						<div class="profile_view3"><img src="images/paypal_logo.jpg"></div>
				</div>
				<div class="profile_view"> 
					<div class="profile_view1">Shipping Method</div>
					<div class="profile_view2">:</div>
					<div class="profile_view3">
						<div class="frm_p2" style="width:500px;">
						<input type="radio" name="ShipMethod" value="1" class="check" onclick="showhide('Address')" checked="checked">&nbsp;<div class="check_text" style="width:300px;"><?php echo $this->_tpl_vars['lang']['pickup']; ?>
</div>  
						<div class="clearxy"></div>
						<input type="radio" name="ShipMethod" value="2" class="check" onclick="showhide('Account')"> &nbsp; <div class="check_text" style="width:470px;"><?php echo $this->_tpl_vars['lang']['preferred']; ?>
</div>  <div class="clearxy"></div>
						<input type="radio" name="ShipMethod" value="3" class="check" onclick="showhide('Carrier')"> &nbsp; <div class="check_text" style="width:300px;"><?php echo $this->_tpl_vars['lang']['carrier']; ?>
</div>
						</div>
					</div>
				</div>
				<div id="Address" style="display: block;">
					<div class="profile_view"> 
						<div class="profile_view1"><?php echo $this->_tpl_vars['lang']['pickOrder']; ?>
</div>
						<div class="profile_view2">:</div>
						<div class="profile_view3" style="font-weight:bold;">15 Nash, Dollard Des Ormeaux, Quebec  H9B 2N9<div class="clearxy"></div><!--
15 Nash Rd., Dollard Des Ormeaux, Quebec. H9B 2N9. 
Toll free:     1-855-880-DOOR
Local calls:    514-264-8884-->
</div>
					</div>
					<div class="profile_view"> 
						<div class="profile_view1"><?php echo $this->_tpl_vars['lang']['pickupDate']; ?>
</div>
						<div class="profile_view2">:</div>
						<div class="profile_view3"> 
							 <input name="availdate" id="availdate" type="text" placeholder="yyyy-mm-dd" class="box2box_CC_Contact hasDatepicker">
							 <br><span id="availdateMsg" style="padding-left:10px;" class="error"></span></div>
					</div>
				</div>
				<div id="Account" style="display: none;">
					<div class="profile_view"> 
						<div class="profile_view1"><?php echo $this->_tpl_vars['lang']['preferTrans']; ?>
</div>
						<div class="profile_view2">:</div>
						<div class="profile_view3"> 
							 <input name="PTransport" id="PTransport" type="text" class="box2box_CC_Contact" style="width:250px;">
							 <br><span id="PTransportMsg" style="padding-left:10px;" class="error"></span></div>
					</div>
					<div class="profile_view"> 
						<div class="profile_view1"><?php echo $this->_tpl_vars['lang']['accNo']; ?>
</div>
						<div class="profile_view2">:</div>
						<div class="profile_view3"> 
							 <input name="AccNo" id="AccNo" type="text" class="box2box_CC_Contact" style="width:250px;">
							 <br><span id="AccNoMsg" style="padding-left:10px;" class="error"></span></div>
					</div>
					<!--
					<div class="profile_view"> 
						<div class="profile_view1">Shipping Address</div>
						<div class="profile_view2">:</div>
						<div class="profile_view3" style="font-weight:bold;"><div class="clearxy"></div></div>
					</div> -->
				</div>
				<div id="Amount" style="display: none;">
					<div class="profile_view"> 
						<div class="profile_view1"><?php echo $this->_tpl_vars['lang']['totalCost']; ?>
</div>
						<div class="profile_view2">:</div>
						<div class="profile_view3">$<?php echo $this->_tpl_vars['invoice']['door_invoice_final_amount']; ?>
 <input name="IPayAmt" type="hidden" class="box2box_CC_Contact" value="98672.41"></div>  <div class="clearxy height10"></div>
						<div class="clearxy"></div>
					</div>
				</div>
				<div id="Carrier" style="">
					<div class="profile_view"> 
						<div class="profile_view1"><?php echo $this->_tpl_vars['lang']['shippingCost']; ?>
</div>
						<div class="profile_view2">:</div>
						<div class="profile_view3">$<?php echo $this->_tpl_vars['shippingCost']; ?>
</div>
					</div>
					<div class="profile_view"> 
						<div class="profile_view1"><?php echo $this->_tpl_vars['lang']['totalCost']; ?>
</div>
						<div class="profile_view2">:</div>
						<div class="profile_view3"> $<?php echo $this->_tpl_vars['totalCost']; ?>

							 <input name="SPayAmt" id="PayAmt" type="hidden" class="box2box_CC_Contact" value="<?php echo $this->_tpl_vars['invoice']['door_invoice_final_amount']; ?>
" style="width:250px;">  </div><div class="clearxy height10"></div>
						<!--<div class="profile_view3" style=" border:#000000 solid 1px; width:550px;font-weight:bold;">You can place the order without paying by entering 00.00 in the total cost field , otherwise you can edit the cost to pay partially or just pay as it is to pay in full.<div class="clearxy"></div>
					</div>-->
					</div>
				</div>
				
				<div class="profile_view"> 
					<div class="profile_view1">&nbsp; </div>
					<div class="profile_view2">&nbsp; </div>
					<div class="profile_view3">
						<div class="btn btn-warning" onclick="window.location='<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
invoice.html?act_type=view&invoice=<?php echo $this->_tpl_vars['invoice']['door_invoice_code']; ?>
';" > <?php echo $this->_tpl_vars['lang']['cancel']; ?>
 </div> 
						<input type="submit" class="btn btn-success" style="height: 30px;" onclick="return GoToSignValidate()" value="<?php echo $this->_tpl_vars['lang']['clickToPay']; ?>
">
					</div>
				</div>
				</form>
<script type="text/javascript">// src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.11/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
js/ui.js"></script>
<link href="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
js/ui.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
js/time_picker.js"></script> 
 
<script type="text/javascript" src="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
js/invoice.js">  </script>
			
				<!-- ============================== END =================================== -->				
					<div class="productbtm"></div>
				</div> <!-- .product -->
			</div> <!-- .productmain -->
		<div class="clearxy"></div>
	</div> <!-- .connetentin -->
</div> <!-- .content -->