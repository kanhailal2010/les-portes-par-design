<?php /* Smarty version 2.6.3, created on 2013-05-09 14:23:43
         compiled from payment_history.html */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'stripslashes', 'payment_history.html', 65, false),array('modifier', 'date_format', 'payment_history.html', 73, false),)), $this); ?>
<div class="Content">
	<div class="connetentin">
		  <div class="common_titlesm">
			<div class="common_titles">
				<div class="title_lft"></div>
				<div class="title_mid"><?php echo $this->_tpl_vars['PageTitle']; ?>
</div>
			</div>
		</div>
		  <div class="clearxy height10"></div>
		  <div class="productmain">
			  <div class="product">
				<div class="configurator_main">
				
					<div class="dash_menuss">
						 <!-- ****************** MENUCONTENT START ************************** -->
						 <!-- ******************** MENU DASHBOARD START *********************** -->
		  <div id="menu" >
		<div id="icons">
	<div id="iconDescription" class="statut1" style=" width:200px; padding:16px 0 0 0; 
	font-weight:bold; font-family:Arial, Helvetica, sans-serif;
	text-align:center; font-size:15px; color:#FFFFFF; cursor:pointer;">Dash Board <img src="images/iconDescription.png" alt="" style="margin-top:-4px; float:right; margin-right:10px;"/></div>
	</div>
		  </div>
	
	  <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['IncludeTpl1'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>	
		</div>
				<div class="main_dash">		
				<div class="dash_con_r">
			<?php if ($this->_tpl_vars['ErrMessage'] || $this->_tpl_vars['SuccessMessages']): ?>
			 <div id="ErrMsgDiv" style="width:350px; margin:0 auto;">
					<?php if ($this->_tpl_vars['ErrMessage'] != ''): ?>
						<div class="notification hideit failure" style="width:450px;"><p><?php echo $this->_tpl_vars['ErrMessage']; ?>
</p></div>
					<?php elseif ($this->_tpl_vars['SuccessMessages'] != ''): ?>
						<div class="notification hideit success" style="width:450px;"><p><?php echo $this->_tpl_vars['SuccessMessages']; ?>
</p></div>    
				<?php endif; ?></div>
			 <?php endif; ?>
					<div class="main_in1_CC_table"> 
 				</div>
 				<div class="clearxy height10"></div>
<form name="frm_Video" method="post" action="">
	<input type="hidden" name="Action_Type" />
	<input type="hidden" name="ConSId"/>	
					 
					<div class="evenths_order" >
				<div class="clearxy"></div>
				<table width="680" id="box-table-a_order" summary="Employee Pay Sheet">
					<thead>
						<tr>
							<th scope="col">SL No</th>
							<th scope="col">Quote ID</th>
							<th scope="col">Profile</th>
							<th scope="col">Invoice Number</th>
							<th scope="col">Order Number</th>
							<th scope="col">Initial Amount Paid</th>
							<th scope="col">Balance Amount </th>
							<th scope="col">Paid Date</th>
							<th scope="col">Status</th>
					  </tr>
					</thead>
					<tbody> 
						<?php $this->assign('i', 1); ?>
						<?php if (count($_from = (array)$this->_tpl_vars['PayList'])):
    foreach ($_from as $this->_tpl_vars['PDet']):
?>
						<tr>
							<td><?php echo $this->_tpl_vars['i']++; ?>
</td>
							<td><?php echo ((is_array($_tmp=$this->_tpl_vars['PDet']['QuoteId'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</td>
							<td><?php if ($this->_tpl_vars['PDet']['ProfileId'] == 1): ?>Door Profile with Glass
								   <?php elseif ($this->_tpl_vars['PDet']['ProfileId'] == 2): ?>Door Profile without Glass
								   <?php elseif ($this->_tpl_vars['PDet']['ProfileId'] == 3): ?>Frame Only<?php else: ?>Glass Only<?php endif; ?></td>
							<td><?php if ($this->_tpl_vars['PDet']['InvoiceNum'] != ''):  echo ((is_array($_tmp=$this->_tpl_vars['PDet']['InvoiceNum'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp));  else: ?>----------------------<?php endif; ?></td>
							<td><?php echo ((is_array($_tmp=$this->_tpl_vars['PDet']['OrderRefNo'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</td>
							 <td>$ <?php echo ((is_array($_tmp=$this->_tpl_vars['PDet']['InAmt'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</td>
							 <td>$  <?php echo $this->_tpl_vars['PDet']['BalAmt']; ?>
</td>
							 <td><?php echo ((is_array($_tmp=$this->_tpl_vars['PDet']['PayDate'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%b %d, %Y %I:%M %p") : smarty_modifier_date_format($_tmp, "%b %d, %Y %I:%M %p")); ?>
</td>
							 <td><?php echo $this->_tpl_vars['PDet']['Paystatus']; ?>
</td> 
						</tr> 
						<?php endforeach; unset($_from); else: ?>     
						<tr>
							<div class="notification failure" style=" width:300px; padding-left:40px;">
							<p>No Orders found.</p></div>
						</tr>   	
 						<?php endif; ?> 
					</tbody>
				</table>
				   </div>
</form>	
<div class="clearxy"></div>
				</div>
			</div>
		</div>
		<div class="productbtm"></div>
</div>
		  </div>
		  <div class="clearxy"></div>
	</div>
</div>
<?php echo '
<script type="text/javascript">
var FormName=\'frm_Video\';
var FormObj=document.forms[FormName];
var MgmtName=\'Order(s)\';
var Seltype=\'PaymentMgmt\'; 
</script>
'; ?>