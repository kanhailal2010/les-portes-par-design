<?php /* Smarty version 2.6.3, created on 2013-08-11 15:06:57
         compiled from preview_cart.html */ ?>

<link rel="stylesheet" href="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
css/door_configurator.css">

<div class="Content">
	<div class="connetentin">
		<div class="common_titlesm">
		<div class="common_titles">
			<div class="title_lft"></div>
			<div class="title_mid">Invoice</div>
		</div>
		</div>
			<div class="clearxy height10"></div>
			<div class="productmain">
				<div class="product">
				
				<table class="table table-bordered" id="shopping-cart" style="width:100%;<?php echo $this->_tpl_vars['style']; ?>
">
					<thead>
						<tr>
							<th colspan="2"> &nbsp;  </th>
							<th colspan="4"> Invoice Generated <br/> <div class="btn btn-warning btn-block" > Invoice Id : <?php echo $this->_tpl_vars['cartCode']; ?>
 </div> </th>
							<th colspan="2"> &nbsp;  </th>
						</tr>
						<tr>
							<th colspan="2"> Shopping Cart </th>
							<td colspan="4"> &nbsp;  </td>
							<th colspan="2" > &nbsp;  </th>
						</tr>
						<tr class="underline">
							<th width="12"> &nbsp; </th>
							<th> Profile </th>
							<th> &nbsp; </th>
							<th> Dimensions </th>
							<th> Quantity </th>
							<th> Price </th>
							<th> Total </th>
						</tr>
					</thead>
					<tbody>
							<?php if (count($_from = (array)$this->_tpl_vars['cartSession'])):
    foreach ($_from as $this->_tpl_vars['c']):
?>
								<tr>
									<th width="12"> &nbsp; </th>
									<th> <?php echo $this->_tpl_vars['c']['profile_type']; ?>
 </th>
									<td style="text-align:left;"> 
										Finish: <?php echo $this->_tpl_vars['c']['profile_finish']; ?>
 <br> Insert: <?php echo $this->_tpl_vars['c']['insert_type']; ?>
 <?php if ($this->_tpl_vars['c']['sub_insert'] != ''): ?>/<?php echo $this->_tpl_vars['c']['sub_insert']; ?>
 <?php endif; ?>
										<hr/>
										Orientation: <?php echo $this->_tpl_vars['c']['orientation']; ?>

										<hr/>
										Hinge Holes : <?php echo $this->_tpl_vars['c']['hinge_holes']; ?>
 | Drilling Holes : <?php echo $this->_tpl_vars['c']['handle_holes']; ?>

									</td>
									<td> <?php echo $this->_tpl_vars['c']['width']; ?>
 x <?php echo $this->_tpl_vars['c']['height']; ?>
 </td>
									<td> <?php echo $this->_tpl_vars['c']['quantity']; ?>
 </td>
									<td> $<?php echo $this->_tpl_vars['c']['price']; ?>
  </td>
									<th> $<?php echo $this->_tpl_vars['c']['sub_total']; ?>
</th>
								</tr>
							<?php endforeach; unset($_from); endif; ?>
					</tbody>
					<tfoot>
						<tr> 
							<td colspan="5"> &nbsp;  </td>
							<th> Total </th>
							<th> $<span id="cartTotal"><?php echo $this->_tpl_vars['cartTotal']; ?>
 </span> </th>
						</tr>
						<tr> 
							<td colspan="5"> &nbsp;  </td>
							<td colspan="2"> <div class="btn btn-success btn-block" onclick="window.location='process.php?invoice=<?php echo $this->_tpl_vars['cartCode']; ?>
';" > Place Order and Checkout </div> </td>
						</tr>
					</tfoot>
					</table>
			
				<!-- ============================== END =================================== -->				
					<div class="productbtm"></div>
				</div> <!-- .product -->
			</div> <!-- .productmain -->
		<div class="clearxy"></div>
	</div> <!-- .connetentin -->
</div> <!-- .content -->

<script type="text/javascript"> BASEPATH="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
";</script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
js/koverlay/koverlay.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
js/custom.js"> </script>