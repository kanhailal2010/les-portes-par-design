<?php /* Smarty version 2.6.3, created on 2013-05-29 09:13:19
         compiled from quotes.html */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'stripslashes', 'quotes.html', 122, false),array('modifier', 'date_format', 'quotes.html', 123, false),array('modifier', 'md5', 'quotes.html', 129, false),array('function', 'SmartyPlugIn', 'quotes.html', 127, false),)), $this); ?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
css/popup.css">
<?php echo ' 
<script type="text/javascript">
 jQuery(document).ready(function(){
	closetimer = 0;
 	if($("#nav")) {
 		$("#nav li div").each(function(i) {
		eheight = $(this).height();
		$(this).children().css("top",-eheight+"px");
		});

		$("#nav img").mouseover(function() {
		clearTimeout(closetimer);
 				eheight = $("#nav img.clicked").next().children().height()+20;
				$("#nav img.clicked").next().children().animate({"top": -eheight+"px"}, {queue:false,duration:(300)}, "swing");
				$("#nav img.clicked").next().hide(50);
				$("#nav img").removeClass();
				$(this).addClass("clicked");
				$(this).next().show(10);
				eheight = $("#nav img.clicked").next().children().height();
				$(this).next().children().animate({"top":"0px"}, {queue:false,duration:(300)}, "swing");
		});
		$("#nav").mouseover(function() {
		clearTimeout(closetimer);
		});
		$("#nav").mouseout(function() {
			closetimer = window.setTimeout(function(){
			eheight = $("#nav img.clicked").next().children().height()+20;
			$("#nav img.clicked").next().children().animate({"top":-eheight+"px"}, {queue:false,duration:(300)}, "swing");
			$("#nav img.clicked").next().hide(10);
			$("#nav img").removeClass("clicked");
			}, 100);
		}); 
	}
	});
  	$(document).ready(function(){
		$(\'a.poplight[href^=#]\').click(function() {
			var popID = $(this).attr(\'rel\'); //Get Popup Name
			var popURL = $(this).attr(\'href\'); //Get Popup href to define size
			//Pull Query & Variables from href URL
			var query= popURL.split(\'?\');
			var dim= query[1].split(\'&\');
			var popWidth = dim[0].split(\'=\')[1]; //Gets the first query string value
			//Fade in the Popup and add close button
			$(\'#\' + popID).fadeIn().css({ \'width\': Number( popWidth ) }).prepend(\'<a href="#" class="close"><img src="\'+\'images/close_pop.png" class="btn_close" title="Close Window" border="0" alt="Close" /></a>\');
			var popMargTop = ($(\'#\' + popID).height() + 80) / 2;
			var popMargLeft = ($(\'#\' + popID).width() + 80) / 2;
			//Apply Margin to Popup
			$(\'#\' + popID).css({ 
				\'margin-top\' : -popMargTop,\'margin-left\' : -popMargLeft
			});
			//Fade in Background
			$(\'body\').append(\'<div id="fade"></div>\'); //Add the fade layer to bottom of the body tag.
			$(\'#fade\').css({\'filter\' : \'alpha(opacity=80)\'}).fadeIn(); //Fade in the fade layer 
			return false;
		});
		//Close Popups and Fade Layer
		$(\'a.close, .DisWarClose, #fade\').live(\'click\', function() { //When clicking on the close or fade layer...
			$(\'.popup_block\').fadeOut(function() {
					$(\'#fade, a.close\').remove();  
			}); //fade them both out
			return false;
		});	
	});
</script>
'; ?>

<div class="Content">
	<div class="connetentin">
		  <div class="common_titlesm">
			<div class="common_titles">
				<div class="title_lft"></div>
				<div class="title_mid">Quotes</div>
			</div>
		</div>
		  <div class="clearxy height10"></div>
		  <div class="productmain">
			  <div class="product">
				<div class="configurator_main">
				
					<div class="dash_menuss"> 
		  <div id="menu" >
		<div id="icons">
   <div id="iconDescription" class="statut1" style=" width:200px; padding:16px 0 0 0; 
font-weight:bold; font-family:Arial, Helvetica, sans-serif;
text-align:center; font-size:15px; color:#FFFFFF; cursor:pointer; z-index:-999999;">Dash Board <img src="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
images/iconDescription.png" alt="" style="margin-top:-4px; float:right; margin-right:10px;"/></div>
</div>
		  </div>

	  <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['IncludeTpl1'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>	
						</div>
				<div class="main_dash">		
				<div class="dash_con_r">
<?php if ($this->_tpl_vars['ErrMessage'] || $this->_tpl_vars['SuccessMessages']): ?>
 <div id="ErrMsgDiv" style="width:350px; margin:0 auto;">
		<?php if ($this->_tpl_vars['ErrMessage'] != ''): ?>
			<div class="notification hideit failure" style="width:450px;"><p><?php echo $this->_tpl_vars['ErrMessage']; ?>
</p></div>
		<?php elseif ($this->_tpl_vars['SuccessMessages'] != ''): ?>
			<div class="notification hideit success" style="width:450px;"><p><?php echo $this->_tpl_vars['SuccessMessages']; ?>
</p></div>    
	<?php endif; ?></div>
 <?php endif; ?>
					<div class="main_in1_CC_table"> 
				</div>
				<div class="clearxy height10"></div>
					<div class="evenths" >
				<div class="clearxy"></div>
				
				<table width="570" id="box-table-a_acc" summary="Employee Pay Sheet">
					<thead>
						<tr>
						  <th scope="col">SL No</th>
						  <th scope="col">Quote ID</th>
						  <th scope="col">Date</th>
						  <th scope="col">Profile</th>
						  <th scope="col">Action</th>
					  </tr>
					</thead>
					<tbody> 
					<?php $this->assign('i', 1); ?>
			<?php if (count($_from = (array)$this->_tpl_vars['QList'])):
    foreach ($_from as $this->_tpl_vars['PDet']):
?>
				<tr>
					 <td><?php echo $this->_tpl_vars['i']++; ?>
</td>
					 <td><?php echo ((is_array($_tmp=$this->_tpl_vars['PDet']['QuoteId'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</td>
					 <td><?php echo ((is_array($_tmp=$this->_tpl_vars['PDet']['QuotedOn'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%b %d, %Y %I:%M %p") : smarty_modifier_date_format($_tmp, "%b %d, %Y %I:%M %p")); ?>
</td>
					 <td><?php if ($this->_tpl_vars['PDet']['ProfileId'] == 1): ?>Door Profile with Glass
					   <?php elseif ($this->_tpl_vars['PDet']['ProfileId'] == 2): ?>Door Profile without Glass
					   <?php elseif ($this->_tpl_vars['PDet']['ProfileId'] == 3): ?>Frame Only<?php else: ?>Glass Only<?php endif; ?></td>
					 <td><?php echo SmartyPlugIn(array('GetType' => 'Order','ProfId' => $this->_tpl_vars['PDet']['ProfileId'],'InpVal' => $this->_tpl_vars['PDet']['QGId']), $this);?>
 

<a href="<?php echo $this->_tpl_vars['SiteMainPath']; ?>
quotes.html?type=delete&con_id=<?php echo ((is_array($_tmp=$this->_tpl_vars['PDet']['QGId'])) ? $this->_run_mod_handler('md5', true, $_tmp) : md5($_tmp)); ?>
" title="Delete"><img src="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
images/delete.png" border="0"></a>&nbsp;&nbsp;
<a href="#?w=890" id="InviteBut" title="View" class="poplight" rel="popup<?php echo $this->_tpl_vars['PDet']['QGId']; ?>
" ><img src="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
images/view_new.png" border="0"></a>&nbsp;&nbsp;
<a href="<?php echo $this->_tpl_vars['SiteMainPath']; ?>
print.html?type=print&con_id=<?php echo ((is_array($_tmp=$this->_tpl_vars['PDet']['QGId'])) ? $this->_run_mod_handler('md5', true, $_tmp) : md5($_tmp)); ?>
" target="_blank" title="Print"><img src="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
images/printer.png" border="0"></a>&nbsp;&nbsp;
<a href="<?php echo $this->_tpl_vars['SiteMainPath']; ?>
print.html?type=download&con_id=<?php echo ((is_array($_tmp=$this->_tpl_vars['PDet']['QGId'])) ? $this->_run_mod_handler('md5', true, $_tmp) : md5($_tmp)); ?>
" title="Download"><img src="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
images/download.png" border="0"></a>
					</td>
				</tr>
				<div id="popup<?php echo $this->_tpl_vars['PDet']['QGId']; ?>
" class="popup_block"> 
<div class="configurator_main">
			<div class="productlfts"> 
			<div class="prod_detatl">  
			<div class="frm_p1view">Quantity :</div>
			<div class="frm_p2view"><?php echo $this->_tpl_vars['PDet']['Qty']; ?>
</div>
			<div class="clearxy"></div> 
			<?php if ($this->_tpl_vars['PDet']['ProfileId'] == 1 || $this->_tpl_vars['PDet']['ProfileId'] == 4): ?>
				<div class="frm_p1view">Glass Type :</div>
				<div class="frm_p2view"><?php echo $this->_tpl_vars['PDet']['GlassName']; ?>
</div>
				<div class="clearxy"></div>
				<div class="frm_p1view">Glass Code :</div>
				<div class="frm_p2view"><?php echo $this->_tpl_vars['PDet']['GlassCode']; ?>
</div>
				<div class="clearxy"></div>
				<div class="frm_p1view">Glass Group :</div>
				<div class="frm_p2view"><?php echo $this->_tpl_vars['PDet']['GlassGroup']; ?>
</div>
				<div class="clearxy"></div>
				<div class="frm_p1view">Glass Model :</div>
				<div class="frm_p2view"><?php echo $this->_tpl_vars['PDet']['GlassModel']; ?>
</div>
				<div class="clearxy"></div>
				<div class="frm_p1view">Thickness :</div>
				<div class="frm_p2view"><?php echo $this->_tpl_vars['PDet']['Thickness']; ?>
</div>
				<div class="clearxy"></div>
			<?php endif; ?>
			<?php if ($this->_tpl_vars['PDet']['ProfileId'] == 1 || $this->_tpl_vars['PDet']['ProfileId'] == 2): ?>
				<div class="frm_p1view">Hinge Holes :</div>
				<div class="frm_p2view"><?php echo $this->_tpl_vars['PDet']['HingeHoleSta']; ?>
</div>
				<div class="clearxy"></div>
				<?php if ($this->_tpl_vars['PDet']['HingeHoleSta'] == 'Yes'): ?>
					<div class="frm_p1view">Hinge Holes Location :</div>
					<div class="frm_p2view"><?php echo $this->_tpl_vars['PDet']['HingeLocation']; ?>
</div>
					<div class="clearxy"></div>
					<div class="frm_p1view">Need Extra Hinge Holes :</div>
					<div class="frm_p2view"><?php echo $this->_tpl_vars['PDet']['ExtraHingeHoleSta']; ?>
</div>
					<div class="clearxy"></div>
					<div class="frm_p1view">Total Extra Hinge Holes :</div>
					<div class="frm_p2view"><?php echo $this->_tpl_vars['PDet']['TotExtraHingeHole']; ?>
</div>
					<div class="clearxy"></div>
					<div class="frm_p1view">Holes (35mm):</div>
					<div class="frm_p2view"><?php echo $this->_tpl_vars['PDet']['HoleStatus']; ?>
</div>
					<div class="clearxy"></div>
				<?php endif; ?>
				<div class="frm_p1view">Model :</div>
				<div class="frm_p2view"><?php echo $this->_tpl_vars['PDet']['Model']; ?>
</div>
				<div class="clearxy"></div>
			<?php endif; ?>
				<div class="frm_p1view">Length (A) :</div>
				<div class="frm_p2view"><?php echo $this->_tpl_vars['PDet']['HoleA']; ?>
 in</div>
				<div class="clearxy"></div>
				<div class="frm_p1view">Height (B) :</div>
				<div class="frm_p2view"><?php echo $this->_tpl_vars['PDet']['LengthB']; ?>
 in</div>
				<div class="clearxy"></div> 
			<?php if ($this->_tpl_vars['PDet']['HingeHoleSta'] == 'Yes'): ?>
				<div class="frm_p1view">C :</div>
				<div class="frm_p2view"><?php echo $this->_tpl_vars['PDet']['WidthC']; ?>
 in</div>
				<div class="clearxy"></div>
				<div class="frm_p1view">D:</div>
				<div class="frm_p2view"><?php echo $this->_tpl_vars['PDet']['WidthD']; ?>
 in</div>
				<div class="clearxy"></div>
			<?php endif; ?>
			<?php if ($this->_tpl_vars['PDet']['ProfileId'] == 3): ?>
				<div class="frm_p1view">Length (Running.ft) :</div>
				<div class="frm_p2view"><?php echo $this->_tpl_vars['PDet']['Length']; ?>
 Running.ft</div>
				<div class="clearxy"></div>
			<?php endif; ?>
			<?php if ($this->_tpl_vars['PDet']['ProfileId'] == 4): ?> 
				<div class="frm_p1view">Drilling Holes :</div>
				<div class="frm_p2view"><?php echo $this->_tpl_vars['PDet']['DrillHoleStatus']; ?>
</div>
				<div class="clearxy"></div>
				<?php if ($this->_tpl_vars['PDet']['DrillHoleStatus'] == 'Yes'): ?>
					<div class="frm_p1view">Total Drilling holes :</div>
					<div class="frm_p2view"><?php echo $this->_tpl_vars['PDet']['TotDrillHoles']; ?>
</div>
					<div class="clearxy"></div>
				<?php endif; ?>
			<?php endif; ?>
			<div class="frm_p1view">Delivery Type :</div>
			<div class="frm_p2view"><?php echo $this->_tpl_vars['PDet']['OrderType']; ?>
 Order</div>
			<div class="clearxy"></div>
			<div class="frm_p1view">Comments</div>
			<div class="frm_p2view"><?php echo $this->_tpl_vars['PDet']['Comments']; ?>
</div>
			<div class="clearxy"></div>
			</div>
			</div>
			<div class="productrgts">
			<?php if ($this->_tpl_vars['PDet']['ProfileId'] != 4): ?>
				<div class="forca">
					<label for="Profile Type"><?php echo $this->_tpl_vars['PDet']['ProfileNumber']; ?>
</label>
					<div class="types">
							<img border='0' src="<?php echo $this->_tpl_vars['ShapeImageDisp'];  echo $this->_tpl_vars['PDet']['ShapeImage']; ?>
" width="185" height="79"/>
						</div>
				</div>
			<?php endif; ?>
			<?php if ($this->_tpl_vars['PDet']['ProfileId'] != 4): ?>
				<div class="forca">
						<label for="Profile Type"><?php echo $this->_tpl_vars['PDet']['FinishType']; ?>
</label>
							<div class="types">
								<img border='0' src="<?php echo $this->_tpl_vars['FinishImageDisp'];  echo $this->_tpl_vars['PDet']['FinishImage']; ?>
" width="157" height="209"/>
							</div>
						</div>
			<?php endif; ?>
						<?php if ($this->_tpl_vars['PDet']['ProfileId'] == 1 || $this->_tpl_vars['PDet']['ProfileId'] == 4): ?>
							<?php if ($this->_tpl_vars['PDet']['GlassDirection'] == 'Vertical'): ?>
								<div class="forca">
								<label for="Profile Type">Vertical</label>
									<div class="types">
										<img src="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
images/ver_glass.png" alt="Finish Type">
									</div>
								</div>
							<?php else: ?>
								<div class="forca">
								<label for="Profile Type">Horizontal</label>
									<div class="types">
										<img src="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
images/hori_glass.png" alt="Finish Type">
									</div>
								</div>
							<?php endif; ?>
						<?php endif; ?>
						<div class="clearxy"></div>
						<div class="total_amtbg">	
							<div class="amts">Total Amount :<span>$ <?php echo $this->_tpl_vars['PDet']['QuotePrice']; ?>
</span></div>
						</div>	
						<div class="clearxy"></div>
						<div class="total_amtbg">	 
							<div class="amtscode">Code :<span><?php echo $this->_tpl_vars['PDet']['QuoteId']; ?>
</span></div>
						</div>	
					</div>
		<div class="clearxy height10"></div>	
		<div class="clearxy height10"></div>	
			</div>
</div>
			<?php endforeach; unset($_from); else: ?>     
						<tr>
			<div class="notification failure" style=" width:300px; padding-left:40px;">
				<p>No Quotes found.</p></div>
						</tr>   	
			<?php endif; ?> 
			
						
					</tbody>
				</table>
				   </div>
<div class="clearxy"></div>
				</div>
			</div>
		</div>
		<div class="productbtm"></div>
</div>
  </div>
  <div class="clearxy"></div>
</div>
</div>