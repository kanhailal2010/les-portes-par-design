<?php /* Smarty version 2.6.3, created on 2013-08-19 12:08:12
         compiled from products.html */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'products.html', 4, false),array('modifier', 'stripslashes', 'products.html', 24, false),array('function', 'SmartyPlugIn', 'products.html', 26, false),)), $this); ?>
<link href="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
css/pro_style.css" type="text/css" rel="stylesheet" />
<script src="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
js/jquery.bxSlider_arabic.js" type="text/javascript"></script> 
<script type="text/javascript">
	var ProList = "<?php echo count($this->_tpl_vars['ProList']); ?>
"; 
</script>
<script src="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
js/home_page.js" type="text/javascript"></script> 
<div class="Content">
	<div class="connetentin">
		<div class="common_titlesm">
			<div class="common_titles">
				<div class="title_lft"></div>
				<div class="title_mid">Products</div>
			</div>
		</div>
		<div class="clearxy height10"></div>
		<div class="productmain">
			<div class="product">
				<div class="Main980Con" style="padding-top:5px;">
					<div class="demo-wrap">
						<ul id="slider_bot" class="multiple">
						<?php if (count($_from = (array)$this->_tpl_vars['ProList'])):
    foreach ($_from as $this->_tpl_vars['PDet']):
?>
						<li>
							<div class="NProList">
								<h2><a href="<?php echo ((is_array($_tmp=$this->_tpl_vars['PDet']['ProductURL'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['PDet']['ItemName'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</a></h2>
								<div class="cate"><?php echo ((is_array($_tmp=$this->_tpl_vars['PDet']['CatName'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
								<p><?php echo SmartyPlugIn(array('GetType' => 'Proimage','InpVal' => $this->_tpl_vars['PDet']['SellId']), $this);?>
</p>
								<div class="mview"><a href="<?php echo ((is_array($_tmp=$this->_tpl_vars['PDet']['ProductURL'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
"><?php echo $this->_tpl_vars['data']['more']; ?>
</a></div>
							</div>
						</li> 
						<?php endforeach; unset($_from); else: ?>        	
						<div class="clearxy height10" style="height:130px;"></div>  
						<div class="notification failure" style=" width:300px; padding-left:40px;">
							<p>No products found in selected category.</p></div>
						<?php endif; ?> 
 						</ul>
					</div>
				</div>
				<div class="productbtm"></div>
			</div>
		</div>
		<div class="clearxy height10"></div>
	</div>
	<div class="clearxy"></div>
</div>