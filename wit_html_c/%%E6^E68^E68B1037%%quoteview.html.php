<?php /* Smarty version 2.6.3, created on 2013-05-29 09:10:55
         compiled from quoteview.html */ ?>
 
<?php echo '
<script type="text/javascript">  
function GoEditVaildate(){	
	ErrCss=\'txt_con1\'; BErrCss=\'txt_con1\'; var FlagIns=true;
	FlagIns=FormValidateFunction(\'Text\',\'#Qty\',\'Please enter Quantity\'); 
	if(FlagIns)
	{	
		FlagIns=FormValidateFunction(\'Text\',\'#HoleA\',\'Please enter Hole size\');
	}
	if(FlagIns)
	{	
		FlagIns=FormValidateFunction(\'Text\',\'#LengthB\',\'Please enter Length\');
	}
	if(FlagIns)
	{	
		FlagIns=FormValidateFunction(\'Text\',\'#WidthC\',\'Please enter Width\');
	} 
	if(FlagIns){	
 		document.forms[FormName1].submit();
	}
}  
function GoToJSONAjaxFunction(){
	jQuery(\'#www\').hide(); jQuery(\'#errormsg\').hide();
	jQuery(\'#cont\').show(); 
}   
function ShowErrortoSelect(){   
 	jQuery(\'#errormsg\').show(); 
}  
function ShowHingeQty(type){
	if(type==\'Yes\'){   
 		jQuery(\'#HingeQty\').show(); 
	}
	else{   
 		jQuery(\'#HingeQty\').hide(); 
	}
} 
</script>
'; ?>

<div class="Content">
	<div class="connetentin">
		  <div class="common_titlesm">
			<div class="common_titles">
				<div class="title_lft"></div>
				<div class="title_mid"><?php echo $this->_tpl_vars['PageTitle']; ?>
</div>
			</div>
		</div>
		  <div class="clearxy height10"></div>
		  <div class="productmain">
			 <div class="product">
				<form action="" name="post_form1" method="post">
				 <div id="sub6">
						<div class="configurator_main">
						<div class="instut"><p>Here are your Quote Details:....</p></div>
						<div class="clearxy height10"></div>
						<div class="productlfts"> 
							<div class="prod_detatl">  
								<div class="frm_p1view">Quantity :</div>
								<div class="frm_p2view"><?php echo $this->_tpl_vars['Arr']['Qty']; ?>
</div>
								<div class="clearxy"></div> 
								<?php if ($_SESSION['ProfileId'] == 1 || $_SESSION['ProfileId'] == 4): ?>
									<div class="frm_p1view">Glass Type :</div>
									<div class="frm_p2view"><?php echo $this->_tpl_vars['Arr']['GlassName']; ?>
</div>
									<div class="clearxy"></div>
									<div class="frm_p1view">Glass Code :</div>
									<div class="frm_p2view"><?php echo $this->_tpl_vars['Arr']['GlassCode']; ?>
</div>
									<div class="clearxy"></div>
									<div class="frm_p1view">Glass Group :</div>
									<div class="frm_p2view"><?php echo $this->_tpl_vars['Arr']['GlassGroup']; ?>
</div>
									<div class="clearxy"></div>
									<div class="frm_p1view">Glass Model :</div>
									<div class="frm_p2view"><?php echo $this->_tpl_vars['Arr']['GlassModel']; ?>
</div>
									<div class="clearxy"></div>
									<div class="frm_p1view">Thickness :</div>
									<div class="frm_p2view"><?php echo $this->_tpl_vars['Arr']['Thickness']; ?>
</div>
									<div class="clearxy"></div>
								<?php endif; ?>
								<?php if ($_SESSION['ProfileId'] == 1 || $_SESSION['ProfileId'] == 2): ?>
									<div class="frm_p1view">Hinge Holes :</div>
									<div class="frm_p2view"><?php echo $this->_tpl_vars['Arr']['HingeHoleSta']; ?>
</div>
									<div class="clearxy"></div>
									<?php if ($this->_tpl_vars['Arr']['HingeHoleSta'] == 'Yes'): ?>
										<div class="frm_p1view">Hinge Holes Location :</div>
										<div class="frm_p2view"><?php echo $this->_tpl_vars['Arr']['HingeLocation']; ?>
</div>
										<div class="clearxy"></div>
										<div class="frm_p1view">Need Extra Hinge Holes :</div>
										<div class="frm_p2view"><?php echo $this->_tpl_vars['Arr']['ExtraHingeHoleSta']; ?>
</div>
										<div class="clearxy"></div>
										<div class="frm_p1view">Total Extra Hinge Holes :</div>
										<div class="frm_p2view"><?php echo $this->_tpl_vars['Arr']['TotExtraHingeHole']; ?>
</div>
										<div class="clearxy"></div>
										<div class="frm_p1view">Holes (35mm) :</div>
										<div class="frm_p2view"><?php echo $this->_tpl_vars['Arr']['HoleStatus']; ?>
</div>
										<div class="clearxy"></div>
									<?php endif; ?>
									<div class="frm_p1view">Model :</div>
									<div class="frm_p2view"><?php echo $this->_tpl_vars['Arr']['Model']; ?>
</div>
									<div class="clearxy"></div>
								<?php endif; ?>
								<div class="frm_p1view">Length (A) :</div>
								<div class="frm_p2view"><?php echo $this->_tpl_vars['Arr']['HoleA']; ?>
 in</div>
								<div class="clearxy"></div>
								<div class="frm_p1view">Height (B) :</div>
								<div class="frm_p2view"><?php echo $this->_tpl_vars['Arr']['LengthB']; ?>
 in</div>
								<div class="clearxy"></div>
								<?php if ($this->_tpl_vars['Arr']['HingeHoleSta'] == 'Yes'): ?>
									<div class="frm_p1view">C :</div>
									<div class="frm_p2view"><?php echo $this->_tpl_vars['Arr']['WidthC']; ?>
 in</div>
									<div class="clearxy"></div>
									<div class="frm_p1view">D:</div>
									<div class="frm_p2view"><?php echo $this->_tpl_vars['Arr']['WidthD']; ?>
 in</div>
									<div class="clearxy"></div>
								<?php endif; ?>
								<?php if ($_SESSION['ProfileId'] == 3): ?>
									<div class="frm_p1view">Length (Running.ft) :</div>
									<div class="frm_p2view"><?php echo $this->_tpl_vars['Arr']['Length']; ?>
 Running.ft</div>
									<div class="clearxy"></div>
								<?php endif; ?>
								<?php if ($_SESSION['ProfileId'] == 4): ?>
									<div class="frm_p1view">Drilling Holes :</div>
									<div class="frm_p2view"><?php echo $this->_tpl_vars['Arr']['DrillHoleStatus']; ?>
</div>
									<div class="clearxy"></div>
									<?php if ($this->_tpl_vars['Arr']['DrillHoleStatus'] == 'Yes'): ?>
										<div class="frm_p1view">Total Drilling holes :</div>
										<div class="frm_p2view"><?php echo $this->_tpl_vars['Arr']['TotDrillHoles']; ?>
</div>
										<div class="clearxy"></div>
									<?php endif; ?>
								<?php endif; ?>
								<div class="frm_p1view">Delivery Type :</div>
								<div class="frm_p2view"><?php echo $this->_tpl_vars['Arr']['OrderType']; ?>
 Order</div>
								<div class="clearxy"></div>
								<div class="frm_p1view">Comments :</div>
								<div class="frm_p2view"><?php echo $this->_tpl_vars['Arr']['Comments']; ?>
</div>
								<div class="clearxy"></div>
							</div>
						</div>
						<div class="productrgts">
						<?php if ($_SESSION['ProfileId'] != 4): ?>
							<div class="forca">
								<label for="Profile Type"><?php echo $this->_tpl_vars['Arr']['ProfileNumber']; ?>
</label>
								<div class="types">
										<img border='0' src="<?php echo $this->_tpl_vars['ShapeImageDisp'];  echo $this->_tpl_vars['Arr']['ShapeImage']; ?>
" width="185" height="79"/>
									</div>
							</div>
						<?php endif; ?>
						<?php if ($_SESSION['ProfileId'] != 4): ?>
							<div class="forca">
								<label for="Profile Type"><?php echo $this->_tpl_vars['Arr']['FinishType']; ?>
</label>
								<div class="types">
									<img border='0' src="<?php echo $this->_tpl_vars['FinishImageDisp'];  echo $this->_tpl_vars['Arr']['FinishImage']; ?>
" />
								</div>
							</div>
						<?php endif; ?>
						<?php if ($_SESSION['ProfileId'] == 1 || $_SESSION['ProfileId'] == 4): ?>
							<?php if ($this->_tpl_vars['Arr']['GlassDirection'] == 'Vertical'): ?>
								<div class="forca">
								<label for="Profile Type">Vertical</label>
									<div class="types">
										<img src="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
images/ver_glass.png" alt="Finish Type">
									</div>
								</div>
							<?php else: ?>
								<div class="forca">
								<label for="Profile Type">Horizontal</label>
									<div class="types">
										<img src="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
images/hori_glass.png" alt="Finish Type">
									</div>
								</div>
							<?php endif; ?>
						<?php endif; ?>
						<div class="clearxy"></div>
						<div class="total_amtbg">	
							<div class="amts">Total Amount :<span>$ <?php echo $this->_tpl_vars['Arr']['QuotePrice']; ?>
</span></div>
						</div>	
						<div class="clearxy"></div>
						<div class="total_amtbg">	
							<div class="amtscode">Code :<span><?php echo $this->_tpl_vars['Arr']['QuoteId']; ?>
</span></div>
						</div>	
					</div>
					<div class="clearxy height10"></div>
					<div class="frm_p1view" style="padding-left:50px; width:800px;">* All price quotations require written confirmation from us to validate. This will be sent within moments of your quotation via e-mail or fax.</div> 
					<div class="clearxy"></div>	
					<div class="clearxy height10"></div>	
						<div class="button_next11" style="width:400px;">
	<input name="Print" type="submit" class="button_next1a" value="Print"/>
	<input name="Download" type="submit" class="button_next1ab" value="Download as PDF"/>
    <input name="" type="button" class="button_next1ab" value="Place Order and Check Out" onClick="GoBackRedirct('<?php echo $this->_tpl_vars['SiteMainPath']; ?>
checkout.html');"/>
							</div>
						</div>
						</div> 
				</form>
				<div class="productbtm"></div>
	 		</div>
		  </div>
		  <div class="clearxy"></div>
	</div>
</div>

<script type="text/javascript">
	var FormName1='post_form1';
	var FormObj=document.forms[FormName1];
</script>