<?php /* Smarty version 2.6.3, created on 2013-08-30 14:37:22
         compiled from edit_billinfo.html */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'stripslashes', 'edit_billinfo.html', 82, false),)), $this); ?>
 <script type="text/javascript" src="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
js/state.js"></script> 
<?php echo '
<script type="text/javascript">

var SiteMainPath="{SiteMainPath}";
var AllowExtension= new Array();
function GoToFormValidate(ValiType)
{	
	ErrCss=\'form\'; BErrCss=\'form\'; var FlagIns=true;
	 
	if(FlagIns){
		FlagIns=FormValidateFunction(\'Text\',\'#UBillUName\',\'Please enter First Name\');
	}  
	if(FlagIns){
		FlagIns=FormValidateFunction(\'Text\',\'#UBillPhone\',\'Please enter Phone Number\');
	}
	if(FlagIns){
		FlagIns=FormValidateFunction(\'Text\',\'#UBillEmail\',\'Please enter Email\');
	}
	
	if(FlagIns){
		FlagIns=FormValidateFunction(\'EMail\',\'#UBillEmail\',\'Please enter Valid Email\');
	} 
 	if(FlagIns){
		FlagIns=FormValidateFunction(\'Text\',\'#UBillAdds\',\'Please enter Address \');
	} 
	if(FlagIns){
		FlagIns=FormValidateFunction(\'Text\',\'#UBillCity\',\'Please enter City\');
	}
	if(FlagIns){ErrCss=\'Seltop\'; BErrCss=\'Seltop\';
		FlagIns=FormValidateFunction(\'Text\',\'#UBillCountry\',\'Please select  Country\');
	}
	if(FlagIns){ErrCss=\'Seltop\'; BErrCss=\'Seltop\';
		FlagIns=FormValidateFunction(\'Text\',\'#UBillState\',\'Please select State\');
	}
	if(FlagIns){	ErrCss=\'form\'; BErrCss=\'form\';
		FlagIns=FormValidateFunction(\'Text\',\'#UBillZip\',\'Please enter Zip/Postal Code\');
	}  
	if(FlagIns){
		document.forms[FormName].submit();
	}
	else{	return false;	}
} 
</script>
'; ?>
 
<div class="Content">
		<div class="connetentin">
			  <div class="common_titlesm">
				<div class="common_titles">
					<div class="title_lft"></div>
					<div class="title_mid"><?php echo $this->_tpl_vars['PageTitle']; ?>
</div>
				</div>
			</div>
			  <div class="clearxy height10"></div>
			  <div class="productmain">
				  <div class="product">
					<div class="configurator_main">
					<div class="dash_menuss"> 
			  <div id="menu" >
			<div id="icons">
	   <div id="iconDescription" class="statut1" style=" width:200px; padding:16px 0 0 0; font-weight:bold; font-family:Arial, Helvetica, sans-serif;text-align:center; font-size:15px; color:#FFFFFF; cursor:pointer;">Dash Board <img src="images/iconDescription.png" alt="" style="margin-top:-4px; float:right; margin-right:10px;"/></div>
	</div>
			  </div>

		   <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['IncludeTpl1'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>	
			</div>
				<div class="main_dash">		
				<div class="dash_con_r">
			<?php if ($this->_tpl_vars['ErrMessage'] || $this->_tpl_vars['SuccessMessages']): ?>
			 <div id="ErrMsgDiv" style="width:350px; margin:0 auto;">
					<?php if ($this->_tpl_vars['ErrMessage'] != ''): ?>
						<div class="notification hideit failure" style="width:350px;"><p><?php echo $this->_tpl_vars['ErrMessage']; ?>
</p></div>
					<?php elseif ($this->_tpl_vars['SuccessMessages'] != ''): ?>
						<div class="notification hideit success" style="width:350px;"><p><?php echo $this->_tpl_vars['SuccessMessages']; ?>
</p></div>    
				<?php endif; ?></div>
			<div class="clearxy height10"></div><?php endif; ?>
                   <form method="post" name="post_form" class="form"> 
					<ul>
					<div class="clearxy height10"></div> 
					<li>
					<p>Name <em> * </em></p>
 				<input type="text" name="UBillUName" id="UBillUName" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['Arr']['UBillUName'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
">
                <br /><span id="UBillUNameMsg" style="padding-left:170px;" class="error"></span>  
					</li><div class="clearxy"></div>
					<li>
					<p>Phone No <em> * </em></p>
 		<input type="text" name="UBillPhone" id="UBillPhone" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['Arr']['UBillPhone'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
">
                <br /><span id="UBillPhoneMsg" style="padding-left:170px;" class="error"></span> 
					</li>
					<div class="clearxy"></div>
					<li>
					<p>Email <em> * </em></p>
 			<input type="text" name="UBillEmail" id="UBillEmail" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['Arr']['UBillEmail'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
">
                <br /><span id="UBillEmailMsg" style="padding-left:170px;" class="error"></span>  
					</li><div class="clearxy"></div>  
					<li>
					<p>Address <em> * </em></p>
 				<input type="text" name="UBillAdds" id="UBillAdds" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['Arr']['UBillAdds'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
">
                <br /><span id="UBillAddsMsg" style="padding-left:170px;" class="error"></span>
					</li><div class="clearxy"></div>
					<li>
					<p>City <em> * </em></p>
		<input type="text" name="UBillCity" id="UBillCity" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['Arr']['UBillCity'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
">
                <br /><span id="UBillCityMsg" style="padding-left:170px;" class="error"></span>
					</li><div class="clearxy"></div>
					<li>
					<p>Country <em>*</em></p>
				<select name="UBillCountry" id="UBillCountry" class="Seltop" onChange="GoToPrintJSONAjaxDetails('State',this.value,'UBillState');">
					<option value="" >Select Country</option>
					<?php if (count($_from = (array)$this->_tpl_vars['StateList'])):
    foreach ($_from as $this->_tpl_vars['DataDet']):
?>  
					<option <?php if ($this->_tpl_vars['Arr']['UBillCountry'] == $this->_tpl_vars['DataDet']['country_id'] || ( $this->_tpl_vars['DataDet']['country_id'] == '219' && $this->_tpl_vars['Arr']['UBillCountry'] == '' )): ?> selected="selected" <?php endif; ?> value="<?php echo $this->_tpl_vars['DataDet']['country_id']; ?>
">  
						<?php echo ((is_array($_tmp=$this->_tpl_vars['DataDet']['country_name'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
  
					</option>
					<?php endforeach; unset($_from); endif; ?> 
				</select>
                <br /><span id="UBillCountryMsg" style="padding-left:170px;" class="error"></span>
					</li><div class="clearxy"></div>
					<li>
					<p>State <em>*</em></p> 
					<select name="UBillState" id="UBillState" class="Seltop" >
						<option value="">Select State</option>
					</select>
                	<br /><span id="UBillStateMsg" style="padding-left:170px;" class="error"></span>
					</li>
					<div class="clearxy"></div>
					<li>
					<p>Zip <em> * </em></p> 
 				<input name="UBillZip" id="UBillZip" type="text" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['Arr']['UBillZip'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
" />
					<br /><span id="UBillZipMsg" style="padding-left:170px;" class="error"></span>
					</li>
					<div class="clearxy height10"></div>
					<div class="main_in1_CC_dash1"> 
 					<div class="main_in1_CC_dash_in"> 
			<input name="ShipInfo" type="hidden" class="btn1box" value="Update" />
			<input name="ShipInfo" type="submit" class="btn1box" value="Update" onclick="return GoToFormValidate();"/>	</div>
					<div class="clearxy"></div>
					</div>
					
					</ul>
					</form>
				</div>
			</div>
		</div>
		<div class="productbtm"></div>
</div>
  </div>
  <div class="clearxy"></div>
</div>
</div>

 <script type="text/javascript">
var FormName='AddUpdate';
var FormObj=document.forms[FormName];
</script>
<script type="text/javascript">	
<?php if ($this->_tpl_vars['Arr']['UBillCountry'] != ''): ?>
	GoToPrintJSONAjaxDetails('State','<?php echo $this->_tpl_vars['Arr']['UBillCountry']; ?>
','UBillState','<?php echo $this->_tpl_vars['Arr']['UBillState']; ?>
');
<?php else: ?>
	GoToPrintJSONAjaxDetails('State','219','UBillState','<?php echo $this->_tpl_vars['Arr']['UBillState']; ?>
');
<?php endif; ?> 
</script>