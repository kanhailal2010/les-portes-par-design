<?php /* Smarty version 2.6.3, created on 2013-09-02 05:29:43
         compiled from site_main.html */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'md5', 'site_main.html', 97, false),array('modifier', 'stripslashes', 'site_main.html', 128, false),)), $this); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=9" />
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
<title><?php echo $this->_tpl_vars['SiteTitle']; ?>
</title>
<?php echo $this->_tpl_vars['MetaText']; ?>

<link rel="stylesheet" href="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
css/css_style.css" type="text/css"/>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
js/1.7.2_jquery.min.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
js/site_common.js"></script>
<script type="text/javascript">// src="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
js/jquery.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
js/jquery.scripts.js"></script>

<!--Banner Slider-->
<link rel="stylesheet" href="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
css/style1.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
css/dev_style.css" type="text/css"/>
<link rel="stylesheet" href="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
css/nivo-slider.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
css/default.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
css/style_table.css" type="text/css"/>
<link type="text/css" rel="stylesheet" media="screen" href="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
css/styles.css"/>

<script type="text/javascript">// src="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
js/google-analytics.js"></script>

<script type="text/javascript" src="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
js/jquery.nivo.slider.pack.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
js/SiteAdminValidateFunction.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
js/SiteAdminCommonFunction.js"></script>
<script type="text/javascript">
 	var SiteDateTime='<?php echo $this->_tpl_vars['SiteDateTime']; ?>
';
	var SiteMainPath='<?php echo $this->_tpl_vars['SiteMainPath']; ?>
';
	var SiteHttpPath='<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
';
	var HTTPSURLPath='<?php echo $this->_tpl_vars['HTTPSURLPath']; ?>
';
</script>
<?php echo ' 
 <script type="text/javascript">
    jQuery(window).load(function() {
        jQuery(\'#slider\').nivoSlider();
    });
    </script>
  <!--Banner Slider-->
'; ?>
 
<meta name="google-translate-customization" content="69ba10bd4f139bf7-c92a512521492c6a-ge43ac1aa047c2c6f-1a"></meta> 
</head>
<body>

<div class="wrapper">
	<div class="header_blue"></div>
		<div class="headermain">
			<div class="header">
				<div class="header_left">
					<div class="logo"><a href="<?php echo $this->_tpl_vars['SiteMainPath']; ?>
index.html"><img src="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
images/logo.png" alt=""/></a></div>
				</div>
				<div class="header_right">
					<?php if ($_SESSION['PJ525_UserId'] == ''): ?> 	
						<ul id="Mainnav12">
							<li><a href="<?php echo $this->_tpl_vars['SiteMainPath']; ?>
signin.html" title="Sign In">Sign In</a></li>
							<li><span><img src="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
images/signin_line.png" alt="separator" align="absmiddle"/></span></li>
							<li><a href="<?php echo $this->_tpl_vars['SiteMainPath']; ?>
signup.html" title="Sign Up">Sign Up</a></li>
						</ul>
					<?php else: ?> 	
						<ul id="Mainnav12">
							<li><a href="<?php echo $this->_tpl_vars['SiteMainPath']; ?>
myprofile.html" title="My Account">My Account</a></li>
							<li><span><img src="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
images/signin_line.png" alt="separator" align="absmiddle"/></span></li>
							<li><a href="<?php echo $this->_tpl_vars['SiteMainPath']; ?>
logout.html" title="Log Out">Logout</a></li>
						</ul> 
				 	<?php endif; ?>
					
					
					<!-- Begin Translate This button code. -->
<!-- If you already have jQuery embeded in your page, remove the following line: -->

<!-- Translate This button: -->
<div class="language_selection">
<?php echo $this->_tpl_vars['languageSelection']; ?>

</div>
<!-- End Translate This button code. -->
					
					<div class="clearxy"></div>
					<div class="DivSearch"> 
 				<form method="get" name="search" action="<?php echo $this->_tpl_vars['SiteMainPath']; ?>
products.html">
					<input type="hidden" name="search" value="<?php echo $_GET['search']; ?>
" /> 
					<input id="search_box" type="text" name="search" value="<?php echo $_GET['search']; ?>
"  class="searchbg_Val" > 
					<input type="submit" name="" value="" class="searchbg_btn">
 				</form> 
					</div>
					<div class="contact-us-div">
					<?php echo $this->_tpl_vars['common']['address']; ?>

					</div> <!-- .contact-us-div -->
 					<div class="menu_main">
							<ul class="btn btn-warning main_menu">
								<li><a href="<?php echo $this->_tpl_vars['SiteMainPath']; ?>
index.html" title="Home" <?php echo $this->_tpl_vars['Home']; ?>
 <?php echo $this->_tpl_vars['homeStyle']; ?>
 ><?php echo $this->_tpl_vars['common']['homemenu']; ?>
</a></li>
								<li><a href="<?php echo $this->_tpl_vars['StatPageList'][0]['ContentURL']; ?>
" <?php echo $this->_tpl_vars['about']; ?>
 title="About Us" <?php echo $this->_tpl_vars['homeStyle']; ?>
><?php echo $this->_tpl_vars['common']['aboutmenu']; ?>
</a></li>
								<?php echo $this->_tpl_vars['catalogue']; ?>

								<li><a href="<?php echo $this->_tpl_vars['SiteMainPath']; ?>
products.html" <?php echo $this->_tpl_vars['Product']; ?>
 title="Products" <?php echo $this->_tpl_vars['homeStyle']; ?>
><?php echo $this->_tpl_vars['common']['productmenu']; ?>
</a>
									<ul class="level2 btn-info">
											<?php if (count($_from = (array)$this->_tpl_vars['CatList'])):
    foreach ($_from as $this->_tpl_vars['CatDet']):
?>
												<li><a href="<?php echo $this->_tpl_vars['SiteMainPath']; ?>
products.html?cat_id=<?php echo ((is_array($_tmp=$this->_tpl_vars['CatDet']['CatId'])) ? $this->_run_mod_handler('md5', true, $_tmp) : md5($_tmp)); ?>
"><?php echo $this->_tpl_vars['CatDet']['CatName']; ?>
</a></li>
											<?php endforeach; unset($_from); endif; ?>
									</ul>
								</li>
								<li><a href="<?php echo $this->_tpl_vars['SiteMainPath']; ?>
whatsnew.html"<?php echo $this->_tpl_vars['What']; ?>
 title="What's New" <?php echo $this->_tpl_vars['homeStyle']; ?>
><?php echo $this->_tpl_vars['common']['newmenu']; ?>
</a></li>
								<li><a class="create_quotation" href="<?php echo $this->_tpl_vars['SiteMainPath']; ?>
door.html"<?php echo $this->_tpl_vars['ProConfig']; ?>
 title="Create a Quotation" <?php echo $this->_tpl_vars['homeStyle']; ?>
><?php echo $this->_tpl_vars['common']['quotationmenu']; ?>
</a></li>
							</ul>
							
	<!--					<div class="menu_main1"></div>
						<div class="menu_main2">
							<div class="main_li">

 						   </div>
						</div>
						<div class="menu_main3"></div>
						-->
					</div> <!-- .menu_main -->					
					<div class="headerText">
						<?php echo $this->_tpl_vars['common']['headertxt']; ?>

					</div> <!-- .headerText -->
				</div> <!-- .header_right -->
 		 <div class="clearxy"></div> 
		</div>
		</div>
        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['IncludeTpl'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>	
        <div class="footer">
            <div class="footer_left">
            	 <div class="fleft"> 
                     <a href="<?php echo $this->_tpl_vars['SiteMainPath']; ?>
index.html" title="Home"><?php echo $this->_tpl_vars['common']['homemenu']; ?>
</a>|
                     <a href="<?php echo $this->_tpl_vars['SiteMainPath']; ?>
contactus.html" title="Contact Us"><?php echo $this->_tpl_vars['common']['contactus']; ?>
 </a>
					 <?php if (count($_from = (array)$this->_tpl_vars['ContPageList'])):
    foreach ($_from as $this->_tpl_vars['CPageDet']):
?> |
						<a href="<?php echo ((is_array($_tmp=$this->_tpl_vars['CPageDet']['ContentURL'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
" title="<?php echo ((is_array($_tmp=$this->_tpl_vars['CPageDet']['page_title'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
">
						<?php echo ((is_array($_tmp=$this->_tpl_vars['CPageDet']['page_title'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</a> 
					<?php endforeach; unset($_from); endif; ?> 
   				</div>
                <div class="clearxy height10"></div>
                <div class="fleft1"> &copy; <?php echo $this->_tpl_vars['site_copyright']; ?>
</div>
            </div>
            <div class="footer_right">
            	<div class="social_logo">
                	<div class="face">
					<!--
					<a href="http://www.facebook.com/sharer.php?s=100&amp;p[title]=<?php echo $this->_tpl_vars['SiteTitle']; ?>
&amp;p[url]=index.html" target="_blank" class="active_bright"><img src="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
images/face_bright.png" alt="Facebook"/></a> -->
					<a href="http://www.facebook.com/lesportespardesign" target="_blank" class="active_bright"><img src="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
images/face_bright.png" alt="Facebook"/></a>
					</div>
                    <div class="tweet"> <!--
						<a href="http://twitter.com/share?text=<?php echo $this->_tpl_vars['SiteTitle']; ?>
,<?php echo $this->_tpl_vars['SiteMainPath']; ?>
index.html?&url=<?php echo $this->_tpl_vars['SiteMainPath']; ?>
?&lang=en" target="_blank"><img src="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
images/tweeter_bright.png" alt="Twitter"/></a> -->
						<a href="http://twitter.com/Portes_Aluminum" target="_blank"><img src="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
images/tweeter_bright.png" alt="Twitter"/></a>
						</div>
                    <div class="in"> <!--
						<a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo $this->_tpl_vars['SiteMainPath']; ?>
&title=<?php echo $this->_tpl_vars['SiteTitle']; ?>
&summary=&source=<?php echo $this->_tpl_vars['SiteMainPath']; ?>
" target="_blank"><img src="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
images/in_bright.png" alt="Linkedin"/></a> -->
						<a href="http://ca.linkedin.com/pub/les-portes-par-design/7a/815/124" target="_blank"><img src="<?php echo $this->_tpl_vars['SiteHttpPath']; ?>
images/in_bright.png" alt="Linkedin"/></a>
					</div>
                </div>
            </div>
        </div>
    </div>
</body> 
<?php echo ' 
<script type="text/javascript" language="javascript">
 jQuery("div").click(function(){
 //window.location=jQuery(this).find("a").attr("href"); return false;
	});
</script>
'; ?>

</html>