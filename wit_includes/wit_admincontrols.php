<?php 
	/***************************	Admin and Sub admin Header Menu Creation	**************************************/
	 
	$PJ525A_Ident=(!empty($_SESSION['PJ525A_Ident']))?$_SESSION['PJ525A_Ident']:'';
		$MainMenu=array( 'User'=>array('PageTitle'=>'User','PageURL'=>'users_management.php'), 
						'Gallery'=>array('PageTitle'=>'Gallery','PageURL'=>'gallery_mgmt.php'), 
						'Category'=>array('PageTitle'=>'Category','PageURL'=>'category_mgmt.php'), 
						'Product'=>array('PageTitle'=>'Product','PageURL'=>'product_mgmt.php'),  
						'Glass'=>array('PageTitle'=>'Glass','PageURL'=>'glass_mgmt.php'), 
						/*'Frame'=>array('PageTitle'=>'Frame','PageURL'=>'frame_mgmt.php'), */
						'Quote Generator'=>array('PageTitle'=>'Quote Generator','PageURL'=>'quotegen_mgmt.php'), 
 						'Orders'=>array('PageTitle'=>'Orders','PageURL'=>'order_list_home.php'), 
 						'Financial Report'=>array('PageTitle'=>'Financial Report','PageURL'=>'financial_report.php'), 
						'Location'=>array('PageTitle'=>'Location','PageURL'=>'loc_country_mgmt.php'),
						'Content'=>array('PageTitle'=>'Content','PageURL'=>'content_management.php'),
						'Other'=>array('PageTitle'=>'Other','PageURL'=>'mail_temp_mgmt.php?act_type=send'),
						'Configurator'=>array('PageTitle'=>'Configurator','PageURL'=>'profile_type.php') 
		);
	
 		/***************	Add Main Menu Title and URL Here	*************************************/	
  	$SubMenu=array( 
				
				'User'=>array(
					'ManageUser'=>array('Name'=>'Manage User','URL'=>'users_management.php'),
					'AddUser'=>array('Name'=>'Add User','URL'=>'users_management.php?act_type=addnew')	 
				), 
				'Gallery'=>array(
					'Manage Gallery'=>array('Name'=>'Manage Gallery','URL'=>'gallery_mgmt.php'),
					'Add Gallery'=>array('Name'=>'Add Gallery','URL'=>'gallery_mgmt.php?act_type=addnew'), 	
				),
				'Category'=>array(
					'ManageCategory'=>array('Name'=>'Manage Category','URL'=>'category_mgmt.php'),
					'AddCategory'=>array('Name'=>'Add Category','URL'=>'category_mgmt.php?act_type=addnew'),  
				),
				'Product'=>array(
					'ManageProduct'=>array('Name'=>'Manage Product','URL'=>'product_mgmt.php'),
					'AddProduct'=>array('Name'=>'Add Product','URL'=>'product_mgmt.php?act_type=addnew'),
					'ManageInquiry'=>array('Name'=>'Manage Inquiry','URL'=>'inquiry_mgmt_home.php'), 
				),
				'Glass'=>array(
					'ManageGlass'=>array('Name'=>'Manage Glass','URL'=>'glass_mgmt.php'),
					'AddGlass'=>array('Name'=>'Add Glass','URL'=>'glass_mgmt.php?act_type=addnew'), 
				),  
				'Frame'=>array(
					'ManageFrame'=>array('Name'=>'Manage Frame Pices','URL'=>'frame_mgmt.php'),
					'AddFrame'=>array('Name'=>'Add Frame Price','URL'=>'frame_mgmt.php?act_type=addnew'), 
				),  
				'Quote Generator'=>array(
					'ManageQuotes'=>array('Name'=>'Manage Quotes','URL'=>'quotegen_mgmt.php'),
					'ManageProfileType'=>array('Name'=>'Manage ProfileType','URL'=>'profiletype_mgmt.php'),
					'AddProfileType'=>array('Name'=>'Add ProfileType','URL'=>'profiletype_mgmt.php?act_type=addnew'), 
					'ManageFinishType'=>array('Name'=>'Manage FinishType','URL'=>'finishtype_mgmt.php'),
					'AddFinishType'=>array('Name'=>'Add FinishType','URL'=>'finishtype_mgmt.php?act_type=addnew'), 
					'ManageTax'=>array('Name'=>'Manage Tax','URL'=>'tax_mgmt.php'), 
					'ManageDPrices'=>array('Name'=>'Manage Door Prices','URL'=>'doorprices_mgmt.php'), 
					'AddDPrices'=>array('Name'=>'Add Door Prices','URL'=>'doorprices_mgmt.php?act_type=addnew'), 
 				),  
				'Content'=>array(
					'ManageContent'=>array('Name'=>'Manage Content','URL'=>'content_management.php'),
					'ManageStatic'=>array('Name'=>'Manage Static Pages','URL'=>'static_pages.php'),
					'AddContent'=>array('Name'=>'Add Content','URL'=>'content_management.php?act_type=addnew')	 
				),  	
				'Orders'=>array(
					'ManageProductPayment'=>array('Name'=>'Manage Orders','URL'=>'order_list_home.php'),
					'ManageCreditnote'=>array('Name'=>'Manage Credit Note','URL'=>'credit_note.php'),
					'AddCreditnote'=>array('Name'=>'Add Credit Note','URL'=>'credit_note.php?act_type=addnew'),
					'ManageBarazinOrders'=>array('Name'=>'Manage BARAZIN Purchase Orders','URL'=>'barazin_orders.php')
				),			
				'Financial Report'=>array(
					'ManageSales'=>array('Name'=>'Manage Sales','URL'=>'financial_report.php'),
					'ManagePurchase'=>array('Name'=>'Manage Purchase','URL'=>'financial_report.php?act_type=purchase'),
					'ManageProfit'=>array('Name'=>'Manage Profit','URL'=>'financial_report.php?act_type=profit')
				),		
				'Location'=>array(
					'ManageCountry'=>array('Name'=>'Manage Country','URL'=>'loc_country_mgmt.php'),
					'AddCountry'=>array('Name'=>'Add Country','URL'=>'loc_country_mgmt.php?act_type=addnew'),
					'ManageState'=>array('Name'=>'Manage State','URL'=>'state_mgmt.php'),
					'AddState'=>array('Name'=>'Add State','URL'=>'loc_country_mgmt.php?act_type=addnewstate')
				),				
				'Other'=>array(
					'ManageMailTemplate'=>array('Name'=>'Mail Templates','URL'=>'mail_temp_mgmt.php')
				),				
				'Configurator'=>array(
					'profileType'=>array('Name'=>'Profile Types','URL'=>'profile_type.php'),
					'profileFinish'=>array('Name'=>'Profile Finish Types','URL'=>'profile_finish.php'),
					'profileInsert'=>array('Name'=>'Profile Insert Types','URL'=>'profile_insert.php')
				)
			);
				
	 $PJ525A_Type=(isset($_SESSION['PJ525A_Type']))?$_SESSION['PJ525A_Type']:'';
	
	
	/******************  			Admin Management Control Arrays List	 		**********************/
	$MgmtType=(isset($_SESSION['PJ525A_Mgmt']))?$_SESSION['PJ525A_Mgmt']:'';
	$AdminPageType=(isset($_SESSION['PJ525A_PageType']))?$_SESSION['PJ525A_PageType']:'';
	$ControlList=array();$MCtrlArray=array('Active','InActive','Delete');$MenuText='';	
	$MMSelect='';	//	DFor Main Menu Selection
	
	$LastIndex=key(array_reverse($MainMenu));
	
		foreach($MainMenu as $MenuKey=>$MenuDet){
			$MMSelect=' class="select" ';$MMSubShow='';
			if($MenuKey==$MgmtType){
				$MMSelect='  class="current" '; $MMSubShow=' show ';
			}
			$SMenuText='';
			if(array_key_exists($MenuKey,$SubMenu)){
				
				$SMenuText.='<div class="select_sub '.$MMSubShow.'">'."\n".'
			<ul class="sub">'."\n"; 			
				foreach($SubMenu[$MenuKey] as $SMenuKey=>$SMenuDet){
					$MMSubSelect='';
					if($SMenuKey==$AdminPageType){
						$MMSubSelect=' class="sub_show"';
					}
					$SMenuText.='<li'.$MMSubSelect.'><a href="'.$SMenuDet['URL'].'">'.$SMenuDet['Name'].'</a></li>'."\n";
					
				}
				$SMenuText.='</ul>'."\n".'
		</div>'."\n";
			}
			$MenuText.='<ul'.$MMSelect.'><li><a href="'.$MenuDet['PageURL'].'"><b>'.$MenuDet['PageTitle'].'</b><!--[if IE 7]><!--></a><!--<![endif]-->'."\n".'
		<!--[if lte IE 6]><table><tr><td><![endif]-->'."\n".$SMenuText.'
		<!--[if lte IE 6]></td></tr></table></a><![endif]-->'."\n".'
		</li>'."\n".'
		</ul>';
		
			
			if($MenuKey!=$LastIndex){
				$MenuText.='<div class="nav-divider">&nbsp;</div>'."\n"; 
			}
		}
		$objSmarty->assign("MenuText",$MenuText);
	
		
		/******************  			Admin Management Control Arrays List	 		**********************/
		/*$SelAdm="Select * from pj_subadmin_controls Where ACtrlId!=''";
		$AdmMgmt	= $objMysqlFns->ExecuteQuery($SelAdm, "select"); 
		if(sizeof($AdmMgmt)>0){
			foreach($AdmMgmt as $MgmtDet){
				$MgmtCtrls=explode(',',$MgmtDet['AdminCtrls']);
				
				foreach($MgmtCtrls as $CtrlDet){
				
					if(in_array($CtrlDet,$MCtrlArray)){
						$ControlList[$CtrlDet]=$CtrlDet;
					}
				}
			}
		}
			
	$objSmarty->assign("ControlList",$ControlList);	*/	//	Assign Control Arrays For Smarty		
	
	/*************************		Function Print Feature Event in Home Page		************************************/
	$objSmarty->register_function('CheckAdminMgmtPermission', 'CheckAdminMgmtPermission');
	
?>