<?php ob_start();session_start();
//error_reporting(E_ALL);ini_set("display_errors",1);
	//date_default_timezone_set('America/New_York');
	//date_default_timezone_set('Asia/Calcutta');
	define("_MAINSITEPATH_",dirname(dirname(__FILE__))."/wit_includes/");
	define('WITProNum','525');define('WITTempPath','wit_html');global $ngconfig;
 	define('AdminFolderName','pj525_admin');
	
	include_once _MAINSITEPATH_."wit_config/wit_config.php";
	include_once $ngconfig['SiteClassPath']."class.SqlFunctions.php";
	include_once _MAINSITEPATH_."wit_smarty/Smarty.class.php";
	$objSmarty	= new Smarty();$objMysqlFns=new MysqlFns;	
	include_once _MAINSITEPATH_."wit_SmartyFunction.php";
	//$timezone=mysql_query("SET time_zone = 'America/New_York'");
	//$timezone=mysql_query("SET time_zone = 'Asia/Calcutta'");
	include_once _MAINSITEPATH_."wit_modules/class.Language.php";

	include_once $ngconfig['SiteLocalPath'].'wit_lang/index.php';
	$objSmarty->assign('common',$data[currentLanguage()]);//$data['english']);
	$objSmarty->assign('SiteHttpPath',$ngconfig['SiteGlobalPath']);//$data['english']);
	
	/**********************************		Codeing for select Admin Setting 	*************************************/
	$SqlQuery="select * from  pj_siteadmin where AdminIdent!=''";
	$Result=$objMysqlFns->ExecuteQuery($SqlQuery,"select");
	define('AdminProName',$Result[0]['admin_name']); //echo '<pre>';print_r($Result);exit;
		
	$SqlQuery1="select * from  pj_sitesettings where AdminIdent!=''";
	$Result1=$objMysqlFns->ExecuteQuery($SqlQuery1,"select");
	
	define('GDHolePrice',$Result1[0]['GDHolePrice']);
	$objSmarty->assign("GDHolePrice",GDHolePrice);
	
	define('HolesPrice',$Result1[0]['HolesPrice']);
	 $objSmarty->assign("HolesPrice",HolesPrice);
	 
	define('BasePrice',$Result1[0]['BasePrice']);
	$objSmarty->assign("BasePrice",BasePrice);
	
	define('ShipCost',$Result1[0]['ShipCost']);
	$objSmarty->assign("ShipCost",ShipCost);
	
	define('ExtraHingesPrice',$Result1[0]['ExtraHingesPrice']);
	$objSmarty->assign("ExtraHingesPrice",ExtraHingesPrice);
	
	define('BarazinMail',$Result1[0]['BarazinMail']);
	$objSmarty->assign("BarazinMail",BarazinMail);
	
	define('GMapAddress',$Result1[0]['GMapAddress']); 
	$objSmarty->assign("GMapAddress",GMapAddress);
	
	define('admin_phone',$Result1[0]['admin_phone']);
	$objSmarty->assign("admin_phone",admin_phone);

	define('SiteMainTitle',$Result1[0]['site_name']);
	$objSmarty->assign("SiteMainTitle",SiteMainTitle);
	
	define('site_copyright',$Result1[0]['site_copyright']);
	if($objLang->is_active())
	$objSmarty->assign("site_copyright","Tous droits réservés 2013 Lesportespardesign.com");
	else
	$objSmarty->assign("site_copyright",site_copyright);
	
	define('contact_mail',$Result1[0]['contact_mail']);
	$objSmarty->assign("contact_mail",contact_mail); 
	
	define('AdminMail',$Result1[0]['admin_mail']);	
	define('OurSiteNameLink',$Result1[0]['OurSiteFooter']);
	define('SiteMainPath',$ngconfig['SiteGlobalPath']);	
	$objSmarty->assign("SiteMainPath",SiteMainPath);
	$objSmarty->assign("AdminProName",AdminProName);
 	define('SiteDateTime',date("Y-m-d H:i:s"));
	 $objSmarty->assign("SiteDateTime",SiteDateTime);
	 define('HtmlToPDFPath',$ngconfig['SiteLocalPath'].'html2pdf');
	 
	 
	// convert object to array
	function objectToArray( $obj ) 
	{
		if(is_object($obj)) $obj = (array) $obj;
		if(is_array($obj)) {
		  $new = array();
		  foreach($obj as $key => $val) {
			$new[$key] = objectToArray($val);
		  }
		}
		else { 
		  $new = $obj;
		}
		return $new;
	}
	
	/***********************************		Function For Site Page Redirction		**********************************/
	function Redirect($Url)
	{	header("Location:".$Url);exit;}
	
	/**************************		For Include User and admin Support File include 	*****************************/
	
	if(strpos($_SERVER['SCRIPT_FILENAME'],AdminFolderName)){
		include_once _MAINSITEPATH_."wit_adminsupport.php";
	}
	else{
		include_once _MAINSITEPATH_."wit_usersupport.php";
	}
	$objSmarty->assign("SiteMainPath",$ngconfig['SiteGlobalPath']);
	
	/***********************************		Function For SQL Injection Function		**********************************/
	function EscapeInj($String){
		if(function_exists('mysql_real_escape_string')){
			if(get_magic_quotes_gpc())
				$String = stripslashes($String);
			$String = mysql_real_escape_string($String);
		}
		elseif(function_exists('mysql_escape_string')){
			if(get_magic_quotes_gpc())
				$String = mysql_escape_string(stripslashes($String));
			else 	$String = mysql_escape_string($String);
		}
		return $String;
	}
	
	function TrimAddSlashes($string){
		if (is_string($string)) {
			if(get_magic_quotes_gpc())
				return trim($string);
			else	return trim(addslashes($string));
		}
		elseif (is_array($string)){
			reset($string);
			while (list($key, $value) = each($string)) {
				$string[$key] = TrimAddSlashes($value);
			}
			return $string;
		}
		else{	return $string;		}
	}	
	
	/***********************************		Check Post Value For SQl Injection		**********************************/
	
	if(sizeof($_POST)>0){
		$PostArrName=''; $PostArrValue='';$PostName=''; $PostValue='';$PostString='';$PostArrString='';	
		foreach($_POST as $PostName=> $PostValue)
		{
		
			if(is_array($_POST[$PostName])){
				foreach($_POST[$PostName] as $PostArrName=> $PostArrValue){
					$_POST[$PostName][$PostArrName]=EscapeInj(htmlspecialchars(trim($PostArrValue),ENT_QUOTES));	
				}
			}
			else{
				$_POST[$PostName]=EscapeInj(htmlspecialchars(trim($PostValue),ENT_QUOTES));
			}	
		}
	}

	/***********************************		Check _GET Value For SQl Injection		**********************************/
	if(sizeof($_GET)>0){
		foreach($_GET as $GetName=> $GetValue){
			$_GET[$GetName]=EscapeInj(htmlspecialchars(trim($GetValue),ENT_QUOTES));
		}
	}		
	
	function ErrorMessage($error,$width) {
		$Message = "<div style ='width:".$width."px'>";
		
		$Message .="<ul>";
		$Message .= "<ul class='err_head'><b>Please Correct the Errors</b></ul>";
		for($i=0;$i<count($error);$i++)
		{
		 $Message .= "<li class='err_content'>$error[$i]</li>";
		}
		$Message .="</ul></div>";
		
		return $Message;
	} 
?>