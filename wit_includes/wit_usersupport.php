<?php 
	if (isset($_SESSION['PJ525C_Session']) && !empty($_SESSION['PJ525C_Session']))
	{
		if(session_id()!=$_SESSION['PJ525C_Session'])
		{
			UserLoginSessionClear('Remember');Redirect(SiteMainPath.'index.html?login=newlog');
		}
	}
	define('HTTPSURLPath',$ngconfig['HTTPSURLPath']);
	define('HTTPSDynPath',$ngconfig['HTTPSDynPath']);
	if(isset($_SESSION['PJ525C_Time']) && !empty($_SESSION['PJ525C_Time']))
	{	
		$JobLogDiff='';$JobLogDiff=strtotime("now")-$_SESSION['PJ525C_Time'];
		if($JobLogDiff>36000){
			UserLoginSessionClear('Remember');Redirect(HTTPSURLPath.'signin.html?login=timeexpire');
		}
	}		
	if(isset($_SESSION['PJ525_UserId']) && !empty($_SESSION['PJ525_UserId']))
	{	$_SESSION['PJ525C_Time']		= strtotime("now");		}
	
	$objSmarty->template_dir =$ngconfig['SiteLocalPath'].'wit_html';
	$objSmarty->compile_dir =$ngconfig['SiteLocalPath'].'wit_html_c';	
	
	if(isset($_SERVER['HTTPS'])){
		$objSmarty->assign("SiteHttpPath",HTTPSURLPath);
	}
	else{
		$objSmarty->assign("SiteHttpPath",SiteMainPath);		
	}	
	$objSmarty->assign("HTTPSURLPath",HTTPSURLPath);
	
	/********************************* For Define Regular Expression Values		***********************************/
	define('MailRegx',"/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/");	
	define('UNameRegx','/^[a-z\d_[:blank:]]{5,20}$/i');
	define('UInputCheck',"/^[A-Za-z0-9_[:blank:]]{1,50}$/");	
	define('UPasswordRegx',"/^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*$/");
	define('FirstLastName',"/^[A-Za-z_[:blank:]]{1,50}$/");
	$CurDate=date("M d, Y");$objSmarty->assign('CurDate',$CurDate);
	define('SCurrentTime',date("Y-m-d H:i:s"));	
   $SelQry="SELECT page_title,page_content,SEOString,
   			CONCAT('".SiteMainPath."','content/',SEOString,'.html') as ContentURL from pj_content_pages where page_id!='' 
			and page_status='1' and CPage_type='static'  ";
   $CPageList=$objMysqlFns->ExecuteQuery($SelQry,"select"); 
   //echo '<pre>';print_r($CPageList);exit;
   $objSmarty->assign('StatPageList',$CPageList);
	
   $SelPage="SELECT page_title,page_content,SEOString,
   			CONCAT('".SiteMainPath."','content/',SEOString,'.html') as ContentURL
			 from ".$objLang->tableName('pj_content_pages',true)." where page_id!='' 
			and page_status='1' and CPage_type='content'  order by page_id asc";
   $FPageList=$objMysqlFns->ExecuteQuery($SelPage,"select"); //echo '<pre>';print_r($FPageList);exit;
   $objSmarty->assign('ContPageList',$FPageList);
	
   $SelAds="SELECT * from pj_banners_list where BannerID!='' and BannerStatus='1' ORDER BY RAND() limit 3";
   $AdsList=$objMysqlFns->ExecuteQuery($SelAds,"select"); //echo '<pre>';print_r($FPageList);exit;
   $objSmarty->assign('AdsList',$AdsList);
	
   $SelCat="SELECT * from ".$objLang->tableName('pj_category_main',true)." where CatId!='' and CatStatus='1'";
   $CatList=$objMysqlFns->ExecuteQuery($SelCat,"select"); //echo '<pre>';print_r($FPageList);exit;
   $objSmarty->assign('CatList',$CatList);
    //$objSmarty->assign('BannerImgDisp',$ngconfig['BannerImgDisp']);
	function createRandom($length){		
		$chars = "0123456789876543210";srand((double)microtime()*1000000);$i = 0;$pass = '' ;$new_string = '' ;	
		while ($i <= $length) { 	$num = rand() % 33;$tmp = substr($chars, $num, 1);$new_string = $new_string . $tmp;	$i++;	}
    	return $new_string;
	} 
	/********************************* Select Create Random Number ***************************/
	function CreateRandomNumber($length){		
		$chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";
		srand((double)microtime()*1000000);$i = 0;$pass = '' ;$new_string='M';
		while ($i < $length) { 	$num = rand() % 33;$tmp = substr($chars, $num, 1);$new_string = $new_string . $tmp;	$i++;	}
    	return $new_string;
	}	

	/********************************* Select Meta data For Static Pages ***************************/
	function SelectMetaDetailsForCurrentPage($PageType,$SelVal=''){
	
		$MetaText='';$MetaDesc='';$MetaKey='';
		switch($PageType){
			case 'ContentPage': 
					$SelQry= 'Select * from `pj_content_pages` WHERE SEOString = "'.$SelVal.'"' ;
					$SelDet=mysql_query($SelQry);
					if(mysql_num_rows($SelDet)>0){
						$SelMDet=mysql_fetch_array($SelDet);
						$MetaKey=strip_tags(stripslashes($SelMDet['CMetaKey']));
						$MetaDesc=strip_tags(stripslashes($SelMDet['CMetaDesc']));
					}
				break;					
		}
		$SelQry= 'Select * from `pj_sitesettings` WHERE AdminIdent!=""' ;$MetaText='';
		$SelDet=mysql_query($SelQry);$SelMDet=mysql_fetch_array($SelDet);
		
		$MetaTitle=strip_tags(stripslashes($SelMDet['site_name']));
		if($MetaDesc=='' || $MetaKey==''){
			$MetaKey=strip_tags(stripslashes($SelMDet['meta_key']));
			$MetaDesc=strip_tags(stripslashes($SelMDet['meta_desc']));
		} 
		$MetaText='<meta name="description" content="'.$MetaDesc.'" />'."\n".
				'<meta name="keywords" content="'.$MetaKey.'"  />'."\n".
				'<meta name="title" content="'.$MetaTitle.'"  />'."\n".
				'<meta property="og:title" content="'.$MetaTitle.'" />'."\n".
				'<meta property="og:description" content="'.$MetaDesc.'" /> ';
		
		return $MetaText;		
	}
 	
	function CheckLoginMemberSession()
	{ 
		global $ngconfig;//$_SESSION['PJ525_UserId']=128;
		if(isset($_SESSION['PJ525_UserId']) && !empty($_SESSION['PJ525_UserId'])){
			$objMysqlFns=new MysqlFns();
		    $SelMem="Select UserId from pj_user_list  Where UserId ='".$_SESSION['PJ525_UserId']."' and UserSta='1'";
			$MemDet=$objMysqlFns->ExecuteQuery($SelMem,"select"); 
			if(sizeof($MemDet)!=1 ) {	
				UserLoginSessionClear('Remember');Redirect(SiteMainPath."signin.html?login=newlog");
			}
		}
		else{	
			UserLoginSessionClear('Remember');Redirect(SiteMainPath."signin.html?login=newlog");
		}
	}/*****************************		Check Login Details 		***********************************************/
	if(!empty($_POST['SignIn']) || !empty($_POST['LogIn'])){  
		
		extract($_POST);$LErrMsg=array();$ErrMsg=array();$SelType='';	
		$act_type=(isset($_GET['act_type']))?$_GET['act_type']:'';$user_type=(isset($_GET['user_type']))?$_GET['user_type']:'';
		
		if($UserEMail == ''){
			$LErrMsg[]='Please enter your EMail Address';
 		} 
		if($UserPass == ''){
			$LErrMsg[]='Please enter your Password';
		}
		
		if(sizeof($LErrMsg)==0){  
			$SelMem ="SELECT * from pj_user_list where UserId!='' and UserEMail='".$_POST['UserEMail']."' ";	
			$MemDet=$objMysqlFns->ExecuteQuery($SelMem,"select"); 
 				
			if(sizeof($MemDet)==1){ 
				if($MemDet[0]['UserSta']=='1'){
 					$MemPass=OurTechEncryptionDecryption('Decrypt',$MemDet[0]['UserPass']);
		//echo "<pre>";print_r($_SESSION);print_r($_POST);print_r($_GET);print_r($MemDet);echo $UserPass.'--'.$MemPass; exit;
					if($UserPass==$MemPass){ 	
						$_SESSION['PJ525C_Session'] =session_id();
						$_SESSION['PJ525_UserId']=$MemDet[0]['UserId'];
						$_SESSION['PJ525_UFirstName']=$MemDet[0]['UserFName'];
						$_SESSION['PJ525_ULastName']=$MemDet[0]['UserLName'];
						$_SESSION['PJ525_UserName']=$MemDet[0]['UserName'];
						$_SESSION['PJ525_UserMail']=$MemDet[0]['UserEMail'];
						$_SESSION['PJ525_UserType']=$MemDet[0]['UserType'];
						$_SESSION['PJ525_Time']=strtotime("now"); 
						
						if(isset($_SESSION['PJ525_PrevPageURL']) && !empty($_SESSION['PJ525_PrevPageURL'])){
		//echo "<pre>";print_r($_SESSION);print_r($_POST);print_r($_GET);print_r($MemDet);echo $UserPass.'--'.$MemPass; exit;
							 
						$UpSql="UPDATE pj_user_quotes SET UserId='".$MemDet[0]['UserId']."' WHERE QGId='".$_SESSION['QGId']."'";  
						$checkval=$objMysqlFns->ExecuteQuery($UpSql, "update");
							$RedirectUrl=$_SESSION['PJ525_PrevPageURL'];
							//session_unregister('PJ525_PrevPageURL');
							unset($_SESSION['PJ525_PrevPageURL']); 
						}
						else{  //echo "<PRE>"; print_r($_POST);print_r($_SESSION);exit;
							$RedirectUrl=HTTPSURLPath.'myprofile.html';
						}
						Redirect($RedirectUrl);
					}
					else{ 	$LErrMsg[]='Email and password do not match. Please try again';	}
  				}
				else{	$LErrMsg[]='Your account is not active at this time. Please contact Admin to resolve the issue.';	}
			}
			else{  
				$LErrMsg[]='Email and password do not match. Please try again'; 
			}
 		}
		if(sizeof($LErrMsg)!=0){
			if($_POST['SignIn']){
				$objSmarty->assign('ErrMessage',implode('</p><p>',$LErrMsg));return false;
			}
			else{
				$objSmarty->assign('ErrMessage',implode('</p><p>',$LErrMsg));return false;
			}
		}
	}
	
	/*************************** 	Function For Contact Us	Page	***************************/
	if(!empty($_POST['FConNameq']))
	{	
		include $ngconfig['SiteClassPath']."class.GeneralAdmin.php";
		global $objSmarty,$ngconfig;extract($_POST);$ErrMsg=array();
		$act_type=(isset($_GET['act_type'])) ? $_GET['act_type']:'';
		
		if(empty($FConNameq)){
			$ErrMsg[]= 'Please enter Your Name';
		 } 
 		if(empty($EnqCmtq)){
			$ErrMsg[]= 'Please enter Your Comments'; 	
		} 
		if(sizeof($ErrMsg)==0){ 
			$mail_Det='<table width="669" border="0" cellspacing="0" cellpadding="0"><tr><td width="660" valign="top">
				<h1 style="padding:0px; margin:0px; font:bold 16px Arial; color:#d64444;line-height:25px;"></h1>							
				<p style="padding:0px; margin:0px; font:13px Arial; line-height:25px;padding-left:10px;"></p>
				</td></tr></table>
				<table width="665" border="0" cellspacing="0" align="center" cellpadding="0"><tr>
					<td width="101" align="center" valign="top" style="padding-top:10px;"></td>
					<td width="564" valign="top" style="font:normal 13px Arial;line-height:18px;">
						<table width="564" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="164"><strong>Name</strong></td>
							<td width="400">: '.stripslashes($FConNameq).'</td>
						</tr> 
						<tr>
							<td width="164"><strong>Comment</strong></td>
							<td width="400">: '.stripslashes($EnqCmtq).'</td>
						</tr>
 						</table>
					</td>
				</tr></table>';	 
			$Subject = 'General Inquiry has been send from '.SiteMainTitle;	
			$headers  = "MIME-Version: 1.0\r\n";
			$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
			$headers .= 'From: '.AdminMail.'<'.AdminMail."> \r\n"; 
			GeneralAdmin::WesoIshMailTemplate(AdminMail,$Subject,$mail_Det,$headers);
			$objSmarty->assign('SuccessMessagesQ','Mail sent successfully!');
		}
		else{	$objSmarty->assign('ErrorMessagesQ',join('</p><p>',$ErrMsg));$objSmarty->assign('Arr',$_POST);	} 
	}
	
	/************************	Clear Curreant Seesion and Remember Curent page url	*********************************/
	function UserLoginSessionClear($RemCurPage=''){
 		unset($_SESSION['PJ525C_Session']);unset($_SESSION['PJ525_UserId']);
		unset($_SESSION['PJ525_UserName']);unset($_SESSION['PJ525_UserMail']);
		unset($_SESSION['PJ525_UniqueId']);unset($_SESSION['PJ525_DispName']);
		unset($_SESSION['PJ525_UserType']);	unset($_SESSION['PJ525C_Time']);
		if($RemCurPage!=''){
			$_SESSION['PJ525_PrevPageURL']=$_SERVER['REQUEST_URI'];
		}	
 	}	
?>