<?php 
								/*   Global Variables For Site Configuration   */
	$HTTPSOn=(isset($_SERVER['HTTPS']))?'http':'http';	
	 						
	/**********************************Global Variables For Local Server********************************************/
	$ngconfig['SiteGlobalPath']		= 	"http://".$_SERVER['HTTP_HOST']."/";
	$ngconfig['HTTPSDynPath']		= 	$HTTPSOn."://".$_SERVER['HTTP_HOST']."/";
	$ngconfig['HTTPSURLPath']		= 	"http://".$_SERVER['HTTP_HOST']."/";
	$ngconfig['SiteLocalPath']	    =	$_SERVER['DOCUMENT_ROOT']."/"."lesportespardesign.net/";
	$ngconfig['TinyEditor']			= 	$HTTPSOn."://".$_SERVER['HTTP_HOST']."/";
	
	/***************************	Global Variables For Pear Path For Image Resize		*****************************/
	define('PEARServerPath','/PEAR/');//$_SERVER['DOCUMENT_ROOT'].'/PEAR/');
	define('PEAR_include_path','.:'.PEARServerPath);
	
	
	
	/**********************************Global  Variables For Database Connection***********************************/
	$ngconfig['DBHostName']			= 	"lesportespar.db.11408016.hostedresource.com";//db464613106.db.1and1.com";	//	MYSQL Host Name
	$ngconfig['DBUserName']			= 	"lesportespar";//dbo464613106";			//	Database User Name
	$ngconfig['DBPassword']			= 	"L#%P0rt#%par";//weso525";				//	Database USer Password
	$ngconfig['DBName']				= 	"lesportespar";//db464613106";	//	Database Host Name
	
	
	/**********************************		Site Smarty and Class File Location		***********************************/
	$ngconfig['SiteClassPath']	   	=	$ngconfig['SiteLocalPath']."wit_includes/wit_modules/";	
	$ngconfig['GlassImageLoad']    	=	$ngconfig['SiteLocalPath'].'GlassImages/';
	$ngconfig['GlassImageDisp']    	=	$ngconfig['HTTPSDynPath'].'GlassImages/';
	$ngconfig['StoreImageLoad']    	=	$ngconfig['SiteLocalPath'].'ProductImages/';
	$ngconfig['StoreImageDisp']    	=	$ngconfig['HTTPSDynPath'].'ProductImages/';
	$ngconfig['ShapeImageLoad']    	=	$ngconfig['SiteLocalPath'].'ShapeImages/';
	$ngconfig['ShapeImageDisp']    	=	$ngconfig['HTTPSDynPath'].'ShapeImages/';
	$ngconfig['FinishImageLoad']    =	$ngconfig['SiteLocalPath'].'FinishImages/';
	$ngconfig['FinishImageDisp']    =	$ngconfig['HTTPSDynPath'].'FinishImages/';
	$ngconfig['GalleryImgLoad']    	=	$ngconfig['SiteLocalPath'].'GalleryImages/';
	$ngconfig['GalleryImgDisp']    	=	$ngconfig['HTTPSDynPath'].'GalleryImages/'; 
	$ngconfig['PDFLoad']     	= 	$ngconfig['SiteLocalPath'].'QuotePdf/';
	$ngconfig['MaxUploadFileSize']	= 	26214400;

	
	/**********************************		Site paypal Payment URL		***********************************/
	$ngconfig['DemoPaypalURL']		= 	'https://www.sandbox.paypal.com/webscr&cmd=_express-checkout&token=';
	$ngconfig['PaypalURL']			= 	'https://www.paypal.com/webscr&cmd=_express-checkout&token=';
	$ngconfig['DPaypal_ENDPOINT']	= 	'https://api-3t.sandbox.paypal.com/nvp';
	$ngconfig['TPaypal_ENDPOINT']	= 	'https://api-3t.paypal.com/nvp';
	$ngconfig['StdDemoPaypalURL']	= 	"https://sandbox.paypal.com/cgi-bin/webscr"; //for test mode
	$ngconfig['StdPaypalURL']		= 	"https://www.paypal.com/cgi-bin/webscr"; //for live mode	
?>