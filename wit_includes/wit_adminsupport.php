<?php 
	if (isset($_SESSION['PJ525A_Session']) && !empty($_SESSION['PJ525A_Session']))
	{
		if(session_id()!=$_SESSION['PJ525A_Session']){
			unset($_SESSION['PJ525A_Session']);unset($_SESSION['PJ525A_Name']);unset($_SESSION['PJ525A_Time']);
			unset($_SESSION['PJ525A_Type']);unset($_SESSION['PJ525A_Ident']);
			$_SESSION['PJ525A_PrevPageURL']=$_SERVER['REQUEST_URI'];
			Redirect('index.php?login=newlog');
		}
	}
	if(isset($_SESSION['PJ525A_Time']) && !empty($_SESSION['PJ525A_Time']))
	{	
		$ALogDiff='';$JobLogDiff=strtotime("now")-$_SESSION['PJ525A_Time'];
		if($ALogDiff>3600){
			unset($_SESSION['PJ525A_Session']);unset($_SESSION['PJ525A_Name']);unset($_SESSION['PJ525A_Time']);
			unset($_SESSION['PJ525A_Type']);unset($_SESSION['PJ525A_Ident']);
			$_SESSION['PJ525A_PrevPageURL']=$_SERVER['REQUEST_URI'];
			Redirect('index.php?login=timeexpire');
		}
	}		
	if(isset($_SESSION['PJ525A_Ident']) && !empty($_SESSION['PJ525A_Ident']))
	{	$_SESSION['PJ525A_Time']		= strtotime("now");		}
	
	$objSmarty->template_dir =$ngconfig['SiteLocalPath'].AdminFolderName.'/'.WITTempPath;
	$objSmarty->compile_dir =$ngconfig['SiteLocalPath'].AdminFolderName.'/'.WITTempPath.'_c';
	
	function CreateRandomNumber($length){		
		$chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ023456789";srand((double)microtime()*1000000);$i = 0;$pass = '' ;$new_string='M';
		while ($i < $length) { 	$num = rand() % 69;$tmp = substr($chars, $num, 1);$new_string = $new_string . $tmp;	$i++;	}
    	return $new_string;
	}		
	
	function CheckAdminLoginSession($CheckType,$ALogType='',$MgmtType='',$ActionType='')
	{
		//	$CheckType	--	Login chech for redirect
		//	$ALogType	--	which admin can login current page
		//	$MgmtType	--	Cuurent Page Mgmt 
		//	$ActionType	--	current action type	(Active,Inactive)
		
		if(!isset($_SESSION['PJ525A_Ident'])){	
			Redirect("logout.php");	
		}	
		else{
			$_SESSION['PJ525A_Mgmt']=$MgmtType;
			
			/*if($_SESSION['PJ525A_Type']=='SubAdmin'){	
				if($ALogType=='MainAdmin'){
					Redirect("index.php");
				}	
				else{				
					$SelCon='';					
					if(!empty($ActionType)){
						$SelCon=" and FIND_IN_SET('".$MgmtType.'@'.$ActionType."',SMgmtCtrls) ";
					}
					$SelAdm="Select SubAdmId from pj_subadmin_list Where SubAdmId = '".$_SESSION['PJ525A_Ident']."' and 
							FIND_IN_SET('".$MgmtType."',SSelMgmts) $SelCon";
					$AdmDet	=mysql_query($SelAdm); 
					if(mysql_num_rows($AdmDet)==0){
						if($CheckType='Redirct'){
							Redirect("index.php");
						}
						else{
							return false;
						}
					}	
				}
			}*/	
		}		
	}		
	
	function CheckAdminMgmtPermission($CheckType,$ALogType='',$MgmtType='',$PageType='',$ActionType='')
	{
		//	$CheckType	--	Login chech for redirect
		//	$PageType	--	which admin can login current page i.e. Listing/Add/Edit;
		//	$MgmtType	--	Cuurent Page Mgmt 
		//	$ActionType	--	current action type	(Active,Inactive)
		
		if(!isset($_SESSION['PJ525A_Ident'])){	
			Redirect("logout.php");	
		}	
		else{
			$_SESSION['PJ525A_Mgmt']=$MgmtType;
			$_SESSION['PJ525A_PageType']=$PageType;
			
			/*if($_SESSION['PJ525A_Type']=='SubAdmin'){	
				$SelCon='';					
				if(!empty($ActionType)){
					$SelCon=" and FIND_IN_SET('".$MgmtType.'@'.$ActionType."',SMgmtCtrls) ";
				}
				$SelAdm="Select SubAdmId from pj_subadmin_list Where SubAdmId = '".$_SESSION['PJ525A_Ident']."' and 
						FIND_IN_SET('".$MgmtType."',SSelMgmts) $SelCon";
				$AdmDet	=mysql_query($SelAdm); 
				if(mysql_num_rows($AdmDet)==0){
					if($CheckType='Redirct'){
						Redirect("index.php");
					}
					else{
						return false;
					}
				}	
			}*/	
		}		
	}	


		/********************************* Select Meta data For Static Pages ***************************/
	define('MailRegx',"/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/");	
	define('UNameRegx','/^[a-z\d_]{5,20}$/i');
	define('UInputCheck',"/^[A-Za-z0-9_-]{1,50}$/");	
	define('FirstLastName',"/^[A-Za-z_[:blank:]]{1,50}$/");
	define('PhoneRegx',"/^[0-9]{1,15}$/");
	
?>