<?php   #####	Class Function for Banner	####
class Banner extends MysqlFns
{

    /*****************************************************************************************************************/
	/**********************************				Admin Panel			*********************************************/
	/*****************************************************************************************************************/
	
	/**********************************		Function For Select Banner List		***************************************/
	
	function GetBannerLists($ResVal,$PageVal)
	{
		global $objSmarty;$SelCon="";$PageURL='banner_mgmt.php?';
		
		$seaname=(isset($_GET['seaname'])) ? $_GET['seaname']:'';
		if($seaname!=""){
			$SelCon.=" and BannerName LIKE '".trim($seaname)."%'";$PageURL.="seaname=".urlencode($seaname)."&";
		}
		$SelFields=array('*');//	Fields To select Table listing	
		$SelQuery="SELECT count(BannerId) from pj_banner Where BannerId!='' $SelCon order by BannerId desc";
		GeneralAdmin::SelectTableContentLists($ResVal,$SelQuery,$SelFields,$PageVal,$PageURL);
	}
	
	/*****************************		Function For Add and Edit Banner DSetails		*********************************/
	function AdminAddUpdatteBannerDetails($AdminAction,$con_id)
	{
		global $objSmarty,$ngconfig;extract($_POST);$ErrMsg=array();$Che_con='';
		
		if($con_id)
		{	$Che_con=" and md5(BannerId)!='".$con_id."'";	}
		
		$CheMem= "Select BannerId from pj_banner Where BannerName='".addslashes($BannerName)."' $Che_con";
		if(!GeneralAdmin::CheckDupRecord($CheMem))
		{	$ErrMsg[]="Banner Title Already Exists";$flag_ins=false;	} 
		
		if($BannerType=='Image'){
			if($AdminAction!='Update' || $_FILES['BannerImage']['name']!=''){
				if($_FILES['BannerImage']['name']=='')
				{
					$ErrMsg[]="Please Select Banner Image";
				}
				elseif($_FILES['BannerImage']['name']!='' && $_FILES['BannerImage']['error']==0){
					$PlaceArr=explode('_',$_POST['BannerPlace']);$BanImg=explode('x',$PlaceArr[1]);
					list($ImgWidth, $ImgHeight) = getimagesize($_FILES['BannerImage']['tmp_name']);
					$BanWidth=$BanImg[0];$BanHeight=$BanImg[1];
					if($BanWidth>0){
						$ErrText=($ImgWidth>$BanWidth)?" Width 0px - ".$BanWidth."px":'';
						if($ImgHeight>$BanHeight && $BanHeight>0){
							$ErrText.=" and Height 0px - ".$BanHeight."px";
						}	
						if($ErrText!=''){	
							$ErrMsg[]="Banner Image Size Should be ".$ErrText;
						}
					}
				}
			}
		}
		if(count($ErrMsg)==0)
		{
			$CImgDet='';
			if($BannerType=='Image'){
				$LoadLoc=$ngconfig['BannerImgLoad'];
				if($_FILES['BannerImage']['name']!='' && $_FILES['BannerImage']['error']==0)
				{	
					$path_info=pathinfo($_FILES['BannerImage']['name']);$extension = strtolower($path_info['extension']);
					$BannerImage=date("Ymd").time().rand(5,10000).'.'.$path_info['extension'];
					$ResizeArr=array("755||88||l_".$BannerImage,"70||90||m_".$BannerImage);
					$ImgSuc=GeneralAdmin::WesoUploadedFile('Image',$_FILES['BannerImage']['tmp_name'],$LoadLoc,$BannerImage,
									$ResizeArr);		
					if($ImgSuc==6)
					{	
						$SelQuery="SELECT `BannerImage` from  pj_banner where md5(BannerId)='".$con_id."'";
						$Result=$this->ExecuteQuery($SelQuery, "select");
						if($Result[0]['BannerImage']!=''){
							$FileDelete=array($LoadLoc.'m_'.$Result[0]['BannerImage'],$LoadLoc.$Result[0]['BannerImage']);
							GeneralAdmin::WesoFileFolderDelete($FileDelete);	
						}
						$CImgDet.=" ,BannerImage='".$BannerImage."' ";
					}
				}
			}
			else{
				$BannerImage='';$BannerURL='';$CImgDet='';
			}
			if($AdminAction=='Update'){
				$AltBan = "Update pj_banner set BannerName= '".$BannerName."',BannerType='".$BannerType."',
							BannerText='".$BannerText."',BannerURL='".$BannerURL."',BannerStatus='".$BannerStatus."',
							BannerPlace='".$BannerPlace."' $CImgDet Where md5(BannerId)='".$con_id."'";
							
				$AltCheck=$this->ExecuteQuery($AltBan, "update"); 	
				if(!empty($AltCheck)){
					Redirect('banner_mgmt.php?act_type=addnew&mgtact=usucc&con_id='.$con_id);
				}
				else{
					$objSmarty->assign("ErrMessage", "IT IS ALREADY UPDATED PLEASE DO SOME MODIFICATIONS TO UPDATE"); 
				}
			}
			else{
				$BanIns="insert into pj_banner (BannerName,BannerType,BannerText,BannerImage,BannerURL,BannerStatus,BannerPlace) 
						values('".$BannerName."','".$BannerType."','".$BannerText."','".$BannerImage."','".$BannerURL."',
						'".$BannerStatus."','".$BannerPlace."')";
				$this->ExecuteQuery($BanIns,"insert");
				Redirect('banner_mgmt.php?act_type=addnew&mgtact=asucc');
			}
		}
		else
		{	$objSmarty->assign("ErrMessage",implode('<br/>',$ErrMsg));$objSmarty->assign("ArrDet",$_POST);		}	
	}
	
    /*****************************************************************************************************************/
	/**********************************				User Panel			*********************************************/
	/*****************************************************************************************************************/
	/**********************************		Function For Select Banner List		***************************************/
	function UserBannerLists()///indexPage banner select (9-4-12) by palanisamy
	{
		global $objSmarty;$SelCon="";$PageURL=SiteMainPath.'index.php?';
		$seaname=(isset($_GET['seaname'])) ? $_GET['seaname']:'';
		$SelQuery="SELECT * from pj_banner where bannerid!='' order by rand() limit 1";
		$Banner=$this->ExecuteQuery($SelQuery,"select");
		$objSmarty->assign("BannerView",$Banner);
	}

}
?>