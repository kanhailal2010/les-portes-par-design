<?php
class Content extends MysqlFns
{
	/*****************************************************************************************************************/
	/**********************************				User Panel			*********************************************/
	/*****************************************************************************************************************/
	/*************************** 	Function For Contact Us	Page	***************************/
	function PostContactDetailsToAdmin()
	{	//echo "<PRE>"; print_r($_POST);exit;
		global $objSmarty,$ngconfig;extract($_POST);$ErrMsg=array();
		$FLName='/[\'\/~`\!@#\$%\^&\*\(\)_\-\+=\{\}\[\]\|;:"\<\>,\.\?\\\]/'; 
		$act_type=(isset($_GET['act_type'])) ? $_GET['act_type']:'';
		
		if(empty($FConName)){
			$ErrMsg[]= 'Please enter Your Name';
		 } 
		elseif(preg_match($FLName,$FConName)){	
			$ErrMsg[]='Name not allowed special characters ';
		}
		if(empty($UConMail)){
			$ErrMsg[]= 'Please enter Email Address'; 	
		}
		elseif(!preg_match(MailRegx,$UConMail)){
			$ErrMsg[]= 'Please enter Valid Email Address';
		}	 
		if(empty($Phone)){
			$ErrMsg[]= 'Please enter Your Phonenumber'; 	
		}
		if(empty($Address)){
			$ErrMsg[]= 'Please enter Your Address'; 	
		}
		if(empty($EnqCmt)){
			$ErrMsg[]= 'Please enter Your Comments'; 	
		} 
		if(sizeof($ErrMsg)==0){ 
			$mail_Det='<table width="669" border="0" cellspacing="0" cellpadding="0"><tr><td width="660" valign="top">
				<h1 style="padding:0px; margin:0px; font:bold 16px Arial; color:#d64444;line-height:25px;"></h1>							
				<p style="padding:0px; margin:0px; font:13px Arial; line-height:25px;padding-left:10px;"></p>
				</td></tr></table>
				<table width="665" border="0" cellspacing="0" align="center" cellpadding="0"><tr>
					<td width="101" align="center" valign="top" style="padding-top:10px;"></td>
					<td width="564" valign="top" style="font:normal 13px Arial;line-height:18px;">
						<table width="564" border="0" cellspacing="0" cellpadding="0">
						<tr>
										<td width="164"><strong>Name</strong></td>
										<td width="400">: '.stripslashes($FConName).'</td>
									</tr>
									<tr>
										<td width="164"><strong>Phone Number</strong></td>
										<td width="400">: '.stripslashes($Phone).'</td>
									</tr>
									<tr>
										<td width="164"><strong>Address</strong></td>
										<td width="400">: '.stripslashes($Address).'</td>
									</tr>
									<tr>
										<td width="164"><strong>Comment</strong></td>
										<td width="400">: '.stripslashes($EnqCmt).'</td>
									</tr>
									
							<tr><td height="30" colspan="2" style="font: bold 12px  Arial;color:#021A3C;">Please Contact me at 
								<a href="mailto:'.$UConMail.'">'.$UConMail.'</a></td></tr>
						</table>
					</td>
				</tr></table>';	 
			$Subject = 'General Inquiry has been send from '.SiteMainTitle;	
			$headers  = "MIME-Version: 1.0\r\n";
			$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
			$headers .= 'From: '.$UConMail.'<'.$UConMail."> \r\n"; 
			GeneralAdmin::WesoIshMailTemplate(AdminMail,$Subject,$mail_Det,$headers);
			Redirect(SiteMainPath.'contactus.html?pro_msg=asucc');
		}
		else{	$objSmarty->assign('ErrorMessages',join('</p><p>',$ErrMsg));$objSmarty->assign('Arr',$_POST);	} 
	}
	
	
    /*****************************************************************************************************************/
	/**********************************				Admin Panel			*********************************************/
	/*****************************************************************************************************************/
	
	/************************ 				Function For List Static Page Details				************************/
	function GetContentLists($Pagetype,$ResVal,$PageVal)
	{
		global $objSmarty,$objLang;$SelCon="";
		if($Pagetype=='static'){$PageURL='static_pages.php?';}else{
		$PageURL='content_management.php?';}
		
		$OrderCon=" order by ST.page_id desc";
		
		$seaname=(isset($_GET['seaname'])) ? $_GET['seaname']:'';
		if($seaname!=""){
			$SelCon.=" and ST.page_title LIKE '".trim($seaname)."%'";$PageURL.="seaname=".urlencode($seaname)."&";
		}
		$sortby=(isset($_GET['sortby'])) ? $_GET['sortby']:'';$SortArr=array('id'=>'ST.page_id','cont'=>'ST.page_title');
		if(!empty($sortby)){
			$SortDet=explode('_',$sortby);
			if(array_key_exists($SortDet[0],$SortArr)){
				$OrderCon=" order by ".$SortArr[$SortDet[0]]." ".$SortDet[1]."";
			}
		}				
		$objSmarty->assign("PageURL",$PageURL);
		if($Pagetype=="static"){
			$SelCon.=" and ST. page_id!='45'";
		}
		$SelFields=array('ST.*');//	Fields To select Table listing	
		$SelQuery="SELECT count(ST.page_id) from ".$objLang->tableName('pj_content_pages',true)." ST where ST.page_id!='' 
					and CPage_type='".$Pagetype."' $SelCon $OrderCon " ;
		$SUserList=GeneralAdmin::SelectTableContentLists($ResVal,$SelQuery,$SelFields,$PageVal,$PageURL);
	}	
	////////////////////////// 				Function For Add and Edit Static Page				/////////////////////////
	function ContentAddUpdate($FileName,$Ctype)
	{		
		global $objSmarty,$objLang;$ErrMsg=array();extract($_POST);$SelCon='';
		$tablename = $objLang->tableName('pj_content_pages');
		
		$con_id=(isset($_GET['con_id'])) ? $_GET['con_id']:'';
		if(empty($page_title)){
			$ErrMsg[]="Please Enter Title";
		}
		else{
			if(!empty($con_id)){
				$SelCon=" and md5(page_id)!='".$con_id."'";
			}
			$SelQuery="SELECT ST.* from $tablename ST where page_title='".$page_title."' $SelCon";
			if(!GeneralAdmin::CheckDupRecord($SelQuery))
			{
				$ErrMsg[]="Title Already Exists";
			}
		}

		if(count($ErrMsg)==0)
		{
			if($AdminAction=='Update'){
				
			 	$AltCon="Update ".$objLang->tableName('pj_content_pages',true)." set page_title='".$page_title."',page_content='".$page_content."',
						CMetaKey='".$CMetaKey."',CMetaDesc='".$CMetaDesc."',CPage_type='".$Ctype."'
						 Where md5(page_id)='".$con_id."'";
				$AltCheck=$this->ExecuteQuery($AltCon, "update"); 	
				if(!empty($AltCheck)){
					if(!$objLang->is_active())
					{
						GeneralAdmin::MakeAltSEOUrlString($objLang->tableName('pj_content_pages'),'page_id','SEOString',$con_id,$page_title);
						$SelQuery = "SELECT SEOString FROM ".$objLang->tableName('pj_content_pages')." WHERE page_title = '$page_title'";
						$select = $this->ExecuteQuery($SelQuery, "select");
						$pageSeo = $select[0]['SEOString'];
						GeneralAdmin::MakeAltSEOUrlString($objLang->tableName('pj_content_pages','french'),'page_id','SEOString',$con_id,$pageSeo);
					}
						Redirect($FileName.'.php?pro_msg=usucc');
				}
				else{
					$objSmarty->assign("ErrMessage", "IT IS ALREADY UPDATED PLEASE DO SOME MODIFICATIONS TO UPDATE"); 
				}
			}
			else{				
				$InsCon="insert into pj_content_pages(page_title,page_content,CMetaKey,CMetaDesc,CPage_type)
					 	values('".$page_title."','".$page_content."','".$CMetaKey."','".$CMetaDesc."','".$Ctype."')";
				$InsConLanguage="insert into ".$objLang->tableName('pj_content_pages','french')." (page_title,page_content,CMetaKey,CMetaDesc,CPage_type)
					 	values('".$page_title."','".$page_content."','".$CMetaKey."','".$CMetaDesc."','".$Ctype."')";
				$InsCon=$this->ExecuteQuery($InsCon,"insert");
				$con_id=mysql_insert_id();	
				$InsConLanguege	=	$this->ExecuteQuery($InsConLanguege,"insert");
				GeneralAdmin::MakeAltSEOUrlString($objLang->tableName('pj_content_pages'),'page_id','SEOString',$con_id,$page_title);
				GeneralAdmin::MakeAltSEOUrlString($objLang->tableName('pj_content_pages','french'),'page_id','SEOString',$con_id,$page_title);
				Redirect($FileName.'.php?pro_msg=asucc');
			}		
			
			
		}
		else
		{	$objSmarty->assign("ErrMessage",implode('<br/>',$ErrMsg));;$objSmarty->assign("Arr",$_POST);	}
	}	
}
?>