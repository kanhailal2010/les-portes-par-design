<?php   
class Product extends MysqlFns
{		
 
	/*****************************************************************************************************************/
	/**********************************				Admin Panel			*********************************************/
	/*****************************************************************************************************************/
	 function SelectCategoryList()
	{	
		global $objSmarty;
		$SelCat="select * from pj_category_main where CatId!=''";
		$ResCat= $this->ExecuteQuery($SelCat, "select");  
		$objSmarty->assign('CatList',$ResCat);
	} 
	
	/**************************************** 		Select product Images		*********************************************/
	function SelectImage($con_id){
		global $objSmarty;
		$SelImage="select * from pj_sell_images where md5(SellId)='".$con_id."'";
		$Img=$this->ExecuteQuery($SelImage,"select");	//echo "<PRE>";print_r($Img);exit;
		$objSmarty->assign('ImgList',$Img);
	}	
	/*************************** 			Function For Add and update  Seller details	***************************/
	function PostEnquiry($pro_name)
	{	//echo "<PRE>"; print_r($_POST); exit;
		global $objSmarty,$ngconfig;extract($_POST);$ErrMsg=array();  $FlagIns=true; 
		$FLName='/[\'\/~`\!@#\$%\^&\*\(\)_\-\+=\{\}\[\]\|;:"\<\>,\.\?\\\]/';  
 				
		if(empty($Name)){
			$ErrMsg[]= "Please enter name ";
		}
		elseif(preg_match($FLName,$Name)){	
			$ErrMsg[]='Name not allowed special characters ';
		}
 		if(empty($InqMail)){
			$ErrMsg[]= "Please ente Email";
		}	
 		if(empty($InqMsg)){
			$ErrMsg[]= "Please enter Enquiry";
		}  
 		if(sizeof($ErrMsg)==0){ 
			$ProIns="insert into pj_inquiry_list(SellId,Name,InqMail,InqMsg,InqDate,Qty,X,Z,Light,Type,Y,e,Finish,FinishType,
					Hinge,Glass,ConfigBevel,Caming,Dimension,ColorNumber,HoleA,LengthB,WidthC,SqftUnit,SqftTot,Hinges) 
					values('".$SellId."','".$Name."','".$InqMail."','".$InqMsg."',CURDATE(),'".$Qty."','".$X."','".$Z."',
					'".$Light."','".$Type."','".$Y."','".$e."','".$Finish."','".$FinishType."','".$Hinge."','".$Glass."',
					'".$ConfigBevel."','".$Caming."','".$Dimension."','".$ColorNumber."','".$HoleA."','".$LengthB."','".$WidthC."',
					'".$SqftUnit."','".$SqftTot."','".$Hinges."')";
			$this->ExecuteQuery($ProIns,"insert");  
			
			$SelConId="select TP.ItemName,TC.CatName from pj_sell_list TP left join pj_category_main TC on TC.CatId=TP.CatId 
					where TP.SellId='".$SellId."'";
			$ProDet=$this->ExecuteQuery($SelConId, "select");	 
					
			$Subject='Price Request Details From '.SiteMainTitle;
					 
			$mail_Det='<table width="669" border="0" cellspacing="0" cellpadding="0"><tr><td width="660" valign="top">
				<h1 style="padding:0px; margin:0px; font:bold 16px Arial; color:#09b1f1;line-height:25px;">Hello Admin,</h1>	
				<h3 style="padding:0px;margin:0px; font:14px Arial;line-height:25px;">Below are the product enquiry details from your website.</h3> 
				</td></tr></table> <br>
				<table width="100" align="left" border="0" cellspacing="0" cellpadding="0"><tr>
					<td width="0" valign="top" style="padding-top:10px;"></td>
					<td width="800" valign="top" style="font:normal 13px Arial;line-height:18px;">
					<h3 style="padding:0px;color:#037ba9;margin:0px;font:bold 14px Arial;line-height:25px;">Customer Details</h3>
					<hr> 			
						<table width="300" align="left" border="0" cellspacing="0" cellpadding="0">';
 							$mail_Det.='<tr>
										<td width="250"><strong>Name</strong></td>
										<td width="400">: '.stripslashes($Name).'</td>
									</tr>
									<tr>
										<td width="164"><strong>Email</strong></td>
										<td width="400">: '.stripslashes($InqMail).'</td>
									</tr> 
									<tr>
										<td width="164"><strong>Request Price </strong></td>
										<td width="400">: '.stripslashes($InqMsg).'</td>
									</tr> 
									<tr>
										<td width="164"><strong>Date</strong></td>
										<td width="400">: '.date("F d, Y", strtotime(date('Y-m-d'))).'</td>
									</tr>
 						</table> 
					<td width="0" valign="top" style="padding-top:10px;"></td>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td width="600" valign="top" style="font:normal 13px Arial;line-height:18px;">	 			
					<h3 style="padding:0px;color:#037ba9;margin:0px;font:bold 14px Arial;line-height:25px;">Enquiry Summary</h3>
					<hr> 			
						<table width="300" align="right" border="0" cellspacing="0" cellpadding="0">';
				if($SellId=='1'){
 					$mail_Det.='
							<tr>
								<td width="164"><strong>Product Name</strong></td>
								<td width="400">: '.stripslashes($ProDet[0]['ItemName']).'</td>
							</tr>
							<tr>
								<td width="250"><strong>Category</strong></td>
								<td width="400">: '.stripslashes($ProDet[0]['CatName']).'</td>
							</tr> 
							<tr>
								<td width="250"><strong>Finish Type </strong></td>
								<td width="400">: '.stripslashes($FinishType).'</td>
							</tr>
							<tr>
								<td width="250"><strong>Hinge Selections </strong></td>
								<td width="400">: '.stripslashes($Hinge).'</td>
							</tr>
							<tr>
								<td width="250"><strong>Glass</strong></td>
								<td width="400">: '.stripslashes($Glass).'</td>
							</tr>
							<tr>
								<td width="250"><strong>Config Bevel</strong></td>
								<td width="400">: '.stripslashes($ConfigBevel).'</td>
							</tr>
							<tr>
								<td width="250"><strong>Caming</strong></td>
								<td width="400">: '.stripslashes($Caming).'</td>
							</tr>
							<tr>
								<td width="250"><strong>Dimension</strong></td>
								<td width="400">: '.stripslashes($Dimension).'</td>
							</tr>
							<tr>
								<td width="250"><strong>Color Number</strong></td>
								<td width="400">: '.stripslashes($ColorNumber).'</td>
							</tr>
							<tr>
								<td width="250"><strong>Finish</strong></td>
								<td width="400">: '.stripslashes($Finish).'</td>
							</tr>
							<tr>
								<td width="250"><strong>Quantity</strong></td>
								<td width="400">: '.stripslashes($Qty).'</td>
							</tr>
							<tr>
								<td width="250"><strong>Hole size (A)</strong></td>
								<td width="400">: '.stripslashes($HoleA).' in</td>
							</tr>
							<tr>
								<td width="250"><strong>Length (B)</strong></td>
								<td width="400">: '.stripslashes($LengthB).' in</td>
							</tr>
							<tr>
								<td width="250"><strong>Width (C)</strong></td>
								<td width="400">: '.stripslashes($WidthC).' in</td>
							</tr>
							<tr>
								<td width="250"><strong>Hinge(HL-HR)</strong></td>
								<td width="400">: '.stripslashes($Hinges).'</td>
							</tr>
							';/*<tr>
								<td width="250"><strong>Square-Foot(Unit)</strong></td>
								<td width="400">: '.stripslashes($SqftUnit).'</td>
							</tr>
							<tr>
								<td width="250"><strong>Square-Foot(Total)</strong></td>
								<td width="400">: '.stripslashes($SqftTot).'</td>
							</tr>*/
				}
				elseif($SellId=='2'){
 					$mail_Det.='
							<tr>
								<td width="164"><strong>Product Name</strong></td>
								<td width="400">: '.stripslashes($ProDet[0]['ItemName']).'</td>
							</tr>
							<tr>
								<td width="250"><strong>Category</strong></td>
								<td width="400">: '.stripslashes($ProDet[0]['CatName']).'</td>
							</tr>
							<tr>
								<td width="250"><strong>Finish</strong></td>
								<td width="400">: '.stripslashes($Finish).'</td>
							</tr>
							<tr>
								<td width="250"><strong>X</strong></td>
								<td width="400">: '.stripslashes($X).' in</td>
							</tr>
							<tr>
								<td width="250"><strong>Y</strong></td>
								<td width="400">: '.stripslashes($Y).' in</td>
							</tr>
							<tr>
								<td width="250"><strong>Z</strong></td>
								<td width="400">: '.stripslashes($Z).' in</td>
							</tr>
							<tr>
								<td width="250"><strong>e</strong></td>
								<td width="400">: '.stripslashes($e).' in</td>
							</tr>  ';
				}
				elseif($SellId=='3'){
 					$mail_Det.='
							<tr>
								<td width="164"><strong>Product Name</strong></td>
								<td width="400">: '.stripslashes($ProDet[0]['ItemName']).'</td>
							</tr>
							<tr>
								<td width="250"><strong>Category</strong></td>
								<td width="400">: '.stripslashes($ProDet[0]['CatName']).'</td>
							</tr>
							<tr>
								<td width="250"><strong>Quantity</strong></td>
								<td width="400">: '.stripslashes($Qty).'</td>
							</tr>
							<tr>
								<td width="250"><strong>Type</strong></td>
								<td width="400">: '.stripslashes($Type).'</td>
							</tr>
							<tr>
								<td width="250"><strong>X</strong></td>
								<td width="400">: '.stripslashes($X).' in</td>
							</tr>
							<tr>
								<td width="250"><strong>Z</strong></td>
								<td width="400">: '.stripslashes($Z).' in</td>
							</tr>
							<tr>
								<td width="250"><strong>Light</strong></td>
								<td width="400">: '.stripslashes($Light).'</td>
							</tr> ';
				}
				else{
 					$mail_Det.='
							<tr>
								<td width="164"><strong>Product Name</strong></td>
								<td width="400">: '.stripslashes($ProDet[0]['ItemName']).'</td>
							</tr>
							<tr>
								<td width="250"><strong>Category</strong></td>
								<td width="400">: '.stripslashes($ProDet[0]['CatName']).'</td>
							</tr> ';
				}
 				$mail_Det.='
						</table>	
					</td></td>  <br>	</tr><tr>
					 
					<td width="10" style=" float:left;font:normal 13px Arial;line-height:18px;"><br>
						<table width="300" border="0" cellspacing="0" cellpadding="0">
							<tr><td height="30" colspan="2" style="font: bold 12px  Arial;color:#021A3C;">Please Contact me at <a href="mailto:'.$InqMail.'">'.$InqMail.'</a></td></tr>
							 
						</table>
						</td>
				</tr></table>';	  
										
				$headers  = "MIME-Version: 1.0\r\n";
				$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
				$headers .= 'From: '.$InqMail.'<'.$Name."> \r\n";		
						
			//GeneralAdmin::GetMailTemplateDetailsForMail($MemDet[0]['UserEMail'],$MailBody,'PaymentSubscription');	
			GeneralAdmin::WesoIshMailTemplate(AdminMail,$Subject,$mail_Det,$headers); 
 			Redirect(SiteMainPath.'products/'.$pro_name.'.html?pro_msg=asucc');  
		}
		else{	
			$objSmarty->assign("ErrMessage",implode('<br/>',$ErrMsg));$objSmarty->assign("Arr2",$_POST);
		}	
	}	
		
	/*************************** 			Function For Add and update  Seller details	***************************/
	function AddUpdateSellerItemDetails($con_id,$Type)
	{	
		global $objSmarty,$ngconfig;extract($_POST);$ErrMsg=array();$FExtArr=array('gif','png','jpg','jpeg');
		$FlagIns=true;$ImgUpload=false;$banIng='';$ItemImage='';$Che_con='';$AltImg='';$PhotoLoc=$ngconfig['StoreImageLoad'];
		$PageURL='product_mgmt.php?';
				
		if(empty($ItemName)){
			$ErrMsg[]= "Please enter product name ";
		}
		else {
			if(!empty($con_id)){
				$where_con=" and md5(SellId)!='".$con_id."'";
			}
			$SelQuery="SELECT * from pj_sell_list where ItemName='".$ItemName."' $where_con and SellId!=''";
			if(!GeneralAdmin::CheckDupRecord($SelQuery)){
				$ErrMsg[]="This product name is already exists";
			}
 		}
		if(empty($CatId)){
			$ErrMsg[]= "Please select category";
		}	
 		if(empty($ItemDesc)){
			$ErrMsg[]= "Please enter description";
		}
		if(empty($Specification)){
			$ErrMsg[]= "Please enter specification";
		}  

		if($_FILES['ItemImage']['name']!='' && $_FILES['ItemImage']['error']==0){	
			$path_info=pathinfo($_FILES['ItemImage']['name']);
			if(!in_array(strtolower($path_info['extension']),$FExtArr))
			{	
				$ErrMsg[]= "Please Select Valid Image File Only [ ".implode(',',$FExtArr)." ]"; 
			}
		}  
 		if(sizeof($ErrMsg)==0){ 
			if($AdminAction=='Update') {
				$SelConId="select SellId from pj_sell_list where md5(SellId)='".$con_id."'";
				$ProDet=$this->ExecuteQuery($SelConId, "select");
			
				$AltPro = "Update pj_sell_list set ItemName='".$ItemName."',CatId='".$CatId."',ItemDesc='".$ItemDesc."',
						Specification='".$Specification."' Where md5(SellId)='".$con_id."'";
				$Altcheck=$this->ExecuteQuery($AltPro, "update");
				GeneralAdmin::MakeAltSEOUrlString('pj_sell_list','SellId','SEOItemName',$con_id,$ItemName); 
				
				for($t=0;$t<sizeof($_FILES['ItemImage']['name']);$t++){
					if($_FILES['ItemImage']['name'][$t]!='' && $_FILES['ItemImage']['error'][$t]==0) {	
						$path_info=pathinfo($_FILES['ItemImage']['name'][$t]);$extension=strtolower($path_info['extension']);
						$ItemImage=date("Ymd").time().rand(5,10000).'.'.$path_info['extension'];
 						$ResizeArr=array("300||330||l_".$ItemImage,"140||145||m_".$ItemImage,"65||65||t_".$ItemImage);
						GeneralAdmin::WesoUploadedFile('Image',$_FILES['ItemImage']['tmp_name'][$t],$PhotoLoc,$ItemImage,
											$ResizeArr);
						$InsImg="insert into pj_sell_images(SellId,ItemImage,ItemOrgName) values('".$ProDet[0]['SellId']."',
								'".$ItemImage."','".$_FILES['ItemImage']['name'][$t]."')";
						$this->ExecuteQuery($InsImg,"insert");
					}
				}	
 				$RedirStatus=true;$PageURL.='pro_msg=usucc'; 
 			}
			else
			{	
				$ProIns="insert into pj_sell_list(ItemName,CatId,ItemPostDate,ItemDesc,Specification) 
						values('".$ItemName."','".$CatId."',now(),'".$ItemDesc."','".$Specification."')";
 				$this->ExecuteQuery($ProIns,"insert");$SellId=mysql_insert_id();
				GeneralAdmin::MakeAltSEOUrlString('pj_sell_list','SellId','SEOItemName',$SellId,$ItemName);
 				 
				for($t=0;$t<sizeof($_FILES['ItemImage']['name']);$t++){
					if($_FILES['ItemImage']['name'][$t]!='' && $_FILES['ItemImage']['error'][$t]==0) {	
						$path_info=pathinfo($_FILES['ItemImage']['name'][$t]);$extension=strtolower($path_info['extension']);
						$ItemImage=date("Ymd").time().rand(5,10000).'.'.$path_info['extension'];
						$ResizeArr=array("300||330||l_".$ItemImage,"140||145||m_".$ItemImage,"65||65||t_".$ItemImage);
						GeneralAdmin::WesoUploadedFile('Image',$_FILES['ItemImage']['tmp_name'][$t],$PhotoLoc,$ItemImage,
											$ResizeArr);
						$InsImg="insert into pj_sell_images(SellId,ItemImage,ItemOrgName) values('".$SellId."',
								'".$ItemImage."','".$_FILES['ItemImage']['name'][$t]."')";
						$this->ExecuteQuery($InsImg,"insert");
					}
				}	
				$RedirStatus=true;$PageURL.='pro_msg=asucc';
			}
			if($RedirStatus) {
				Redirect($PageURL);
			}
			else {
				$objSmarty->assign("ErrMessage", "IT IS ALREADY UPDATED PLEASE DO SOME MODIFICATIONS TO UPDATE"); 
			}
		}
		else{	
			$objSmarty->assign("ErrMessage",implode('<br/>',$ErrMsg));$objSmarty->assign("Arr",$_POST);
		}	
	}	
	
	/****************************************		Get Product Lists		***********************************************/
 	function GetProductLists($ResVal,$PageVal)
	{	
 		global $objSmarty;$SelCon="";extract($_GET);$PageURL='product_mgmt.php?';$OrderCon=' order by PSL.SellId desc';
		$pronamesea=(isset($_GET['pronamesea']))?$_GET['pronamesea']:'';$catsea=(isset($_GET['catsea']))?$_GET['catsea']:'';
 
		if($pronamesea!=""){
			$SelCon.=" and PSL.ItemName LIKE '".trim($pronamesea)."%'";$PageURL.="seaname=".urlencode($pronamesea)."&";
		}		
		if($catsea!=""){
			$SelCon.=" and PC.CatName LIKE '".trim($catsea)."%'";$PageURL.="catname=".urlencode($catsea)."&";
		} 
		$objSmarty->assign("PageURL",$PageURL);
		$SelQuery="Select count(PSL.SellId) from pj_sell_list PSL left join pj_category_main PC on PC.CatId=PSL.CatId
				Where PSL.SellId!='' $SelCon $OrderCon ";
		$SelFields=array('PSL.*','PC.*');
		$SUsersList=GeneralAdmin::SelectTableContentLists($ResVal,$SelQuery,$SelFields,$PageVal,$PageURL,15);
 	} 	

	/**********************************		For Product Listing	*********************************************/
	function SelectProductLists()
	{
		global $objSmarty;$SelCon="";$PageURL=SiteMainPath.'stores.html';$OrderCon=' order by PRO.SellId desc';
		 
 		if(isset($_GET['search'])){ 
			$SelCon.=" and(PRO.ItemName LIKE '%".$_GET['search']."%' or PRO.ItemDesc LIKE '%".$_GET['search']."%' or 
						PRO.Specification LIKE '%".$_GET['search']."%' or PC.CatName LIKE '%".$_GET['search']."%')";  
		}
		 
		$cat_id=$_GET['cat_id'];
 		if($cat_id!=""){
			$SelCon.=" and md5(PRO.CatId)='".$cat_id."'";
		}  
		$SelQuery="Select PRO.*,PC.*,CONCAT('".SiteMainPath."','products/',SEOItemName,'.html') as ProductURL from pj_sell_list PRO
				left join pj_category_main PC on PC.CatId=PRO.CatId Where PRO.SellId!='' and PRO.ItemStatus='1' $SelCon $OrderCon";					
 		$ProList=$this->ExecuteQuery($SelQuery,"select");	 
		$objSmarty->assign('ProList',$ProList); 
 	}
	/**************************************		Select Posted Product Details		*****************************************/
	function SelectProductDetailsForView($act_type,$pro_name)
	{
		global $objSmarty,$ngconfig;
		$SelQry="SELECT PL.*,PC.* from pj_sell_list PL left join pj_category_main PC on PC.CatId=PL.CatId where 
				PL.SEOItemName='".$pro_name."' and PL.ItemStatus ='1' order by PL.SellId ";
		$SProDet=$this->ExecuteQuery($SelQry, "select");//echo "<PRE>"; print_r($SProDet);exit;
		if(sizeof($SProDet)!=0){
			$objSmarty->assign("SProDet",$SProDet[0]);
			
			$ProImg="select * from pj_sell_images where SellId='".$SProDet[0]['SellId']."'";
			$ProImgList=$this->ExecuteQuery($ProImg,"select");
			$objSmarty->assign("ImgList",$ProImgList);
			
 			/****************		Assign Page Title To Smarty		*************************/				
			$objSmarty->assign('PageTitle',stripslashes($SProDet[0]['ItemName']));
			$objSmarty->assign('SiteTitle','Products - '.stripslashes($SProDet[0]['ItemName']).' - '.SiteMainTitle);	
		}
		else{
			Redirect(SiteMainPath.'stores.html');	
		}
	} 
	
	/******************************* 		Function For Payment	*****************************************/
	function SubmitOrderToPaymentGatWay($CTotalAmt)
	{	
		global $objSmarty,$ngconfig;$ErrMsg='';$FlagIns=true;$SitePath=$ngconfig['SiteGlobalPath'];extract($_POST);
		$MailRegx= "/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/";
		
		$act_type=(isset($_GET['act_type'])) ? $_GET['act_type']:'';
		if($WebType=='Authorization'){
		if(empty($x_card_type)){
			$ErrMsg.='Please Select Your Card Type<br/>';$FlagIns=false;
		}
		if(empty($x_card_num)){
			$ErrMsg.='Please Enter Credit Card Number<br/>';$FlagIns=false;
		}		
		if(empty($month)){
			$ErrMsg.='Please Select the month<br/>';$FlagIns=false;
		}
		if(empty($year)){
			$ErrMsg.='Please Select the year<br/>';$FlagIns=false;
		}
		if(empty($x_card_code)){
			$ErrMsg.='Please Enter Credit Card CCV<br/>';$FlagIns=false;
		} 
 		if($FlagIns){	
		 
 		if(!empty($CTotalAmt)){
				$SelQry="select TU.*,ST.state_name,CN.country_name from pj_user_list TU left join
						pj_location_state ST on TU.UserState=ST.state_id left join pj_location_country CN on
						ST.country_ident=CN.country_id where TU.UserId='".$_SESSION['PJ525_UserId']."' and	ST.state_id!='' and 
						CN.country_id!='' ";
				$MLocDet=$this->ExecuteQuery($SelQry,"select");
						//echo "<PRE>"; print_r($MLocDet); echo $CTotalAmt;exit;
				$CTotalAmt=$CTotalAmt; 
				
				$creditCardType =urlencode($x_card_type);$creditCardNumber = urlencode($x_card_num);
				$exp_Month = str_pad($month, 2, '0', STR_PAD_LEFT);
				$x_exp_date=$exp_Month.$year;
				$cvv2Number = urlencode($x_card_code);
				$currencyCode="USD";$amount = urlencode(number_format($CTotalAmt,2));
				$UserName=$MLocDet[0]['UserName'];
  				$Email=$MLocDet[0]['UserEMail'];
				$address=$MLocDet[0]['UserAdds'];
				$Phone=$MLocDet[0]['UserPhone'];
				$CITY=$MLocDet[0]['UserCity'];
				$state=$MLocDet[0]['state_name'];
				$zip=$MemDet[0]['UserZip'];
				$COUNTRYCODE=$MLocDet[0]['country_name'];
				$ProdileInfo="FIRSTNAME=$UserName&STREET=$address1&CITY=$CITY&PHONE=$Phone&STATE=$state"
				."&ZIP=$zip&COUNTRYCODE=$COUNTRYCODE&";
             
							//	 Construct the request string that will be sent to PayPal.
//				The variable $nvpstr contains all the variables and is a name value pair string with & as a delimiter 
				
				$nvpstr="&PAYMENTACTION=Sale&AMT=$amount&CREDITCARDTYPE=$creditCardType&ACCT=$creditCardNumber&EXPDATE=".
				         $x_exp_date."&CVV2=$cvv2Number&".$ProdileInfo;
				
				// Make the API call to PayPal, using API signature.
//				The API response is stored in an associative array called $resArray 
				
				/****************			Class Objects					*************************/
				$ObjPaypal	=new PaypalClass;
 				$resArray=$ObjPaypal->hash_call("doDirectPayment",$nvpstr);
				
				if(is_array($resArray))
				{			
					$ack = strtoupper($resArray["ACK"]); 
					if($ack!="SUCCESS")
					{
						$ErrMsg='<table class="api" width=350>';
						foreach($resArray as $key => $value)
						{
							if (preg_match("/LONGMESSAGE/i",$key) || preg_match("/SHORTMESSAGE/i",$key)) {
								$ErrMsg.='<tr><td>'.$value.'</td></tr>';
							}	
						}
						$ErrMsg.='</table>';$PaymentCheck=false;
 					}
					else{	$PaymentCheck=true;		}		   		
				}
				else{	$PaymentCheck=false;$ErrMsg='Invalid Server Response.';	}
				 $objSmarty->assign("ErrorMessage",$ErrMsg);
				// echo "<PRE>";print_r($_POST);  echo $ErrMsg;exit;
				if($PaymentCheck){
 
					$InvoiceNum=$resArray["TRANSACTIONID"];
					$OrderRefNo=CreateRandomNumber(8);
					$SelProId="select SellId,ProQty from pj_item_baskets where PayId='0' and DownStatus!='1' and 
						BasketSession='".$_SESSION['PJ525_BasSession']."'";
					$ProIdDet=$this->ExecuteQuery($SelProId,"select");
						 
					$InsPur="INSERT INTO pj_item_payments(UserId,POrdDate,PayAmt,Paystatus,InvoiceNum,OrderRefNo,PayDate,PayType) 
							VALUES('".$_SESSION['PJ525_UserId']."',now(),'".number_format($CTotalAmt,2,'.','')."','1',
							'".$InvoiceNum."','".$OrderRefNo."',now(),'CreditCard')"; 
					$this->ExecuteQuery($InsPur,"insert");$BPayId=mysql_insert_id(); 
					
 					for($i=0;$i<sizeof($ProIdDet);$i++){
						$AltBas='Update pj_item_baskets Set DownStatus="1",PayId="'.$BPayId.'",OrderRefNo="'.$OrderRefNo.'"
									Where BasketSession="'.$_SESSION['PJ525_BasSession'].'" ';
						$this->ExecuteQuery($AltBas,"update");
					}
					
					$SelProBas="SELECT TP.ItemName,TP.SellId,TB.BasketSession,TB.PayId,TB.OrderRefNo,TB.ProPrice,
						TB.ProQty,TB.ProAddDate from pj_sell_list TP left join pj_item_baskets TB on
						TB.SellId=TP.SellId where TB.SellId!='' and TB.BasketSession='".$_SESSION['PJ525_BasSession']."'";
					$BResult=$this->ExecuteQuery($SelProBas,"select"); 
					
 					$Subject=$InvoiceNum." Purchase Details From ".SiteMainTitle;
					
						$mail_Det='<table width="800" border="0" cellspacing="0" cellpadding="0"><tr><td width="700" valign="top">
							<h1 style="padding:0px; margin:0px; font:bold 16px Arial, Helvetica; color:#d64444;line-height:25px;">
								Hi '.$MLocDet[0]['UserName'].',</h1>							
							<p style="padding:5px 0px 10px 0px; margin:0px; font:13px Arial; line-height:25px;padding-left:10px;">
							Thank you for your order. Order Invoice Number - 
							<span style="color:#009900; margin:0px;padding:10px 0px;">'.$InvoiceNum.'.</span></p>
							</td></tr></table>
							<p style="margin:0px;padding:5px 0px;">If you have further questions or additional concerns, 
							please contacts via e-mail or give us a call and we will address it quickly. If  not, simply sit back
							and anxiously wait for our products, which we are sure you will enjoy.</p>
							<p style="margin:0px;padding:5px 0px;">Thank you again for ordering with '.SiteMainTitle.'. We look 
								forward to serving you, your family and friends, and making you all long  lasting customers. </p>										
								<div style="clear:both;"></div>
 								
				<table style="margin-bottom:30px; float:left;" width="655" border="1" cellspacing="0" cellpadding="0">
					<tr>
						<td height="25" style="border:1px solid #A8A498; padding:5px;">Sl.No</td>
						<td style="border:1px solid #A8A498; padding:5px;">Product Name</td>
						<td style="border:1px solid #A8A498; padding:5px;">Order Ref No.</td> 
						<td style="border:1px solid #A8A498; padding:5px;">Price</td>
						<td style="border:1px solid #A8A498; padding:5px;">Qty</td>
						<td style="border:1px solid #A8A498; padding:5px;">Total</td> 
					</tr>';
					$i=1;
					foreach($BResult as $BasDet){
 		$mail_Det.='<tr>
		<td width="8%" height="25" style="border:1px solid #A8A498; padding:5px;">'.$i++.'</td>
		<td width="35%" style="border:1px solid #A8A498; padding:5px;">'.$BasDet['ItemName'].'</td>
		<td width="15%" style="border:1px solid #A8A498; padding:5px;">'.$BasDet['OrderRefNo'].'</td> 
		<td width="15%" style="border:1px solid #A8A498; padding:5px;">'.number_format($BasDet['ProPrice'],2).'</td>
		<td width="15%" style="border:1px solid #A8A498; padding:5px;">'.$BasDet['ProQty'].'</td>
		<td width="15%" style="border:1px solid #A8A498; padding:5px;">'.number_format(($BasDet['ProPrice']*$BasDet['ProQty']),2).'</td> 
		</tr>';
					}
		$mail_Det.='<tr>
		<td width="8%" height="25" style="border:1px solid #A8A498; padding:5px;">&nbsp;</td> 
		<td width="15%" style="border:1px solid #A8A498; padding:5px;">&nbsp;</td>
		<td width="15%" style="border:1px solid #A8A498; padding:5px;">&nbsp;</td>
		<td width="15%" style="border:1px solid #A8A498; padding:5px;">&nbsp;</td>
		<td width="15%" style="border:1px solid #A8A498; padding:5px;">Total</td>
		<td width="15%" style="border:1px solid #A8A498; padding:5px;">'.number_format($CTotalAmt,2).'</td> 
		</tr></table><br>';
						$headers  = "MIME-Version: 1.0\r\n";
						$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
						$headers .= 'From: '.SiteMainTitle.'<'.AdminMail."> \r\n";
					 
						GeneralAdmin::WesoIshMailTemplate($MLocDet[0]['UserEMail'],$Subject,$mail_Det,$headers);
						unset($_SESSION['PJ525_BasSession']);
						Redirect(SiteMainPath.'transactionhistory.html?user_act=bupdsucc');
 				}
				else{
					$objSmarty->assign("ErrorMessage", $ErrMsg);$objSmarty->assign('ArrPost', $_POST);
				}				
			}
		}
		else{	
			$objSmarty->assign('ErrorMessage',$ErrMsg);	$objSmarty->assign('ArrDet',$_POST);
		}
		
	  }
	  	elseif($WebType=='Bank') { 
			$SelQry="select TU.*,ST.state_name,CN.country_name from pj_user_list TU left join
					pj_location_state ST on TU.UserState=ST.state_id left join pj_location_country CN on
					ST.country_ident=CN.country_id where TU.UserId='".$_SESSION['PJ525_UserId']."' and	ST.state_id!='' and 
					CN.country_id!='' ";
			$MLocDet=$this->ExecuteQuery($SelQry,"select");  
 			 
			$OrderRefNo=CreateRandomNumber(8);	 
			$SelProId="select SellId,ProQty from pj_item_baskets where PayId='0' and DownStatus!='1' and 
				BasketSession='".$_SESSION['PJ525_BasSession']."'";
			$ProIdDet=$this->ExecuteQuery($SelProId,"select");
			$InsPur="INSERT INTO pj_item_payments(UserId,POrdDate,PayAmt,Paystatus,PayDate,PayType,OrderRefNo,Paystatus) 
					VALUES('".$_SESSION['PJ525_UserId']."',now(),'".number_format($CTotalAmt,2,'.','')."','0',now(),'Bank',
					'".$OrderRefNo."','1')"; 
			$this->ExecuteQuery($InsPur,"insert");$BPayId=mysql_insert_id();
			
			for($i=0;$i<sizeof($ProIdDet);$i++){
				$AltBas='Update pj_item_baskets Set DownStatus="1",PayId="'.$BPayId.'",OrderRefNo="'.$OrderRefNo.'"
						Where BasketSession="'.$_SESSION['PJ525_BasSession'].'" ';
				$this->ExecuteQuery($AltBas,"update");
			}
			$SelProId="select SellId,ProQty from pj_item_baskets where PayId='0' and DownStatus!='1' and 
					BasketSession='".$_SESSION['PJ525_BasSession']."'";
			$ProIdDet=$this->ExecuteQuery($SelProId,"select"); 
			
			$SelProBas="SELECT TP.ItemName,TP.SellId,TB.BasketSession,TB.PayId,TB.OrderRefNo,TB.ProPrice,
				TB.ProQty,TB.ProAddDate from pj_sell_list TP left join pj_item_baskets TB on
				TB.SellId=TP.SellId where TB.SellId!='' and TB.BasketSession='".$_SESSION['PJ525_BasSession']."'";
			$BResult=$this->ExecuteQuery($SelProBas,"select");
			$Subject1="Purchase Details From ".SiteMainTitle;
					
			$mail_Det1='<table width="800" border="0" cellspacing="0" cellpadding="0"><tr><td width="700" valign="top">
				<h1 style="padding:0px; margin:0px; font:bold 16px Arial, Helvetica; color:#d64444;line-height:25px;">
					Hi Admin,</h1>							
				<p style="padding:5px 0px 10px 0px; margin:0px; font:13px Arial; line-height:25px;padding-left:10px;">
				Review the below Order details by Mr.'.$MLocDet[0]['UserName'].' from Bank Transfer.</p>
				</td></tr></table>
				<p style="margin:0px;padding:5px 0px;">'.$_POST['comment'].'.</p>
				 										
					<div style="clear:both;"></div>
 								
				<table style="margin-bottom:30px; float:left;" width="655" border="1" cellspacing="0" cellpadding="0">
					<tr>
						<td height="25" style="border:1px solid #A8A498; padding:5px;">Sl.No</td>
						<td style="border:1px solid #A8A498; padding:5px;">Product Name</td>
						<td style="border:1px solid #A8A498; padding:5px;">Order Ref No.</td> 
						<td style="border:1px solid #A8A498; padding:5px;">Price</td>
						<td style="border:1px solid #A8A498; padding:5px;">Qty</td>
						<td style="border:1px solid #A8A498; padding:5px;">Total</td> 
					</tr>';
					$i=1;
					foreach($BResult as $BasDet){
 		$mail_Det1.='<tr>
		<td width="8%" height="25" style="border:1px solid #A8A498; padding:5px;">'.$i++.'</td>
		<td width="35%" style="border:1px solid #A8A498; padding:5px;">'.$BasDet['ItemName'].'</td>
		<td width="15%" style="border:1px solid #A8A498; padding:5px;">'.$BasDet['OrderRefNo'].'</td> 
		<td width="15%" style="border:1px solid #A8A498; padding:5px;">'.number_format($BasDet['ProPrice'],2).'</td>
		<td width="15%" style="border:1px solid #A8A498; padding:5px;">'.$BasDet['ProQty'].'</td>
		<td width="15%" style="border:1px solid #A8A498; padding:5px;">'.number_format(($BasDet['ProPrice']*$BasDet['ProQty']),2).'</td> 
		</tr>';
					}
		$mail_Det1.='<tr>
		<td width="8%" height="25" style="border:1px solid #A8A498; padding:5px;">&nbsp;</td> 
		<td width="15%" style="border:1px solid #A8A498; padding:5px;">&nbsp;</td>
		<td width="15%" style="border:1px solid #A8A498; padding:5px;">&nbsp;</td>
		<td width="15%" style="border:1px solid #A8A498; padding:5px;">&nbsp;</td>
		<td width="15%" style="border:1px solid #A8A498; padding:5px;">Total</td>
		<td width="15%" style="border:1px solid #A8A498; padding:5px;">'.number_format($CTotalAmt,2).'</td> 
		</tr></table><br>';
						$headers1  = "MIME-Version: 1.0\r\n";
						$headers1 .= "Content-type: text/html; charset=iso-8859-1\r\n";
						$headers1 .= 'From: '.SiteMainTitle.'<'.$MLocDet[0]['UserEMail']."> \r\n";
				//echo 	 $mail_Det1;exit;
						GeneralAdmin::WesoIshMailTemplate(AdminMail,$Subject1,$mail_Det1,$headers1);
			$Subject="Purchase Details From ".SiteMainTitle;
					
			$mail_Det='<table width="800" border="0" cellspacing="0" cellpadding="0"><tr><td width="700" valign="top">
				<h1 style="padding:0px; margin:0px; font:bold 16px Arial, Helvetica; color:#d64444;line-height:25px;">
					Hi '.$MLocDet[0]['UserName'].',</h1>							
				<p style="padding:5px 0px 10px 0px; margin:0px; font:13px Arial; line-height:25px;padding-left:10px;">
				Thank you for your order.</p>
				</td></tr></table>
				<p style="margin:0px;padding:5px 0px;">If you have further questions or additional concerns, 
				please contacts via e-mail or give us a call and we will address it quickly. If  not, simply sit back
				and anxiously wait for our products, which we are sure you will enjoy.</p>
				<p style="margin:0px;padding:5px 0px;">Thank you again for ordering with '.SiteMainTitle.'. We look 
					forward to serving you, your family and friends, and making you all long  lasting customers. </p>										
					<div style="clear:both;"></div>
 								
				<table style="margin-bottom:30px; float:left;" width="655" border="1" cellspacing="0" cellpadding="0">
					<tr>
						<td height="25" style="border:1px solid #A8A498; padding:5px;">Sl.No</td>
						<td style="border:1px solid #A8A498; padding:5px;">Product Name</td>
						<td style="border:1px solid #A8A498; padding:5px;">Order Ref No.</td> 
						<td style="border:1px solid #A8A498; padding:5px;">Price</td>
						<td style="border:1px solid #A8A498; padding:5px;">Qty</td>
						<td style="border:1px solid #A8A498; padding:5px;">Total</td> 
					</tr>';
					$i=1;
					foreach($BResult as $BasDet){
 		$mail_Det.='<tr>
		<td width="8%" height="25" style="border:1px solid #A8A498; padding:5px;">'.$i++.'</td>
		<td width="35%" style="border:1px solid #A8A498; padding:5px;">'.$BasDet['ItemName'].'</td>
		<td width="15%" style="border:1px solid #A8A498; padding:5px;">'.$BasDet['OrderRefNo'].'</td> 
		<td width="15%" style="border:1px solid #A8A498; padding:5px;">'.number_format($BasDet['ProPrice'],2).'</td>
		<td width="15%" style="border:1px solid #A8A498; padding:5px;">'.$BasDet['ProQty'].'</td>
		<td width="15%" style="border:1px solid #A8A498; padding:5px;">'.number_format(($BasDet['ProPrice']*$BasDet['ProQty']),2).'</td> 
		</tr>';
					}
		$mail_Det.='<tr>
		<td width="8%" height="25" style="border:1px solid #A8A498; padding:5px;">&nbsp;</td> 
		<td width="15%" style="border:1px solid #A8A498; padding:5px;">&nbsp;</td>
		<td width="15%" style="border:1px solid #A8A498; padding:5px;">&nbsp;</td>
		<td width="15%" style="border:1px solid #A8A498; padding:5px;">&nbsp;</td>
		<td width="15%" style="border:1px solid #A8A498; padding:5px;">Total</td>
		<td width="15%" style="border:1px solid #A8A498; padding:5px;">'.number_format($CTotalAmt,2).'</td> 
		</tr></table><br>';
						$headers  = "MIME-Version: 1.0\r\n";
						$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
						$headers .= 'From: '.SiteMainTitle.'<'.AdminMail."> \r\n";
					 
						GeneralAdmin::WesoIshMailTemplate($MLocDet[0]['UserEMail'],$Subject,$mail_Det,$headers);
						unset($_SESSION['PJ525_BasSession']);  
			Redirect(SiteMainPath.'transactionhistory.html?user_act=banksucc');
  		}
	  else {
	  	$SelProId="select SellId,ProQty from pj_item_baskets where PayId='0' and DownStatus!='1' and 
						BasketSession='".$_SESSION['PJ525_BasSession']."'";
		$ProIdDet=$this->ExecuteQuery($SelProId,"select"); 
		
		$SelProBas="SELECT TP.ItemName,TP.SellId,TB.BasketSession,TB.PayId,TB.OrderRefNo,TB.ProPrice,
			TB.ProQty,TB.ProAddDate from pj_sell_list TP left join pj_item_baskets TB on
			TB.SellId=TP.SellId where TB.SellId!='' and TB.BasketSession='".$_SESSION['PJ525_BasSession']."'";
		$BResult=$this->ExecuteQuery($SelProBas,"select");  
  						
	     $SelDet="select * from pj_payment_settings where PayTypeId!=''";
		 $PaypalDet=$this->ExecuteQuery($SelDet,"select");
		$OrderRefNo=CreateRandomNumber(8);	 
		$InsPur="INSERT INTO pj_item_payments(UserId,POrdDate,PayAmt,OrderRefNo,Paystatus,PayDate,PayType) 
				VALUES('".$_SESSION['PJ525_UserId']."',now(),'".number_format($CTotalAmt,2,'.','')."','".$OrderRefNo."',
				'0',now(),'PayPal')"; 
		$this->ExecuteQuery($InsPur,"insert");$BPayId=mysql_insert_id();
					
		for($i=0;$i<sizeof($ProIdDet);$i++){
			$AltBas='Update pj_item_baskets Set DownStatus="1",PayId="'.$BPayId.'",OrderRefNo="'.$OrderRefNo.'"
						Where BasketSession="'.$_SESSION['PJ525_BasSession'].'" ';
			$this->ExecuteQuery($AltBas,"update");
		} 
		 foreach($BResult as $result){
				$OldName[]=$result['ItemName'];
			}
			
		if(sizeof($OldName)>0){
		$OFieldValue=implode(',',$OldName); 
		}
		
			
			if($PaypalDet[0]['ServerType'] == '0')
				$PaypalURL = 'https://www.sandbox.paypal.com/row/cgi-bin/webscr';
			else
				$PaypalURL = 'https://www.paypal.com/row/cgi-bin/webscr';
			
			echo "<html>\n";
			echo "<head><title>Processing Payment...</title></head>\n";
			echo "<body onLoad=\"document.form.submit();\">\n";
			echo "<center><h3><strong>Please wait, your order is being processed.".
					"Do not refresh your browser...</strong></h3></center>\n";
			echo "<div align='center'><img src='".SiteMainPath."images/loader.gif' border='0' /></div>\n";
			echo "<form method=\"post\" name=\"form\" action=\"".$PaypalURL."\">\n"; ?>
			<input type="hidden" name="cmd" value="_xclick">
			<input type="hidden" name="business" value="<?php echo $PaypalDet[0]['PayPalMail']; ?>">
			<input type="hidden" name="currency_code" value="USD">
			<input type="hidden" name="rm" value="2">
			<input type="hidden" name="amount" value="<?php echo number_format($CTotalAmt,2); ?>">
			<input type="hidden" name="item_number" value="<?php echo md5($BPayId); ?>">
		    <input type="hidden" name="item_name" value="Payment for <?php echo stripslashes($OFieldValue).' Product';?>">
			<input type="hidden" name="return" 
			value="<?php echo SiteMainPath.'cart/myitems.html?user_act=bupdsucc'; ?>" />
			<input type="hidden" name="cancel_return" value="<?php echo SiteMainPath.'cart/myitems.html?user_act=fail'; ?>" />
			<?php
			echo "</form>\n";
			echo "</body></html>\n"; 
		}
	}
		
}

?>