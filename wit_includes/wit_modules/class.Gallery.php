<?php 	
class Place extends MysqlFns
{
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	/////////////////////////////////////	 			For User Panel 	 	///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
		/*************************** 			Function For User  Details		***************************/
	function SelectSitePlaceListsForUserpanel($ResVal,$PageVal)
	{
		global $objSmarty,$objLang;$SelCon="";$PageURL='containerhome.html?';$OrderCon=' order by PL.ImageId desc';
		$SelFields=array('PL.*');	
		$SelQuery="SELECT count(PL.ImageId) from ".$objLang->tableName('pj_galleryimage_list',true)." PL where PL.ImageId!=''and PL.ImageStatus='1' $OrderCon";
		GeneralAdmin::SelectTableContentLists($ResVal,$SelQuery,$SelFields,$PageVal,$PageURL,8);
 	}

	
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	/////////////////////////////////////	 			For Admin Panel 	 	///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  	/*************************** 			Function For User  Details		***************************/
	function SelectSitePlaceLists($ResVal,$PageVal)
	{
		global $objSmarty,$objLang;$SelCon="";$PageURL='gallery_mgmt.php?';$OrderCon=' order by PL.ImageId desc';
		 
		$pronamesea=(isset($_GET['pronamesea'])) ? $_GET['pronamesea']:''; 
        $code=(isset($_GET['code'])) ? $_GET['code']:'';	 	
		 
		if($pronamesea!=""){
			$SelCon.=" and PL.Title LIKE '".trim($pronamesea)."%'";$PageURL.="seaname=".urlencode($pronamesea)."&";
		}		
		if($code!=""){
			$SelCon.=" and PL.URL LIKE '".trim($code)."%'";$PageURL.="code=".urlencode($code)."&";
		} 
		$objSmarty->assign("PageURL",$PageURL);
 		 
		/*************	Fields To select Table listing	****************************************/
		$SelFields=array('PL.*');	
		$SelQuery="SELECT count(PL.ImageId) from ".$objLang->tableName('pj_galleryimage_list',true)." PL where PL.ImageId!='' $SelCon $OrderCon";
		GeneralAdmin::SelectTableContentLists($ResVal,$SelQuery,$SelFields,$PageVal,$PageURL,10);
 	}

  	/*************************** 			Function For  User Details		***************************/
	function SelectGalList()
	{
		global $objSmarty,$objLang; 
		$SelQuery="SELECT PL.* from ".$objLang->tableName('pj_galleryimage_list',true)." PL where PL.ImageId!='' order by PL.ImageId desc";
		$SelList=$this->ExecuteQuery($SelQuery, "select"); 	    
  		$objSmarty->assign("GalList",$SelList);	    
	}
   
   /*************************** 				Function For  Admin Edit Users Details			***************************/
	function AdminUpdatePlaceDetails($con_id)
	{	
		global $objSmarty,$objLang,$ngconfig;extract($_POST);
		$FExtArr=array('gif','png','jpg','jpeg');
		$ErrMsg=array();$ImgUpload=false;$banIng='';$product_image='';$PhotoLoc=$ngconfig['GalleryImgLoad'];
		$where_con='';$RedirStatus=false;   
		
 		if(empty($Title)){
			$ErrMsg[]= "Please enter Title";
		}
		if(empty($URL)){
			$ErrMsg[]= "Please enter URL";
		}  
		if(empty($con_id) && $_FILES['GalleryImage']['name']==''){
			$ErrMsg.= "Please Select Image File ";$FlagIns=false;	
		}
		elseif($_FILES['GalleryImage']['name']!=''){
			list($ImgWidth, $ImgHeight) = getimagesize($_FILES['GalleryImage']['tmp_name']);
			if($_FILES['GalleryImage']['name']=='')
			{
				$ErrMsg[]="Please Select Gallery Image.<br/>";$flag_ins=false;
			} 
		} 
		
 		if(count($ErrMsg)==0) { 
		
			if($AdminAction!="Update"){ // if (insert)
				
				$RedirStatus=true;
				 if($_FILES['GalleryImage']['name']!='' && $_FILES['GalleryImage']['error']==0) {	
					$path_info=pathinfo($_FILES['GalleryImage']['name']);$extension = strtolower($path_info['extension']);
					$GalleryImage=date("Ymd").time().rand(5,10000).'.'.$path_info['extension'];
					$ResizeArr=array("250||250||l_".$GalleryImage,"170||170||m_".$GalleryImage,"80||80||t_".$GalleryImage);
					$ImgSuc=GeneralAdmin::WesoUploadedFile('Image',$_FILES['GalleryImage']['tmp_name'],$PhotoLoc,$GalleryImage,$ResizeArr);							
				}
				
 			 	$InsDet="insert into pj_galleryimage_list (Title,URL,GalleryImage) values('".$Title."','".$URL."',
						'".$GalleryImage."')";  
				$this->ExecuteQuery($InsDet, "insert");
				 $ImageId=mysql_insert_id();
 			 	$InsDetLang="insert into ".$objLang->tableName('pj_galleryimage_list','french')." (Title,URL,GalleryImage) values('".$Title."','".$URL."',
						'".$GalleryImage."')";  
				$this->ExecuteQuery($InsDetLang, "insert");
				 $ImageIdLang=mysql_insert_id();
				/* if($_FILES['GalleryImage']['name']!='' && $_FILES['GalleryImage']['error']==0)
					{	
						$path_info=pathinfo($_FILES['GalleryImage']['name']);$extension = strtolower($path_info['extension']);
						$GalleryImage=date("Ymd").time().rand(5,10000).'.'.$path_info['extension'];
						$ResizeArr=array("250||250||l_".$GalleryImage,"120||120||m_".$GalleryImage,"80||80||t_".$GalleryImage);
						$ImgSuc=GeneralAdmin::WesoUploadedFile('Image',$_FILES['GalleryImage']['tmp_name'],$PhotoLoc,$GalleryImage,$ResizeArr);							
						if($_FILES['error']==0){	
							$SelQuery="SELECT GalleryImage from pj_galleryimage_list where ImageId='".$ImageId."' ";
							$Result=$this->ExecuteQuery($SelQuery, "select");
							if($Result[0]['GalleryImage']!=''){ 
								$FileDelete=array($PhotoLoc.$Result[0]['GalleryImage']);
								GeneralAdmin::WesoFileFolderDelete($FileDelete);	
							}
							$AltImg=",GalleryImage='".$GalleryImage."' ";
						}
					}*/
			
					Redirect('gallery_mgmt.php?pro_msg=asucc');
			}
			else{
				
				$SelQry="SELECT * from ".$objLang->tableName('pj_galleryimage_list',true)." where md5(ImageId)='".$con_id."'";
				$SelDet=$this->ExecuteQuery($SelQry, "select"); 
				if(sizeof($SelDet)>0){	 
					/************************** Admin User Update	*************************************/
					if($_FILES['GalleryImage']['name']!='' && $_FILES['GalleryImage']['error']==0)
					{	
						$path_info=pathinfo($_FILES['GalleryImage']['name']);$extension = strtolower($path_info['extension']);
						$GalleryImage=date("Ymd").time().rand(5,10000).'.'.$path_info['extension'];
						$ResizeArr=array("250||250||l_".$GalleryImage,"170||170||m_".$GalleryImage,"80||80||t_".$GalleryImage);
						$ImgSuc=GeneralAdmin::WesoUploadedFile('Image',$_FILES['GalleryImage']['tmp_name'],$PhotoLoc,$GalleryImage,$ResizeArr);						
						if($_FILES['error']==0){	
							$SelQuery="SELECT GalleryImage from pj_galleryimage_list where ImageId='".$SelDet[0]['ImageId']."' ";
							$Result=$this->ExecuteQuery($SelQuery, "select");
							if($Result[0]['GalleryImage']!=''){ 
								$FileDelete=array($PhotoLoc.$Result[0]['GalleryImage']);
								GeneralAdmin::WesoFileFolderDelete($FileDelete);	
							}	
							$AltImg=",GalleryImage='".$GalleryImage."' ";
						}
					}
					
					$AltCon="Update ".$objLang->tableName('pj_galleryimage_list',true)." set Title='".$Title."',URL='".$URL."'$AltImg Where 
							ImageId='".$SelDet[0]['ImageId']."'";
					$AltCheck=$this->ExecuteQuery($AltCon, "update");
					
					if(!empty($AltCheck) || !empty($checkins)){
					Redirect('gallery_mgmt.php?pro_msg=usucc');	
					}
					else{
						$objSmarty->assign("ErrorMessage", "IT IS ALREADY UPDATED PLEASE DO SOME MODIFICATIONS TO UPDATE"); 
					}
 				}
				else{
					Redirect($PageURL.'pro_msg=vfail');
				}
			} 
			if($RedirStatus){
				Redirect($PageURL);
			}
			else{
				$objSmarty->assign("ErrMessage", "IT IS ALREADY UPDATED PLEASE DO SOME MODIFICATIONS TO UPDATE"); 
			}
		}
		else{
			$objSmarty->assign("ErrMessage",implode('</p><p>',$ErrMsg));$objSmarty->assign("Arr",$_POST);
		}
	}	 
	
	/*************************** 				Function For  Select Edit Details			***************************/
	function SelectPlaceDetailsForAdmin($con_id){
		global $objSmarty,$objLang;
		
 		$SelQry="SELECT * from ".$objLang->tableName('pj_galleryimage_list',true)." where ImageId!='' and md5(ImageId)='".$con_id."'";
 		$ArrDet=$this->ExecuteQuery($SelQry, "select"); 
		if(sizeof($ArrDet)>0){
			$objSmarty->assign("Arr",$ArrDet[0]);return true;
		}
		else{
			Redirect(SiteMainPath.'');
		}
 	}
	
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	/////////////////////////////////////	 			For User Panel 	 	///////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/*************************** 				Function For  Select Edit Details			***************************/
	function UserSelectPlaceList(){
			global $objSmarty;
			
		 $SelQuery="SELECT LC.country_name,LS.state_name,LS.state_id,LC.country_id from pj_location_state LS left join 
					 pj_location_country LC on 	LS.country_ident=LC.country_id where LC.country_id='98' and LS.state_id!='' 
					 order by LS.state_name asc ";
			$ResCat=$this->ExecuteQuery($SelQuery, "select"); 
			for($i=0;$i<sizeof($ResCat);$i++){
			$SelQry="SELECT * from pj_galleryimage_list  where ImageId!='' and ImageStatus='ON' and
					 PlaceState='".$ResCat[$i]['state_id']."'";
			$SelPlace1=$this->ExecuteQuery($SelQry, "select"); 
				if(sizeof($SelPlace1)>0){
						$HCatArr[$i]['state_name']=$ResCat[$i]['state_name'];
						$HCatArr[$i]['state_id']=$ResCat[$i]['state_id'];
						$HCatArr[$i]['BList']=$SelPlace1;
				}
			}

			$objSmarty->assign("HCatArr",$HCatArr);return true;
		}
	/*************************** 				Function For  Select Edit Details			***************************/		
	function UserSelectPlaceView($con_id){
		global $objSmarty;	$con_id=(isset($_GET['con_id']))?$_GET['con_id']:'';
		
 		$SelQry="SELECT * from pj_galleryimage_list  where ImageId!='' and ImageStatus='ON'and (md5(ImageId)='".$con_id."' or md5(PlaceState)='".$con_id."')";
 		$SelPlace=$this->ExecuteQuery($SelQry, "select"); 
		if(sizeof($SelPlace)>0){
			$objSmarty->assign("SelVPlace",$SelPlace);return true;
		}
		
	}
		/*************************** 				Function For  Select Edit Details			***************************/
	function SelectTourPackagesDetails($con_id)
	{
		global $objSmarty;	 $con_id=(isset($_GET['con_id']))?$_GET['con_id']:'';

	 	$SelQry="SELECT * from pj_galleryimage_list  where ImageId!='' and ImageStatus='ON' and md5(ImageId)='".$con_id."'";

		$SelUser=$this->ExecuteQuery($SelQry, "SELECT"); 
		if(sizeof($SelUser)>0){
					$objSmarty->assign("SelPlace",$SelUser[0]);return true;
		}
	}
}
?>