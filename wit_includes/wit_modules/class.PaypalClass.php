<?php
class PaypalClass extends MysqlFns{

	/***************************	Load Configuration Settings using Constructor 	**********************************************/
	function PaypalClass(){

		global $ngconfig,$API_Endpoint,$version,$API_UserName,$API_Password,$API_Signature,$nvp_Header,$subject, $AUTH_token,
		$AUTH_signature,$AUTH_timestamp;
		
		$SelPay="select * from pj_payment_settings where PayTypeId!=''";
		$PayDet=$this->ExecuteQuery($SelPay,"select");
		
		define('API_USERNAME',$PayDet[0]['API_Username']);
		define('API_PASSWORD',$PayDet[0]['API_Password']);
		define('API_SIGNATURE',$PayDet[0]['API_Signature']);
		
		if($PayDet[0]['ServerType'] == '1'){
			define('API_ENDPOINT',$ngconfig['TPaypal_ENDPOINT']);
			define('PAYPAL_URL',$ngconfig['PaypalURL']);
		}	
		else{
	
			define('API_ENDPOINT',$ngconfig['DPaypal_ENDPOINT']);
			define('PAYPAL_URL',$ngconfig['DemoPaypalURL']);
		}	

		define('SUBJECT','');
		define('USE_PROXY',FALSE);
		define('PROXY_HOST', '127.0.0.1');
		define('PROXY_PORT', '808');
		define('VERSION', '65.1');
		// Ack related constants
		define('ACK_SUCCESS', 'SUCCESS');
		define('ACK_SUCCESS_WITH_WARNING', 'SUCCESSWITHWARNING');
		

		$API_UserName=API_USERNAME;$API_Password=API_PASSWORD;
		
		$API_Signature=API_SIGNATURE;$API_Endpoint =API_ENDPOINT;
		
		$version=VERSION;$subject = SUBJECT;
		// below three are needed if used permissioning
		
		// below three are needed if used permissioning
		if(defined('AUTH_TOKEN'))
			$AUTH_token= AUTH_TOKEN;
		
		if(defined('AUTH_SIGNATURE'))
			$AUTH_signature=AUTH_SIGNATURE;
		
		if(defined('AUTH_TIMESTAMP'))
			$AUTH_timestamp=AUTH_TIMESTAMP;
				
	}


	function nvpHeader()
	{
		
		global $API_Endpoint,$version,$API_UserName,$API_Password,$API_Signature,$nvp_Header,$subject,$AUTH_token,$AUTH_signature,
			$AUTH_timestamp;$nvpHeaderStr = "";

		if(defined('AUTH_MODE')) {
			//$AuthMode = "3TOKEN"; //Merchant's API 3-TOKEN Credential is required to make API Call.
			//$AuthMode = "FIRSTPARTY"; //Only merchant Email is required to make EC Calls.
			//$AuthMode = "THIRDPARTY";Partner's API Credential and Merchant Email as Subject are required.
			$AuthMode = "AUTH_MODE"; 
		} 
		else {
		
			if((!empty($API_UserName)) && (!empty($API_Password)) && (!empty($API_Signature)) && (!empty($subject))) {
				$AuthMode = "THIRDPARTY";
			}
			
			else if((!empty($API_UserName)) && (!empty($API_Password)) && (!empty($API_Signature))) {
				$AuthMode = "3TOKEN";
			}
			
			elseif (!empty($AUTH_token) && !empty($AUTH_signature) && !empty($AUTH_timestamp)) {
				$AuthMode = "PERMISSION";
			}
			elseif(!empty($subject)) {
				$AuthMode = "FIRSTPARTY";
			}
		}	
		switch($AuthMode) {
		
		case "3TOKEN" : 
				
				$nvpHeaderStr = "&PWD=".urlencode($API_Password)."&USER=".urlencode($API_UserName)."&SIGNATURE=".
				urlencode($API_Signature);
				
			break;
		case "FIRSTPARTY" :
				$nvpHeaderStr = "&SUBJECT=".urlencode($subject);
			break;
		case "THIRDPARTY" :
				$nvpHeaderStr = "&PWD=".urlencode($API_Password)."&USER=".urlencode($API_UserName)."&SIGNATURE=".
				urlencode($API_Signature)."&SUBJECT=".urlencode($subject);
			break;		
		case "PERMISSION" :

				$nvpHeaderStr =$this->formAutorization($AUTH_token,$AUTH_signature,$AUTH_timestamp);
			break;
		}
		
		return $nvpHeaderStr;
	}
	/**
	  * hash_call: Function to perform the API call to PayPal using API signature
	  * @methodName is name of API  method.
	  * @nvpStr is nvp string.
	  * returns an associtive array containing the response from the server.
	*/
	function hash_call($methodName,$nvpStr)
	{
		global $API_Endpoint,$version,$API_UserName,$API_Password,$API_Signature,$nvp_Header,$subject, $AUTH_token,
		$AUTH_signature,$AUTH_timestamp;
		//declaring of global variables
		// form header string
		$nvpheader=$this->nvpHeader();

		//setting the curl parameters.
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$API_Endpoint);
		curl_setopt($ch, CURLOPT_VERBOSE, 1); 
	
		//turning off the server and peer verification(TrustManager Concept).
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_POST, 1);

		//in case of permission APIs send headers as HTTPheders
		if(!empty($AUTH_token) && !empty($AUTH_signature) && !empty($AUTH_timestamp))
		{
			$headers_array[] = "X-PP-AUTHORIZATION: ".$nvpheader;
	  
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers_array);
			curl_setopt($ch, CURLOPT_HEADER, false);//echo $AUTH_token.$AUTH_signature.$AUTH_timestamp;exit;
		}
		else{
			$nvpStr=$nvpheader.$nvpStr;
		}	
	
		// USE_PROXY constant set to TRUE in Constants.php, then only proxy will be enabled.
	   //Set proxy name to PROXY_HOST and port number to PROXY_PORT in constants.php 
		if(USE_PROXY)
			curl_setopt ($ch, CURLOPT_PROXY, PROXY_HOST.":".PROXY_PORT); 

		//check if version is included in $nvpStr else include the version.
		if(strlen(str_replace('VERSION=', '', strtoupper($nvpStr))) == strlen($nvpStr)){
			$nvpStr = "&VERSION=" . urlencode($version) . $nvpStr;	
		}//echo "<pre>";print_r($methodName);print_r($nvpStr);exit;
		
	 	$nvpreq="METHOD=".urlencode($methodName).$nvpStr;

		//setting the nvpreq as POST FIELD to curl
		curl_setopt($ch,CURLOPT_POSTFIELDS,$nvpreq);

		//getting response from server

		$response = curl_exec($ch);

		//convrting NVPResponse to an Associative Array

		$nvpResArray=$this->deformatNVP($response);
		$nvpReqArray=$this->deformatNVP($nvpreq);

		if (curl_errno($ch)) {
			 curl_error($ch);
		}
		else{
			curl_close($ch);
		}

		return $nvpResArray;
		
	}
	
	/** This function will take NVPString and convert it to an Associative Array and it will decode the response.
	  * It is usefull to search for a particular key and displaying arrays.
	  * @nvpstr is NVPString.
	  * @nvpArray is Associative Array.
	*/
	function deformatNVP($nvpstr)
	{ 
		$intial=0;$nvpArray = array();
		
		while(strlen($nvpstr)){
		//postion of Key
			$keypos= strpos($nvpstr,'=');

			//position of value
			 $valuepos = strpos($nvpstr,'&') ? strpos($nvpstr,'&'): strlen($nvpstr);
			/*getting the Key and Value values and storing in a Associative Array*/
			$keyval=substr($nvpstr,$intial,$keypos);
				$valval=substr($nvpstr,$keypos+1,$valuepos-$keypos-1);
			//decoding the respose
			$nvpArray[urldecode($keyval)] =urldecode( $valval);
			$nvpstr=substr($nvpstr,$valuepos+1,strlen($nvpstr));
			
		}
		
		return $nvpArray;
	}
	function formAutorization($auth_token,$auth_signature,$auth_timestamp)
	{
		$authString="token=".$auth_token.",signature=".$auth_signature.",timestamp=".$auth_timestamp ;
		return $authString;
	}
} 
?>