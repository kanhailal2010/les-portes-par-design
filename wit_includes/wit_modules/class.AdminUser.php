<?php
class AdminUsers extends MysqlFns
{
	/***************************	Common function for validate form Fields	*************************************/
	function FormRegxExpressionCehck($ValiType,$FieldVal){///field checking
		/*
			define('UInputCheck',"/^[A-Za-z0-9_-]{1,50}$/");define('FirstLastName',"/^[A-Za-z_[:blank:]]{1,50}$/");
		*/
		switch($ValiType){///if select any one ..
			case 'UserName':
					$UNameRegx="/^[a-z\d_]{5,20}$/i";	
					if(!preg_match($UNameRegx,$FieldVal))
					{	return false;	}
					else{	return true;	}
				break;
			case 'EMail':
					$MailRegx="/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/";	
					if(!preg_match($MailRegx,$FieldVal))
					{	return false;	}
					else{	return true;	}
				break;
			case 'Password':
					$PassRegx="/^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*$/";
					if(!preg_match($PassRegx,$FieldVal))
					{	return false;	}
					else{	return true;	}
				break;
			case 'OnlyNo':
				$UPhoneRegx="/^[0-9]{1,15}$/";
				if(!preg_match($UPhoneRegx,$FieldVal))
				{	return false;	}
				else{	return true;	}
			break;
		}
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	/*********************************************    Admin Panel  ********************************************/
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
	/*********************************************    Country List  ********************************************/
	function SelectConList()
	{	
  		global $objSmarty; 
  		$SelQuery="SELECT PC.* from pj_location_state PS left join pj_location_country PC on PC.country_id=PS.country_ident 
				where PC.country_id!=''  and PC.country_status='1' and PS.StateSta='1' 
				group by PC.country_id order by PC.country_name asc";
		$ConList=$this->ExecuteQuery($SelQuery, "SELECT"); 
		$objSmarty->assign("StateList",$ConList);
	}
	
	/************************************ Buyer User User List  *****************************/
		function SelectBuyersSellersLists($ResVal,$PageVal,$UType)
	{ 	
		global $objSmarty;$SelCon="";$OrderCon=' order by UL.UserId desc';
		$PageURL='users_management.php?';
		$seaname=(isset($_GET['seaname'])) ? $_GET['seaname']:''; $sortby=(isset($_GET['sortby'])) ? $_GET['sortby']:'';	
		$seamail=(isset($_GET['seamail'])) ? $_GET['seamail']:''; $seaPhone=(isset($_GET['seaPhone'])) ? $_GET['seaPhone']:'';
 		  		
		if($seaname!=""){
			$SelCon.=" and UL.UserName LIKE '".addslashes(trim($seaname))."%'";$PageURL.="seaname=".urlencode($seaname)."&";
		} 
		if($seamail!=""){
			$SelCon.=" and UL.UserEMail LIKE '".addslashes(trim($seamail))."%'";$PageURL.="seaname=".urlencode($seamail)."&";
		}
		if($seaPhone!=""){
			$SelCon.=" and UL.UserPhone LIKE '".addslashes(trim($seaPhone))."%'";$PageURL.="seaname=".urlencode($seaPhone)."&";
		}
		if($joindate!=""){
			$joindate= date('Y-m-d',strtotime("$joindate"));
			$SelCon.=" and DAY(UL.UJoinDate)=DAY('".$joindate."') and MONTH(UL.UJoinDate)=MONTH('".$joindate."') 
				and YEAR(UL.UJoinDate)=YEAR('".$joindate."')";
			$PageURL.="expire=".urlencode($expire)."&";
		}
		
		$SortArr=array('username'=>'UL.UserName ','email'=>'UL.UserEMail ','joindate'=>'UL.UJoinDate','phone'=>'UL.UserPhone');
						
		if(!empty($sortby)){
			$SortDet=explode('_',$sortby);
			if(array_key_exists($SortDet[0],$SortArr)){
				$OrderCon=" order by ".$SortArr[$SortDet[0]]." ".$SortDet[1]."";
			}
		}
		$objSmarty->assign("PageURL",$PageURL);
		if($UType=="Provider"){
			$JoinCon.="left join pj_provider_details PD on PD.UserId=UL.UserId";
			$SelFields=array('UL.*','LC.country_name','LS.state_name','PD.*');
		}
		else{
 			$SelFields=array('UL.*','LC.country_name','LS.state_name');
		}
		 $SelQuery="SELECT count(UL.UserId) from pj_user_list UL left join pj_location_state LS on UL.UserState=LS.state_id
		 			left join pj_location_country LC on LS.country_ident=LC.country_id $JoinCon where UL.UserId!='' $SelCon 
					$OrderCon"; 
		$SUsersList=GeneralAdmin::SelectTableContentLists($ResVal,$SelQuery,$SelFields,$PageVal,$PageURL,15);
		
	}
	

/******************************Add and update Patients Details ***************************/
	function AdminAddUpdateBuyersSellers($con_id,$PageURL,$UType)
	{	
 		global $objSmarty,$ngconfig;$ErrMsg=array();extract($_POST);$where_con='';$RedirStatus=false;
		$FLName='/[\'\/~`\!@#\$%\^&\*\(\)_\-\+=\{\}\[\]\|;:"\<\>,\.\?\\\]/'; 
		if(empty($UserFName)){
			$ErrMsg[]="Please enter first name";
		}
		elseif(preg_match($FLName,$UserFName)){	
			$ErrMsg[]='First Name not allowed special characters ';
		}
		if(empty($UserLName)){
			$ErrMsg[]="Please enter last name";
		}
		elseif(preg_match($FLName,$UserLName)){	
			$ErrMsg[]='Last Name not allowed special characters ';
		}
		elseif($UserLName==$UserFName){	
			$ErrMsg[]='first name and last name is same.';
		}
		if(empty($UserName)){
			$ErrMsg[]="Please enter user name";
		}
		elseif(preg_match($FLName,$UserName)){	
			$ErrMsg[]='User Name not allowed special characters ';
		}elseif($AdminAction!="Update"){
				$SelQuery="SELECT * from pj_user_list where UserName='".$UserName."' $where_con";
				if(!GeneralAdmin::CheckDupRecord($SelQuery)){
					$ErrMsg[]="An account using this user name already exists";
				}
			}		
		if(empty($UserPass)){
			$ErrMsg[]="Please enter password";
		}
		if(empty($UserEMail)){
			$ErrMsg[]="Please enter email";
		}
		if(empty($UserPayPalMail)){
			$ErrMsg[]="Please enter PayPal email";
		}
		else{
			if(!preg_match("/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/", $UserEMail)){
				$ErrMsg[]="Please enter valid email";
			}
			elseif($AdminAction!="Update"){
				$SelQuery="SELECT * from pj_user_list where UserEMail='".$UserEMail."' $where_con";
				if(!GeneralAdmin::CheckDupRecord($SelQuery)){
					$ErrMsg[]="An account using this email address already exists";
				}
			}	
		}
		if(empty($UserAdds))
			{	$ErrMsg[]="Please enter address";	}	
		/*if(empty($UserPhone))
			{	$ErrMsg[]="Please enter mobile number";	} */
		if(empty($UserCountry))
			{	$ErrMsg[]="Please select country";	}
		if(empty($UserState))
			{	$ErrMsg[]="Please select state";	}
		if(empty($UserCity))
			{	$ErrMsg[]="Please enter city";	}	
		if(empty($UserZip))
			{	$ErrMsg[]="Please enter zip";	} 
			

		if(count($ErrMsg)==0)
		{	//print_r($_FILES);exit;

			$MemPass=OurTechEncryptionDecryption('Encrypt',$UserPass);
			if($AdminAction!="Update") {
  				$InsDet="insert into pj_user_list(UserFName,UserLName,UserName,UserPass,UserEMail,UserPhone,
						UserState,UserCity,UserZip,UserAdds,UJoinDate,URegIP,UserSta,UserPayPalMail,UserImage,UserCountry)
						values('".$_POST['UserFName']."','".$_POST['UserLName']."','".$_POST['UserName']."',
						'".$MemPass."','".$_POST['UserEMail']."','".$_POST['UserPhone']."','".$_POST['UserState']."',
						'".$_POST['UserCity']."','".$_POST['UserZip']."','".$_POST['UserAdds']."',now(),
						'".$_SERVER['REMOTE_ADDR']."','1','".$_POST['UserPayPalMail']."','".$UserImage."',
						'".$_POST['UserCountry']."')"; 
				$this->ExecuteQuery($InsDet, "insert");$NewTemp=mysql_insert_id(); 
				Redirect("users_management.php?pro_msg=asucc"); 
			}
			else {	
				$SelQry="SELECT UserId,UserEMail from pj_user_list where md5(UserId)='".$con_id."'";
				$SUserDet=$this->ExecuteQuery($SelQry, "select"); 
				if(sizeof($SUserDet)>0) { 
  				 	 $AltCon="Update pj_user_list set UserFName='".$UserFName."',UserLName='".$UserLName."',
					 	UserName='".$UserName."',UserPass='".$MemPass."',UserEMail='".$UserEMail."',UserPhone='".$UserPhone."',
						UserCountry='".$UserCountry."',UserState='".$UserState."',UserCity='".$UserCity."',UserZip='".$UserZip."',
						UserAdds='".$UserAdds."',UserPayPalMail='".$UserPayPalMail."' Where md5(UserId)='".$con_id."'";
					$AltCheck=$this->ExecuteQuery($AltCon, "update");$NewTemp=$SUserDet[0]['UserId'];
 					 
					if(!empty($AltCheck)) {
						Redirect("users_management.php?pro_msg=usucc");
					} 
					else {
						$objSmarty->assign("ErrMessage", "IT IS ALREADY UPDATED PLEASE DO SOME MODIFICATIONS TO UPDATE"); 
					}
  				}
				else{	$objSmarty->assign("ErrMessage",implode('</p><p>',$ErrMsg));$objSmarty->assign("Arr",$_POST);	}
			}
 		}
		else
		{	$objSmarty->assign("ErrMessage",implode('</p><p>',$ErrMsg));$objSmarty->assign("Arr",$_POST);	}
 	}	
  }
	
?>