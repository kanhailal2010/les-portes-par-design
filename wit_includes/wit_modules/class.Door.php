<?php
	
	define('DOOR_INSERT_TABLE','door_insert');
	define('DOOR_PROFILE_TABLE','door_profile');
	define('DOOR_PROFILE_fINISH_TABLE','door_profile_finish');
	define('DOOR_SUB_INSERT_TABLE','door_sub_insert');
	define('SITE_SETTING_TABLE','pj_sitesettings');
	
	
	define('DOOR_FOLDER','door/');
	define('INSERT_FOLDER','insert/');
	define('SUB_INSERT_FOLDER','insert/subinsert/');
	define('PROFILE_FINISH_FOLDER','profilefinish/');
	
	// Thumbnail images width
	define('RESIZE_IMAGE_WIDTH',800); // If resolution width greater than 800px
	define('THUMBNAIL_IMAGES_WIDTH',200);
	
	// Thumbnail folder name
	define('THUMBNAIL_FOLDER','thumbnail/');
	
	define('FILE_NOT_UPLOADED','File could not be uploaded');
	define('FILE_UPLOADED','File Uploaded Successfully');
	
	define('FILE_NOT_ALLOWED','file is not allowed');
	
	// Invoice		
	define('DOOR_INVOICE_TABLE','door_invoice');
	define('PAYMENT_PENDING','pending');

class Door extends MysqlFns
{

private $debug = 0;

	// convert object to array
	function objectToArray( $obj ) 
	{
		if(is_object($obj)) $obj = (array) $obj;
		if(is_array($obj)) {
		  $new = array();
		  foreach($obj as $key => $val) {
			$new[$key] = $this->objectToArray($val);
		  }
		}
		else { 
		  $new = $obj;
		}
		return $new;
	}
	
	// show profile dropdown 
	// used in profile_insert
	function profile_dropdown($selectedArray=array())
	{
		$profileType = $this->profile_factor();
			//var_dump($profileType);
			
			$profile_dropdown = '<select name="profile_type[]" multiple size="6">';
			$profile_dropdown .= "<option value='' selected > none </option>";
			foreach($profileType as $prot)
			{	
				$s = (!empty($selectedArray) && in_array($prot['code'],$selectedArray)) ? "selected" : '';
				$profile_dropdown .= "<option value='".$prot['code']."' $s >".$prot['name']."</option>";
			}
			$profile_dropdown .= "</select>";
		return $profile_dropdown;
	}
	
	
	// for fetching all profile factors
	// optional parameter: profileType Code
	// returns all factors if parameter is null
	function profile_factor($profileType="",$finishType='')
	{
		global $objLang;
		
		$sql = "SELECT  * FROM ".$objLang->tableName(DOOR_PROFILE_TABLE,true);
		
		if($profileType!='')
		$sql .=" WHERE code = '".$profileType."'";
		
		$result = $this->ExecuteQuery($sql, "select") or die(mysql_error());
		//var_dump($result);
		if(sizeof($result)!=0)
		{
			if($profileType!='')
			{	
				$finishType = ($finishType!='') ? $finishType : 'Natural';
				$factor 	= strtolower(substr($finishType,0,1));
				$factor 	= $factor.'factor';
				return $result[0][$factor];
			}
			else
			return $result;
		}
		else
			Redirect(SiteMainPath.'stores.html');	
	}

	// for fetching all profile factors
	// optional parameter: profileType Code
	// returns all factors if parameter is null
	function fetch_profile($profileId="")
	{
		global $objLang;
		$sql = "SELECT  * FROM ".$objLang->tableName(DOOR_PROFILE_TABLE,true);
		
		if($profileId!='')
		$sql .=" WHERE profile_id= '".$profileId."'";
		
		$result = mysql_query($sql) or die(mysql_error());
		//echo mysql_num_rows($result);
		if(mysql_num_rows($result)>0)
		{
			if($profileId!='')
			{
				$row = mysql_fetch_assoc($result);
				return $row;
			}
			else
			{
				$row = array();
				while($r = mysql_fetch_assoc($result))
				$row[] = $r;
				return $row;
			}
		}
	}
	
	
	
	// for fetching all insert factors by title
	// optional parameter: insertType Code
	// returns all factors if parameter is null
	function insert_factor($insertType="")
	{
		global $objSmarty,$ngconfig,$objLang;
		
		$sql = "SELECT  * FROM ".$objLang->tableName(DOOR_INSERT_TABLE,true);
		
		if($insertType!='')
		$sql .=" WHERE insert_code = '".$insertType."'";
		
		$result = $this->ExecuteQuery($sql, "select") or die(mysql_error());
		
		if(sizeof($result)!=0)
		{
			//var_dump($result);
			if($insertType!='')
			return $result[0]['insert_factor'];
			else
			return $result;
		}
		else
			Redirect(SiteMainPath.'stores.html');	
	}
	
	function fetch_insert($insertId="")
	{
		global $objLang;
		$sql = "SELECT  * FROM ".$objLang->tableName(DOOR_INSERT_TABLE,true);
		
		if($insertId!='')
		$sql .=" WHERE insert_id= '".$insertId."'";
		
		$result = mysql_query($sql) or die(mysql_error());
		//echo mysql_num_rows($result);
		if(mysql_num_rows($result)>0)
		{
			if($insertId!='')
			{
				$row = mysql_fetch_assoc($result);
				return $row;
			}
			else
			{
				$row = array();
				while($r = mysql_fetch_assoc($result))
				$row[] = $r;
				return $row;
			}
		}
	}
	
	// for fetching all sub insert 
	// optional parameter: subInsertId Code
	// returns all factors if parameter is null
	function sub_insert_factor($subInsertId="")
	{
		global $objLang;
		$sql = "SELECT  * FROM ".$objLang->tableName(DOOR_SUB_INSERT_TABLE,true);
		
		if($subInsertId!='')
		$sql .=" WHERE door_sub_id = '".$subInsertId."'";
		
		$result = mysql_query($sql) or die(mysql_error());
		//echo mysql_num_rows($result);
		if(mysql_num_rows($result)>0)
		{
			if($subInsertId!='')
			{
				$row = mysql_fetch_assoc($result);
				return $row;
			}
			else
			{
				$row = array();
				while($r = mysql_fetch_assoc($result))
				$row[] = $r;
				return $row;
			}
		}
	}
	
	// for fetching all sub insert of main insert
	// optional parameter: insert_id
	// returns all sub inserts 
	function fetch_sub_insert($insertId="")
	{
		global $objLang;
		$sql = "SELECT  * FROM ".$objLang->tableName(DOOR_SUB_INSERT_TABLE,true);
		
		if($insertId!='')
		$sql .=" WHERE door_insert_id = '".$insertId."'";
		
		$result = mysql_query($sql) or die(mysql_error());
		//echo mysql_num_rows($result);
		if(mysql_num_rows($result)>0)
		{
			$row = array();
			while($r = mysql_fetch_assoc($result))
			$row[] = $r;
			return $row;
		}
	}
	// for fetching all profile finish factors
	// optional parameter: profileFinishTitle Code
	// returns all factors if parameter is null
	function profile_finish_factor($profileFinishTitle="")
	{
		global $objLang;
		$sql = "SELECT  * FROM ".$objLang->tableName(DOOR_PROFILE_fINISH_TABLE,true);
		
		if($profileFinishTitle!='')
		$sql .=" WHERE profile_finish_title = '".$profileFinishTitle."'";
		
		$sql .= " ORDER BY profile_finish_code";
		
		$result = mysql_query($sql) or die(mysql_error());
		//echo mysql_num_rows($result);
		if(mysql_num_rows($result)>0)
		{
			if($profileFinishTitle!='')
			{
				$row = mysql_fetch_assoc($result);
				return $row->profile_finish_factor;
			}
			else
			{
				$row = array();
				while($r = mysql_fetch_assoc($result))
				$row[] = $r;
				return $row;
			}
		}
	}

	
	function fetch_finish($finishId="")
	{
		global $objLang;
		
		$sql = "SELECT  * FROM ".$objLang->tableName(DOOR_PROFILE_fINISH_TABLE,true);
		
		if($finishId!='')
		$sql .=" WHERE profile_finish_id= '".$finishId."'";
		
		$result = mysql_query($sql) or die(mysql_error());
		//echo mysql_num_rows($result);
		if(mysql_num_rows($result)>0)
		{
			if($finishId!='')
			{
				$row = mysql_fetch_assoc($result);
				return $row;
			}
			else
			{
				$row = array();
				while($r = mysql_fetch_assoc($result))
				$row[] = $r;
				return $row;
			}
		}
	}
	
	// for saving/updating profile types
	function save_update_profile_type($profileId="")
	{
		global $objLang;
		
		//$_FILES
		$dptitle	=	$_POST['dptitle'];
		$dpcode		=	$_POST['dpcode'];
		$nfactor	=	$_POST['nfactor'];
		$cfactor	=	$_POST['cfactor'];
		$sfactor	=	$_POST['sfactor'];
		$ofactor	=	$_POST['ofactor'];
		$bfactor	=	$_POST['bfactor'];
		$wfactor	=	$_POST['wfactor'];
		
		if($profileId!='')
		{
			if($objLang->is_active())
			$sql = "UPDATE ".$objLang->tableName(DOOR_PROFILE_TABLE)." SET nfactor='$nfactor', cfactor='$cfactor', sfactor='$sfactor', ofactor='$ofactor', bfactor='$bfactor', wfactor='$wfactor', code='$dpcode' WHERE profile_id='$profileId'";
			else
			$sql = "UPDATE ".$objLang->tableName(DOOR_PROFILE_TABLE,true)." SET nfactor='$nfactor', cfactor='$cfactor', sfactor='$sfactor', ofactor='$ofactor', bfactor='$bfactor', wfactor='$wfactor', code='$dpcode' WHERE profile_id='$profileId'";
			$this->ExecuteQuery($sql,"update");
			$sql = "UPDATE ".$objLang->tableName(DOOR_PROFILE_TABLE,true)." SET name='$dptitle', cfactor='$cfactor', nfactor='$nfactor', sfactor='$sfactor', ofactor='$ofactor', bfactor='$bfactor', wfactor='$wfactor', code='$dpcode' WHERE profile_id='$profileId'";
			$this->ExecuteQuery($sql,"update");
		}
		
		if($profileId=='')
		{
			$sql= "INSERT INTO  ".DOOR_PROFILE_TABLE." (name, code, nfactor, cfactor, sfactor, ofactor, bfactor, wfactor) VALUES ('$dptitle','$dpcode','$nfactor','$cfactor','$sfactor','$ofactor','$bfactor','$wfactor')";
			$sqlfrench= "INSERT INTO  ".$objLang->tableName(DOOR_PROFILE_TABLE,'french')." (name, code, nfactor, cfactor, sfactor, ofactor, bfactor, wfactor) VALUES ('$dptitle','$dpcode','$nfactor','$c84factor','$sfactor','$ofactor','$bfactor','$wfactor')";
			$this->ExecuteQuery($sql,"insert");
			$profileId = mysql_insert_id();
			$this->ExecuteQuery($sqlfrench,"insert");
		}
		
		
		//
		$form['field_name']				= 'dpimage';

		$details['type']					= 'door Profile';
		$details['name'] 				= $dptitle;
		$details['directory']			= '../'.DOOR_FOLDER;
		$details['allowed']				= $this->image_mime_types();

		$dbTableDetail['table'] 			= DOOR_PROFILE_TABLE;
		$dbTableDetail['id'] 			= $profileId;
		$dbTableDetail['id_column']	 	= 'profile_id';
		$dbTableDetail['upload_column'] 	= 'image';
		
		$dbTableDetail['extra_table']	[]	= array(
					'table'			=> $objLang->tableName(DOOR_PROFILE_TABLE,'french'),
					'id'				=> $profileId,
					'id_column'		=> 'profile_id',
					'upload_column'	=> 'image');
		
		$this->upload_img($form,$details,$dbTableDetail);
	}
	
	
	// delete profile Types
	function delete_profile_type($profileId)
	{
		$table['img_column']	= 'image';
		$table['table']		= DOOR_PROFILE_TABLE;
		$table['id']			= $profileId;
		$table['id_column']	= 'profile_id';
		$table['folder']		= '../'.DOOR_FOLDER;
		$this->common_delete_profile($table);
	}
	
	// for saving/updating profile finish types
	function save_update_profile_finish($finish_id="")
	{
		global $objLang;
		
		//$_FILES
		$dptitle	=	$_POST['dptitle'];
		$dpfactor	=	$_POST['dpfactor'];
		$dpcode		=	$_POST['dpcode'];
		//$finish_id	= 	$_POST['finish_id'];
		
		
		if($finish_id!='')
		{
			if($objLang->is_active())
			$sql = "UPDATE ".$objLang->tableName(DOOR_PROFILE_fINISH_TABLE)." SET profile_finish_code='$dpcode', profile_finish_factor='$dpfactor' WHERE  profile_finish_id='$finish_id'";
			else
			$sql = "UPDATE ".$objLang->tableName(DOOR_PROFILE_fINISH_TABLE,true)." SET profile_finish_code='$dpcode', profile_finish_factor='$dpfactor' WHERE  profile_finish_id='$finish_id'";
			$this->ExecuteQuery($sql,"update");
			//echo "<br/> $sql";
			$sql = "UPDATE ".$objLang->tableName(DOOR_PROFILE_fINISH_TABLE,true)." SET profile_finish_code='$dpcode', profile_finish_title='$dptitle', profile_finish_factor='$dpfactor' WHERE  profile_finish_id='$finish_id'";
			$this->ExecuteQuery($sql,"update");
			//echo "<br/> $sql";
		}
		
		if($finish_id=='')
		{
			$sql= "INSERT INTO  ".DOOR_PROFILE_fINISH_TABLE." (profile_finish_title,profile_finish_factor,profile_finish_code) VALUES ('$dptitle','$dpfactor','$dpcode')";
			$sqlfrench= "INSERT INTO  ".$objLang->tableName(DOOR_PROFILE_fINISH_TABLE,'french')." (profile_finish_title,profile_finish_factor,profile_finish_code) VALUES ('$dptitle','$dpfactor','$dpcode')";
			$this->ExecuteQuery($sql,"insert");
			$finish_id = mysql_insert_id();
			$this->ExecuteQuery($sqlfrench,"insert");
		}
		
		
		//
		$form['field_name']				= 'dpimage';

		$details['type']					= 'door profile';
		$details['name'] 				= $dptitle;
		$details['directory']				= '../'.PROFILE_FINISH_FOLDER;
		$details['allowed']				= $this->image_mime_types();

		$dbTableDetail['table'] 			= DOOR_PROFILE_fINISH_TABLE;
		$dbTableDetail['id'] 			= $finish_id;
		$dbTableDetail['id_column']	 	= 'profile_finish_id';
		$dbTableDetail['upload_column'] 	= 'profile_finish_image';
		
		$dbTableDetail['extra_table']	[]	= array(
					'table'			=> $objLang->tableName(DOOR_PROFILE_fINISH_TABLE,'french'),
					'id'				=> $finish_id,
					'id_column'		=> 'profile_finish_id',
					'upload_column'	=> 'profile_finish_image');
		
		$this->upload_img($form,$details,$dbTableDetail);
	}
	
	// delete profile Finish Types
	function delete_profile_finish($finishId)
	{
		global $objLang;
		$table['img_column']	= 'profile_finish_image';
		$table['table']		= DOOR_PROFILE_fINISH_TABLE;
		$table['id']			= $finishId;
		$table['id_column']	= 'profile_finish_id';
		$table['folder']		= '../'.PROFILE_FINISH_FOLDER;
		$this->common_delete_profile($table);
	}
	
	
	// for saving/updating profile Insert types
	function save_update_profile_insert($insertId="")
	{
		global $objLang;
		
		//$_FILES
		$dptitle		=	$_POST['dptitle'];
		$dpfactor		=	$_POST['dpfactor'];
		$dpcode			=	$_POST['dpcode'];
		$profile_type	= 	$_POST['profile_type'];
		$profile_type	= 	json_encode($profile_type);
		
		//$insertId	= 	$_POST['insert_id'];
		
		
		if($insertId!='')
		{
			if($objLang->is_active())
			$sql = "UPDATE ".$objLang->tableName(DOOR_INSERT_TABLE)." SET insert_code='$dpcode', insert_factor='$dpfactor', insert_available_with='$profile_type' WHERE insert_id='$insertId'";
			else
			$sql = "UPDATE ".$objLang->tableName(DOOR_INSERT_TABLE,'french')." SET insert_code='$dpcode', insert_factor='$dpfactor', insert_available_with='$profile_type' WHERE insert_id='$insertId'";
			mysql_query($sql) or die(mysql_error());
			$sql = "UPDATE ".$objLang->tableName(DOOR_INSERT_TABLE,true)." SET insert_title='$dptitle', insert_code='$dpcode', insert_factor='$dpfactor', insert_available_with='$profile_type' WHERE insert_id='$insertId'";
			mysql_query($sql) or die(mysql_error());
			//$this->ExecuteQuery($sql,"update");
		}
		
		if($insertId=='')
		{
			$sql= "INSERT INTO  ".DOOR_INSERT_TABLE." (insert_title,insert_code,insert_factor,insert_available_with) VALUES ('$dptitle','$dpcode','$dpfactor','$profile_type')";
			$sqlfrench= "INSERT INTO  ".$objLang->tableName(DOOR_INSERT_TABLE,'french')." (insert_title,insert_code,insert_factor,insert_available_with) VALUES ('$dptitle','$dpcode','$dpfactor','$profile_type')";
			mysql_query($sql) or die(mysql_error());
			$insertId = mysql_insert_id();
			mysql_query($sqlfrench) or die(mysql_error());
			//$this->ExecuteQuery($sql,"insert");
			//$this->ExecuteQuery($sqlfrench,"insert");
		}
		
		
		//
		$form['field_name']				= 'dpimage';

		$details['type']					= 'door Insert';
		$details['name'] 				= $dptitle;
		$details['directory']			= '../'.INSERT_FOLDER;
		$details['allowed']				= $this->image_mime_types();

		$dbTableDetail['table'] 			= DOOR_INSERT_TABLE;
		$dbTableDetail['id'] 			= $insertId;
		$dbTableDetail['id_column']	 	= 'insert_id';
		$dbTableDetail['upload_column'] 	= 'insert_image';
		
		$dbTableDetail['extra_table']	[]	= array(
					'table'			=> $objLang->tableName(DOOR_INSERT_TABLE,'french'),
					'id'				=> $insertId,
					'id_column'		=> 'insert_id',
					'upload_column'	=> 'insert_image');
		
		$this->upload_img($form,$details,$dbTableDetail);
	}
	
	// delete profile Finish Types
	function delete_profile_insert($insertId)
	{
		$table['img_column']	= 'insert_image';
		$table['table']		= DOOR_INSERT_TABLE;
		$table['id']			= $insertId;
		$table['id_column']	= 'insert_id';
		$table['folder']		= '../'.INSERT_FOLDER;
		$this->common_delete_profile($table);
	}
	
	
	// for saving/updating profile Insert types
	function save_update_profile_sub_insert($insertId,$subInsertId='')
	{
		global $objLang;
		
		//$_FILES
		$dptitle	=	$_POST['dptitle'];
		
		
		if($subInsertId!='')
		{
			$sql = "UPDATE ".$objLang->tableName(DOOR_SUB_INSERT_TABLE,true)." SET door_sub_title='$dptitle' WHERE door_insert_id='$insertId' AND door_sub_id='".$subInsertId."'";
			$this->ExecuteQuery($sql,"update");
		}
		
		if($subInsertId=='')
		{
			$sql= "INSERT INTO  ".DOOR_SUB_INSERT_TABLE." (door_sub_title,door_insert_id) VALUES ('$dptitle','$insertId')";
			$sqlfrench= "INSERT INTO  ".$objLang->tableName(DOOR_SUB_INSERT_TABLE,'french')." (door_sub_title,door_insert_id) VALUES ('$dptitle','$insertId')";
			$this->ExecuteQuery($sql,"insert");
			$subInsertId = mysql_insert_id();
			$this->ExecuteQuery($sqlfrench,"insert");
		}
		
		
		//
		$form['field_name']				= 'dpimage';

		$details['type']					= 'insert';
		$details['name'] 				= $dptitle;
		$details['directory']				= '../'.SUB_INSERT_FOLDER;
		$details['allowed']				= $this->image_mime_types();

		$dbTableDetail['table'] 			= DOOR_SUB_INSERT_TABLE;
		$dbTableDetail['id'] 			= $subInsertId;
		$dbTableDetail['id_column']	 	= 'door_sub_id';
		$dbTableDetail['upload_column'] 	= 'door_sub_image';
		
		$dbTableDetail['extra_table']	[]	= array(
					'table'			=> $objLang->tableName(DOOR_SUB_INSERT_TABLE,'french'),
					'id'				=> $subInsertId,
					'id_column'		=> 'door_sub_id',
					'upload_column'	=> 'door_sub_image');
		
		$this->upload_img($form,$details,$dbTableDetail);
	}
	
	// delete profile Finish Types
	function delete_profile_sub_insert($subInsertId)
	{
		$table['img_column']	= 'door_sub_image';
		$table['table']		= DOOR_SUB_INSERT_TABLE;
		$table['id']			= $subInsertId;
		$table['id_column']	= 'door_sub_id';
		$table['folder']		= '../'.SUB_INSERT_FOLDER;
		$this->common_delete_profile($table);
	}
	
	// common delete function for profile,finish,insert and sub-insert
	function common_delete_profile($table)
	{
		global $objLang;
		$this->delete_image($table);
		
		$sql = "DELETE FROM ".$table['table']." WHERE ".$table['id_column']."='".$table['id']."'";
		mysql_query($sql) or die(mysql_error());
		foreach($objLang->language_list() as $key=>$val)
		{
			$sql = "DELETE FROM ".$objLang->tableName($table['table'],$key)." WHERE ".$table['id_column']."='".$table['id']."'";
			@mysql_query($sql);// or die(mysql_error());
		}
	}
	
	// fetch site settings
	function site_settings()
	{
		$sql = "SELECT * FROM ".SITE_SETTING_TABLE;
		$result = mysql_query($sql);
		if(mysql_num_rows($result)>0)
		{
			$row = mysql_fetch_assoc($result);
			return $row;
		}
		else
		return false;
	}

	// returns all the extensions of image file type.
	function image_mime_types()
	{
		return array( 'image/bmp', 'image/x-windows-bmp', 'image/gif','image/pjpeg', 'image/jpeg', 'image/pjpeg', 'image/pjpeg', 'image/x-png','image/tiff');
	}
	
	// returns extensions of document file type
	function document_mime_types()
	{
		return array('application/pdf', 'application/x-download','application/excel', 'application/vnd.ms-excel', 'application/msexcel','application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document','application/msword', 'application/octet-stream','application/excel');
	}

	// returns all the extensions of document and image file type.
	function document_image_mime_types()
	{
		return array( 'application/pdf', 'application/x-download','application/excel', 'application/vnd.ms-excel', 'application/msexcel','application/powerpoint', 'application/vnd.ms-powerpoint','application/x-zip', 'application/zip', 'application/x-zip-compressed','text/plain','application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet','application/msword', 'application/octet-stream','image/bmp', 'image/x-windows-bmp', 'image/gif','image/pjpeg', 'image/jpeg', 'image/pjpeg', 'image/pjpeg', 'image/x-png','image/tiff','image/png',  'image/x-png');
	}
		
	
	
	// image upload function
	// First Parameters : $form;
	// $form['field_name']='field name of the upload '
	// Second Paramenter :  $details;
	//$details['type']='image type',$details['name'] = 'Owner Name', $details['directory']='image upload directory',$details['allowed']='array of allowed file types';
	// Third Paramentes : $dbTableDetail;
	// $dbTableDetail['table'] = 'table name',$dbTableDetail['id'] = 'id colulmn in that table',$dbTableDetail['id_column'] = 'label of column with primary id ',$dbTableDetail['upload_column'] = 'colulmn where to update the data';
	// Returns : array of upload status (booleen) and image_name if upload successful;
	function upload_img($form=array(),$details=array(),$dbTableDetail=array())
	{
		
		// if no file is uploaded
		if(empty($form))
		return false;
		
		$field_name = $form['field_name'];
		
		$isImageType = false;
		
		$db_upload_column = isset($dbTableDetail['upload_column']) ? $dbTableDetail['upload_column'] : '';
		
		if($this->debug)
		echo "<hr/>The filed name (".$field_name.") db upload column name (".$db_upload_column.")";
		
		if ($_FILES[$field_name]["error"] > 0)
		{
			if($this->debug)
			echo "<hr/>Error Found:  ".$_FILES[$field_name]["error"];
			$new_image="";
			return array('status'=>FILE_NOT_UPLOADED,'error_msg'=>'No File Uploaded','image'=>'');
		}
		else
		{
		
			$tmp_name = $_FILES[$field_name]["tmp_name"];
			$file_ext = $_FILES[$field_name]["name"];
			$exts = preg_split("[\.]", $file_ext) ; $n = count($exts)-1; $exts = strtolower($exts[$n]);
			
			if(in_array($_FILES[$field_name]['type'],$this->image_mime_types()))
			$isImageType = true;
			
			if($this->debug)
			echo "<hr/>Temp name (".$tmp_name.") File extension (".$exts.")";
			
			
			// checks if correct file type is being uploaded
			if(in_array($_FILES[$field_name]['type'],$details['allowed']))
			{
				if($this->debug)
				echo "<hr/>Passed the file mime type Validation";
				
				$random 			= rand(1,999);
				$directory 			= $this->slashTrim($details['directory'].'/');
				$thumbnailFolder	= THUMBNAIL_FOLDER;
				
				// to create directory if doesn't exist
				if(!is_dir($directory.$thumbnailFolder))
				{
					if(@mkdir($directory.$thumbnailFolder, 0700))
					{
						if($this->debug)
						echo "<hr/> Created new directory ".$directory.$thumbnailFolder;
					}
					else
					{
						if($this->debug)
						echo "<hr/><div style='color:red'> Failed to create new directory ".$directory.$thumbnailFolder."</div>";
					}
					
				}
				if($this->debug)
				echo "<hr/> The directory ".$directory.$thumbnailFolder;

					if($_FILES[$field_name]['name']!=''){

						// if database details provided
						// then fetch old uploaded file if exist and replace older file with new file
						if(!empty($dbTableDetail))
						{
							$img['img_column']	= $db_upload_column;
							$img['table']		= $dbTableDetail['table'];
							$img['id']			= $dbTableDetail['id'];
							$img['id_column']	= $dbTableDetail['id_column'];
							$img['folder']		= $directory;
							$this->delete_image($img);
							
						}
						/* ------------ move uploaded image --------- */
						$id = !isset($dbTableDetail['id']) ? 'k' : $dbTableDetail['id'];
						$new_image = $id.'-'.$random."-".str_replace(' ','_',$details['type'])."-".$this->trimThis($details['name']);
						$new_image = trim($new_image).'.'.$exts;
						move_uploaded_file($tmp_name, ($directory.$new_image));
					   
					   if($this->debug)
					   echo "<hr/> file moved to ".$directory.$new_image;

				if($isImageType)
				{
					// -------------------------------- CREATE thumb START -----------------------

					// Add the Image Resizer Class Start ----------------------------------------------
					require('image_resizer.php');
					// Add the Image Resizer Class Ending ----------------------------------------------
	
					// get width and height of original image
					list($image_width, $image_height) = getimagesize($directory.$new_image);

					if($this->debug)
					echo "<hr/> Image width ".$image_width; 
						
					if($image_width > THUMBNAIL_IMAGES_WIDTH)
					{
						if($this->debug)
						echo "<hr/> Image is larger than thumbnail size <br/> Creating Thumb"; 
						
						$image = new SimpleImage();
						$image->load($directory.$new_image);	
						
						// Create Thumbnails ========================================================
						// resize width
						$thumbWidth = THUMBNAIL_IMAGES_WIDTH;
						$thumbDirectory= $directory.$thumbnailFolder;
						
						// calculate thumbnail size
						$new_width = $thumbWidth;
						$new_height = floor( $image_height * ( $thumbWidth / $image_width ) );
					
						// resize image 
						$image->resizeToWidth($new_width);

						// save image
						$image->save($thumbDirectory.$new_image);
						
						if($this->debug)
						echo "<hr/> Thumbnail Created";
					}
					else
					{
						// move original file to thumbnail directory 
						//move_uploaded_file($tmp_name, ($directory.$thumbnailFolder.$new_image));
						@copy($directory.$new_image, $directory.$thumbnailFolder.$new_image);
						if($this->debug)
						echo "<hr/> Moved orginal file to thumbnail folder";
					}
				} // if uploaded file is image	
					// -------------------------------- CREATE thumb END -----------------------


					if(!empty($dbTableDetail))
					{
						//$update = array($db_upload_column=>$new_image);
						//$this->db->where($dbTableDetail['id_column'],$dbTableDetail['id']);
						$sql = "UPDATE ".$dbTableDetail['table']." ";
						$sql .= " SET ".$db_upload_column."='".$new_image."'";
						$sql .= " WHERE ".$dbTableDetail['id_column']."='".$dbTableDetail['id']."'";
						if(mysql_query($sql))
						{
							if($this->debug)
							echo "<hr/> updated image name to db";
							// update additional tables with the same image
							if(!empty($dbTableDetail['extra_table']))
							{
								foreach($dbTableDetail['extra_table'] as $et)
								{
									$esql = "UPDATE ".$et['table']." ";
									$esql .= " SET ".$et['upload_column']."='".$new_image."'";
									$esql .= " WHERE ".$et['id_column']."='".$et['id']."'";
									mysql_query($esql);
								}
							}
						}
						else
						{
							if($this->debug)
							{
							echo "<hr/> error updating image name to db<br/>".mysql_error();
							echo "<hr/>".$sql;
							}
							
						
						}
					}

	/*		   			$sql =" UPDATE ".$table_name." SET ".$field_name."='$new_image' WHERE id='$last_insert_id' ";
					echo "<br /> filed name => ".$_FILES[$field_name]['name'];
					echo "<br /> filed name => ".$field_name;
					echo "<br /> sql => ".$sql;
					mysql_query($sql) or die(mysql_error());*/

				if($this->debug)
				echo "<br/>".str_repeat('*==>@<==*',12);
				}
			}
			else
			{
				return array('status'=>FILE_NOT_ALLOWED,'error_msg'=>'The uploaded File type is not allowed','image'=>'');
			}
			return array('status'=>FILE_UPLOADED,'image'=>$new_image,'error_msg'=>'File uploaded Successfully');
	   	}
   }

	// function to delete uploaded images upon deletion
	function delete_image($img=array())
	{
		if(!empty($img))
		{
			if($this->debug)
			echo "<div> Started deleting image function <pre>".print_r($img,true)."</pre> </div>";
			// fetch imagefile name for deletion of previously uploaded images
			$sql = "SELECT ".$img['img_column']." FROM ".$img['table']." WHERE ".$img['id_column']."='".$img['id']."'";
			
			if($this->debug)
			echo "<div> the sql $sql </div>";
			
			$result = mysql_query($sql);
			if(mysql_num_rows($result)>0)
			{
				$file = mysql_fetch_assoc($result);
				$imgFile = $file[$img['img_column']];
				
				if($this->debug)
				echo "<div> found row with image file name as (".$img['folder'].$imgFile.") </div>";
			
				if($imgFile!='' && file_exists($img['folder'].$imgFile))
					{
						@unlink($img['folder'].$imgFile);
						@unlink($img['folder'].THUMBNAIL_FOLDER.$imgFile);
						
						if($this->debug)
						echo "<div>Deleted image and its thumb </div>";
					}
				else
				{
					if($this->debug)
					echo "<div>Image not found in folder (".$img['folder'].")</div>";
				}
								
			}
			else
			{
				if($this->debug)
				echo "<hr/>Previous file not found";
			}
		}
	}
	function optimizeImgSize($src)
	{
		// get width and height of original image
		list($image_width, $image_height) = getimagesize($src);
		
		// calculate thumbnail size
		$new_width = BLOG_IMAGE_WIDTH;
		$new_height = floor( $image_height * ( $new_width / $image_width ) );
		
		$image_properties = array(
          'src' => $src,
          'width' => BLOG_IMAGE_WIDTH,
          'height' => $new_height
		  );

		if($new_height<BLOG_IMAGE_HEIGHT)
		{
			$padd = floor((BLOG_IMAGE_HEIGHT-$new_height)/2);
			$image_properties['style'] = "padding: ".$padd."px 0;";
			
			return img($image_properties);
		}
		if($new_height>BLOG_IMAGE_HEIGHT)
		{
			return "<div style='overflow:hidden;width:".$new_width."px;height:".BLOG_IMAGE_HEIGHT."px'>".img($image_properties)."</div>";
		}
	}
  // trim given details
	function trimThis($content)
	{
		$str = '';//array();
		$content = trim($content);
		
		if($this->debug)
		echo "<hr/>strpos (".strpos($content,' ').")";
		
		if(strpos($content,' '))
		{
			$str = explode(' ',$content);
			$str = trim($str[0]);
		}
		else
		$str = $content;
		
		return preg_replace('/[^A-Za-z0-9\-]/','',$str);
	}
	
	// replaces 'double slashesh (//)' to single slash(/)
	function slashTrim($str)
	{
		if($this->debug)
		echo "<hr/>strpos of double shash (".strpos($str,'//').")";
		
		if(strpos($str,'//'))
		{
			return str_replace('//','/',$str);
		}
		else return $str;
	}
}
?>