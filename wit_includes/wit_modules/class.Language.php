<?php
header('Content-type: text/html; charset=utf-8');
define('ENGLISH','english');

class language 
{
	
	function language()
	{
		$activeLang = $this->is_active();
		//var_dump($activeLang);
		// load French Language by default
		if(!$activeLang){ changeLanguage('french'); Redirect('index.html'); }
	}
	
	// list of all the languages
	function language_list($all="")
	{
		$lang = array();
		if($all=="all")
		{
			$lang['english']	= 'English';
		}
		$lang['french']='Francais';
		return $lang;
	}
	// check if language is active
	function is_active()
	{
		//if(isset($_SESSION['language']) && $_SESSION['language']!=ENGLISH)
		if(isset($_SESSION['language']))
		return $_SESSION['language'];
		else
		return false;
	}
	
	//for the language
	function tableName($tableName,$autoLang=false)
	{
	
		if($autoLang===true)// && $otherLang=='')
		{
			$table = $tableName;
			$table .= (isset($_SESSION['language']) && $_SESSION['language']!=ENGLISH ) ? '_'.strtolower($_SESSION['language']) : '';
			return $table;
		}
		else if($autoLang!==true && $autoLang!='' && $autoLang!=ENGLISH)
			return $tableName.'_'.strtolower($autoLang);
		else
			return $tableName;
	}

}

	$objLang = new language();
	
	function changeLanguage($language=ENGLISH)
	{
		$_SESSION['language'] = strtolower($language);
	}
	
	function currentLanguage()
	{
		return isset($_SESSION['language']) ? $_SESSION['language'] : ENGLISH;
	}
	
	if(isset($_POST['language'])){
		changeLanguage($_POST['language']); echo "changed langurage to ".$_POST['language'];
	}
	$languages = $objLang->language_list('all');
	$languageSelection = '<select onchange="changeLanguage(this);" name="language"> ';
	foreach($languages as $key=>$val) { 
		$sel	= (isset($_SESSION['language']) && $_SESSION['language']==$key) ? 'selected="selected"' : '';
		$languageSelection .= '<option value="'.$key.'" '.$sel.'>'.$val.'</option>'; 
		}
	$languageSelection .= '</select>';
	$languageSelection .= '<script type="text/javascript">
							function changeLanguage(object)
							{
								var obj = $(object);
								$.post("'.$ngconfig['SiteGlobalPath'].'wit_includes/wit_common.php", { language: obj.val() },function(data){ window.location.reload(true) });
							} 
							</script>';
							
	$objSmarty->assign("languageSelection",$languageSelection);
	
	$lang		= (currentLanguage()=='' || currentLanguage()==ENGLISH) ? 'english' : currentLanguage();
	$objSmarty->assign("lang",$lang);
?>