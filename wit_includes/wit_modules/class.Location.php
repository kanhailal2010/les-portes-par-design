<?php
class Location extends MysqlFns
{
	
	/**********************************************************************************************************************/
	/************************************	For Admin Panel		*******************************************************/
	/**********************************************************************************************************************/
			
			/****************************************************************************************************/
			/************************************	For Country Mgmt	******************************************/
			/****************************************************************************************************/
	
	/*******************************				Select  City Lists		******************************/
	function SelectSiteCountryLists($ResVal,$PageVal)
	{
		global $objSmarty;$where_con="";$OrderCon=" order by country_name asc ";$PageURL='loc_country_mgmt.php?';
		
		$sortby=(isset($_GET['sortby'])) ? $_GET['sortby']:'';$seaname=(isset($_GET['seaname'])) ? $_GET['seaname']:'';
		
		if($seaname!=""){
			$where_con.=" and country_name LIKE '".trim($seaname)."%'";$PageURL.="seaname=".urlencode($seaname)."&";
		}
		$SortArr=array('country'=>'country_name','status'=>'country_status');
		if(!empty($sortby)){
			$SortDet=explode('_',$sortby);
			if(array_key_exists($SortDet[0],$SortArr)){
				$OrderCon=" order by ".$SortArr[$SortDet[0]]." ".$SortDet[1]."";
			}
			$PageURL.="sortby=".urlencode($sortby)."&";
		}				
		$objSmarty->assign("PageURL",$PageURL);
		
		$SelFields=array('*');	//	Fields To select Table listing	
		$SelQuery="SELECT count(country_id) from pj_location_country where country_id!=''  $where_con $OrderCon";
		$SUserList=GeneralAdmin::SelectTableContentLists($ResVal,$SelQuery,$SelFields,$PageVal,$PageURL,15);
	}
		
	/*******************************	Function For Add, Update Country Details 		******************************/
	function AddUpdateCountryDetails($AdminAction,$con_id)
	{
		global $objSmarty;extract($_POST);$FlagIns=true;
		if(!empty($con_id)){
			$where_con=" and md5(country_id)!='".$con_id."'";
		}
		if(empty($country_name)){
			$Err_Msg.="Please enter Country Name";$FlagIns=false;
		}
		else{
			$SelQuery="SELECT ST.* from pj_location_country ST where country_name='".$country_name."' $where_con";
			if(!GeneralAdmin::CheckDupRecord($SelQuery))
			{
				$Err_Msg.="Country Name Already Exists";$FlagIns=false;
			}	
		}	 
		if(empty($CountryCode)){
			$Err_Msg.="Please enter Country Code";$FlagIns=false;
		}
		else{
			$SelQuery="SELECT ST.* from pj_location_country ST where CountryCode='".$CountryCode."' $where_con";
			if(!GeneralAdmin::CheckDupRecord($SelQuery))
			{
				$Err_Msg.="Country Code Already Exists";$FlagIns=false;
			}	
		}	
		if($FlagIns)
		{	
			if($AdminAction=='Update' && !empty($con_id))
			{					
	 			$AltCon = "Update pj_location_country set country_name = '".trim($country_name)."',CountryCode='".trim($CountryCode)."' 
							Where md5(country_id) ='".$con_id."'";
				$checkval=$this->ExecuteQuery($AltCon, "update");   
				GeneralAdmin::MakeAltSEOUrlString('pj_location_country','country_id','CSEOString',$con_id,$country_name);	
				if(!empty($checkval)){
					Redirect('loc_country_mgmt.php?pro_msg=usucc');
				}
				else{
					$objSmarty->assign("ErrMessage", "IT IS ALREADY UPDATED PLEASE DO SOME MODIFICATIONS TO UPDATE"); 
				}
			}
 			else{				 
	 			$InsCon="insert into pj_location_country(country_name,CountryCode) values('".$country_name."','".trim($CountryCode)."')"; 
				$this->ExecuteQuery($InsCon,"insert");$con_id=mysql_insert_id();
				GeneralAdmin::MakeAltSEOUrlString('pj_location_country','country_id','CSEOString',$con_id,$country_name);	
				Redirect('loc_country_mgmt.php?pro_msg=asucc');
			}	
		}
		else{
			$objSmarty->assign("ErrMessage",$Err_Msg);$objSmarty->assign("Arr",$_POST);	
		}
	}
			
			/****************************************************************************************************/
			/************************************	For State Mgmt	******************************************/
			/****************************************************************************************************/
	
	/*******************************	Function For Add, Update State Details 		******************************/
	function AddUpdateStateDetails($AdminAction,$con_id)
	{
		global $objSmarty;$FlagIns=true;$Err_Msg=array();extract($_POST);
		if(!empty($con_id)){
			$where_con=" and md5(state_id)!='".$con_id."'";
		}
		if(empty($state_name)){
			$Err_Msg[]="Please enter State Name";
		}
		else{
			$SelQuery="SELECT ST.* from pj_location_state ST where state_name='".$state_name."' $where_con";
			if(!GeneralAdmin::CheckDupRecord($SelQuery))
			{
				$Err_Msg[]="State Name Already Exists";
			}	
		}	 
		if(empty($State_Code)){
			$Err_Msg[]="Please enter State Code";
		}
		else{
			$SelQuery="SELECT ST.* from pj_location_state ST where State_Code='".$State_Code."' $where_con";
			if(!GeneralAdmin::CheckDupRecord($SelQuery))
			{
				$Err_Msg[]="State Code Already Exists";
			}	
		}
		if(sizeof($Err_Msg)==0)
		{	
			if($AdminAction=='Update' && !empty($con_id))
			{					
	 			$AltCon = "Update pj_location_state set state_name = '".trim($state_name)."',country_ident = '".trim($country_ident)."'
						   ,State_Code = '".$State_Code."'Where md5(state_id) ='".$con_id."'";
				$checkval=$this->ExecuteQuery($AltCon, "update"); 
				GeneralAdmin::MakeAltSEOUrlString('pj_location_state','state_id','StateSEOString',$con_id,$state_name); 
				if(!empty($checkval)){
					Redirect('state_mgmt.php?pro_msg=usucc');
				} 
				else{
					$objSmarty->assign("ErrMessage", "IT IS ALREADY UPDATED PLEASE DO SOME MODIFICATIONS TO UPDATE"); 
				}
			}
 			else{				 
	 		 $InsCon="insert into pj_location_state(state_name,country_ident,State_Code) values('".$state_name."',
				'".trim($country_ident)."','".$State_Code."')"; 
				$this->ExecuteQuery($InsCon,"insert");$con_id=mysql_insert_id();	
				GeneralAdmin::MakeAltSEOUrlString('pj_location_state','state_id','StateSEOString',$con_id,$state_name);
				Redirect('state_mgmt.php?pro_msg=asucc');
			}
		}
		else{
			$objSmarty->assign("ErrMessage",implode('</p><p>',$Err_Msg));$objSmarty->assign("Arr",$_POST);	
		}
	}
	
	/*******************************				Select  City Lists		******************************/
	function GetLocationLists($ResVal,$PageVal)
	{
		global $objSmarty;$where_con="";$OrderCon=" order by LCityId desc ";
		$PageURL='location_mgmt.php?';$sortby=(isset($_GET['sortby'])) ? $_GET['sortby']:'';
		$seaname=(isset($_GET['seaname'])) ? $_GET['seaname']:'';$seastate=(isset($_GET['seastate'])) ? $_GET['seastate']:'';
		$scountry=(isset($_GET['scountry'])) ? $_GET['scountry']:'';
		if($seaname!=""){
			$where_con.=" and  TC.LCityName LIKE '".trim($seaname)."%'";$PageURL.="seaname=".urlencode($seaname)."&";
		}
		if($seastate!=""){
			$where_con.=" and  PS.state_name LIKE '".trim($seastate)."%'";$PageURL.="seastate=".urlencode($seastate)."&";
		}
		if($scountry!=""){
			$where_con.=" and  PC.country_name LIKE '".trim($scountry)."%'";$PageURL.="scountry=".urlencode($scountry)."&";
		}
		$SortArr=array('country'=>'country_name','state'=>'State_name','city'=>'LCityName');
						
		$objSmarty->assign("PageURL",$PageURL);			
		if(!empty($sortby)){
			$SortDet=explode('_',$sortby);
			if(array_key_exists($SortDet[0],$SortArr)){
				$OrderCon=" order by ".$SortArr[$SortDet[0]]." ".$SortDet[1]."";
			}
			$PageURL.="sortby=".urlencode($sortby)."&";
		}	
		$SelFields=array('TC.*','PS.state_name','PC.country_name');	//	Fields To select Table listing
		
		$SelQuery="SELECT count(TC.LCityId) from  pj_location_city TC left join pj_location_country PC on
		 			PC.country_id=TC.country_id left join pj_location_state PS on PS.state_id =TC.state_id where PS.state_id !='' 
					and PC.country_id!='' $where_con $OrderCon";
		$asas=GeneralAdmin::SelectTableContentLists($ResVal,$SelQuery,$SelFields,$PageVal,$PageURL,40);
	}
	
	/*******************************				Select  State Lists		******************************/
	function GetStateLists($ResVal,$PageVal)
	{
		global $objSmarty;$where_con="";$OrderCon=" order by state_id desc ";$PageURL='state_mgmt.php?';
		
		$sortby=(isset($_GET['sortby'])) ? $_GET['sortby']:'';$seaname=(isset($_GET['seaname'])) ? $_GET['seaname']:'';
		
		if($seaname!=""){
			$where_con.=" and  state_name LIKE '".trim($seaname)."%'";$PageURL.="seaname=".urlencode($seaname)."&";
		}
		$SortArr=array('country'=>'country_name','state'=>'State_name');
		if(!empty($sortby)){
			$SortDet=explode('_',$sortby);
			if(array_key_exists($SortDet[0],$SortArr)){
				$OrderCon=" order by ".$SortArr[$SortDet[0]]." ".$SortDet[1]."";
			}
			$PageURL.="sortby=".urlencode($sortby)."&";
		}				
		$objSmarty->assign("PageURL",$PageURL);
		
		$SelFields=array('PS.*','PC.country_name');	//	Fields To select Table listing	
		$SelQuery="SELECT count(PS.state_id) from  pj_location_state PS left join pj_location_country PC on PC.country_id=PS.country_ident  where PS.country_ident !='' and PC.country_id!='' $where_con $OrderCon";
		GeneralAdmin::SelectTableContentLists($ResVal,$SelQuery,$SelFields,$PageVal,$PageURL,15);
	}
	/*******************************	Function For Add, Update Location Details 		******************************/
	function LocationAddUpdate($AltPage,$con_id)
	{
		global $objSmarty;extract($_POST);$FlagIns=true;

		if(!empty($con_id)){
			$where_con=" and md5(LCityId)!='".$con_id."'";
		}
		if(empty($LCityName)){
			$Err_Msg.="Please Enter City Name";$FlagIns=false;
		}
		if(empty($country_id)){
			$Err_Msg.="Please Select Country";$FlagIns=false;
		}
		if(empty($latitude)){
			$Err_Msg.="Please Enter latitude";$FlagIns=false;
		}
		if(empty($longitude)){
			$Err_Msg.="Please Enter longitude";$FlagIns=false;
		}
		if($FlagIns){
			$SelQuery="SELECT ST.* from pj_location_city ST where LCityName='".$LCityName."' 
						and country_id = '".trim($country_id)."' $where_con";
			if(!GeneralAdmin::CheckDupRecord($SelQuery))
			{
				$Err_Msg.="City Name Already Exists";$FlagIns=false;
			}	
		}	
		if($FlagIns)
		{
			if($AltPage!='' && !empty($con_id))
			{					
	 			$AltCon = "Update pj_location_city set LCityName = '".trim($LCityName)."',country_id = '".trim($country_id)."',
						  state_id = '".trim($state_id)."',latitude = '".$latitude."',longitude = '".$longitude."'
						   Where md5(LCityId) ='".$con_id."'";
				$checkval=$this->ExecuteQuery($AltCon, "update"); 
				GeneralAdmin::MakeAltSEOUrlString('pj_location_city','LCityId','LCitySEO',$con_id,$LCityName);
				if(!empty($checkval)){//already exist checking..
					Redirect('location_mgmt.php?act_type=addnew&pro_msg=usucc&con_id='.$con_id);
				} 
				else{
					$objSmarty->assign("ErrMessage", "IT IS ALREADY UPDATED PLEASE DO SOME MODIFICATIONS TO UPDATE"); 
				}  
			}
			else{				 
	  			$InsCon="insert into pj_location_city (LCityName,country_id,state_id,latitude,longitude) 
						values('".trim($LCityName)."','".$country_id."','".trim($state_id)."','".$latitude."','".$longitude."')";
				$this->ExecuteQuery($InsCon,"insert");
				$con_id=mysql_insert_id();
				GeneralAdmin::MakeAltSEOUrlString('pj_location_city','LCityId','LCitySEO',$con_id,$LCityName);
				Redirect('location_mgmt.php?act_type=addnew&pro_msg=asucc');		
			}
		}
		else{
			$objSmarty->assign("ErrMessage",$Err_Msg);$objSmarty->assign("Arr",$_POST);	
		}
	}
	/*************************** 			Select Country List Details			***************************/
	function SelectCountryList()
	{	
		global $objSmarty; 
		
  		$SelQuery="SELECT * from  pj_location_country where country_id!='' order by country_name asc";
		$ConList=$this->ExecuteQuery($SelQuery, "SELECT"); 
		$objSmarty->assign("CountryList",$ConList);
		
	}
	/*************************** 			Select Country List Details			***************************/
	function SelectLocationList()
	{	
		global $objSmarty; 
		
  		$SelQuery="SELECT * from pj_location_city where LCityId!='' and LCitySta='Live' order by LCityName asc";
		$LocatList=$this->ExecuteQuery($SelQuery, "SELECT"); 
		$objSmarty->assign("LocatList",$LocatList);
		
	}	/*************************** 			Select Country List Details			***************************/
	function SelectLogList($ResVal,$PageVal)
	{	
  		global $objSmarty;$where_con="";$OrderCon=" order by LL.PageVisitorID desc ";$PageURL='log_mgmt.php?';
		
		$sortby=(isset($_GET['sortby'])) ? $_GET['sortby']:'';$seaname=(isset($_GET['seaname'])) ? $_GET['seaname']:'';
		$scountry=(isset($_GET['scountry'])) ? $_GET['scountry']:'';$seastate=(isset($_GET['seastate'])) ? $_GET['seastate']:'';
		if($seaname!=""){
			$where_con.=" and  LL.VisitorCityName LIKE '".trim($seaname)."%'";$PageURL.="seaname=".urlencode($seaname)."&";
		}
		if($scountry!=""){
			$where_con.=" and  PC.country_name LIKE '".trim($scountry)."%'";$PageURL.="seaname=".urlencode($scountry)."&";
		}
		if($seastate!=""){
			$where_con.=" and  PS.state_name LIKE '".trim($seastate)."%'";$PageURL.="seaname=".urlencode($seastate)."&";
		}
		$SortArr=array('country'=>'country_name','state'=>'State_name');
		if(!empty($sortby)){
			$SortDet=explode('_',$sortby);
			if(array_key_exists($SortDet[0],$SortArr)){
				$OrderCon=" order by ".$SortArr[$SortDet[0]]." ".$SortDet[1]."";
			}
			$PageURL.="sortby=".urlencode($sortby)."&";
		}				
		$objSmarty->assign("PageURL",$PageURL);
		
		$SelFields=array('LL.*','PC.country_name','PS.state_name');	//	Fields To select Table listing	
		$SelQuery="SELECT count(LL.PageVisitorID) from pj_user_landing_logs LL left join  pj_location_state PS on 	
		LL.state_id=PS.state_id left join pj_location_country PC on LL.country_id=PC.country_id where PS.country_ident !='' and 
		PC.country_id!='' $where_con $OrderCon";
		
		GeneralAdmin::SelectTableContentLists($ResVal,$SelQuery,$SelFields,$PageVal,$PageURL,15);
		
	}
	
}
?>