<?php   
class Blog extends MysqlFns
{		
	
	/*****************************************************************************************************************/
	/**********************************				Admin Panel			*********************************************/
	/*****************************************************************************************************************/
	
	/**********************************		For Admin Review Listing	*********************************************/
	function GetBlogLists($ResVal,$PageVal,$PageURL)
	{	
		global $objSmarty;$SelCon="";extract($_GET);$OrderCon=' order by PRO.BlogId desc';
		$pronamesea=(isset($_GET['pronamesea'])) ? $_GET['pronamesea']:'';$sortby=(isset($_GET['sortby'])) ? $_GET['sortby']:'';
		$showonly=(isset($_GET['showonly'])) ? $_GET['showonly']:'';   
 		if($pronamesea!=""){
			$SelCon.=" and PRO.BlogTitle LIKE '".trim($pronamesea)."%'";$PageURL.="pronamesea=".urlencode($pronamesea)."&";
		} 
 		$SortArr=array('username'=>'UL.UserFName','alertname'=>'PA.AlertName','post'=>'PRO.ReviewOn');
						
		if(!empty($sortby)){
			$SortDet=explode('_',$sortby);
			if(array_key_exists($SortDet[0],$SortArr)){
				$OrderCon=" order by ".$SortArr[$SortDet[0]]." ".$SortDet[1]."";
			}
		}	
		$objSmarty->assign("PageURL",$PageURL);
		$SelFields=array('PRO.*'); 
		$SelQuery="Select count(PRO.BlogId) from pj_blog PRO Where PRO.BlogId!='' $SelCon $OrderCon";
 		$Value=GeneralAdmin::SelectTableContentLists($ResVal,$SelQuery,$SelFields,$PageVal,$PageURL);
	}  
	
	/**********************************		For Admin Review Listing	*********************************************/
	function GetReplyLists($ResVal,$PageVal,$PageURL)
	{	 
		global $objSmarty;$SelCon="";extract($_GET);$OrderCon=' order by PRO.RevReplyId desc';
		$seaname=(isset($_GET['seaname'])) ? $_GET['seaname']:'';$sortby=(isset($_GET['sortby'])) ? $_GET['sortby']:'';
		$showonly=(isset($_GET['showonly'])) ? $_GET['showonly']:'';$alertname=(isset($_GET['alertname'])) ? $_GET['alertname']:''; 
 		if($alertname!=""){
			$SelCon.=" and PA.AlertName LIKE '".trim($alertname)."%'";$PageURL.="alertname=".urlencode($alertname)."&";
		} 
		if($seaname!=""){
			$SelCon.=" and UL.UserName LIKE '".trim($seaname)."%'";$PageURL.="seaname=".urlencode($seaname)."&";
		}  
		if($showsonly!="") {
				$SelCon.="and PRO.Status ='".trim($showsonly)."'";
				$PageURL.="showsonly=".urlencode($showsonly)."&";  
		} 
 		$SortArr=array('username'=>'UL.UserFName','alertname'=>'PA.AlertName','post'=>'PRO.ReplyOn');
						
		if(!empty($sortby)){
			$SortDet=explode('_',$sortby);
			if(array_key_exists($SortDet[0],$SortArr)){
				$OrderCon=" order by ".$SortArr[$SortDet[0]]." ".$SortDet[1]."";
			}
		}	
		$objSmarty->assign("PageURL",$PageURL);
		$SelFields=array('PRO.*','PR.*','PA.AlertName','UL.UserLName','UL.UserFName','PR.UserId as RevBy','UL.UserName'); 
		$SelQuery="Select count(PRO.RevReplyId) from pj_alert_review_reply PRO left join pj_alert_review PR on 
					PR.ReviewId=PRO.AlertRevId LEFT JOIN pj_alert_list PA ON PR.AlertId=PA.AlertId left join pj_user_list UL on
					UL.UserId=PRO.UserId Where PRO.RevReplyId!='' and PA.AlertId!='' $SelCon $OrderCon";
 		$Value=GeneralAdmin::SelectTableContentLists($ResVal,$SelQuery,$SelFields,$PageVal,$PageURL);
		//echo "<PRE>"; print_r($Value);exit;
	} 
	/**************************************		Select Posted Product Details		*****************************************/
	function SelectCommentLists($con_id)
	{
		global $objSmarty,$ngconfig;	
		$SelB="Select BlogTitle from pj_blog PRO Where md5(PRO.BlogId)='".$con_id."'";
		$SelB=$this->ExecuteQuery($SelB,"select");	
		$objSmarty->assign("SiteTitle",SiteMainTitle." - ".$SelB[0]['BlogTitle']." Comments");
		$objSmarty->assign("PageTitle",$SelB[0]['BlogTitle']." - Comments");
		
  		$SelFields=array('PRO.*','PR.*','UL.UserLName','UL.UserFName','PRO.UserId as ComBy','UL.UserName'); 	
		$SelQuery="Select count(PRO.BReplyId) from pj_blog_reply PRO left join pj_blog PR on PR.BlogId=PRO.BlogId left join
			pj_user_list UL on UL.UserId=PRO.UserId Where PRO.BReplyId!='' and PRO.BlogId!='' and 
			md5(PRO.BlogId)='".$con_id."' order by PRO.BReplyId asc";
		$Value=GeneralAdmin::SelectTableContentLists('BReplyList',$SelQuery,$SelFields,'compage',$PageURL);
   	}  	
	
	/*****************************************************************************************************************/
	/**********************************				User Panel			*********************************************/
	/*****************************************************************************************************************/
	
	/**********************************		For User Review Listing	*********************************************/
	function SelectBlogList()
	{	
		global $objSmarty;$SelCon="";extract($_GET);$OrderCon=' order by PRO.BlogId desc'; 
		$PageURL='reviews.html?';				
		 
		$SelFields=array('PRO.*','CONCAT("'.SiteMainPath.'","blog/",PRO.BlogSEO,".html") as BLogURL'); 
		$SelQuery="Select count(PRO.BlogId) from pj_blog PRO Where PRO.BlogId!='' $OrderCon";
 		$Value=GeneralAdmin::SelectTableContentLists('BlogList',$SelQuery,$SelFields,'BlogPage',$PageURL);
	} 
	/**************************************		Select Posted Product Details		*****************************************/
	function SelectBlogDetails($b_name)
	{
		global $objSmarty,$ngconfig;
		$SelQry="SELECT PL.* from pj_blog PL where PL.BlogSEO='".$b_name."' and PL.BlogStatus='1' order by PL.BlogId";
		$SProDet=$this->ExecuteQuery($SelQry, "select");
		if(sizeof($SProDet)!=0){
			$objSmarty->assign("SProDet",$SProDet[0]);
 			
 			$SelQuery="Select PRO.*,PR.*,UL.UserLName,UL.UserFName,PRO.UserId as ComBy,UL.UserName from pj_blog_reply PRO left join
				pj_blog PR on PR.BlogId=PRO.BlogId left join pj_user_list UL on UL.UserId=PRO.UserId Where PRO.BReplyId!='' and 
				PRO.BlogId!='' and PRO.BlogId='".$SProDet[0]['BlogId']."' order by PRO.BReplyId asc";
			$SelQuery=$this->ExecuteQuery($SelQuery, "select");
			$objSmarty->assign("BReplyList",$SelQuery); 			
 	   		 
			/****************		Assign Page Title To Smarty		*************************/				
			$objSmarty->assign('PageTitle',stripslashes($SProDet[0]['BlogTitle'])." Details");
		}
		else{
			Redirect(SiteMainPath.'blog.html');	
		}
	}  
	
	/*************************** 			Function For Add and edit projects details	***************************/
	function PostComments()
	{	
 		global $objSmarty,$ngconfig;extract($_POST);$ErrMsg=array(); 
		$FlagIns=true;  $Che_con='';
		 
		if(empty($Reply)){
			$ErrMsg[]= "Please Enter Comments"; 
		}  
 		if(count($ErrMsg)==0) {	 
 			$ProIns="insert into pj_blog_reply (BlogId,UserId,Name,Email,Reply,ReplyOn,ReplyIP) values('".$BlogId."',
					'".$_SESSION['PJ525_UserId']."','".$_SESSION['PJ525_UFirstName']." ".$_SESSION['PJ525_ULastName']."',
					'".$_SESSION['PJ525_UserMail']."','".$Reply."',now(),'".$_SERVER['REMOTE_ADDR']."')";
			$this->ExecuteQuery($ProIns,"insert"); 
  			Redirect(SiteMainPath.'blog/'.$_GET['b_name'].'.html?pro_msg=asucc');
 		}
		else{	
 			$objSmarty->assign("ErrMessage",implode('</p><p>',$ErrMsg));
			$objSmarty->assign("Arr",$_POST);
		}	
	}   
	
	/*************************** 			Function For Add and edit projects details	***************************/
	function PostSheddetails()
	{	//echo "<PRE>";print_r($_POST); exit;
 		global $objSmarty,$ngconfig;extract($_POST);$ErrMsg=array(); 
		$FlagIns=true;   
		 
		if(empty($NoofRollDoor)){
			$ErrMsg[]= "Please Enter Number Of Roller Door"; 
		}  
		if(empty($RollDoorLocation)){
			$ErrMsg[]= "Please Enter Roller Door Location"; 
		} 
		if(empty($NoofWindows)){
			$ErrMsg[]= "Please Enter Number Of Windows"; 
		}  
		if(empty($WindowsLocation)){
			$ErrMsg[]= "Please Enter Windows Location"; 
		} 
 		if(!empty($NoofRollDoor) || !empty($RollDoorLocation) || !empty($NoofWindows) || !empty($WindowsLocation) || !empty($NoofPADoor) || !empty($PADoorLocation) || !empty($NoofSkylight) || !empty($NoofVentilation)) {	 
 			$AltCon="Update pj_customize_shed set NoofRollDoor='".$NoofRollDoor."',RollDoorLocation='".$RollDoorLocation."',
					 	NoofWindows='".$NoofWindows."',WindowsLocation='".$WindowsLocation."',NoofPADoor='".$NoofPADoor."',
						PADoorLocation='".$PADoorLocation."',NoofSkylight='".$NoofSkylight."',PrefSize='".$PrefSize."',
						NoofVentilation='".$NoofVentilation."',AwningColor='".$Awning."' Where CustId='".$_SESSION['CustomId']."'";
			$this->ExecuteQuery($AltCon,"update"); 
  			Redirect(SiteMainPath.'customizeshed.html?pro_msg=asucc');
 		}
		else{	
 			$objSmarty->assign("ErrMessage",implode('</p><p>',$ErrMsg));
			$objSmarty->assign("Arr",$_POST);
		}	
	} 
	
	function AddUpdate()
	{		
		global $objSmarty;$ErrMsg=array();extract($_POST);$SelCon='';
		
		$con_id=(isset($_GET['con_id'])) ? $_GET['con_id']:'';
		if(empty($BlogTitle)){
			$ErrMsg[]="Please Enter Title";
		}
		else{
			if(!empty($con_id)){
				$SelCon=" and md5(BlogId)!='".$con_id."'";
			}
			$SelQuery="SELECT ST.* from pj_blog ST where BlogTitle='".$BlogTitle."' $SelCon";
			if(!GeneralAdmin::CheckDupRecord($SelQuery)) {
				$ErrMsg[]="Title Already Exists";
			}	
		}	 

		if(count($ErrMsg)==0)
		{
			if($AdminAction=='Update'){
			 	$AltCon="Update pj_blog set BlogTitle='".$BlogTitle."',BlogText='".$BlogText."' 
						 Where md5(BlogId)='".$con_id."'";
				$AltCheck=$this->ExecuteQuery($AltCon, "update"); 	
				if(!empty($AltCheck)){
					GeneralAdmin::MakeAltSEOUrlString('pj_blog','BlogId','BlogSEO',$con_id,$BlogTitle);
					Redirect('blog_mgmt.php?pro_msg=usucc');
				}
				else{
					$objSmarty->assign("ErrMessage", "IT IS ALREADY UPDATED PLEASE DO SOME MODIFICATIONS TO UPDATE"); 
				}
			}
			else{				
				$InsCon="insert into pj_blog(BlogTitle,BlogText,PostedOn) values('".$BlogTitle."','".$BlogText."',now())";
				$InsCon=$this->ExecuteQuery($InsCon,"insert");
				
				$con_id=mysql_insert_id();	
				GeneralAdmin::MakeAltSEOUrlString('pj_blog','BlogId','BlogSEO',$con_id,$BlogTitle);
				Redirect('blog_mgmt.php?pro_msg=asucc');
			}			
		}
		else
		{	$objSmarty->assign("ErrMessage",implode('<br/>',$ErrMsg));;$objSmarty->assign("Arr",$_POST);	}
	}	
}
?>