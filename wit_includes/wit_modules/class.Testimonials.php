<?php 
class Testimonials extends MysqlFns
{	
	function GetTestimonialsLists($ResVal,$PageVal)
	{
		global $objSmarty;$SelCon="";$PageURL='testimonials_mgmt.php?';$OrderCon=" order by ST.TestimonialsId desc ";
		$seaname=(isset($_GET['seaname'])) ? $_GET['seaname']:'';$showonly=(isset($_GET['showonly'])) ? $_GET['showonly']:'';
		$sortby=(isset($_GET['sortby']))?$_GET['sortby']:'';$joindate=(isset($_GET['joindate'])) ? $_GET['joindate']:'';
		if($seaname!=""){
			$SelCon.=" and ST.UserFName LIKE '".trim($seaname)."%'";
			$PageURL.="seaname=".urlencode($seaname)."&";
		}
		if($showonly!="")
		{
			$SelCon.="and ST.TestimonialsStatus = '".$showonly."'";$PageURL.="showonly=".urlencode($showonly)."&";
		}
		if($joindate!=""){ 
			$joindate= date('Y-m-d',strtotime("$joindate"));
			$SelCon.=" and DAY(ST.TestmonDate)=DAY('".$joindate."') and MONTH(ST.TestmonDate)=MONTH('".$joindate."') 
				and YEAR(ST.TestmonDate)=YEAR('".$joindate."')";
			$PageURL.="expire=".urlencode($expire)."&";
		}
				
		$SortArr=array('testby'=>'ST.UserFName','appsta'=>'ST.STAppSta','testdate'=>'ST.TestmonDate');
		if(!empty($sortby)){
			$SortDet=explode('_',$sortby);
			if(array_key_exists($SortDet[0],$SortArr)){
				$OrderCon=" order by ".$SortArr[$SortDet[0]]." ".$SortDet[1]."";
			}
			$PageURL.="sortby=".urlencode($sortby)."&";
		}
		$objSmarty->assign("PageURL",$PageURL);
		
		$SelFields=array('ST.*','TU.*'); 
		$SelQuery="SELECT count(ST.TestimonialsId) from tbl_testimonials ST left join pj_user_list TU on TU.UserId=ST.UserId where 		
				ST.TestimonialsId!='' $SelCon $OrderCon " ;
		$SUserList=GeneralAdmin::SelectTableContentLists($ResVal,$SelQuery,$SelFields,$PageVal,$PageURL,15);
 	}
	
	
	function TestimonialsAddUpdate($FileName,$Ctype)
	{	 
		global $objSmarty;$ErrMsg=array();extract($_POST);$SelCon='';
		$FLName="/^[A-Za-z_[:blank:]]{1,50}$/";
		$con_id=(isset($_GET['con_id'])) ? $_GET['con_id']:''; 
		/*if(empty($UserFName)){
			$ErrMsg[]="Please enter testimonials by";
		}
		elseif(!preg_match($FLName,$UserFName)){	
			$ErrMsg[]="Testimonials By not allowed numbers and special characters ";	
		}*/
		if(empty($TestimonialsDesc)){
			$ErrMsg[]="Please enter description";
		}	 
		if(count($ErrMsg)==0)
		{
			if($AdminAction=='Update'){
			 	$AltCon="Update tbl_testimonials set TestimonialsDesc='".$TestimonialsDesc."',UserId='".$UserId."' Where 
							md5(TestimonialsId)='".$con_id."'";
				$AltCheck=$this->ExecuteQuery($AltCon, "update"); 	
				if(!empty($AltCheck)){
					Redirect('testimonials_mgmt.php?pro_msg=usucc');
				}
				else{
					$objSmarty->assign("ErrMessage", "IT IS ALREADY UPDATED PLEASE DO SOME MODIFICATIONS TO UPDATE"); 
				}
			}
			else{ 
				$InsCon="insert into tbl_testimonials(UserId,TestimonialsDesc,TestmonDate,STAppSta) 
					values('".$UserId."','".$TestimonialsDesc."',now(),'1')";
				$this->ExecuteQuery($InsCon,"insert"); 
				Redirect('testimonials_mgmt.php?pro_msg=asucc');
			}
 		}
		else
		{	$objSmarty->assign("ErrMessage",implode('<br/>',$ErrMsg));;$objSmarty->assign("Arr",$_POST);	}
	}
	
	
	function PostTestimonialsUser()
	{	 
		global $objSmarty;$ErrMsg=array();extract($_POST);$SelCon='';
 		 
		if(empty($Reply)){
			$ErrMsg[]="Please enter comments";
		}	
		if(empty($VerifyCode)){
			$ErrMsg[]= 'Please enter Verify Code';
		}
		elseif($_SESSION['security_code']!= strtolower($_POST['VerifyCode'])){
			$ErrMsg[]='You have entered incorrect verification code. Please type it again';
		}	 

		if(count($ErrMsg)==0) {  
			$InsCon="insert into tbl_testimonials(UserId,TestimonialsDesc,TestmonDate,STAppSta) 
				values('".$_SESSION['PJ525_UserId']."','".$Reply."',now(),'0')";
			$this->ExecuteQuery($InsCon,"insert"); 
			Redirect('testimonial.html?pro_msg=asucc');
 		}
		else
		{	$objSmarty->assign("ErrMessage",implode('<br/>',$ErrMsg));$objSmarty->assign("Arr",$_POST);	}
 	}
	
	function SelectTestimonialList()
	{	
		global $objSmarty;$SelCon=""; 
		$SelQuery="SELECT TT.*,TU.* from tbl_testimonials TT left join pj_user_list TU on TU.UserId=TT.UserId where
				TT.TestimonialsId!='' and TT.TestimonialsStatus='1' order by TT.TestimonialsId desc";
		$TestmonLists=$this->ExecuteQuery($SelQuery,"select");  
		$objSmarty->assign('TestmonListUser',$TestmonLists); 
	} 
}
?>