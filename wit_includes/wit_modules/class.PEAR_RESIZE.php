<?php
class PEARImageResize extends MysqlFns
{
	function PEARImageResize($ImagePath, $ThumbPath, $Size,$Height)
	{
	  	if(is_dir(PEARServerPath))//server
			ini_set('include_path',PEAR_include_path);
		require_once 'Image/Transform.php';global $objSmarty;
		//create transform driver object
		$it = Image_Transform::factory('GD');
		if (PEAR::isError($it)){
			//$error[] = $it->getMessage();
			$errmsg = ErrorMessage($error,'400');
			$objSmarty->assign('errmsg', $errmsg);
			return false;
		}
		//load the original file
		$ret = $it->load($ImagePath);
		if (PEAR::isError($ret)) {
			//$error[] = $it->getMessage();
			$errmsg = ErrorMessage($error,'400');
			$objSmarty->assign('errmsg', $errmsg);
			return false;
		}
		//scale it to $Size px
		$ret = $it->fit($Size,$Height);
		if (PEAR::isError($ret)) {
			$error[] = $it->getMessage();
			$errmsg = ErrorMessage($error,'400');
			$objSmarty->assign('errmsg', $errmsg);
			return false;
		}
		//save it into a different file
		$ret = $it->save($ThumbPath);
		if (PEAR::isError($ret)) {
			$error[] = $it->getMessage();
			$errmsg = ErrorMessage($error,'400');
			$objSmarty->assign('errmsg', $errmsg);
			return false;
		}
	}	
}
?>