<?php   
class Glass extends MysqlFns
{		
 	/*****************************************************************************************************************/
	/**********************************				Admin Panel			*********************************************/
	/*****************************************************************************************************************/
	 function SelectCategoryList()
	{	
		global $objSmarty;
		$SelCat="select * from pj_category_main where CatId!=''";
		$ResCat= $this->ExecuteQuery($SelCat, "select");  
		$objSmarty->assign('CatList',$ResCat);
	} 
	
	/**************************************** 		Select product Images		*********************************************/
	function SelectImage($con_id){
		global $objSmarty;
		$SelImage="select * from pj_sell_images where md5(SellId)='".$con_id."'";
		$Img=$this->ExecuteQuery($SelImage,"select");	//echo "<PRE>";print_r($Img);exit;
		$objSmarty->assign('ImgList',$Img);
	}	
	/*************************** 			Function For Add and update  Seller details	***************************/
	function AddUpdateGlassDetails($con_id,$Type)
	{	//echo "<PRE>"; print_r($_POST); exit;
		global $objSmarty,$ngconfig;extract($_POST);$ErrMsg=array();$FExtArr=array('gif','png','jpg','jpeg');
		$FlagIns=true;$ImgUpload=false;$banIng='';$ItemImage='';$Che_con='';$AltImg='';$PhotoLoc=$ngconfig['GlassImageLoad'];
		$PageURL='glass_mgmt.php?';
				
		if(empty($GlassCode)){
			$ErrMsg[]= "Please enter Glass Code";
		}
		else {
			if(!empty($con_id)){
				$where_con=" and md5(GlassId)!='".$con_id."'";
			}
			$SelQuery="SELECT * from pj_glass_list where GlassCode='".$GlassCode."' $where_con and GlassId!=''";
			if(!GeneralAdmin::CheckDupRecord($SelQuery)){
				$ErrMsg[]="This Glass Code is already exists";
			}
 		}
 		if(empty($GlassName)){
			$ErrMsg[]= "Please enter Glass Name";
		}
		if(empty($GlassGroup)){
			$ErrMsg[]= "Please enter Glass Group";
		}  
		if(empty($Thickness)){
			$ErrMsg[]= "Please select Thickness";
		}	 
		if(empty($Sqfeet)){
			$ErrMsg[]= "Please select Square feet";
		}
 		if(empty($LRegularPrice)){
			$ErrMsg[]= "Please enter Lesportes Regular Price";
		}
		if(empty($LTemperedPrice)){
			$ErrMsg[]= "Please enter Lesportes Tempered Price";
		}  
 		if(empty($LPercent)){
			$ErrMsg[]= "Please enter Lesportes percentage";
		}
		if(empty($BPercent)){
			$ErrMsg[]= "Please enter Barazin percentage";
		}  

		if($_FILES['GlassImage']['name']!='' && $_FILES['GlassImage']['error']==0){	
			$path_info=pathinfo($_FILES['GlassImage']['name']);
			if(!in_array(strtolower($path_info['extension']),$FExtArr)) {	
				$ErrMsg[]= "Please Select Valid Image File Only [ ".implode(',',$FExtArr)." ]"; 
			}
		}  
 		if(sizeof($ErrMsg)==0){ 
			if($AdminAction=='Update') { 
 				if($_FILES['GlassImage']['name']!=''){
					if($_FILES['GlassImage']['name']!='' && $_FILES['GlassImage']['error']==0) {	
						$path_info=pathinfo($_FILES['GlassImage']['name']);$extension=strtolower($path_info['extension']);
						$GlassImage=date("Ymd").time().rand(5,10000).'.'.$path_info['extension'];
 						$ResizeArr=array("289||192||l_".$GlassImage,"140||145||m_".$GlassImage,"65||65||t_".$GlassImage);
						GeneralAdmin::WesoUploadedFile('Image',$_FILES['GlassImage']['tmp_name'],$PhotoLoc,$GlassImage,$ResizeArr);
						$AltImgDet=",GlassImage='".$GlassImage."'"; 
					}
				}	
			
				$AltPro = "Update pj_glass_list set GlassCode='".$GlassCode."',GlassName='".$GlassName."',Sqfeet='".$Sqfeet."',
						GlassGroup='".$GlassGroup."',Thickness='".$Thickness."',LRegularPrice='".$LRegularPrice."',
						LTemperedPrice='".$LTemperedPrice."',BRegularPrice='".$BRegularPrice."',LPercent='".$LPercent."',
						BPercent='".$BPercent."',BTemperedPrice='".$BTemperedPrice."'$AltImgDet Where md5(GlassId)='".$con_id."'";
				$Altcheck=$this->ExecuteQuery($AltPro, "update");
				GeneralAdmin::MakeAltSEOUrlString('pj_glass_list','GlassId','SEOGlassName',$con_id,$GlassName); 
 				$RedirStatus=true;$PageURL.='pro_msg=usucc';
 			}
			else {	
				if($_FILES['GlassImage']['name']!='' && $_FILES['GlassImage']['error']==0) {	
					$path_info=pathinfo($_FILES['GlassImage']['name']);$extension=strtolower($path_info['extension']);
					$GlassImage=date("Ymd").time().rand(5,10000).'.'.$path_info['extension'];
					$ResizeArr=array("289||192||l_".$GlassImage,"140||145||m_".$GlassImage,"65||65||t_".$GlassImage);
					GeneralAdmin::WesoUploadedFile('Image',$_FILES['GlassImage']['tmp_name'],$PhotoLoc,$GlassImage,$ResizeArr);
 				}
				$ProIns="insert into pj_glass_list(GlassCode,GlassName,GlassPostDate,GlassGroup,Thickness,Sqfeet,LRegularPrice,
						LTemperedPrice,LPercent,BPercent,GlassImage) values('".$GlassCode."','".$GlassName."',now(),
						'".$GlassGroup."','".$Thickness."','".$Sqfeet."','".$LRegularPrice."','".$LTemperedPrice."',
						'".$LPercent."','".$BPercent."','".$GlassImage."')";
 				$this->ExecuteQuery($ProIns,"insert");$GlassId=mysql_insert_id();
				GeneralAdmin::MakeAltSEOUrlString('pj_glass_list','GlassId','SEOGlassName',$GlassId,$GlassName); 
				$RedirStatus=true;$PageURL.='pro_msg=asucc';
			}
			if($RedirStatus) {
				Redirect($PageURL);
			}
			else {
				$objSmarty->assign("ErrMessage", "IT IS ALREADY UPDATED PLEASE DO SOME MODIFICATIONS TO UPDATE"); 
			}
		}
		else{	
			$objSmarty->assign("ErrMessage",implode('<br/>',$ErrMsg));$objSmarty->assign("Arr",$_POST);
		}	
	}	
	
	/****************************************		Get Product Lists		***********************************************/
 	function GetGlassLists($ResVal,$PageVal)
	{	
 		global $objSmarty;$SelCon="";extract($_GET);$PageURL='glass_mgmt.php?';$OrderCon=' order by PSL.GlassId desc';
		$pronamesea=(isset($_GET['pronamesea']))?$_GET['pronamesea']:'';$catsea=(isset($_GET['catsea']))?$_GET['catsea']:'';
 
		if($pronamesea!=""){
			$SelCon.=" and PSL.GlassName LIKE '".trim($pronamesea)."%'";$PageURL.="seaname=".urlencode($pronamesea)."&";
		}		
		if($code!=""){
			$SelCon.=" and PSL.GlassCode LIKE '".trim($code)."%'";$PageURL.="code=".urlencode($code)."&";
		} 
		$objSmarty->assign("PageURL",$PageURL);
		$SelQuery="Select count(PSL.GlassId) from pj_glass_list PSL Where PSL.GlassId!='' $SelCon $OrderCon ";
		$SelFields=array('PSL.*');
		$SUsersList=GeneralAdmin::SelectTableContentLists($ResVal,$SelQuery,$SelFields,$PageVal,$PageURL,15);
 	} 	
	
	/*************************** 			Function For Add and update  Seller details	***************************/
	function AddUpdateProfiletypeDetails($con_id,$Type)
	{	
		global $objSmarty,$ngconfig;extract($_POST);$ErrMsg=array();$FExtArr=array('bmp','gif','png','jpg','jpeg');
		$FlagIns=true;$ImgUpload=false;$banIng='';$ItemImage='';$Che_con='';$AltImg='';$PhotoLoc=$ngconfig['ShapeImageLoad'];
		$PageURL='profiletype_mgmt.php?';
				
		if(empty($ProfileNumber)){
			$ErrMsg[]= "Please enter Profile Number";
		}
		else {
			if(!empty($con_id)){
				$where_con=" and md5(PTypeId)!='".$con_id."'";
			}
			$SelQuery="SELECT * from pj_profile_type where ProfileNumber='".$ProfileNumber."' $where_con and PTypeId!=''";
			if(!GeneralAdmin::CheckDupRecord($SelQuery)){
				$ErrMsg[]="This Profile Number is already exists";
			}
 		}  
 		if($_FILES['ShapeImage']['name']!='' && $_FILES['ShapeImage']['error']==0){	
			$path_info=pathinfo($_FILES['ShapeImage']['name']);
			if(!in_array(strtolower($path_info['extension']),$FExtArr)) {	
				$ErrMsg[]= "Please Select Valid Image File Only [ ".implode(',',$FExtArr)." ]"; 
			}
		}  
 		if(sizeof($ErrMsg)==0){ 
			if($AdminAction=='Update') { 
 				if(!empty($_FILES['ShapeImage']['name'])){
					if($_FILES['ShapeImage']['name']!='' && $_FILES['ShapeImage']['error']==0) {	
						$path_info=pathinfo($_FILES['ShapeImage']['name']);$extension=strtolower($path_info['extension']);
						$ShapeImage=date("Ymd").time().rand(5,10000).'.'.$path_info['extension'];
 						$ResizeArr=array("289||192||l_".$ShapeImage,"140||145||m_".$ShapeImage,"65||65||t_".$ShapeImage);
						GeneralAdmin::WesoUploadedFile('Image',$_FILES['ShapeImage']['tmp_name'],$PhotoLoc,$ShapeImage,$ResizeArr);
						$AltImgDet=",ShapeImage='".$ShapeImage."'"; 
					}
				}	
			
				$AltPro = "Update pj_profile_type set ProfileNumber='".$ProfileNumber."'$AltImgDet 
						Where md5(PTypeId)='".$con_id."'";
				$Altcheck=$this->ExecuteQuery($AltPro, "update"); 
 				$RedirStatus=true;$PageURL.='pro_msg=usucc';
 			}
			else
			{	
				if($_FILES['ShapeImage']['name']!='' && $_FILES['ShapeImage']['error']==0) {	
					$path_info=pathinfo($_FILES['ShapeImage']['name']);$extension=strtolower($path_info['extension']);
					$ShapeImage=date("Ymd").time().rand(5,10000).'.'.$path_info['extension'];
					$ResizeArr=array("289||192||l_".$ShapeImage,"140||145||m_".$ShapeImage,"65||65||t_".$ShapeImage);
					GeneralAdmin::WesoUploadedFile('Image',$_FILES['ShapeImage']['tmp_name'],$PhotoLoc,$ShapeImage,$ResizeArr);
 				}
				$ProIns="insert into pj_profile_type(ProfileNumber,ShapeImage) values('".$ProfileNumber."','".$ShapeImage."')";
 				$this->ExecuteQuery($ProIns,"insert");$PTypeId=mysql_insert_id();  
				$RedirStatus=true;$PageURL.='pro_msg=asucc';
			}
			if($RedirStatus) {
				Redirect($PageURL);
			}
			else {
				$objSmarty->assign("ErrMessage", "IT IS ALREADY UPDATED PLEASE DO SOME MODIFICATIONS TO UPDATE"); 
			}
		}
		else{	
			$objSmarty->assign("ErrMessage",implode('<br/>',$ErrMsg));$objSmarty->assign("Arr",$_POST);
		}	
	}	
	
	/****************************************		Get Product Lists		***********************************************/
 	function GetProfiletypeLists($ResVal,$PageVal)
	{	
 		global $objSmarty;$SelCon="";extract($_GET);$PageURL='profiletype_mgmt.php?';$OrderCon=' order by PSL.PTypeId desc';
		$pronamesea=(isset($_GET['pronamesea']))?$_GET['pronamesea']:'';$catsea=(isset($_GET['catsea']))?$_GET['catsea']:'';
 
		if($pronamesea!=""){
			$SelCon.=" and PSL.ProfileNumber LIKE '".trim($pronamesea)."%'";$PageURL.="seaname=".urlencode($pronamesea)."&";
		} 
		$objSmarty->assign("PageURL",$PageURL);
		$SelQuery="Select count(PSL.PTypeId) from pj_profile_type PSL Where PSL.PTypeId!='' $SelCon $OrderCon ";
		$SelFields=array('PSL.*');
		$SUsersList=GeneralAdmin::SelectTableContentLists($ResVal,$SelQuery,$SelFields,$PageVal,$PageURL,15);
 	} 	 	
	
	/*************************** 			Function For Add and update  Seller details	***************************/
	function AddUpdateFinishTypeDetails($con_id,$Type)
	{	
		global $objSmarty,$ngconfig;extract($_POST);$ErrMsg=array();$FExtArr=array('bmp','gif','png','jpg','jpeg');
		$FlagIns=true;$ImgUpload=false;$banIng='';$ItemImage='';$Che_con='';$AltImg='';$PhotoLoc=$ngconfig['FinishImageLoad'];
		$PageURL='finishtype_mgmt.php?';
				
		if(empty($FinishType)){
			$ErrMsg[]= "Please enter Finish Type";
		}
		else {
			if(!empty($con_id)){
				$where_con=" and md5(FinishId)!='".$con_id."'";
			}
			$SelQuery="SELECT * from pj_finish_type where FinishType='".$FinishType."' $where_con and FinishId!=''";
			if(!GeneralAdmin::CheckDupRecord($SelQuery)){
				$ErrMsg[]="This Finish Type is already exists";
			}
 		}  
 		if($_FILES['FinishImage']['name']!='' && $_FILES['FinishImage']['error']==0){	
			$path_info=pathinfo($_FILES['FinishImage']['name']);
			if(!in_array(strtolower($path_info['extension']),$FExtArr)) {	
				$ErrMsg[]= "Please Select Valid Image File Only [ ".implode(',',$FExtArr)." ]"; 
			}
		}  
 		if(sizeof($ErrMsg)==0){ 
			if($AdminAction=='Update') { 
 				if(!empty($_FILES['FinishImage']['name'])){
					if($_FILES['FinishImage']['name']!='' && $_FILES['FinishImage']['error']==0) {	
						$path_info=pathinfo($_FILES['FinishImage']['name']);$extension=strtolower($path_info['extension']);
						$FinishImage=date("Ymd").time().rand(5,10000).'.'.$path_info['extension'];
 						$ResizeArr=array("289||192||l_".$FinishImage,"140||145||m_".$FinishImage,"65||65||t_".$FinishImage);
						GeneralAdmin::WesoUploadedFile('Image',$_FILES['FinishImage']['tmp_name'],$PhotoLoc,$FinishImage,
									$ResizeArr);
						$AltImgDet=",FinishImage='".$FinishImage."'"; 
					}
				}	
			
				$AltPro = "Update pj_finish_type set FinishType='".$FinishType."'$AltImgDet 
						Where md5(FinishId)='".$con_id."'";
				$Altcheck=$this->ExecuteQuery($AltPro, "update"); 
 				$RedirStatus=true;$PageURL.='pro_msg=usucc';
 			}
			else {	
				if($_FILES['FinishImage']['name']!='' && $_FILES['FinishImage']['error']==0) {	
					$path_info=pathinfo($_FILES['FinishImage']['name']);$extension=strtolower($path_info['extension']);
					$FinishImage=date("Ymd").time().rand(5,10000).'.'.$path_info['extension'];
					$ResizeArr=array("289||192||l_".$FinishImage,"140||145||m_".$FinishImage,"65||65||t_".$FinishImage);
					GeneralAdmin::WesoUploadedFile('Image',$_FILES['FinishImage']['tmp_name'],$PhotoLoc,$FinishImage,$ResizeArr);
 				}
				$ProIns="insert into pj_finish_type(FinishType,FinishImage) values('".$FinishType."','".$FinishImage."')";
 				$this->ExecuteQuery($ProIns,"insert");$PTypeId=mysql_insert_id();  
				$RedirStatus=true;$PageURL.='pro_msg=asucc';
			}
			if($RedirStatus) {
				Redirect($PageURL);
			}
			else {
				$objSmarty->assign("ErrMessage", "IT IS ALREADY UPDATED PLEASE DO SOME MODIFICATIONS TO UPDATE"); 
			}
		}
		else{	
			$objSmarty->assign("ErrMessage",implode('<br/>',$ErrMsg));$objSmarty->assign("Arr",$_POST);
		}	
	}	
	
	/****************************************		Get Product Lists		***********************************************/
 	function GetFinishTypeLists($ResVal,$PageVal)
	{	
 		global $objSmarty;$SelCon="";extract($_GET);$PageURL='profiletype_mgmt.php?';$OrderCon=' order by PSL.FinishId desc';
		$pronamesea=(isset($_GET['pronamesea']))?$_GET['pronamesea']:'';$catsea=(isset($_GET['catsea']))?$_GET['catsea']:'';
 
		if($pronamesea!=""){
			$SelCon.=" and PSL.FinishType LIKE '".trim($pronamesea)."%'";$PageURL.="seaname=".urlencode($pronamesea)."&";
		} 
		$objSmarty->assign("PageURL",$PageURL);
		$SelQuery="Select count(PSL.FinishId) from pj_finish_type PSL Where PSL.FinishId!='' $SelCon $OrderCon ";
		$SelFields=array('PSL.*');
		$SUsersList=GeneralAdmin::SelectTableContentLists($ResVal,$SelQuery,$SelFields,$PageVal,$PageURL,15);
 	} 	 			
	
	/****************************************		Get Product Lists		***********************************************/
 	function GetFramePriceLists($ResVal,$PageVal)
	{	
 		global $objSmarty;$SelCon="";extract($_GET);$PageURL='frame_mgmt.php?';$OrderCon=' order by PSL.FPriceId desc';
		$pronamesea=(isset($_GET['pronamesea']))?$_GET['pronamesea']:'';$finsea=(isset($_GET['finsea']))?$_GET['finsea']:'';
 		$groupsea=(isset($_GET['groupsea']))?$_GET['groupsea']:'';
		if($pronamesea!=""){
			$SelCon.=" and PT.ProfileNumber LIKE '".trim($pronamesea)."%'";$PageURL.="seaname=".urlencode($pronamesea)."&";
		}		
		if($finsea!=""){
			$SelCon.=" and PC.FinishType LIKE '".trim($finsea)."%'";$PageURL.="finsea=".urlencode($finsea)."&";
		} 	 
		$objSmarty->assign("PageURL",$PageURL);
		$SelQuery="Select count(PSL.FPriceId) from pj_frame_prices PSL left join pj_finish_type PC on PC.FinishId=PSL.FinishId
				left join pj_profile_type PT on PT.PTypeId=PSL.PTypeId Where PSL.FPriceId!='' $SelCon $OrderCon ";
		$SelFields=array('PSL.*','PC.*','PT.*');
		$SUsersList=GeneralAdmin::SelectTableContentLists($ResVal,$SelQuery,$SelFields,$PageVal,$PageURL,15);
 	}  	
	/*************************** 			Function For Add and update  Seller details	***************************/
	function AddUpdateFramePriceDetails($con_id,$Type)
	{	 
		global $objSmarty,$ngconfig;extract($_POST);$ErrMsg=array(); 
		$FlagIns=true;$ImgUpload=false;$banIng=''; $Che_con=''; 
		$PageURL='frame_mgmt.php?';
				
		if(empty($PTypeId)){
			$ErrMsg[]= "Please select profile type";
		} 
		if(empty($FinishId)){
			$ErrMsg[]= "Please select finish";
		} 
		if(empty($FramePrice)){
			$ErrMsg[]= "Please enter frame price";
		}   
		if(empty($FLPercent)){
			$ErrMsg[]= "Please enter Lesportes percentage";
		} 
		if(empty($FBPercent)){
			$ErrMsg[]= "Please enter Barazin percentage";
		} 
 		if(sizeof($ErrMsg)==0){ 
			if($AdminAction=='Update') { 
 				$AltPro = "Update pj_frame_prices set PTypeId='".$PTypeId."',FinishId='".$FinishId."',FramePrice='".$FramePrice."',
							 FLPercent='".$FLPercent."',FBPercent='".$FBPercent."' Where md5(FPriceId)='".$con_id."'";
				$Altcheck=$this->ExecuteQuery($AltPro, "update"); 
 				$RedirStatus=true;$PageURL.='pro_msg=usucc';
 			}
			else {	
				$ProIns="insert into pj_frame_prices(PTypeId,FinishId,FramePrice,FBPercent,FLPercent) values('".$PTypeId."',
						'".$FinishId."','".$FramePrice."','".$FBPercent."','".$FLPercent."')";
 				$this->ExecuteQuery($ProIns,"insert");  
				$RedirStatus=true;$PageURL.='pro_msg=asucc';
			}
			if($RedirStatus) {
				Redirect($PageURL);
			}
			else {
				$objSmarty->assign("ErrMessage", "IT IS ALREADY UPDATED PLEASE DO SOME MODIFICATIONS TO UPDATE"); 
			}
		}
		else{	
			$objSmarty->assign("ErrMessage",implode('<br/>',$ErrMsg));$objSmarty->assign("Arr",$_POST);
		}	
	}

	/**********************************		For Product Listing	*********************************************/
	function SelectProductLists()
	{
		global $objSmarty;$SelCon="";$PageURL=SiteMainPath.'stores.html';$OrderCon=' order by PRO.SellId desc';
		 
		$seastr=(isset($_POST['seastr']))?$_POST['seastr']:$_GET['seastr'];
		$PageURL.="?";
		if($seastr!=""){
			$SelCon.=" and PRO.ItemName LIKE '%".trim($seastr)."%'";$PageURL.="seastr=".urlencode($seastr)."&";
		}  
		$SelQuery="Select count(PRO.SellId) from pj_sell_list PRO Where PRO.SellId!='' and PRO.ItemStatus='1' 
					$SelCon $OrderCon ";					
		$SelFields=array('PRO.*',"CONCAT('".SiteMainPath."','garage/',SEOItemName,'.html') as ProductURL");	
		GeneralAdmin::SelectTableContentLists('ProductList',$SelQuery,$SelFields,'ProductPage',$PageURL,12);
 	}
	/**************************************		Select Posted Product Details		*****************************************/
	function SelectProductDetailsForView($act_type,$pro_name)
	{
		global $objSmarty,$ngconfig;
		$SelQry="SELECT PL.* from pj_sell_list PL where PL.SEOItemName='".$pro_name."' and PL.ItemStatus ='1' 
				order by PL.SellId ";
		$SProDet=$this->ExecuteQuery($SelQry, "select");
		if(sizeof($SProDet)!=0){
			$objSmarty->assign("SProDet",$SProDet[0]);
			
			$ProImg="select * from pj_sell_images where SellId='".$SProDet[0]['SellId']."'";
			$ProImgList=$this->ExecuteQuery($ProImg,"select");
			$objSmarty->assign("ImgList",$ProImgList);
			
			$ProAcc="select AccId from pj_sell_access where SellId='".$SProDet[0]['SellId']."'";
			$ProAccList=$this->ExecuteQuery($ProAcc,"select");//echo "<PRE>";print_r($ProAccList);exit; 
 			for($i=0;$i<sizeof($ProAccList);$i++){
				$AccRes[]=$ProAccList[$i]['AccId'];
 			}
			$AccRes=implode(',',$AccRes);   
 			$ProAcc1="select PA.*,PSA.* from pj_sell_access PSA left join pj_accessories PA on PA.AccId=PSA.AccId where 
						PSA.SellId='".$SProDet[0]['SellId']."'"; 
			$ProAccList1=$this->ExecuteQuery($ProAcc1,"select"); 
 			$objSmarty->assign("AccList",$ProAccList1);				
 	   
			/****************		Assign Page Title To Smarty		*************************/				
			$objSmarty->assign('PageTitle',stripslashes($SProDet[0]['ItemName'])." Details");
		}
		else{
			Redirect(SiteMainPath.'stores.html');	
		}
	} 
		
}

?>