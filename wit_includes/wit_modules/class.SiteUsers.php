<?php 	
class SiteUsers extends MysqlFns
{
	/***************************	Common function for validate form Fields	*************************************/
	function FormRegxExpressionCehck($ValiType,$FieldVal){///field checking
		/*
			define('UInputCheck',"/^[A-Za-z0-9_-]{1,50}$/");define('FirstLastName',"/^[A-Za-z_[:blank:]]{1,50}$/");
		*/
		switch($ValiType){///if select any one ..
			case 'UserName':
					$UNameRegx="/^[a-z\d_]{5,20}$/i";	
					if(!preg_match($UNameRegx,$FieldVal))
					{	return false;	}
					else{	return true;	}
				break;
			case 'EMail':
					$MailRegx="/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/";	
					if(!preg_match($MailRegx,$FieldVal))
					{	return false;	}
					else{	return true;	}
				break;
			case 'Password':
					$PassRegx="/^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*$/";
					if(!preg_match($PassRegx,$FieldVal))
					{	return false;	}
					else{	return true;	}
				break;
			case 'OnlyNo':
				$UPhoneRegx="/^[0-9]{1,15}$/";
				if(!preg_match($UPhoneRegx,$FieldVal))
				{	return false;	}
				else{	return true;	}
			break;
		}
	}
	
	
	/*********************************************    palanivelu  Admin Pannel********************************************/
	function SelectConList()
	{	
  		global $objSmarty; 
  		$SelQuery="SELECT PC.* from pj_location_state PS left join pj_location_country PC on PC.country_id=PS.country_ident 
				where PC.country_id!='' and PC.country_status='1' and PS.StateSta='1' group by PC.country_id 
				order by PC.country_name asc";
		$ConList=$this->ExecuteQuery($SelQuery, "SELECT"); 
		$objSmarty->assign("StateList",$ConList);
	}
	
	/************	Function For Business User Subscription payment Details		********************************/
	function SelectPackageList(){
		global $objSmarty;
		$SelSub="select * from pj_packages where PackId!=''";
		$SelSubUser=$this->ExecuteQuery($SelSub, "Select");
		if(sizeof($SelSubUser)>0){
			$objSmarty->assign("SelSubUser",$SelSubUser);
		}
	} 
	

	/******************************	Add and update User Details ***************************/
	function AddUpdateUserDetails()
	{	
 		global $objSmarty,$ngconfig;$ErrMsg=array();extract($_POST);$where_con='';$RedirStatus=false;
		$FLName='/[\'\/~`\!@#\$%\^&\*\(\)_\-\+=\{\}\[\]\|;:"\<\>,\.\?\\\]/'; 
		if(empty($UserFName)){
			$ErrMsg[]="Please enter first name";
		}
		elseif(preg_match($FLName,$UserFName)){	
			$ErrMsg[]='First Name not allowed special characters ';
		}
		if(empty($UserLName)){
			$ErrMsg[]="Please enter last name";
		}
		elseif(preg_match($FLName,$UserLName)){	
			$ErrMsg[]='Last Name not allowed special characters ';
		}
		elseif($UserLName==$UserFName){	
			$ErrMsg[]='first name and last name is same.';
		}
		if(empty($UserName)){
			$ErrMsg[]="Please enter user name";
		}
		elseif(preg_match($FLName,$UserName)){	
			$ErrMsg[]='User Name not allowed special characters ';
		}elseif($Register!="Update"){
				$SelQuery="SELECT * from pj_user_list where UserName='".$UserName."' $where_con";
				if(!GeneralAdmin::CheckDupRecord($SelQuery)){
					$ErrMsg[]="An account using this user name already exists";
				}
			}		
		if($Register!="Update"){
			if(empty($UserPass)){
				$ErrMsg[]="Please enter password";
			}
		}
		if(empty($UserEMail)){
			$ErrMsg[]="Please enter email";
		}
		if(empty($UserPayPalMail)){
			$ErrMsg[]="Please enter PayPal email";
		}
		else{
			if(!preg_match("/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/", $UserEMail)){
				$ErrMsg[]="Please enter valid email";
			}
			elseif($Register!="Update"){
				$SelQuery="SELECT * from pj_user_list where UserEMail='".$UserEMail."' $where_con";
				if(!GeneralAdmin::CheckDupRecord($SelQuery)){
					$ErrMsg[]="An account using this email address already exists";
				}
			}	
		} 
		/*if(empty($UserPhone))
			{	$ErrMsg[]="Please enter mobile number";	} */
		if(empty($UserCountry))
			{	$ErrMsg[]="Please select country";	}
		if(empty($UserState))
			{	$ErrMsg[]="Please select state";	}
		if(empty($UserCity))
			{	$ErrMsg[]="Please enter city";	}	
		if(empty($UserZip))
			{	$ErrMsg[]="Please enter zip";	} 
 		
		if(count($ErrMsg)==0)
		{	 //echo "<PRE>";print_r($_POST);exit;
  			if($Register!="Update") { 
				$mem_pass=OurTechEncryptionDecryption('Encrypt',$_POST['UserPass']);
  				$InsDet="insert into pj_user_list(UserFName,UserLName,UserName,UserPass,UserEMail,UserPhone,UserState,UserCity,
						UserZip,UserAdds,UJoinDate,URegIP,UserSta,UserPayPalMail,UserCountry) values('".$_POST['UserFName']."',
						'".$_POST['UserLName']."','".$_POST['UserName']."','".$mem_pass."','".$_POST['UserEMail']."',
						'".$_POST['UserPhone']."','".$_POST['UserState']."','".$_POST['UserCity']."','".$_POST['UserZip']."',
						'".$_POST['UserAdds']."',now(),'".$_SERVER['REMOTE_ADDR']."','1','".$_POST['UserPayPalMail']."',
						'".$_POST['UserCountry']."')"; 
				$this->ExecuteQuery($InsDet, "insert");$NewTemp=mysql_insert_id(); 
				// add billing details
				$billingSQL = "insert into pj_billing_info(UserId,UBillUName,UBillPhone,UBillEmail,UBillAdds,UBillCity,UBillCountry,
						UBillState,UBillZip) values('".$NewTemp."','".$_POST['UserFName']."',
						'".$_POST['UserPhone']."','".$_POST['UserEMail']."','".$_POST['UserAdds']."','".$_POST['UserCity']."',
						'".$_POST['UserCountry']."','".$_POST['UserState']."','".$_POST['UserZip']."')"; 
				$this->ExecuteQuery($billingSQL, "insert");
				
  					$MailBody='<table width="700" border="0" cellspacing="0" cellpadding="0"><tr>
							<td width="700" valign="top"> 
							<tr><td height="30"></td></tr>					
 					</td></tr></table>
					<table width="700" border="0" cellspacing="0" align="center" cellpadding="0"><tr>
						<td width="101" align="center" valign="top" style="padding-top:10px;"></td>
						<td width="564" valign="top" style="font:normal 13px Arial, Helvetica;line-height:25px;">
							<table width="564" border="0" cellspacing="0" cellpadding="0">
								<tr><td width="110" height="28"><strong>Name</strong></td>
									<td width="450" >: '.$_POST['UserFName'].' '. $_POST['UserLName'].'</td></tr>
								<tr><td height="28"><strong>Email</strong></td><td >: '.$_POST['UserEMail'].'</td></tr>
								<tr><td height="28"><strong>Username</strong></td><td >: '.$_POST['UserName'].'</td></tr>
								<tr><td height="28"><strong>Password</strong></td><td>: '.$_POST['UserPass'].'</td></tr>
								<tr><p>{MailConTent}</p></tr>
					</table></td></tr></table>'; 
				//echo $MailBody.'--'.$UserEMail;exit;
					GeneralAdmin::GetMailTemplateDetailsForMail($UserEMail,$MailBody,'UserNewLoginDetails');
 				Redirect(SiteMainPath.'signin.html?pro_msg=asucc');
			}
			else {	 
				$SelQry="SELECT UserId,UserEMail from pj_user_list where UserId='".$_SESSION['PJ525_UserId']."'";
				$SUserDet=$this->ExecuteQuery($SelQry, "select"); 
				if(sizeof($SUserDet)>0) { 
  				 	 $AltCon="Update pj_user_list set UserFName='".$UserFName."',UserLName='".$UserLName."',
					 	UserName='".$UserName."',UserEMail='".$UserEMail."',UserPhone='".$UserPhone."',
						UserCountry='".$UserCountry."',UserState='".$UserState."',UserCity='".$UserCity."',UserZip='".$UserZip."',
						UserAdds='".$UserAdds."',UserPayPalMail='".$UserPayPalMail."' 
						Where UserId='".$_SESSION['PJ525_UserId']."'";  
					$AltCheck=$this->ExecuteQuery($AltCon, "update");$NewTemp=$SUserDet[0]['UserId'];
 					 
					if(!empty($AltCheck)) {
						Redirect("editprofile.html?pro_msg=usucc");
					} 
					else {
						$objSmarty->assign("ErrMessage", "IT IS ALREADY UPDATED PLEASE DO SOME MODIFICATIONS TO UPDATE"); 
					}
  				}
				else{	$objSmarty->assign("ErrMessage",implode('</p><p>',$ErrMsg));$objSmarty->assign("Arr",$_POST);	}
			}
 		}
		else
		{	$objSmarty->assign("ErrMessage",implode('</p><p>',$ErrMsg));$objSmarty->assign("Arr",$_POST);	}
 	}	 
 
	/******************************	Add and update User Details ***************************/
	function AddUpdateShipDetails()
	{	
 		global $objSmarty,$ngconfig;$ErrMsg=array();extract($_POST);$where_con='';$RedirStatus=false;
		$FLName='/[\'\/~`\!@#\$%\^&\*\(\)_\-\+=\{\}\[\]\|;:"\<\>,\.\?\\\]/'; 
		 
		if(empty($UShipUName)){
			$ErrMsg[]="Please enter user name";
		}
		elseif(preg_match($FLName,$UShipUName)){	
			$ErrMsg[]='User Name not allowed special characters ';
		} 	 
		if(empty($UShipEMail)){
			$ErrMsg[]="Please enter email";
		} 
		if(empty($UShipPhone))
			{	$ErrMsg[]="Please enter mobile number";	}
		elseif(!is_numeric($UShipPhone)){
			$ErrMsg[]="Mobile number allowed number only";
		}
		if(empty($UShipCountry))
			{	$ErrMsg[]="Please select country";	}
		if(empty($UShipState))
			{	$ErrMsg[]="Please select state";	}
		if(empty($UShipCity))
			{	$ErrMsg[]="Please enter city";	}	
		if(empty($UShipZip))
			{	$ErrMsg[]="Please enter zip";	} 
 		
		if(count($ErrMsg)==0)
		{	
  			$SelQry="SELECT * from pj_shipping_info where UserId!='' and UserId='".$_SESSION['PJ525_UserId']."'";
			$ArrDet=$this->ExecuteQuery($SelQry, "select"); 
  			if(sizeof($ArrDet)==0) { // echo "<PRE>";print_r($_POST);exit;
   				$InsDet="insert into pj_shipping_info(UserId,UShipUName,UShipPhone,UShipEMail,UShipAdds,UShipCity,UShipCountry,
						UShipState,UShipZip) values('".$_SESSION['PJ525_UserId']."','".$_POST['UShipUName']."',
						'".$_POST['UShipPhone']."','".$_POST['UShipEMail']."','".$_POST['UShipAdds']."','".$_POST['UShipCity']."',
						'".$_POST['UShipCountry']."','".$_POST['UShipState']."','".$_POST['UShipZip']."')"; 
				$this->ExecuteQuery($InsDet, "insert");$NewTemp=mysql_insert_id();  
				Redirect("shipping_info.html?pro_msg=usucc");
			}
			else { 
				if(sizeof($ArrDet)>0) { 
  				 	 $AltCon="Update pj_shipping_info set UShipUName='".$UShipUName."',UShipPhone='".$UShipPhone."',
					 	UShipEMail='".$UShipEMail."',UShipAdds='".$UShipAdds."',UShipCity='".$UShipCity."',
						UShipCountry='".$UShipCountry."',UShipState='".$UShipState."',UShipZip='".$UShipZip."'
						Where UserId='".$_SESSION['PJ525_UserId']."'";  
					$AltCheck=$this->ExecuteQuery($AltCon, "update");$NewTemp=$SUserDet[0]['UserId'];
 					 
					if(!empty($AltCheck)) {
						Redirect("shipping_info.html?pro_msg=usucc");
					} 
					else {
						$objSmarty->assign("ErrMessage", "IT IS ALREADY UPDATED PLEASE DO SOME MODIFICATIONS TO UPDATE"); 
					}
  				}
				else{	$objSmarty->assign("ErrMessage",implode('</p><p>',$ErrMsg));$objSmarty->assign("Arr",$_POST);	}
			}
 		}
		else
		{	$objSmarty->assign("ErrMessage",implode('</p><p>',$ErrMsg));$objSmarty->assign("Arr",$_POST);	}
 	}	 
 
	/******************************	Add and update User Details ***************************/
	function AddUpdateBillDetails()
	{	
 		global $objSmarty,$ngconfig;$ErrMsg=array();extract($_POST);$where_con='';$RedirStatus=false;
		$FLName='/[\'\/~`\!@#\$%\^&\*\(\)_\-\+=\{\}\[\]\|;:"\<\>,\.\?\\\]/'; 
		 
		if(empty($UBillUName)){
			$ErrMsg[]="Please enter user name";
		}
		elseif(preg_match($FLName,$UBillUName)){	
			$ErrMsg[]='User Name not allowed special characters ';
		} 	 
		if(empty($UBillEmail)){
			$ErrMsg[]="Please enter email";
		} 
		if(empty($UBillPhone))
			{	$ErrMsg[]="Please enter mobile number";	}
		elseif(!preg_match('/^[0-9\(\)\- ]+$/', $UBillPhone)) { //if(!is_numeric($UBillPhone)){
			$ErrMsg[]="Mobile number allowed number only <br/> or this format (123) XXX-XXXX";
		}
		if(empty($UBillCountry))
			{	$ErrMsg[]="Please select country";	}
		if(empty($UBillState))
			{	$ErrMsg[]="Please select state";	}
		if(empty($UBillCity))
			{	$ErrMsg[]="Please enter city";	}	
		if(empty($UBillZip))
			{	$ErrMsg[]="Please enter zip";	} 
 		
		if(count($ErrMsg)==0)
		{	
  			$SelQry="SELECT * from pj_billing_info where UserId!='' and UserId='".$_SESSION['PJ525_UserId']."'";
			$ArrDet=$this->ExecuteQuery($SelQry, "select"); 
  			if(sizeof($ArrDet)==0) { // echo "<PRE>";print_r($_POST);exit;
   				$InsDet="insert into pj_billing_info(UserId,UBillUName,UBillPhone,UBillEmail,UBillAdds,UBillCity,UBillCountry,
						UBillState,UBillZip) values('".$_SESSION['PJ525_UserId']."','".$_POST['UBillUName']."',
						'".$_POST['UBillPhone']."','".$_POST['UBillEmail']."','".$_POST['UBillAdds']."','".$_POST['UBillCity']."',
						'".$_POST['UBillCountry']."','".$_POST['UBillState']."','".$_POST['UBillZip']."')"; 
				$this->ExecuteQuery($InsDet, "insert");$NewTemp=mysql_insert_id();  
				Redirect("billing_info.html?pro_msg=usucc");
			}
			else { 
				if(sizeof($ArrDet)>0) { 
  				 	 $AltCon="Update pj_billing_info set UBillUName='".$UBillUName."',UBillPhone='".$UBillPhone."',
					 	UBillEmail='".$UBillEmail."',UBillAdds='".$UBillAdds."',UBillCity='".$UBillCity."',
						UBillCountry='".$UBillCountry."',UBillState='".$UBillState."',UBillZip='".$UBillZip."'
						Where UserId='".$_SESSION['PJ525_UserId']."'";  
					$AltCheck=$this->ExecuteQuery($AltCon, "update");$NewTemp=$SUserDet[0]['UserId'];
 					 
					if(!empty($AltCheck)) {
						Redirect("billing_info.html?pro_msg=usucc");
					} 
					else {
						$objSmarty->assign("ErrMessage", "IT IS ALREADY UPDATED PLEASE DO SOME MODIFICATIONS TO UPDATE"); 
					}
  				}
				else{	$objSmarty->assign("ErrMessage",implode('</p><p>',$ErrMsg));$objSmarty->assign("Arr",$_POST);	}
			}
 		}
		else
		{	$objSmarty->assign("ErrMessage",implode('</p><p>',$ErrMsg));$objSmarty->assign("Arr",$_POST);	}
 	}	
	
	/*************************** 				Function For All Users Edit  Details ***************************/
	function SubscribeToPaymentGatWay($con_id)
	{	
		global $objSmarty,$ngconfig;$ErrMsg=array();$FlagIns=true;$SitePath=$ngconfig['SiteGlobalPath'];extract($_POST); 
		if($WebType=='Authorization'){
			
			if(empty($x_card_type))
				{	$ErrMsg[]="Please enter Card Type ";	}	
			if(empty($x_card_num))
				{	$ErrMsg[]="Please enter Credit Card Number";	} 
			if(empty($month))
				{	$ErrMsg[]="Please select month";	}
			if(empty($year))
				{	$ErrMsg[]="Please select year";	}
			if(empty($x_card_code))
				{	$ErrMsg[]="Please enter CCV code";	}
				
			if(count($ErrMsg)==0){ 
				$SelCom="select TS.* from pj_packages TS left join pj_user_subscribe_details SUB on 
						SUB.PackId=TS.PackId WHERE md5(SUB.UserId)='".$con_id."' "; 
				$ComDet=$this->ExecuteQuery($SelCom,"select");
			
				$SelMem = "SELECT * from pj_user_list where md5(UserId)='".$con_id."'";
				$MemDet= $this->ExecuteQuery($SelMem,"select");
				if(sizeof($ComDet)==1 && sizeof($MemDet)==1)
				{	
					if(!empty($Amount)){
						  $CTotalAmt=$Amount;
					}else{
						$CTotalAmt=$ComDet[0]['PackAmt'];
					}
				} 
				//if(!empty($CTotalAmt) && $CTotalAmt>0){
					$SelQry="select TU.*,TS.*,ST.state_name,CN.country_name from pj_user_list TU left join 
						pj_user_subscribe_details TS on TS.UserId=TU.UserId left join pj_location_state ST on
						TU.UserState=ST.state_id left join pj_location_country CN	on ST.country_ident=CN.country_id where 
						md5(TU.UserId)='".$con_id."' and ST.state_id!='' and CN.country_id!='' ";
					$MLocDet=$this->ExecuteQuery($SelQry,"select");
					
					//echo "<PRE>"; print_r($MLocDet);exit; 
					$creditCardType =urlencode($x_card_type);$creditCardNumber = urlencode($x_card_num);
					$exp_Month = str_pad($month, 2, '0', STR_PAD_LEFT);
					$x_exp_date=$exp_Month.$year;
					$cvv2Number = urlencode($x_card_code);
					$currencyCode="USD";$amount = urlencode(number_format($CTotalAmt,2));
					//$CompanyName=$MLocDet[0]['BusinessCompanyName']; 
					$CompanyAdminName=$MLocDet[0]['UserName']; 
					$CSubscribeMonth=$ComDet[0]['PackDuration']; 
					$Email=$MLocDet[0]['UserEMail'];
					$address=$MLocDet[0]['UserAdds'];
					$Phone=$MLocDet[0]['UserPhone'];
					$CITY=$MLocDet[0]['UserCity'];
					$state=$MLocDet[0]['state_name'];
					$zip=$MemDet[0]['UserZip'];
					$COUNTRYCODE=$MLocDet[0]['country_name'];
					
					$ProdileInfo="FIRSTNAME=$UserName&STREET=$address&CITY=$CITY&PHONE=$Phone&STATE=$state"
					."&ZIP=$zip&COUNTRYCODE=$COUNTRYCODE&";
			
									/* Construct the request string that will be sent to PayPal.
					The variable $nvpstr contains all the variables and is a name value pair string with & as a delimiter */
					
					$nvpstr="&PAYMENTACTION=Sale&AMT=$amount&CREDITCARDTYPE=$creditCardType&ACCT=$creditCardNumber&EXPDATE=".
							 $x_exp_date."&CVV2=$cvv2Number&".$ProdileInfo;
					
					/* Make the API call to PayPal, using API signature.
					The API response is stored in an associative array called $resArray */
					
					/****************			Class Objects					*************************/
					$ObjPaypal	=new PaypalClass;
						
					$resArray=$ObjPaypal->hash_call("doDirectPayment",$nvpstr);
					
					if(is_array($resArray))
					{			
						$ack = strtoupper($resArray["ACK"]);
						if($ack!="SUCCESS")
						{
							$ErrMsg='<table class="api" width=350>';
							foreach($resArray as $key => $value)
							{
								if (preg_match("/LONGMESSAGE/i",$key) || preg_match("/SHORTMESSAGE/i",$key)) {
									$ErrMsg.='<tr><td></td></tr>';
								}	
							}
							$ErrMsg.='<tr><td>The credit card information you entered is not valid or incomplete.  Please recheck the information and try again.</td></tr>';
							$ErrMsg.='</table>';$PaymentCheck=false;
							
						}
						else{	$PaymentCheck=true;		}		   		
					}
					else{	$PaymentCheck=false;$ErrMsg='Invalid Server Response.';	}
					 
				
					if($PaymentCheck){ 
						$OrderNumber=CreateRandomNumber(8);
						$InvoiceNum=$resArray["TRANSACTIONID"];
						 $InsQuey="insert into pj_payments(UserId,TransactionPayType,TransactionOrderNumber,TransactionPayAmount, 	
								TransactionInvoiceNumber,TransactionDate,TransactionStatus,PackId) 
								values('".$MLocDet[0]['UserId']."','CreditCard','".$OrderNumber."',
								'".number_format($CTotalAmt,2,'.','')."','".$InvoiceNum."',now(),'Paid',
								'".$ComDet[0]['PackId']."') ";
						$this->ExecuteQuery($InsQuey, "insert");$UTransactionID=mysql_insert_id();	
 					 
						$SubEDate=strtotime('+'.$ComDet[0]['PackDuration'].' days',strtotime(SiteDateTime));
						$SubExpireOn=date('Y-m-d H:i:s',$SubEDate); 
						
						 $AltCheck="Update pj_user_subscribe_details set SubExpireOn='".$SubExpireOn."' 
						 		where md5(UserId)='".$con_id."'";	
								
						$this->ExecuteQuery($AltCheck, "update");
						
						/***************	Update Deal Remaining Stock		********************************/
						$Subject=$InvoiceNum.' - Payment Subscription Details From '.SiteMainTitle;
						$MemPass=OurTechEncryptionDecryption('Decrypt',$MemDet[0]['UserPass']);	
						$MailBody='<table width="800" border="0" cellspacing="0" cellpadding="0"><tr><td width="700" valign="top">
							<p style="padding:5px 0px 10px 0px; margin:0px; font:13px Arial; line-height:25px;padding-left:10px;">
							Order Invoice Number - 
							<span style="color:#009900; margin:0px;padding:10px 0px;">'.$InvoiceNum.'.</span></p>
							</td></tr>
							<tr><td height="30"></td></tr>
							<tr><td width="110" height="28"><strong>First Name</strong>:'.
							$MemDet[0]['UserFName'].'</td>
							</tr>
							<tr><td width="110" height="28"><strong>Last Name</strong>:
							'.$MemDet[0]['UserLName'].'</td></tr>
							<tr><td width="110" height="28"><p>Welcome to MeeDoom.com </p> </tr>
							<tr><td width="110" height="28"><p>Your registration is complete!  Thank you for signing up with us.</p> </tr>
 							</table><p style="margin:0px;padding:5px 0px;"></p>'; 
										
							$headers  = "MIME-Version: 1.0\r\n";
							$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
							$headers .= 'From: '.AdminProName.'<'.AdminMail."> \r\n";		
									
						//GeneralAdmin::GetMailTemplateDetailsForMail($MemDet[0]['UserEMail'],$MailBody,'PaymentSubscription');	
						GeneralAdmin::WesoIshMailTemplate($MemDet[0]['UserEMail'],$Subject,$MailBody,$headers); 
 						Redirect(HTTPSURLPath.'signin.html?pro_msg=psucc');
					}
					else{
						$objSmarty->assign("ErrorMessage", $ErrMsg);$objSmarty->assign('Arr', $_POST);
					}
				/*}
				else{
					$objSmarty->assign('ErrorMessage','Total Amount should be greater than zero'); 
				}*/
			}
			else{
				$objSmarty->assign('ErrorMessage',implode('<br/>',$ErrMsg));	$objSmarty->assign('Arr',$_POST);
			}
		}
		else{	 
			$SelCom="select TS.*,SUB.* from pj_packages TS left join pj_user_subscribe_details SUB on 
					SUB.PackId=TS.PackId WHERE md5(SUB.UserId)='".$con_id."' "; 
			$ComDet=$this->ExecuteQuery($SelCom,"select");
		
			$SelMem = "SELECT * from pj_user_list where md5(UserId)='".$con_id."'";
			$MemDet= $this->ExecuteQuery($SelMem,"select");
			if(sizeof($ComDet)==1 && sizeof($MemDet)==1)
			{	
				if(!empty($Amount)){
					  $CTotalAmt=$Amount;
				}else{
					$CTotalAmt=$ComDet[0]['PackAmt'];
				}
			}
			$SelQry="select TU.*,TS.*,ST.state_name,CN.country_name from pj_user_list TU left join 
				pj_user_subscribe_details TS on TS.UserId=TU.UserId left join pj_location_state ST on
				TU.UserState=ST.state_id left join pj_location_country CN on ST.country_ident=CN.country_id where 
				md5(TU.UserId)='".$con_id."' and ST.state_id!='' and CN.country_id!='' ";
			$MLocDet=$this->ExecuteQuery($SelQry,"select");	
			 
			$OrderNumber=CreateRandomNumber(8);
 			 $InsQuey="insert into pj_payments(UserId,TransactionPayType,TransactionOrderNumber,TransactionPayAmount, 	
					TransactionInvoiceNumber,TransactionDate,TransactionStatus,PackId) 
					values('".$MLocDet[0]['UserId']."','Paypal','".$OrderNumber."',
					'".number_format($CTotalAmt,2,'.','')."','".$InvoiceNum."',now(),'Paid',
					'".$ComDet[0]['PackId']."') ";
			$this->ExecuteQuery($InsQuey, "insert");$UTransactionID=mysql_insert_id();	
			//echo $UTransactionID;exit;
			$SubEDate=strtotime('+'.$ComDet[0]['PackDuration'].' days',strtotime(SiteDateTime));
			$SubExpireOn=date('Y-m-d H:i:s',$SubEDate); 
			
			 $AltCheck="Update pj_user_subscribe_details set SubExpireOn='".$SubExpireOn."' 
					where md5(UserId)='".$con_id."'";	
 			$this->ExecuteQuery($AltCheck, "update");
				
			$SelDet="select * from pj_payment_settings where PayTypeId!=''";
			$PaypalDet=$this->ExecuteQuery($SelDet,"select");	
		 
			//echo "<PRE>";print_r($MLocDet);print_r($_POST);exit;	
			if($PaypalDet[0]['ServerType'] == '0')
				$PaypalURL = 'https://www.sandbox.paypal.com/row/cgi-bin/webscr';
			else
				$PaypalURL = 'https://www.paypal.com/row/cgi-bin/webscr';
			
			echo "<html>\n";
			echo "<head><title>Processing Payment...</title></head>\n";
			echo "<body onLoad=\"document.form.submit();\">\n";
			echo "<center><h3><strong>Please wait, your payment is being processed.".
					"Do not refresh your browser...</strong></h3></center>\n";
			echo "<div align='center'><img src='".SiteMainPath."images/loader.gif' border='0' /></div>\n";
			echo "<form method=\"post\" name=\"form\" action=\"".$PaypalURL."\">\n"; ?>
			<input type="hidden" name="cmd" value="_xclick">
			<input type="hidden" name="business" value="<?php echo $MLocDet[0]['UserPayPalMail']; ?>">
			<input type="hidden" name="currency_code" value="USD">
			<input type="hidden" name="rm" value="2">
			<input type="hidden" name="amount" value="<?php echo number_format($CTotalAmt,2); ?>">
			<input type="hidden" name="item_number" value="<?php echo md5($ComDet[0]['SubscribeID']); ?>">
			<input type="hidden" name="item_number1" value="<?php echo md5($UTransactionID); ?>">
			<input type="hidden" name="item_name" value="Subscription Payment">
			<input type="hidden" name="return" value="<?php echo HTTPSURLPath.'signin.html?pro_msg=psucc&TranId='.md5($UTransactionID); ?>" />
			<input type="hidden" name="cancel_return" value="<?php echo HTTPSURLPath.'payment.html?act_type=subscribe&totalval='.$CTotalAmt.'&con_id='.md5($MLocDet[0]['UserId']); ?>" />
			<?php
			echo "</form>\n";
			echo "</body></html>\n"; 
	}	
	}	
		
	/*************************** 				Function For All Users View  Details ***************************/
	function SelectUserBillinfo($con_id){
		global $objSmarty;
		if($con_id!=''){
			$Wherecon=" and md5(UserId)='".$con_id."'";
		}
		else{
			$Wherecon=" and UserId='".$_SESSION['PJ525_UserId']."'";
		}
		$SelQry="SELECT * from pj_billing_info where UserId!='' $Wherecon";
		$ArrDet=$this->ExecuteQuery($SelQry, "select"); 
		if(sizeof($ArrDet)>0){ 
 			$SelQry="SELECT LC.country_name,LS.state_name from pj_location_state LS left join pj_location_country LC on 	
				LS.country_ident=LC.country_id where state_id='".$ArrDet[0]['UBillState']."' ";
			$SateDet=$this->ExecuteQuery($SelQry, "select"); 
			if(sizeof($SateDet)>0){ 	
				$ArrDet[0]['country_name']=$SateDet[0]['country_name'];$ArrDet[0]['state_name']=$SateDet[0]['state_name'];
			}	 
			$objSmarty->assign("Arr",$ArrDet[0]); //echo "<PRE>"; print_r($ArrDet[0]);exit;
			return true;
		}
		else{
			$SelQry="SELECT * from pj_user_list  where UserId!='' $Wherecon";
			$ArrDet=$this->ExecuteQuery($SelQry, "select"); //echo "<PRE>"; print_r($ArrDet);exit;
			$InsDet="insert into pj_billing_info(UserId,UBillUName,UBillPhone,UBillEmail,UBillAdds,UBillCity,UBillCountry,
						UBillState,UBillZip) values('".$_SESSION['PJ525_UserId']."','".$ArrDet[0]['UserName']."',
					'".$ArrDet[0]['UserPhone']."','".$ArrDet[0]['UserEMail']."','".$ArrDet[0]['UserAdds']."',
					'".$ArrDet[0]['UserCity']."','".$ArrDet[0]['UserCountry']."','".$ArrDet[0]['UserState']."',
					'".$ArrDet[0]['UserZip']."')"; 
			$this->ExecuteQuery($InsDet, "insert"); 
			Redirect("billing_info.html");
		}
	}			
		
	/*************************** 				Function For All Users View  Details ***************************/
	function SelectUserShipinfo($con_id){
		global $objSmarty;
		if($con_id!=''){
			$Wherecon=" and md5(UserId)='".$con_id."'";
		}
		else{
			$Wherecon=" and UserId='".$_SESSION['PJ525_UserId']."'";
		}
		$SelQry="SELECT * from pj_shipping_info  where UserId!='' $Wherecon";
		$ArrDet=$this->ExecuteQuery($SelQry, "select"); 
		if(sizeof($ArrDet)>0){ 
 			$SelQry="SELECT LC.country_name,LS.state_name from pj_location_state LS left join pj_location_country LC on 	
				LS.country_ident=LC.country_id where state_id='".$ArrDet[0]['UShipState']."' ";
			$SateDet=$this->ExecuteQuery($SelQry, "select"); 
			if(sizeof($SateDet)>0){ 	
				$ArrDet[0]['country_name']=$SateDet[0]['country_name'];$ArrDet[0]['state_name']=$SateDet[0]['state_name'];
			}	 
			$objSmarty->assign("Arr",$ArrDet[0]); //echo "<PRE>"; print_r($ArrDet[0]);exit;
			return true;
		}
		else{
			$SelQry="SELECT * from pj_user_list  where UserId!='' $Wherecon";
			$ArrDet=$this->ExecuteQuery($SelQry, "select"); //echo "<PRE>"; print_r($ArrDet);exit;
			$InsDet="insert into pj_shipping_info(UserId,UShipUName,UShipPhone,UShipEMail,UShipAdds,UShipCity,UShipCountry,
					UShipState,UShipZip) values('".$_SESSION['PJ525_UserId']."','".$ArrDet[0]['UserName']."',
					'".$ArrDet[0]['UserPhone']."','".$ArrDet[0]['UserEMail']."','".$ArrDet[0]['UserAdds']."',
					'".$ArrDet[0]['UserCity']."','".$ArrDet[0]['UserCountry']."','".$ArrDet[0]['UserState']."',
					'".$ArrDet[0]['UserZip']."')"; 
			$this->ExecuteQuery($InsDet, "insert");  
		}
	}		
		
	/*************************** 				Function For All Users View  Details ***************************/
	function SelectUserDetailsForUser($con_id){
		global $objSmarty;
		if($con_id!=''){
			$Wherecon=" and md5(UserId)='".$con_id."'";
		}
		else{
			$Wherecon=" and UserId='".$_SESSION['PJ525_UserId']."'";
		}
		$SelQry="SELECT * from pj_user_list  where UserId!='' $Wherecon";
		$ArrDet=$this->ExecuteQuery($SelQry, "select"); 
		if(sizeof($ArrDet)>0){ 
 			$SelQry="SELECT LC.country_name,LS.state_name from pj_location_state LS left join pj_location_country LC on 	
				LS.country_ident=LC.country_id where state_id='".$ArrDet[0]['UserState']."' ";
			$SateDet=$this->ExecuteQuery($SelQry, "select"); 
			if(sizeof($SateDet)>0){ 	
				$ArrDet[0]['country_name']=$SateDet[0]['country_name'];$ArrDet[0]['state_name']=$SateDet[0]['state_name'];
			}	 
			$objSmarty->assign("Arr",$ArrDet[0]); //echo "<PRE>"; print_r($ArrDet[0]);exit;
			return true;
		}
		else{
			return false;
		}
	}	
		
	/*************************** 				Function For All Users View  Details ***************************/
	function SelectUserPortfolio(){
		global $objSmarty; 
		$SelDet="select Portfolio_image from pj_user_portfolio where ImageId!='' and UserId='".$_SESSION['PJ525_UserId']."' ";
		$ArrDet=$this->ExecuteQuery($SelDet, "select");  
		$objSmarty->assign("Arr",$ArrDet); //echo "<PRE>"; print_r($ArrDet[0]);exit;
 	}
	
	/********************************** User Forgot password send to mail in User *******************************/
	function UserForgotPassword(){ 
		global $objSmarty;$ErrMsg=array();extract($_POST);
		if(empty($UserEMail)){
			$ErrMsg[]="Please Enter MailID";
		}
		elseif(!preg_match("/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/", $UserEMail)){
			$ErrMsg[]="Please Enter Valid MailID";
		}
		
		if(sizeof($ErrMsg)==0){ 
		
			$SelQry="select UserEMail,UserFName,UserLName,UserPass,UserSta,UserName from pj_user_list where 
					UserEMail='".$_POST['UserEMail']."' ";
			$MemDet=$this->ExecuteQuery($SelQry,"select");
			if(sizeof($MemDet)>0){
				if($MemDet[0]['UserSta'] == '1'){/// status checking...
					 $MemPass=OurTechEncryptionDecryption('Decrypt',$MemDet[0]['UserPass']);					
	 				$MailBody='<tr><td height="10">&nbsp;</td></tr>
								<tr><td height="10"><p style=" padding-left:10px;margin:10px;">{MailConTent}</p>
								<table width="655" cellpadding="0" cellspacing="0" border="0" style="padding-left:25px;">
								<tr><td height="28"><strong>First Name</strong></td><td >: '.$MemDet[0]['UserFName'].'</td></tr>
								<tr><td height="28"><strong>Last Name</strong></td><td >: '.$MemDet[0]['UserLName'].'</td></tr>
								<tr><td height="28"><strong>User Name</strong></td><td >: '.$MemDet[0]['UserName'].'</td></tr>
								<tr><td height="28"><strong>Email</strong></td><td >: '.$MemDet[0]['UserEMail'].'</td></tr>
								<tr><td width="150" height="28"><strong>Password </strong></td><td >: '.$MemPass.'</td></tr>
								</table>	
							</td></tr>';  
					$Subject="Login Details From ".SiteMainTitle;
					$headers  = "MIME-Version: 1.0\r\n";
					$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
					$headers .= 'From: '.AdminProName.'<'.AdminMail."> \r\n";
					

					GeneralAdmin::GetMailTemplateDetailsForMail($MemDet[0]['UserEMail'],$MailBody,'ForgotPassword');//mail send
					Redirect(SiteMainPath.'forgot.html?pro_msg=usucc');
				}///all the else condition error message is display...
				else{
					$objSmarty->assign("ErrorMessages",'Your Account Has Been Inactive Status');
				}
			}
			else{	$objSmarty->assign("ErrorMessages","Entered mail-id not associate with any of the account.");	}
		}
		else{	$objSmarty->assign('ErrorMessages',implode('</p><p>',$ErrArr));	}			
	}

	/********************************** For User Profile Details  **************************************************/
	function UserProfileChangeActions($act_type){///User Profile Details (5-4-12 && 6-4-12) by palanisamy
		global $objSmarty;$ErrArr=array();		
		switch($act_type){///url value is set, select to case..
			
			case 'changePass':
					$ValiArr=array('UserPass'=>array('Text'=>'Please enter Current Password'),
									'NewPassword'=>array('Text'=>'Please enter New Password'),
									'RePassword'=>array('Text'=>'Please enter Retype Your Password'),
								);
					$ErrArr=$this->ValidatePostForm($ValiArr);
					if(sizeof($ErrArr)==0){///error checking
						if($_POST['NewPassword']!=$_POST['RePassword'])
							$ErrArr[]='Mismatch Retype Password';
						elseif($_POST['UserPass']==$_POST['NewPassword'])
							$ErrArr[]='New password and retyped password must be same';	
					}		
					
					if(count($ErrArr)==0){//password checking...
						 $SelQry="select UserPass from pj_user_list where UserId!='' and  UserId='".$_SESSION['PJ525_UserId']."' "; 
						$MemDet=$this->ExecuteQuery($SelQry, "select");
						if(count($MemDet)==1){/// select one password in query
							$MemPass=OurTechEncryptionDecryption('Decrypt',$MemDet[0]['UserPass']); 
							if($_POST['UserPass']==$MemPass){	
								$mem_pass=OurTechEncryptionDecryption('Encrypt',$_POST['NewPassword']);
								$UpQuery = "Update pj_user_list set UserPass = '".$mem_pass."' where 
											UserId ='".$_SESSION['PJ525_UserId']."'";
								$checkval=$this->ExecuteQuery($UpQuery, "update");
								if(!empty($checkval)){///already update or not update checking
									Redirect(HTTPSURLPath.'changepassword.html?pro_msg=success');
								}
								else{///all the else condition is error message display..
									$objSmarty->assign("ErrorMessages", "IT IS ALREADY UPDATED PLEASE DO SOME MODIFICATIONS TO UPDATE"); 
								}
 							}
							else{
								$objSmarty->assign("ErrorMessages", "Invalid Current Password"); 
							}
						}
					}
					else{
						$objSmarty->assign('ErrorMessages',implode('</p><p>',$ErrArr));
					}
				break;
		}
	} 
		
	/***************************	Common function for validate form Fields	*************************************/
	function ValidatePostForm($FFieldArr){///above error use this functions..
		$ErrArr=array();
		foreach($FFieldArr as $PostKey=>$FErrMsg)
		{
			if(empty($_POST[$PostKey])){
				$ErrArr[]=$FErrMsg['Text'];//this field is  checking in keys values
			}
			else{
				if(count($FFieldArr[$PostKey])>1){
					array_splice($FFieldArr[$PostKey], 0, 1);
					foreach($FFieldArr[$PostKey] as $ValiType=>$ValiMsg){
						if(!$this->FormRegxExpressionCehck($ValiType,$_POST[$PostKey]))
							$ErrArr[]=$ValiMsg;
					}
				}
			}
		}
		return $ErrArr;
	}
	
 	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	function SelectBuyersSellersLists($ResVal,$PageVal)
	{ 	
		global $objSmarty;$SelCon="";$OrderCon=' order by UL.UserId desc';
		$PageURL='users_management.php?';
		$seaname=(isset($_GET['seaname'])) ? $_GET['seaname']:'';$showonly=(isset($_GET['showonly'])) ? $_GET['showonly']:'';
		$seaemail=(isset($_GET['seaemail'])) ? $_GET['seaemail']:'';
		$seaPhone=(isset($_GET['seaPhone'])) ? $_GET['seaPhone']:'';
		$joindate=(isset($_GET['joindate'])) ? $_GET['joindate']:'';
		$sortby=(isset($_GET['sortby'])) ? $_GET['sortby']:'';
		
		if($seaname!=""){
			$SelCon.=" and UL.UserName LIKE '".addslashes(trim($seaname))."%'";$PageURL.="seaname=".urlencode($seaname)."&";
		}
		if($seaemail!=""){
			$SelCon.=" and UL.UserEMail LIKE '".addslashes(trim($seaemail))."%'";$PageURL.="seaname=".urlencode($seaemail)."&";
		}
		if($seaPhone!=""){
			$SelCon.=" and UL.UserPhone LIKE '".addslashes(trim($seaPhone))."%'";$PageURL.="seaname=".urlencode($seaPhone)."&";
		}
		if($joindate!=""){
			$joindate= date('Y-m-d',strtotime("$joindate"));
			$SelCon.=" and DAY(UL.UJoinDate)=DAY('".$joindate."') and MONTH(UL.UJoinDate)=MONTH('".$joindate."') 
				and YEAR(UL.UJoinDate)=YEAR('".$joindate."')";
			$PageURL.="expire=".urlencode($expire)."&";
		}
		
		$SortArr=array('username'=>'UL.UserName ','email'=>'UL.UserEMail ','joindate'=>'UL.UJoinDate','phone'=>'UL.UserPhone','status'=>'UL.UserSta ','wallet'=>'UL.Wallet_Amt ','gift'=>'UL.Gift_Link_count ');
						
		if(!empty($sortby)){
			$SortDet=explode('_',$sortby);
			if(array_key_exists($SortDet[0],$SortArr)){
				$OrderCon=" order by ".$SortArr[$SortDet[0]]." ".$SortDet[1]."";
			}
		}
		$objSmarty->assign("PageURL",$PageURL);
		
	$SelFields=array('UL.*','LC.country_name','LS.state_name');
		 $SelQuery="SELECT count(UL.UserId) from pj_user_list UL  left join 
					pj_location_country LC on  UL.UserCountry=LC.country_id left join 
					pj_location_state LS on  UL.UserState=LS.state_id where UL.UserId!='' 		 $SelCon  $OrderCon"; 
		$SUsersList=GeneralAdmin::SelectTableContentLists($ResVal,$SelQuery,$SelFields,$PageVal,$PageURL,15);
		
	}
	
	
	function SelectUsersList($ResVal,$PageVal)
	{ 	
		global $objSmarty;$SelCon="";$OrderCon=' order by UL.UserId desc';
 		
		$SelFields=array('UL.*','LC.country_name','LS.state_name');
		 $SelQuery="SELECT count(UL.UserId) from pj_user_list UL left join pj_location_country LC on UL.UserCountry=LC.country_id
		 			left join pj_location_state LS on  UL.UserState=LS.state_id where UL.UserId!='' and UL.UserType='User'
					$OrderCon"; 
		$SUsersList=GeneralAdmin::SelectTableContentLists($ResVal,$SelQuery,$SelFields,$PageVal,$PageURL,15);
		$objSmarty->assign("$ResVal",$SUsersList);	
	}
/******************************Add and update Patients Details ***************************/
	function AdminAddUpdateBuyersSellers($con_id,$Type,$PageURL)
	{	
		
		global $objSmarty,$ngconfig;$ErrMsg=array();extract($_POST);$where_con='';$RedirStatus=false;
		$FLName="/^[A-Za-z_[:blank:]]{1,50}$/";
		if(empty($UserName)){
			$ErrMsg[]="Please enter user name";
		}
		elseif(!preg_match($FLName,$UserName)){	
			$ErrMsg[]='User Name not allowed numbers and special characters ';
		}	
		if(empty($UserPass)){
			$ErrMsg[]="Please enter password";
		}
		if(empty($UserEMail)){
			$ErrMsg[]="Please enter email";
		}
		else{
			if(!preg_match("/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/", $UserEMail)){
				$ErrMsg[]="Please enter valid email";
			}
			elseif($AdminAction!="Update"){
				$SelQuery="SELECT * from pj_user_list where UserEMail='".$UserEMail."' $where_con";
				if(!GeneralAdmin::CheckDupRecord($SelQuery)){
					$ErrMsg[]="An account using this email address already exists";
				}
			}	
		}
		if(empty($UserAdds))
			{	$ErrMsg[]="Please enter address";	}	
		if(empty($UserPhone))
			{	$ErrMsg[]="Please enter mobile number";	}
		elseif(!is_numeric($UserPhone)){
			$ErrMsg[]="Mobile number allowed number only";
		}
		if(empty($UserCountry))
			{	$ErrMsg[]="Please select country";	}
		if(empty($UserState))
			{	$ErrMsg[]="Please select state";	}
		if(empty($UserCity))
			{	$ErrMsg[]="Please enter city";	}	
		if(empty($UserZip))
			{	$ErrMsg[]="Please enter zip";	}
			
		if(!empty($_FILES['UserImage']['name']))
		{
			
			if($_FILES['UserImage']['error']!=0)
			{	
			$ErrMsg[UserImage]="Please select valid image file";$FlagIns=false;
			 }
			else{
				$FExtArr=array('jpg','bmp','jpeg','gif','png');
				$path_info=pathinfo($_FILES['UserImage']['name']);
				if(!in_array(strtolower($path_info['extension']),$FExtArr))
				{	
				$ErrMsg[UserImage]="Please select valid image fileOnly [ ".implode(',',$FExtArr)." ]";$FlagIns=false;
				}
			}	
 		} 

		if(count($ErrMsg)==0)
		{	//print_r($_FILES);exit;
			$MemPass=OurTechEncryptionDecryption('Encrypt',$UserPass);
			$LoadLoc=$ngconfig['UserImageLoad'];
			if($_FILES['UserImage']['name']!='' && $_FILES['UserImage']['error']==0)
			{	
				$path_info=pathinfo($_FILES['UserImage']['name']);$extension = strtolower($path_info['extension']);
				$UserImage=date("Ymd").time().rand(5,10000).'.'.$path_info['extension'];
				$ResizeArr=array("400||300||l_".$UserImage,"150||150||m_".$UserImage,"100||100||t_".$UserImage,"75||60||st_".$UserImage);
				GeneralAdmin::WesoUploadedFile('Image',$_FILES['UserImage']['tmp_name'],$LoadLoc,$UserImage,$ResizeArr);					
				if($AdminAction=='Update'){ 
 					$SelQuery="SELECT UserImage from pj_user_list where md5(UserId)='".$con_id."' ";
					$Result=$this->ExecuteQuery($SelQuery, "select");
 					
					if($Result[0]['UserImage']!=''){ 
						$FileDelete=array($LoadLoc.'l_'.$Result[0]['UserImage'],$LoadLoc.'t_'.$Result[0]['UserImage'],
							$LoadLoc.'m_'.$Result[0]['UserImage'],$LoadLoc.'st_'.$Result[0]['UserImage'],
							$LoadLoc.$Result[0]['UserImage']);
						GeneralAdmin::WesoFileFolderDelete($FileDelete);	
					}
					$updateimg=",UserImage='".$UserImage."'";
				}
			}
				
			if($AdminAction!="Update")
			{
 				$InsDet="insert into pj_user_list(UserFName,UserLName,UserName,UserPass,UserEMail,UserPhone,UserCountry,
					UserState,UserCity,UserZip,UserAdds,UJoinDate,URegIP,UVerifySta,UserSta,UserImage,UserImageOrgName) 
					values('".$_POST['UserFName']."','".$_POST['UserLName']."','".$_POST['UserName']."','".$MemPass."',
					'".$_POST['UserEMail']."','".$_POST['UserPhone']."','".$_POST['UserCountry']."','".$_POST['UserState']."',
					'".$_POST['UserCity']."','".$_POST['UserZip']."','".$_POST['UserAdds']."',now(),'".$_SERVER['REMOTE_ADDR']."',
					'".$_POST['UVerifySta']."','".$_POST['UserSta']."','".$UserImage."','".$_FILES['UserImage']['name']."')"; 
 						
				$this->ExecuteQuery($InsDet, "insert");$NewTemp=mysql_insert_id();$RedirStatus=true;$PageURL.='pro_msg=asucc';
			}
			else
			{	
				$SelQry="SELECT UserId,UserEMail from pj_user_list where md5(UserId)='".$con_id."'";
				$SUserDet=$this->ExecuteQuery($SelQry, "select"); 
				if(sizeof($SUserDet)>0)
				{ 
					/************************** Admin User Update	*************************************/
 					 $AltCon="Update pj_user_list set UserFName='".$UserFName."',UserLName='".$UserLName."', 
					 UserName='".$UserName."',UserPass='".$MemPass."',UserEMail='".$UserEMail."',
					 Wallet_Amt='".$Wallet_Amt."',UserPhone='".$UserPhone."', UserCountry='".$UserCountry."',
					 UserState='".$UserState."',UserCity='".$UserCity."',UserZip='".$UserZip."',UserAdds='".$UserAdds."',
					 UVerifySta='".$UVerifySta."',UserSta='".$UserSta."',UserImageOrgName='".$_FILES['UserImage']['name']."'
						$updateimg Where md5(UserId)='".$con_id."'";
					$AltCheck=$this->ExecuteQuery($AltCon, "update");$NewTemp=$SUserDet[0]['UserId'];
					if(!empty($AltCheck))
					{
						$RedirStatus=true;
						$PageURL.='pro_msg=usucc';
					} 
				
				}
			}
	
			if($RedirStatus)
			{
				Redirect($PageURL);
			}
			else
			{
				$objSmarty->assign("ErrMessage", "IT IS ALREADY UPDATED PLEASE DO SOME MODIFICATIONS TO UPDATE"); 
			}
		}
		else
		{	$objSmarty->assign("ErrMessage",implode('</p><p>',$ErrMsg));$objSmarty->assign("Arr",$_POST);	}
	
	}	
		/***************************** User Pannel Codings  ******************************/

	function AddUpdateUsers($con_id,$PageURL)
	{
		//echo '<pre>';print_r($_POST);exit;
				
		global $objSmarty,$ngconfig;$ErrMsg=array();extract($_POST);$where_con='';$RedirStatus=false;
		$FLName="/^[A-Za-z_[:blank:]]{1,50}$/";
		if(empty($UserFName)){	
			$ErrMsg[]="Please enter first name";	
		}elseif(!preg_match($FLName,$UserFName)){	
			$ErrMsg[]="First Name not allowed numbers and special characters ";	
		}
		
		if(empty($UserLName))
			{	$ErrMsg[]="Please enter last name";	}
		elseif(!preg_match($FLName,$UserLName)){	
			$ErrMsg[]='last name not allowed numbers and special characters ';
		}	
		if($UserFName==$UserLName){
			$ErrMsg[]='first and last names are the same';
		}
		
		if($AdminAction!="Update"){
		
			if($UserPass == ''){
				$ErrMsg[]='Please enter Password';
			}
			if($ConPass==''){
				$ErrMsg[]='Please enter Confirm Password ';
			}
			elseif($UserPass!=$ConPass){
				$ErrMsg[]='Confirm Password Does Not Match';
			}
		}
		if(empty($UserName)){	
			$ErrMsg[]="Please enter user name";	
		}
		elseif(!preg_match(UInputCheck,$UserName)){
				$ErrMsg[]='Valid User Name Must Contain Alpha Numeric Character only';
			}
			else{
			if($_SESSION['PJ525_UserId']!="")
			{
				$where_con="and UserId!='".$_SESSION['PJ525_UserId']."'";
			}	
			 $SelQuery="SELECT * from pj_user_list where UserName='".$UserName."' $where_con";
			if(!GeneralAdmin::CheckDupRecord($SelQuery))
				{
				$ErrMsg[]="user name already exists";
				}
		}
		
		if(empty($UserEMail)){
			$ErrMsg[]="Please enter Email";
		}
		else
		{
			if(!preg_match("/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/", $UserEMail)){
				$ErrMsg[]="Please enter valid email";
			}
			if($_SESSION['PJ525_UserId']!="")
			{
				$where_con="and UserId!='".$_SESSION['PJ525_UserId']."'";
			}	
			$SelQuery="SELECT * from pj_user_list where UserEMail='".$UserEMail."' $where_con";
			if(!GeneralAdmin::CheckDupRecord($SelQuery))
				{
				$ErrMsg[]="An account using this email address already exists";
				}
		}
		if(empty($UserAdds))
			{	$ErrMsg[]="Please enter address";	}	
		if(empty($UserCountry))
			{	$ErrMsg[]="Please select country";	}
		if(empty($UserState))
			{	$ErrMsg[]="Please select state";	}
		if(empty($UserCity))
			{	$ErrMsg[]="Please enter city";	}	
		if(empty($UserZip))
			{	$ErrMsg[]="Please enter zip code";	}
		if(empty($UserPhone))
			{	$ErrMsg[]="Please enter mobile number";	}
		elseif(!is_numeric($UserPhone)){
			$ErrMsg[]="Mobile number allowed number only";
		}
		if(!empty($_FILES['UserImage']['name'])){
			if($_FILES['UserImage']['error']!=0)
			{	
			$ErrMsg[UserImage]="Please select valid image file";$FlagIns=false;
			 }
			else{
				$FExtArr=array('jpg','bmp','jpeg','gif','png');
				$path_info=pathinfo($_FILES['UserImage']['name']);
				if(!in_array(strtolower($path_info['extension']),$FExtArr))
				{	
				$ErrMsg[UserImage]="Please select valid image fileOnly [ ".implode(',',$FExtArr)." ]";$FlagIns=false;
				}
			}	
		}  
		//print_r($ErrMsg);exit;
		if(count($ErrMsg)==0)
		{
			
			$LoadLoc=$ngconfig['UserImgLoad'];
			if($_FILES['UserImage']['name']!='' && $_FILES['UserImage']['error']==0)
			{	
				$path_info=pathinfo($_FILES['UserImage']['name']);$extension = strtolower($path_info['extension']);
				$UserImage=date("Ymd").time().rand(5,10000).'.'.$path_info['extension'];
				$ResizeArr=array("200||200||l_".$UserImage,"150||150||m_".$UserImage,"100||100||t_".$UserImage,"75||60||st_".$UserImage);
				$ImgSuc=GeneralAdmin::WesoUploadedFile('Image',$_FILES['UserImage']['tmp_name'],$LoadLoc,$UserImage,$ResizeArr);					
				if($AdminAction=='Update')
				{ 
					$SelQuery="SELECT UserImage from pj_user_list where UserId='".$_SESSION['PJ525_UserId']."' ";
					$Result=$this->ExecuteQuery($SelQuery, "select");
					if($Result[0]['UserImage']!='')
					{ 
						$FileDelete=array($LoadLoc.'l_'.$Result[0]['UserImage'],$LoadLoc.'t_'.$Result[0]['UserImage'],
						$LoadLoc.'m_'.$Result[0]['UserImage'],$LoadLoc.'st_'.$Result[0]['UserImage'],$LoadLoc.$Result[0]['UserImage']);
						GeneralAdmin::WesoFileFolderDelete($FileDelete);	
					}
				$updateimg=",UserImage='".$UserImage."'";
				}
			}
			
			if($AdminAction!="Update")
			{	
				$MemPass=OurTechEncryptionDecryption('Encrypt',$UserPass);	
 				$InsDet="insert into pj_user_list(UserFName,UserLName,UserEMail,UserName,UserPass,UserAdds,UserCity,UserState,
						UserCountry,UserZip,UserPhone,UJoinDate,URegIP,UserSta,UserImage,UserImageOrgName) 
 						values('".$_POST['UserFName']."','".$_POST['UserLName']."','".$_POST['UserEMail']."',
 						'".$_POST['UserName']."','".$MemPass."','".$_POST['UserAdds']."','".$_POST['UserCity']."',
						'".$_POST['UserState']."','".$_POST['UserCountry']."','".$_POST['UserZip']."','".$_POST['UserPhone']."',
 						now(),'".$_SERVER['REMOTE_ADDR']."','1','".$UserImage."','".$_FILES['UserImage']['name']."')"; 
				$this->ExecuteQuery($InsDet, "insert");
				$InsCon=mysql_insert_id();
						$_SESSION['PJ525C_Session'] =session_id();
						$_SESSION['PJ525_UserId']=$InsCon;	
						$_SESSION['PJ525_UserName']=$UserName;
						$_SESSION['PJ525_UserMail']=$UserEMail;
						$_SESSION['PJ525_Time']=strtotime("now");
					 $MailBody='<table width="700" border="0" cellspacing="0" cellpadding="0"><tr><td width="700" valign="top">
							<h1 style="padding:0px; margin:0px; font:bold 16px Arial, Helvetica; color:#d64444;line-height:25px;">
								Hello  '.$_POST['UserFName'].' '.$_POST['UserLName'].',</h1>	
								<br/><p><strong>User Name</strong>: '.$UserName.'</p>
								<br/><p><strong>Password</strong>: '.$UserPass.'</p>
							<br/><p>{MailConTent}</p></td></tr></table>
							<table width="700" border="0" cellspacing="0" align="center" cellpadding="0"><tr>
							<td width="101" align="center" valign="top" style="padding-top:10px;"></td>
							<td width="564" valign="top" style="font:normal 13px Arial, Helvetica;line-height:25px;">
							<table width="564" border="0" cellspacing="0" cellpadding="0">
								<tr><td height="28"><strong></strong></td> </tr>
							</table></td></tr></table>
							</table>';
				GeneralAdmin::GetMailTemplateDetailsForMail($_POST['UserEMail'],$MailBody,'UserNewLoginDetails');
				Redirect(SiteMainPath.'myprofile.html?user_act=regsucc');
			}
			else
			{	
					/************************** Admin User Update	*************************************/
 					 $AltCon="Update pj_user_list set UserFName='".$UserFName."',
						UserLName='".$UserLName."',UserName='".$UserName."',
						UserEMail='".$UserEMail."',UserCity='".$UserCity."',
						UserAdds='".$UserAdds."',UserState='".$UserState."',UserZip='".$UserZip."',	UserPhone='".$UserPhone."',					
						UserCountry='".$UserCountry."' $updateimg
						Where UserId='".$_SESSION['PJ525_UserId']."'";
					$AltCheck=$this->ExecuteQuery($AltCon, "update");$NewTemp=$SUserDet[0]['UserId'];
					
					if(!empty($AltCheck))
					{
						$RedirStatus=true;
					} 
			}
			if($RedirStatus){
				Redirect(SiteMainPath.'myprofile.html?user_act=usucc');
			}
			else{
				$objSmarty->assign("ErrorMessages", "IT IS ALREADY UPDATED PLEASE DO SOME MODIFICATIONS TO UPDATE"); 
			}
		}
		else
		{	$objSmarty->assign("ErrorMessages",implode('</p><p>',$ErrMsg));$objSmarty->assign("Arr",$_POST);	}
	
	
	}
	
	 function UserLoginProfile()
	 {
		global $objSmarty;extract($_POST);$ErrMsg=array();$where="";
		if($UserEMail == '')
			$ErrMsg[]='Enter User Email';
		if($UserPass == '')
			$ErrMsg[]='Enter your Password';
		if(sizeof($ErrMsg)==0){
			$selqry="select * from pj_user_list  where UserId!='' and UserEMail='".$UserEMail."'";
			$MemDet=$this->ExecuteQuery($selqry,"select");
			 
			if(sizeof($MemDet)==1){
				if($MemDet[0]['UserSta']=='1'){
				  $MemPass=OurTechEncryptionDecryption('Decrypt',$MemDet[0]['UserPass']);
					if($UserPass==$MemPass){
						$_SESSION['PJ525C_Session'] =session_id();echo 
						$_SESSION['PJ525_UserId']=$MemDet[0]['UserId'];
						$_SESSION['PJ525_UserName']=$MemDet[0]['UserName'];
						$_SESSION['PJ525_UserMail']=$MemDet[0]['UserEMail'];
						$_SESSION['PJ525_Time']=strtotime("now");
						if(isset($_SESSION['PJ525_PrevPageURL']) && !empty($_SESSION['PJ525_PrevPageURL'])){
							$RedirectUrl=$_SESSION['PJ525_PrevPageURL'];
							session_unregister('PJ525_PrevPageURL');
						}
						else{
							$RedirectUrl="myprofile.html";
						}
						Redirect($RedirectUrl);
					}
					else{
						$objSmarty->assign("ErrorMessages",'Username/ Password not valid');	
					}
				}
				else{	
					$objSmarty->assign("ErrorMessages",'Your Account blocked by administrator.');
				}
			}
			else{	$objSmarty->assign("ErrorMessages",'Username/ Password not valid');		}
		}
		else{	$objSmarty->assign('ErrorMessages',implode('</p><p>',$ErrMsg));	}
	 
	 }
	 
	 function MyprofileDetails()
	{
		global $objSmarty,$ngconfig;
	
		 $SelQuery="SELECT * from pj_user_list UL  left join 
					pj_location_country LC on  UL.UserCountry=LC.country_id left join 
					pj_location_state LS on  UL.UserState=LS.state_id where UL.UserId!='' 
					and UL.UserId='".$_SESSION['PJ525_UserId']."' "; 
		$Userdet=$this->ExecuteQuery($SelQuery, "select"); //echo '<pre>';print_r($Userdet);exit;
		$objSmarty->assign("UserDetails",$Userdet[0]);
		$objSmarty->assign("Arr",$Userdet[0]);

	}
	
	
	function UpdatePassword()
	{	//print_r();exit;
		global $objSmarty;extract($_POST);$ErrMsg=array();
		if($UserPass== '')
			$ErrMsg[]='Enter your Current Password';
		if($NewPassword== '')
			$ErrMsg[]='Enter your New Password';
		
		if($RePassword=='')
			$ErrMsg[]='Enter your Confirm Password';
		elseif($NewPassword!=$RePassword)
			$ErrMsg[]='Mismatch Confirm Password';
		elseif($UserPass==$NewPassword)
			$ErrMsg[]='New And Current User Password Is Same,Please Enter Some Other';	
			
		if(sizeof($ErrMsg)==0){
			$SelQuery="select UserPass from pj_user_list where UserId!='' and  UserId='".$_SESSION['PJ525_UserId']."' "; 
			$SelResult=$this->ExecuteQuery($SelQuery, "select");
			if(sizeof($SelResult)==1){
				$MemPass=OurTechEncryptionDecryption('Decrypt',$SelResult[0]['UserPass']); 
				if($UserPass==$MemPass)
				{	
					$mem_pass=OurTechEncryptionDecryption('Encrypt',$NewPassword);
					$UpQuery = "Update pj_user_list set UserPass = '".$mem_pass."' where UserId ='". $_SESSION['PJ525_UserId']."'";
					$checkval=$this->ExecuteQuery($UpQuery, "update");
					Redirect(SiteMainPath.''.$_SESSION['PJ
					_UserType'].'changepassword.html?user_act=passsuc');
				}
				else{
					$objSmarty->assign("ErrorMessages", "Invalid Current Password"); 
				}
			}
		}
		else{
			$objSmarty->assign('ErrorMessages',implode('</p><p>',$ErrMsg));
		}
	}
}




?>