<?php

	// Invoice		
	define('DOOR_INVOICE_TABLE','door_invoice');
	define('PAYMENT_PENDING','pending');

class Invoice extends MysqlFns
{


	// convert object to array
	function objectToArray( $obj ) 
	{
		if(is_object($obj)) $obj = (array) $obj;
		if(is_array($obj)) {
		  $new = array();
		  foreach($obj as $key => $val) {
			$new[$key] = $this->objectToArray($val);
		  }
		}
		else { 
		  $new = $obj;
		}
		return $new;
	}

	// calculate the total of invoice
	// parameter : Cart Array;
	function invoice_total($cart)
	{
		$tot = 0;
		if(!empty($cart))
		foreach($cart as $key=>$val)
		{
			$tot +=$val['sub_total'];
		}
		return array('total'=>$tot,'sub_total'=>$this->final_amount($tot));
	}
	
	// get taxes from database
	function get_taxes()
	{
		$sql="select * from pj_tax_list where TaxId!=''";
		$result = mysql_query($sql);
		
		$tax = array();
		if(mysql_num_rows($result)>0)
		{
			while($row = mysql_fetch_object($result))
			{
				//array_push($tax,array($row->Tax => $row->Percentage));
				$tax[$row->Tax] = $row->Percentage;
			}
		return $tax;
		}
		else 
		return false;	
	}
	
	// calculate final amount by adding GST and QST
	function final_amount($total)
	{
		$tax = $this->get_taxes();
		$gst = $total*$tax['GST']/100;
		$qst = $total*$tax['QST']/100;
		return round($total+$gst+$qst,2);
	}
	
	// add to payment table
	function add_shipping_details($s)
	{
		$sql = "INSERT INTO pj_item_payments";
		$sql .= " (userId,POrdDate,PayAmt,OrderRefNo,InvoiceNum,PayType,PayDate,ShipMethod,ShipDate,PTransport,AccNo,PayStatus,PaidFull)";
		$sql .= "VALUES ";
		$sql .= " ('".$_SESSION['PJ525_UserId']."','".date('Y-m-d')."','".$s['final_amount']."','".$s['invoice_code']."','".$s['invoice_code']."','PayPal','".date('y/m/d H:i:s')."','".$s['ship_method']."','0000-00-00 00:00:00','".$s['preferred_transport']."','".$s['account_no']."','InProgress','0') ";
		if(mysql_query($sql))
		return true;
		else
		return false;
	}
	
	// generate invoice html
	function invoice_html($invoiceCode,$cart,$style='',$forPdf=false,$lang)
	{
		if(!empty($cart))
		{
			$tax			= $this->get_taxes();
			$invoiceTotal	= $this->invoice_total($cart);
			$total 			= $invoiceTotal['total'];
			$tax_total		= $invoiceTotal['sub_total'];
		
			$html = '<table class="table table-bordered" id="shopping-cart" '.(($forPdf) ? $style : ' style="'.$style.'"') .'>';
			$html .= '<thead>';
			$html .= '<tr>';
			$html .= '	<th colspan="2"> &nbsp;  </th>
						<th colspan="4"> '.(($forPdf) ? '' : $lang['invGenerated'].' <br/>' ).' <div class="btn btn-warning btn-block" > '.$lang['invoice'].' ID : '.$invoiceCode.' </div> </th>
						<th colspan="2"> &nbsp;  </th>';
			$html .= '</tr>';
			
			if(!$forPdf) $html .= '<tr><th colspan="2"> '.$lang['shoppingCart'].' </th> <td colspan="6"> &nbsp;  </td> </tr>';
			
			$html .= '<tr  class="underline">';
			$html .= '	<th width="12"> &nbsp; </th>
						<th> '.$lang['profile'].' </th>
						<th> &nbsp; </th>
						<th> '.$lang['dimension'].' </th>
						<th> '.$lang['quantity'].'</th>
						<th> '.$lang['price'].' </th>
						<th> '.$lang['total'].' </th>';
			$html .= '</tr>';
			$html .= '<tr><th colspan="7" style="border-bottom:1px solid">  </th></tr>';
			$html .= '</thead>';
			
			$html .= '<tbody>';
				foreach($cart as $c)
				{
					if($forPdf) $html .= '<tr> <th colspan="7"> &nbsp;  </th></tr>';
					$html .= '<tr>';
					$html .= '	<th width="12"> &nbsp; </th>';
					$html .= '	<th valign="top"> '.$c['profile_type'].' </th>';
					$html .= '	<td style="text-align:left;"> 
									Finish: '.$c['profile_finish'].' <br> Insert: '.$c['insert_type'].(($c['sub_insert'] !='') ? '/' : '') .$c['sub_insert'].'
									<hr/>
									Orientation: '.$c['orientation'].'
									<hr/>
									Hinge Holes : '.$c['hinge_holes'].' | Drilling Holes : '.$c['handle_holes'].'
								</td>';
					$html .= '	<td> '.$c['width'].' x '.$c['height'].' </td>';
					$html .= '	<td> '.$c['quantity'].' </td>';
					$html .= '	<td> $'.$c['price'].'  </td>';
					$html .= '	<th> $'.$c['sub_total'].'</th>';
					$html .= '</tr>';
					if($forPdf) $html .= '<tr> <th colspan="7"> &nbsp;  </th></tr>';
					$html .= '<tr><th colspan="7" style="border-bottom:1px solid">  </th></tr>';
				}
			$html .= '</tbody>';
			
			$html .= '<tfoot>';
			$html .= '<tr>';
			$html .= '	<td colspan="5"> &nbsp;  </td>
						<th> Total </th>
						<th> $<span id="cartTotal">'.$total.'</span> </th>';
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= '	<td colspan="5"> &nbsp;  </td>
						<th> '.$lang['subTotal'].' <br/> <small style="font: normal 11px arial;" > ('.$lang['GST'].' '.$tax['GST'].'% and '.$lang['QST'].' '.$tax['QST'].'%) </small> </th>
						<th> $<span >'.$tax_total.'</span> </th>';
			$html .= '</tr>';
			
			if($forPdf) $html .= '<tr> <th colspan="7"> &nbsp;  </th></tr>';
			
			if(!$forPdf)
			{
			$html .= '<tr>';
			$html .= '	<td colspan="2"> &nbsp;  </td>
						<td colspan="3">   
							<div class="btn btn-success" onclick="window.location=\'door.html\';" > '.$lang['backToConfigurator'].' </div>';
			$html .= '	</td>
						<td colspan="2"> 
							<div class="btn btn-success btn-block" onclick="window.location=\'invoice.php?act_type=final&invoice='.$invoiceCode.'\';" > '.$lang['checkout'].' </div> 
						</td>';
			$html .= '</tr>';
			}
			$html .= '</tfoot>';
			$html .= '</table>';		
			
			return $html;
		}
		else
		return false;
	}
	// add invoice to database
	function add_invoice($data,$totalAmount,$invoiceCode='',$pdf='')
	{
		$return['status']	= 0;
		if(!empty($data))
		{
			$detail = json_encode($data);
			$detail = addslashes($detail);
			$code	= $invoiceCode;
			$user	= isset($_SESSION['PJ525_UserId']) ? $_SESSION['PJ525_UserId'] : 0;
			$status	= PAYMENT_PENDING;
			$date	= date('Y/m/d H:i:s');

			// check if the same invoice exists in database
			$sql = "SELECT door_invoice_detail,door_invoice_code FROM ".DOOR_INVOICE_TABLE." WHERE door_invoice_detail='$detail' AND door_invoice_status='".PAYMENT_PENDING."'";
			$result = mysql_query($sql);
				if(mysql_num_rows($result)==0)
				{
					// duplicate invoice doesn't exist 
					$code			= strtoupper(uniqid("LPPD"));
					$_SESSION['generatedCode'] = $code;
					$sql = "INSERT INTO ".DOOR_INVOICE_TABLE." (door_invoice_code, door_invoice_user, door_invoice_detail, door_invoice_status, door_invoice_amount, door_invoice_pdf, door_invoice_date) VALUES ('$code','$user','$detail','$status','$totalAmount','$pdf','$date')";
					$this->ExecuteQuery($sql, "insert");
					$return['status'] = 1;
				}
				else if(mysql_num_rows($result)>0)
				{
					// duplicate invoice exist
					$row = mysql_fetch_assoc($result);
					//print_r($row);
					$code = $row['door_invoice_code'];
				}
			$return['code']=$code;
			return $return;
		}
		$return['code'] 		= $_SESSION['generatedCode'];
		return $return;
	}
	
	
	// list all invoice of the curent user
	function list_invoice()
	{
		$user	= isset($_SESSION['PJ525_UserId']) ? $_SESSION['PJ525_UserId'] : 0;
		$sql = "SELECT *  FROM ".DOOR_INVOICE_TABLE." WHERE door_invoice_user!='0' && door_invoice_user='$user' ";
		$result = mysql_query($sql);
		if(mysql_num_rows($result)>0)
		{
			$row = mysql_fetch_assoc($result);
			return $row;
		}
		else
		return false;
	}
	// update invoice status
	function update_invoice($invoiceCode,$status,$pdf='')
	{
	
		$user	= isset($_SESSION['PJ525_UserId']) ? $_SESSION['PJ525_UserId'] : 0;
		if($pdf!='')
		$sql = "UPDATE ".DOOR_INVOICE_TABLE." SET door_invoice_pdf='$pdf', door_invoice_user='".$user."', door_invoice_status='".$status."' WHERE door_invoice_code='".$invoiceCode."'";
		else
		$sql = "UPDATE ".DOOR_INVOICE_TABLE." SET door_invoice_user='".$user."', door_invoice_status='".$status."' WHERE door_invoice_code='".$invoiceCode."'";
		$this->ExecuteQuery($sql, "update");
	}
	
	// fetch invoice details
	function invoice_details($invoiceCode)
	{
		$sql = "SELECT *  FROM ".DOOR_INVOICE_TABLE." WHERE door_invoice_code='$invoiceCode'";
		$result = mysql_query($sql);
		if(mysql_num_rows($result)>0)
		{
					
			$row = mysql_fetch_assoc($result);
			$finalamount = $this->final_amount($row['door_invoice_amount']);
			$row['door_invoice_final_amount'] = $finalamount;
			return $row;
		}
		else
		return false;
	}
	
	// paypal payment
	function paypal_payment($paymentDetails)
	{
		// user Details
		$SelQry="select TU.*,TB.*,ST.state_name,CN.country_name  from pj_user_list TU left join pj_billing_info TB on 
			TB.UserId=TU.UserId left join pj_location_state ST on TB.UBillState=ST.state_id left join 
			pj_location_country CN	on ST.country_ident=CN.country_id where 
			(TU.UserId)='".$_SESSION['PJ525_UserId']."' and ST.state_id!='' and CN.country_id!='' ";
		
		$MLocDet=$this->ExecuteQuery($SelQry,"select");	
		
		// paypal settings
		$SelDet="select * from pj_payment_settings where PayTypeId!=''";
		$PaypalDet=$this->ExecuteQuery($SelDet,"select");
			
			//var_dump($PaypalDet); exit;
		 
			if($PaypalDet[0]['ServerType'] == '0')
				$PaypalURL = 'https://www.sandbox.paypal.com/row/cgi-bin/webscr';
			else
				$PaypalURL = 'https://www.paypal.com/row/cgi-bin/webscr';
			
			echo "<html>\n";
			echo "<head><title>Processing Payment...</title></head>\n";
			echo "<body onLoad=\"document.form.submit();\">\n";
			echo "<center><h3><strong>Please wait, your payment is being processed.".
					"Do not refresh your browser...</strong></h3></center>\n";
			echo "<div align='center'><img src='".SiteMainPath."images/loader.gif' border='0' /></div>\n";
			echo "<form method=\"post\" name=\"form\" action=\"".$PaypalURL."\">\n"; ?>
			
			<input type="hidden" name="cmd" value="_xclick">
			<input type="hidden" name="business" value="<?php echo $PaypalDet[0]['PayPalMail']; ?>">
			<input type="hidden" name="currency_code" value="CAD">
<input type="hidden" name="lc" value="CA">
			<input type="hidden" name="rm" value="2">
			<input type="hidden" name="amount" value="<?php echo number_format($paymentDetails['total'],2); ?>">
			<input type="hidden" name="item_number" value="<?php echo $paymentDetails['invoice_code']; ?>">
			<input type="hidden" name="item_name" value="Buy_Now">
<input type="hidden" name="button_subtype" value="services">
<input type="hidden" name="no_note" value="0">
<input type="hidden" name="bn" value="PP-BuyNowBF:btn_buynowCC_LG.gif:NonHostedGuest">
<!--
<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!"> -->
<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
			<input type="hidden" name="return" value="<?php echo HTTPSURLPath.'invoice.html?act_type=success&invoice='.$paymentDetails['invoice_code']; ?>" />
			<input type="hidden" name="cancel_return" value="<?php echo HTTPSURLPath.'invoice.html?act_type=view&invoice='.$paymentDetails['invoice_code']; ?>" />
			<?php
			echo "</form>\n";
			echo "</body></html>\n"; 
	}
	
	
	// create PDF of the invoice
	function invoice_to_pdf($content)
	{
		global $ngconfig;
		$return = array();
		require_once(HtmlToPDFPath.'/html2pdf.class.php');
		try
		{	set_time_limit(500);
			$NewName = 'Quote_'.CreateRandomNumber(5).".pdf";
			$PdfFileLoad=$ngconfig['PDFLoad'];
			$html2pdf = new HTML2PDF('P', 'A4', 'en');
			$html2pdf->setDefaultFont('times');
			$html2pdf->pdf->SetDisplayMode('fullpage');
	
			$html2pdf->writeHTML($content,isset($_GET['vuehtml']));

			$html2pdf->Output($PdfFileLoad.$NewName,'F');
			
			$ourFileName=$ngconfig['PDFLoad'].$NewName;

			/*
			// set the download rate limit (=> 20,5 kb/s)

			$download_rate = 20.5; 	//echo $ourFileName;exit;
			if(file_exists($ourFileName) && is_file($ourFileName)) {
				// send headers
				if($browser['browser']=='IE'){
					header("Pragma: public");
					header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
					if(strstr($_SERVER["HTTP_USER_AGENT"],"MSIE")==false) {
						header("X-Download-Options: noopen "); // For IE8
						header("X-Content-Type-Options: nosniff"); // For IE8
					}
					header("Content-type: application/pdf");
					header("Content-Transfer-Encoding: Binary");
					header('Content-Length: '.filesize($ourFileName));
					if($_GET['type']=='print'){
						header('Content-Disposition: inline; filename='.basename($ourFileName));
					}
					else{
						header('Content-Disposition: attachment; filename='.basename($ourFileName));
					}
				}
				else{
					header("Cache-Control: private");
					header("Content-type: application/pdf");
					header("Content-Transfer-Encoding: Binary");
					header('Content-Length: '.filesize($ourFileName));
					if($_GET['type']=='print'){
						header('Content-Disposition: inline; filename='.basename($ourFileName));
					}
					else{
						header('Content-Disposition: attachment; filename='.basename($ourFileName));
					}
					//header('Content-Disposition: inline; filename='.basename($ourFileName));
				}
				
				// flush content
				flush();
				
				// open file stream
				$file = fopen($ourFileName, "r");
				while (!feof($file)) {
					// send the current file part to the browser
					print fread($file, round($download_rate * 1024));
					// flush the content to the browser
					flush();
					// sleep one second
					sleep(1);
				}
				// close file stream
				fclose($file);
				
			 }
			else {
				die('Error: The file '.$ourFileName.' does not exist!');
			}
			*/
		}
		catch(HTML2PDF_exception $e) {
			echo $e;exit;
		}  	
		catch (Exception $e) {
			echo $e->getMessage(); //Boring error messages from anything else!
		}
		$return['filepath_full'] 	= $ourFileName;
		$return['name']			= $NewName;
		$return['path']			= $ngconfig['PDFLoad'];
		return $return;
	}
}
?>