<?php 
class GeneralAdmin extends MysqlFns
{	
	/******************************* 	Function For Check Cart Total Amount    ***********************************************/
	function CheckCartTotalAmount($CartPrice,$CartQty,$ProId='')
	{
		$TotalAmount=0; $Tax=0;
		if(!empty($ProId)){
			$SelCon.=" and PL.SellId!='".$ProId."'";	
		}
 		$SelQry="SELECT sum(PB.ProQty) TotQty,sum(PB.ProQty*PB.ProPrice) TotAmount from pj_item_baskets PB LEFT JOIN 
				pj_sell_list PL ON PL.SellId=PB.SellId where PL.ItemStatus='1' and  PL.SellId!='' and 
				PB.DownStatus!='1' and PB.BasketSession ='".$_SESSION['PJ525_BasSession']."' $SelCon";
		$UCartDet=GeneralAdmin::ExecuteQuery($SelQry, "SELECT");
		
 		if(sizeof($UCartDet)>0){
			$TotalAmount=$UCartDet[0]['TotAmount'];
		}
	 $TotalAmount=($CartPrice*$CartQty)+$TotalAmount;  
		 
		if($TotalAmount<10000){
			return true;
		}
		else{	return false;	}
	}
	//**********************	Common Function For Delete,Active Inactive Table Details 	*********************************/
	function AdminControlMgmt($TableName,$TableField,$ConSta,$RedURL='')
	{	
		global $objSmarty;$ConMsg='';
		if(!empty($_POST['ConId']) && is_array($_POST['ConId']))
			 $UserIds		= implode("," , $_POST['ConId']);
		elseif(!empty($_POST['ConSId']) && $_POST['ConSId']!='')
			  $UserIds= $_POST['ConSId'];		
		else
			$objSmarty->assign("ErrorMessage", "No ".$ConMsg." Selected");

		if(!empty($UserIds)){ 
			$ActionCheck=true;$staschek='';
			 $MgmtType=(isset($_SESSION['PJ525A_Mgmt']))?$_SESSION['PJ525A_Mgmt']:'';
			 $Action_Type=(isset($_POST['Action_Type']))?$_POST['Action_Type']:'';
			 $PJ525A_Type=(isset($_SESSION['PJ525A_Type']))?$_SESSION['PJ525A_Type']:'';
			
			if($ActionCheck || !empty($PJ525A_Type)){
				switch($Action_Type)
				{	
					case "Delete":
							GeneralAdmin::NextGenReferenceTableDelete($TableName,$UserIds); 
							
						 	$DelQuery="delete from $TableName where $TableField IN(".$UserIds.")";
							$AltChcek=GeneralAdmin::ExecuteQuery($DelQuery,"delete");
							if(!empty($AltChcek)){
								$staschek="sucs";
							}
						break;				
					case "Active":
							$UpQuery = "Update $TableName Set $ConSta='1' Where $TableField IN ( ".$UserIds." )" ;
							$AltChcek=GeneralAdmin::ExecuteQuery($UpQuery, "update");
							if($TableName=='pj_user_list'){
								$UpQuery = "Update $TableName Set UVerifySta='1' Where $TableField IN ( ".$UserIds." )" ;
								$AltChcek=GeneralAdmin::ExecuteQuery($UpQuery, "update");
							}
							if(!empty($AltChcek)){
								$staschek="sucs";
							}
						break;				
					case "InActive":
							$UpQuery = "Update $TableName Set $ConSta='0' Where $TableField IN ( ".$UserIds." )" ;
							$AltChcek=GeneralAdmin::ExecuteQuery($UpQuery, "update");
							if(!empty($AltChcek)){
								$staschek="sucs";
							}
						break;
				    case "Paid":
							$UpQuery = "Update $TableName Set $ConSta='Paid' Where $TableField IN ( ".$UserIds." )" ;
							$AltChcek=GeneralAdmin::ExecuteQuery($UpQuery, "update");
							if(!empty($AltChcek)){
								$staschek="sucs";
							}
						break;
					case "InProgress":
							$UpQuery = "Update $TableName Set $ConSta='InProgress' Where $TableField IN ( ".$UserIds." )" ;
							$AltChcek=GeneralAdmin::ExecuteQuery($UpQuery, "update");
							if(!empty($AltChcek)){
								$staschek="sucs";
							}
						break;
							
					 case "Completed":
							$UpQuery = "Update $TableName Set $ConSta='Completed' Where $TableField IN ( ".$UserIds." )" ;
							$AltChcek=GeneralAdmin::ExecuteQuery($UpQuery, "update");
							if(!empty($AltChcek)){
								$staschek="sucs";
							}
						break;		
					case "Shipped":
							 $UpQuery = "Update $TableName Set $ConSta='Shipped' Where $TableField IN ( ".$UserIds." )" ;
							$AltChcek=GeneralAdmin::ExecuteQuery($UpQuery, "update");
							if(!empty($AltChcek)){
								$staschek="sucs";
							}
						break;	
					case "Delivered":
							$UpQuery = "Update $TableName Set $ConSta='Delivered' Where $TableField IN ( ".$UserIds." )" ;
							$AltChcek=GeneralAdmin::ExecuteQuery($UpQuery, "update");
							if(!empty($AltChcek)){
								$staschek="sucs";
							}
						break; 
					case "Suspended":
						$UpQuery = "Update $TableName Set $ConSta='Suspended' Where $TableField IN ( ".$UserIds." )" ;
						$AltChcek=GeneralAdmin::ExecuteQuery($UpQuery, "update");
						if(!empty($AltChcek)){
							$staschek="sucs";
						}
					break;
				    case "Pending":
							$UpQuery = "Update $TableName Set $ConSta='pending' Where $TableField IN ( ".$UserIds." )" ;
							$AltChcek=GeneralAdmin::ExecuteQuery($UpQuery, "update");
							if(!empty($AltChcek)){
								$staschek="sucs";
							}
						break;
				  case "Verified":
						$UpQuery = "Update $TableName Set $ConSta='Verified' Where $TableField IN ( ".$UserIds." )" ;
						$AltChcek=GeneralAdmin::ExecuteQuery($UpQuery, "update");
						if(!empty($AltChcek)){
							$staschek="sucs";
						}
					break; 
				}
				if(!empty($RedURL)){
					
					$ActArray=array("Delete"=>"delsuc","Active"=>"actsuc","InActive"=>"inasuc","Paid"=>"paisucc",
					"UnPaid"=>"unpaisucc" ,"UnVerified"=>"cancelsuc","Verified"=>"actcon","Approved"=>"pactcon","UnApproved"=>"pcancelsuc",'InProgress'=>'InProgress','Completed'=>'Completed','Paid'=>'Paid','Shipped'=>'Shipped','Delivered'=>'Delivered',);
						
					if (array_key_exists('mgtact', $_GET)) {
						$_GET['mgtact']=$ActArray[$Action_Type];$URLSep='';
						 $_GET['checkstas']=$staschek;
						foreach($_GET as $GetName=> $GetValue){
							 $RedURL.=$URLSep.$GetName."=".urlencode($GetValue);$URLSep="&";
						}
					}
					else{
						if(!empty($_SERVER['QUERY_STRING'])){
							$RedURL.=$_SERVER['QUERY_STRING'].'&';
						}
						$RedURL.="mgtact=".$ActArray[$Action_Type];
						$RedURL.=(empty($staschek))?'':"&checkstas=".$staschek;
					}
					Redirect($RedURL);
				}
			}
			else{
				if(!empty($_SERVER['QUERY_STRING'])){
					$RedURL.=$_SERVER['QUERY_STRING'].'&';
				}
				Redirect($RedURL.'persta=fail');
			}
		}
	}
	function DownloadFileZip($TableName,$TableField,$RedURL='')
	{	
		global  $ngconfig,$objSmarty;$ConMsg='';
		if(!empty($_POST['ConId']) && is_array($_POST['ConId']))
			 $UserIds		= implode("," , $_POST['ConId']);
		elseif(!empty($_POST['ConSId']) && $_POST['ConSId']!='')
			  $UserIds= $_POST['ConSId'];		
		else
			$objSmarty->assign("ErrorMessage", "No ".$ConMsg." Selected");

		if(!empty($UserIds)){ 
			$ActionCheck=true;$staschek='';
			 $Action_Type=(isset($_POST['Action_Type']))?$_POST['Action_Type']:'';
			if(!empty($Action_Type)){
				if($TableName=='pj_item_file'){
				$PhotoLoc = $ngconfig['ProductfilesLoad'];
				$file_folder = "Productfiles";
				$SelQry="Select FileName from pj_item_file Where ItemId  IN ( ".$UserIds." )";
					$ImgList=GeneralAdmin::ExecuteQuery($SelQry,"select");//echo '<pre>';print_r($ImgList);exit;
						if($ImgList[0]['FileName']!=''){
						foreach($ImgList as $LangDet){
							$Result[]=$LangDet['FileName'];
						}
						}
				}
				elseif($TableName=='pj_item_image'){
				$PhotoLoc = $ngconfig['ProductImageLoad']; 
				$file_folder = "ProductImage";
					$SelQry="Select ImageName from pj_item_image Where ImageId  IN ( ".$UserIds." )";
					$ImgList=GeneralAdmin::ExecuteQuery($SelQry,"select");//echo '<pre>';print_r($ImgList);exit;
					if($ImgList[0]['ImageName']!=''){
						foreach($ImgList as $LangDet){
							$Result[]=$LangDet['ImageName'];
						}
						}
				}
			
				if(extension_loaded('zip')){	// Checking ZIP extension is available
			if(sizeof($Result)>0){	// Checking files are selected
				$zip = new ZipArchive();			// Load zip library	
				$zip_name = $file_folder.time().".zip";			// Zip name
				if($zip->open($zip_name, ZIPARCHIVE::CREATE)!==TRUE){		// Opening zip file to load files
					$objSmarty->assign("ErrMessage", "Sorry ZIP creation failed at this time");
				}
				foreach($Result as $file){	
					
					$zip->addFile($PhotoLoc.$file,$file);			// Adding files into zip
				}
				$zip->close();
				if(file_exists($zip_name)){
					// push to download the zip
					header('Content-type: application/zip');
					header('Content-Disposition: attachment; filename="'.$zip_name.'"');
					readfile($zip_name);
					// remove zip file is exists in temp path
					unlink($zip_name);
				}
				
				
			}else{
			
				$objSmarty->assign("ErrMessage", "Please select file to zip");
				}
		}else{
			$objSmarty->assign("ErrMessage", " You dont have ZIP extension");
			}
			}
		}
	}
	/******************************		Function For View Table Full Details 	*********************************/
	function SelectTableContentLists($RetVal,$SelQuery,$SelFields,$ResultPage,$PageURL,$limit='15',$PagingVal='page')
	{	
		//		$RetVal - Table result String for smarty  assign 
		///		$SelQuery - select query
		///		$SelFields - Fields To select Table fields List
		///		$PagingVal - page number
		//		$ResultPage - pagination result String for smarty  assign 
		//		$limit - Number of Records Per Page
		
		global $objSmarty,$ngconfig;$sel_detail='';$pagination = "";$adjacents = "4";
		
		$tot_rec=GeneralAdmin::ExecuteQuery($SelQuery,"select");
		
		$total_pages=$tot_rec[0][0];///		Total Record Length
	
		
		$page = (int) (!isset($_GET[$PagingVal]) ? 1 : $_GET[$PagingVal]);$page = ($page == 0 ? 1 : $page);
		if($page)	{	$start = ($page - 1) * $limit;	}
		else		{	$start = 0;		}
		
				/***************	For Selected Page Values	****************************************/
				
		$SelQuery=preg_replace("/count\((.*)\) from/",' '.implode(',',$SelFields).' from ', $SelQuery);
				
		$sel_detail=$SelQuery." limit $start, $limit";	 
		$sel_view=GeneralAdmin::ExecuteQuery($sel_detail, "select");$SelTotal=sizeof($sel_view);
		$objSmarty->assign($RetVal,$sel_view);

		$objSmarty->assign('PageStartNum',($start+1));
		
		if(isset($_GET[$PagingVal]) && $SelTotal==0){
			Redirect($PageURL);
		}
		$prev = $page - 1;$next = $page + 1;$lastpage = ceil($total_pages/$limit);$lpm1 = $lastpage - 1;
		
		if($lastpage > 1)
		{
			$PageURL=$PageURL.$PagingVal;	///		Paging Redirect URL	
			
			$pagination .= "<div class='pagination'>";
			if ($page > 1)
				$pagination.= "<a href='".$PageURL."=$prev'>&lt;&lt; Previous</a>";
			else
				$pagination.= "<span class='disabled'>&lt;&lt; Previous</span>";   
			if ($lastpage < 7 + ($adjacents * 2))
			{   
				for ($counter = 1; $counter <= $lastpage; $counter++)
				{
					if ($counter == $page)
						$pagination.= "<span class='current'>$counter</span>";
					else
						$pagination.= "<a href='".$PageURL."=$counter'>$counter</a>";                   
				}
			}
			elseif($lastpage > 5 + ($adjacents * 2))
			{
				if($page < 1 + ($adjacents * 2))       
				{
					for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
					{
						if ($counter == $page)
							$pagination.= "<span class='current'>$counter</span>";
						else
							$pagination.= "<a href='".$PageURL."=$counter'>$counter</a>";                   
					}
					$pagination.= "...";$pagination.= "<a href='".$PageURL."=$lpm1'>$lpm1</a>";
					$pagination.= "<a href='".$PageURL."=$lastpage'>$lastpage</a>";       
				}
				elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
				{
					$pagination.= "<a href='".$PageURL."=1'>1</a>";
					$pagination.= "<a href='".$PageURL."=2'>2</a>";$pagination.= "...";
					
					for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
					{
						if ($counter == $page)
							$pagination.= "<span class='current'>$counter</span>";
						else
							$pagination.= "<a href='".$PageURL."=$counter'>$counter</a>";                   
					}
					$pagination.= "..";$pagination.= "<a href='".$PageURL."=$lpm1'>$lpm1</a>";
					$pagination.= "<a href='".$PageURL."=$lastpage'>$lastpage</a>";       
				}
				else
				{
					$pagination.= "<a href='".$PageURL."=1'>1</a>";$pagination.= "<a href='".$PageURL."=2'>2</a>";
					$pagination.= "..";
					for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
					{
						if ($counter == $page)
							$pagination.= "<span class='current'>$counter</span>";
						else
							$pagination.= "<a href='".$PageURL."=$counter'>$counter</a>";                   
					}
				}
			}
			
			if ($page < $counter - 1)
				$pagination.= "<a href='".$PageURL."=$next'>Next &gt;&gt;</a>";
			else
				$pagination.= "<span class='disabled'>Next &gt;&gt;</span>";
				
			$pagination.= "</div>\n";       
		}//echo $pagination;exit;
		$objSmarty->assign($ResultPage,$pagination);
		return $sel_view;
	}		
		
	/****************************	Function For View Table Row Details		*********************************/
	function SelectSingleTableRowDetails($tname,$tid,$SRetVal,$val)
	{ 
		global $objSmarty;
		 $SelList="select * from ".$tname." where md5(".$tid.")='".$val."' limit 0,1";
		$SelDet=GeneralAdmin::ExecuteQuery($SelList,"select");
		if(sizeof($SelDet)>0){
			$objSmarty->assign($SRetVal,$SelDet[0]);return true;	
		}
		else{
			return false;	
		}	
	}
		
	//******************************		Function For Check Duplicate title Exist	*********************************/
	function CheckTitleExist($tablename,$m_id,$UniField,$FieldId='',$FieldVal)
	{
		$where_con='';
		if(is_numeric($FieldId) && $FieldId !="")
			$where_con=" and ".$m_id." != '".$FieldId."'";
		elseif($FieldId !="" && !is_numeric($FieldId))
			$where_con=" and md5(".$m_id.") != '".$FieldId."'";
		
		$SelList="select * from ".$tablename." where ".$UniField."='".$FieldVal."'".$where_con;
		$SelDet=GeneralAdmin::ExecuteQuery($SelList,"select");
		if(sizeof($SelDet)>0)
			return false;
		else
			return true;
	}
	
	
	//*********************************	 Function For Make String as SEO URL String		*********************************/
	function MakeAltSEOUrlString($tablename,$field_idname,$field_titlename,$field_id,$title_name)
	{
		$where_con='';$select_con='';$AltCheck=false;
		if(is_numeric($field_id) && $field_id !=""){
			$select_con=" ".$field_idname." = '".$field_id."'";$AltCheck=true;
		}
		elseif($field_id !="" && !is_numeric($field_id)){
			$select_con=" md5(".$field_idname.")= '".$field_id."'";$AltCheck=true;
		}
		
		if($AltCheck){
			$SelList="select * from ".$tablename." where ".$select_con;
			$SelDet=GeneralAdmin::ExecuteQuery($SelList,"select");
			if(sizeof($SelDet)>0){		
				$seo_title=StripStringToSEOUrlString($title_name);	/// Make SEo URL String
				
				if(!GeneralAdmin::CheckTitleExist($tablename,$field_idname,$field_titlename,$field_id,$seo_title))
				{
					$seo_title=$seo_title.$SelDet[0][$field_idname];
				}
				
				$AltCon="Update ".$tablename." set ".$field_titlename." ='".$seo_title."' where ".$select_con;
				GeneralAdmin::ExecuteQuery($AltCon, "update");
			}	
		}	
	}
	
	/********************************* 	Function For Check Duplicate title Exist	*********************************/
	function CheckDupRecord($CheckQuery)
	{
		$CheDup=GeneralAdmin::ExecuteQuery($CheckQuery,"select");
		if(sizeof($CheDup)>0)
			return false;
		else
			return true;
	}	
				
	/**********************	 Function For Delete Reference Table Details For Main Table		**************************/
	function NextGenReferenceTableDelete($TableName,$UserIds)
	{	
		global $ngconfig;$ImgArr=array();
		switch(trim($TableName))
		{	
			case 'pj_item_file':
					$PhotoLoc = $ngconfig['ProductfilesLoad'];
					$SelQry="Select FileName from pj_item_file Where ItemId  IN ( ".$UserIds." )";
					$ImgList=GeneralAdmin::ExecuteQuery($SelQry,"select");//echo '<pre>';print_r($ImgList);exit;
						if($ImgList[0]['FileName']!=''){
						foreach($ImgList as $LangDet){
							unlink($PhotoLoc.$LangDet['FileName']);
						}
						}
			break;
			case 'pj_item_image':	
					$PhotoLoc = $ngconfig['ProductImageLoad']; 
					$SelQry="Select ImageName from pj_item_image Where ImageId  IN ( ".$UserIds." )";
					$ImgList=GeneralAdmin::ExecuteQuery($SelQry,"select");//echo '<pre>';print_r($ImgList);exit;
					if($ImgList[0]['ImageName']!=''){
						foreach($ImgList as $LangDet){
							unlink($PhotoLoc.$LangDet['ImageName']);
						}
						}
			break;
			default:
			break;	
		}
		if(sizeof($ImgArr)>0)
		{		
			foreach($ImgArr as $LangDet=>$LangImg)
			{
				if($LangImg!='')
				{
					if(file_exists($LangImg))	
						unlink($LangImg);
				}
			}
		}	
	}
	
	//////////////////////// Function For Mail Template Details //////////////////////////
	function GetMailTemplateDetailsForMail($SendTo,$MailBody,$Type)
	{	
	   	$SelMQuery="select * from pj_mail_template where MailNotify='1' and MailType='".$Type."'";
		$SelMailDet=MysqlFns::ExecuteQuery($SelMQuery,"select");
		if(sizeof($SelMailDet)>0){
			$MailBody=str_replace("{MailConTent}",$SelMailDet[0]['MailCotent'],$MailBody);
			
			$headers= "MIME-Version: 1.0\r\n";
			$headers.= 'From: '.AdminProName.'<'.AdminMail."> \r\n";
			$headers.= "Content-type:text/html; charset=iso-8859-1\r\n";
			GeneralAdmin::WesoIshMailTemplate($SendTo,$SelMailDet[0]['MailSub'],$MailBody,$headers);
		
		}
	}
	
	////////////////////////  		Function For Sent Mail Template			 //////////////////////////
	function WesoIshMailTemplate($mail_user,$Subject,$MailBody,$headers)
	{ 
		$MailCon='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
		"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>'.SiteMainTitle.'</title>
		</head>
		<body style="font: bold 12px  Arial;">
			<table style="border:solid 3px #001B46;" width="700" align="center" cellpadding="0" cellspacing="0">
				<tr bgcolor="#ffffff"><td><img src="'.SiteMainPath.'images/logo.png" border="0" height="100" width="100"/></td></tr>
				<tr bgcolor="#001B46"><td height="5px"></td></tr>
				<tr><td style="font: bold 12px  Arial; padding:10px;border-top:solid 3px #FFFFFF;" align="center">
						'.html_entity_decode($MailBody).'
				</td></tr>
				<tr><td height="10">&nbsp;</td></tr>
				<tr><td height="10">Sincerely,</td></tr>
				<tr><td height="10"><strong><a href="'.SiteMainPath.'" style=" text-decoration:none;">'.OurSiteNameLink.' Team.</a></strong></td></tr>
				<tr><td height="10">&nbsp;</td></tr><br /><br />
				
				<tr bgcolor="#001B46"><td style="padding:10px;border-top:solid 3px #FFFFFF;color:#FFFFFF" align="center" >
				&#169;&nbsp;'.site_copyright.' </td>
				</tr></table>
			</body>
		</html>';
	//echo	$mail_user."<br />".$Subject."<br />".$MailCon."<br />".$headers;exit;

/*<tr><td height="10">Regards,</td></tr>
				<tr><td height="20"><strong>'.SiteMainTitle.' Team</strong></td></tr>*/
		if(@mail($mail_user,$Subject,$MailCon,$headers))
		{	}
	
	}
		
	/********************************* Function For Check Duplicate title Exist		*********************************/
	function WesoUploadedFile($FileType,$LoadFileName,$UploadLoc,$FileName,$ResizeArr=array())
	{
		if(copy($LoadFileName,$UploadLoc.$FileName))
		{	
			if($FileType=='Image' && !empty($ResizeArr))
			{	
				$FileLoc=$UploadLoc.$FileName;
				if(chmod($FileLoc, 0777))
				{
					list($width, $height) = getimagesize($FileLoc);					
					for($i=0;$i<sizeof($ResizeArr);$i++)
					{
						list($NewWid,$HewHei,$FileName) = explode('||',$ResizeArr[$i]);
						
						if($width >= $NewWid && $height >= $HewHei)
							$objPear= new PEARImageResize($FileLoc, $UploadLoc.$FileName,$NewWid,$HewHei);
						else if($width < $NewWid && $height >= $HewHei)
							$objPear= new PEARImageResize($FileLoc, $UploadLoc.$FileName, $width,$HewHei);
						else if($width >= $NewWid && $height < $HewHei)
							$objPear= new PEARImageResize($FileLoc, $UploadLoc.$FileName, $NewWid,$height);		
						else
							$objPear= new PEARImageResize($FileLoc, $UploadLoc.$FileName,$width,$height);
					}
					return 6;					
				}
				else{  return 5;	}
			}
			else{  return 4;	}
		}
		else{  return 3;	}		
	}
	
	/********************************* Function For Check Duplicate title Exist		*********************************/
	function WesoResizeFile($FileType,$LoadFileName,$UploadLoc,$FileName,$ResizeArr=array())
	{
		if($FileType=='Image' && !empty($ResizeArr))
		{	
			$FileLoc=$UploadLoc.$FileName;
			if(chmod($FileLoc, 0777))
			{
				list($width, $height) = getimagesize($FileLoc);					
				for($i=0;$i<sizeof($ResizeArr);$i++)
				{
					list($NewWid,$HewHei,$FileName) = explode('||',$ResizeArr[$i]);
					
					if($width >= $NewWid && $height >= $HewHei)
						$objPear= new PEARImageResize($FileLoc, $UploadLoc.$FileName,$NewWid,$HewHei);
					else if($width < $NewWid && $height >= $HewHei)
						$objPear= new PEARImageResize($FileLoc, $UploadLoc.$FileName, $width,$HewHei);
					else if($width >= $NewWid && $height < $HewHei)
						$objPear= new PEARImageResize($FileLoc, $UploadLoc.$FileName, $NewWid,$height);		
					else
						$objPear= new PEARImageResize($FileLoc, $UploadLoc.$FileName,$width,$height);
				}
				return 6;					
			}
			else{  return 5;	}
		}
		else{  return 4;	}	
	}
	function FileUploadErrorMessage($FErrCode,$FileDet,$FileType) {
	
		switch ($FErrCode) {
			case UPLOAD_ERR_OK:
					if($FileType=='Image'){
						if((($FileDet[0]*$FileDet[1]*$FileDet['bits'])/1048576)>=(int)(ini_get('memory_limit'))){
							return 'The uploaded file exceeds the Maximum Memory Limit. Please use low Quality Images';
						}
						else{	return true;	}
					}
					else{	return true;	}		
				break;	
			case UPLOAD_ERR_INI_SIZE:
				return 'The uploaded file exceeds the Maximum upload size';
			case UPLOAD_ERR_FORM_SIZE:
				return 'The uploaded file exceeds the Maximum File upload size';
			case UPLOAD_ERR_PARTIAL:
				return 'The uploaded file was only partially uploaded';
			case UPLOAD_ERR_NO_FILE:
				return 'No file was uploaded';
			case UPLOAD_ERR_NO_TMP_DIR:
				return 'Missing a temporary folder';
			case UPLOAD_ERR_CANT_WRITE:
				return 'Failed to write file to disk';
			case UPLOAD_ERR_EXTENSION:
				return 'File upload stopped by extension';
			default:
				return 'Unknown upload error';
		}
	} 
	function WesoFileFolderDelete($ImgArr)
	{
		if(count($ImgArr)>0)
		{		
			foreach($ImgArr as $LangDet=>$LangImg)
			{
				if($LangImg!='')
				{
					if(file_exists($LangImg))	
						unlink($LangImg);
				}
			}
		}	
	}	
}
?>