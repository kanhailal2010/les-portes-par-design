<?php 
class Admin extends MysqlFns
{	
	/**************************		Function Change Admin Profile, site and payment setting		*******************************/
	function AdminProfileChangeSettings($PageType)
	{
		global $objSmarty;extract($_POST);$FlagIns=true;
		
		switch($PageType){
			case 'username':
					if(empty($TxtUername) || empty($TxtRUername) || empty($TxtNUername)){
						$objSmarty->assign("ErrMessage", "All Fields are Mandatory. Please Fill All Fields."); $FlagIns=false;
					}
					elseif($TxtRUername==$TxtNUername){
					
						$SelMem= "Select AdminIdent,admin_name from pj_siteadmin Where AdminIdent = ".$_SESSION['PJ525A_Ident']."";
						$SelMem= $this->ExecuteQuery($SelMem, "select");
						if(sizeof($SelMem)>0 && $SelMem[0]['admin_name']==$TxtUername){
							if($TxtUername!=$TxtRUername){		
								$AltSet="Update `pj_siteadmin` set admin_name='".$TxtNUername."' where 
											AdminIdent = ". $_SESSION['PJ525A_Ident'];
								$AltCheck=$this->ExecuteQuery($AltSet, "update");
								//echo $AltCheck;exit;
								if(!empty($AltCheck)) {
									unset($_SESSION['PJ525A_Session']);		//	Clear Admin	Login Session Details
									unset($_SESSION['PJ525A_Name']);
									unset($_SESSION['PJ525A_Type']);
									unset($_SESSION['PJ525A_Ident']);
									unset($_SESSION['PJ525A_Time']);
									Redirect("index.php?pro_msg=unamesucc"); 
								}
							}
							else{
								$objSmarty->assign("ErrMessage","Choose a UserName you haven't previously used with this Admin.");
							}
						}
						else{	$objSmarty->assign("ErrMessage", "Invalid Current Username"); 	}
					}
					else{	$objSmarty->assign("ErrMessage", "New Username and Retype Username must be same"); 		}
				break;			
			case 'profile':
					$MailRegx= "/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/";$Err=array();				
					if (empty($ProfileEMail))
					{	$Err[] = 'Please Enter Your Email Address <br>'; 			}
					elseif (!preg_match($MailRegx,$ProfileEMail))
					{	$Err[]= 'Please Enter the valid Valid Email Address<br>'; 	}		
					if (empty($ProfileName))
					{	$Err[]='Please Enter your Usrename<br>'; 	}
						
					if(sizeof($Err)==0){///empty is checking 
						 $AltSet="Update `pj_siteadmin` set ProfileName='".$ProfileName."',ProfileEMail='".$ProfileEMail."' 
									where AdminIdent ='".$_SESSION['PJ525A_Ident']."'";
						$AltCheck=$this->ExecuteQuery($AltSet, "update");
						if(!empty($AltCheck)){///update is empty,values checking
							$_SESSION['PJ525A_Name']	= $ProfileName;
							Redirect('admin_site_settings.php?act_type=profile&pro_msg=succ');
						}
						else{
							$objSmarty->assign("ErrMessage", "IT IS ALREADY UPDATED PLEASE DO SOME MODIFICATIONS TO UPDATE"); 
						}
					}
					else{	$objSmarty->assign('ErrMessage',join('<br/>',$Err));		}
				break;
		case 'video':
				if(empty($VideoDesc)){
					$objSmarty->assign("ErrMessage", "This Field Mandatory. Please Fill this Field."); $FlagIns=false;
				}
				else{
					
					$AltSet = "Update pj_sitesettings set VideoDesc = '".$VideoDesc."'
								where  AdminIdent = ". $_SESSION['PJ525A_Ident'];
					$this->ExecuteQuery($AltSet, "update");
					Redirect('admin_site_settings.php?act_type=video&pro_msg=succ');
					
				}
				break;	
			case 'password':
					if(empty($txtCurPwd) || empty($txtNewPwd) || empty($txtConPwd)){
						$objSmarty->assign("ErrMessage", "All Fields are Mandatory. Please Fill All Fields."); $FlagIns=false;
					}
					elseif($txtNewPwd==$txtConPwd){
						$SelDet= "Select AdminIdent from pj_siteadmin Where admin_pass = '".$txtCurPwd."' AND 
									AdminIdent=".$_SESSION['PJ525A_Ident']."";
						$SelDet	= $this->ExecuteQuery($SelDet, "select");
						if(sizeof($SelDet)==1){
							if($txtCurPwd!=$txtNewPwd){
								$AltSet = "Update pj_siteadmin set admin_pass = '".$txtNewPwd."' 
											where AdminIdent = ". $_SESSION['PJ525A_Ident'];
								$AltCheck=$this->ExecuteQuery($AltSet, "update");
								if(!empty($AltCheck)) {
									unset($_SESSION['PJ525A_Session']);		//	Clear Admin	Login Session Details
									unset($_SESSION['PJ525A_Name']);
									unset($_SESSION['PJ525A_Type']);
									unset($_SESSION['PJ525A_Ident']);
									unset($_SESSION['PJ525A_Time']);
									Redirect("index.php?pro_msg=passsucc");
 								}
							}
							else{
								$objSmarty->assign("ErrMessage","Choose a password you haven't previously used with this Admin.");
							}
						}
						else{	$objSmarty->assign("ErrMessage", "Invalid Current Password");	}
					}
					else{	$objSmarty->assign("ErrMessage", "New Password and Retype Password must be same");	}
				break;
			case 'settings':
				//echo '<pre>';print_r($_POST);exit;
				if(!empty($admin_mail) && !empty($site_name) && !empty($site_copyright) && 
					!empty($contact_mail) && !empty($meta_desc) && !empty($meta_key) && !empty($GMapAddress) && !empty($ExtraHingesPrice) && !empty($BasePrice) && !empty($HolesPrice) && !empty($BarazinMail) && !empty($ShipCost))
				{	
  		 			$AltSet = "Update pj_sitesettings set admin_mail='".$admin_mail."',contact_mail='".$contact_mail."',
						site_name='".$site_name."',site_copyright='".$site_copyright."',meta_key='".$meta_key."',
						OurSiteFooter='".$OurSiteFooter."',meta_desc='".$meta_desc."',ExtraHingesPrice='".$ExtraHingesPrice."', price_level_factor='".$price_level_factor."',
						BasePrice='".$BasePrice."',GMapAddress='".$GMapAddress."',admin_phone='".$admin_phone."',
						HolesPrice='".$HolesPrice."',BarazinMail='".$BarazinMail."',ShipCost='".$ShipCost."',
						GDHolePrice='".$GDHolePrice."' where AdminIdent = ". $_SESSION['PJ525A_Ident'];
					$AltCheck=$this->ExecuteQuery($AltSet, "update");
							
					$AltSet1="Update `pj_siteadmin` set ProfileEMail='".$admin_mail."' where 
								AdminIdent = ". $_SESSION['PJ525A_Ident'];
					$AltCheck1=$this->ExecuteQuery($AltSet1, "update");
					if(!empty($AltCheck)){///update is empty,values checking
						Redirect('admin_site_settings.php?act_type=settings&pro_msg=succ');
					}
					else{
						$objSmarty->assign("ErrMessage", "IT IS ALREADY UPDATED PLEASE DO SOME MODIFICATIONS TO UPDATE"); 
					}
				}
				else{
					$objSmarty->assign("ErrMessage", "All Fields are Mandatory. Please Fill All Fields."); 
				}
			 	break;
			case 'paypalset':
					if(!empty($API_Username) && !empty($API_Password) && !empty($API_Signature) && !empty($PayPalMail)){
						$ServerType=(isset($_POST['ServerType']))?$_POST['ServerType']:'0';
						$AltSet="Update pj_payment_settings set ServerType='".$ServerType."',API_Username='".$API_Username."',
								API_Password='".$API_Password."',API_Signature='".$API_Signature."',PayPalMail='".$PayPalMail."'";
						$AltCheck=$this->ExecuteQuery($AltSet, "update");
						if(!empty($AltCheck)){///update is empty,values checking
							Redirect('admin_site_settings.php?act_type=paypalset&pro_msg=succ');
						}
						else{
							$objSmarty->assign("ErrMessage", "IT IS ALREADY UPDATED PLEASE DO SOME MODIFICATIONS TO UPDATE"); 
						}
					}
					else{
						$objSmarty->assign("ErrMessage", "All Filelds are Manadatory.Please Fill All Filelds."); 
					}
				break;
			default:
				break;
		}
	}
	
	/*************************** 			Function For List Article Message Details		***************************/
	function SelectAdminLists($ResVal,$PageVal)
	{
		global $objSmarty;$SelCon="";$PageURL='admin_mgmt.php?';
		
		$seaname=(isset($_GET['seaname'])) ? $_GET['seaname']:'';$showonly=(isset($_GET['showonly'])) ? $_GET['showonly']:'';
		if($seaname!=""){
			$SelCon.=" and  admin_name LIKE '".trim($seaname)."%'";$PageURL.="seaname=".urlencode($seaname)."&";
		}
		if($showonly!="")
		{
			$SelCon.="and Ad.AdminStatus='".$showonly."'";$PageURL.="showonly=".urlencode($showonly)."&";
		}
		if($_SESSION['PJ525A_Ident']!= 1)
		{	
			$SelCon.="and Ad.AdminIdent='".$_SESSION['PJ525A_Ident']."' ";
		}		
		$SelFields=array("Ad.*");
 		$SelQuery="SELECT count(Ad.AdminIdent) from pj_siteadmin Ad where Ad.AdminIdent!='' $SelCon ";
		GeneralAdmin::SelectTableContentLists($ResVal,$SelQuery,$SelFields,$PageVal,$PageURL,15);
	}
	
	/********************admin panel  ******************************/
	function AddUpdateAdmin($con_id){ //echo $_SESSION['PJ525A_Ident'];exit;
		global $objSmarty,$ngconfig;$ErrMsg=array();extract($_POST);$where_con='';
		
		if(!empty($con_id)){
			$where_con=" and md5(AdminIdent)!='".$con_id."' ";
		}
 		if(empty($admin_name)){
			$ErrMsg[]='Please enter Admin User Name';
		}
		else{
			$SelQuery="SELECT * from pj_siteadmin where admin_name='".$admin_name."' $where_con";
			if(!GeneralAdmin::CheckDupRecord($SelQuery)){	
				$ErrMsg[]='User Name already exists, please enter another User Name';
			}	
		} 		
		if(empty($admin_pass)){
			$ErrMsg[]='Please enter Password';
		}
		if(empty($admin_mail)){	
			$ErrMsg[]="Please enter Email Address";
		}
		elseif(!preg_match(MailRegx, $admin_mail)){
			$ErrMsg[]="Please enter Valid Email Address";
		}
		else{
			$SelQuery="SELECT * from pj_siteadmin where ProfileEMail='".$admin_mail."' $where_con";
			if(!GeneralAdmin::CheckDupRecord($SelQuery)){	
				$ErrMsg[]='Email already exists, Please enter another Email';
			}	
		} 		
		if(count($ErrMsg)==0){
			if($AdminAction=='Update'){
				/************************** Admin User Update	*************************************/
			 	$AltCon="Update pj_siteadmin set admin_name='".$admin_name."',admin_pass='".$admin_pass."',
				 		ProfileEMail='".$admin_mail."' Where AdminIdent!='' and md5(AdminIdent)='".$con_id."'";
				$AltCheck=$this->ExecuteQuery($AltCon, "update");
 				if(!empty($AltCheck)){ 
					$checkquery="select AdminIdent from pj_siteadmin where md5(AdminIdent)='".$con_id."'";
					$Check=$this->ExecuteQuery($checkquery, "select");
					if($Check[0]['AdminIdent']==$_SESSION['PJ525A_Ident'])
					{
					unset($_SESSION['PJ525A_Session']);		//	Clear Admin	Login Session Details
					unset($_SESSION['PJ525A_Name']);
					unset($_SESSION['PJ525A_Type']);
					unset($_SESSION['PJ525A_Ident']);
					unset($_SESSION['PJ525A_Time']);
					Redirect("index.php?pro_msg=ausucc");
					}
					else
					{
						Redirect('admin_mgmt.php?pro_msg=usucc');
					}
				}
				else{
					$objSmarty->assign("ErrorMessage", "IT IS ALREADY UPDATED PLEASE DO SOME MODIFICATIONS TO UPDATE"); 
				}
			}
			else{	
				/*****************************	User Panel Registraion	*************************************/
				$InsDet="insert into pj_siteadmin(admin_name,admin_pass,ProfileEMail,AdminStatus) 
							values('".$admin_name."','".$admin_pass."','".$admin_mail."','1')"; 
				$this->ExecuteQuery($InsDet,"insert");
  				Redirect('admin_mgmt.php?pro_msg=asucc');
			}
		}
		else
		{	$objSmarty->assign("ErrorMessage",implode('</p><p>',$ErrMsg));$objSmarty->assign("Arr",$_POST);		}
	}
	
	/**************************		Function For Check Admin Login setting	Details	*******************************/
	function CheckAdminLogin()
	{ 	
		global $objSmarty;$ErrMsg=array();extract($_POST);
		
		if(empty($SLogPass)){
			$ErrMsg[]="Please enter User Name.";
		}
		if(empty($SLogPass)){
			$ErrMsg[]="Please enter Login Passowrd.";
		}
		if(sizeof($ErrMsg)==0){ 
		switch($ALogType)
			{
				case 'MasterAdmin':	

					$SelAdm="Select * from pj_siteadmin Where admin_name= '".$SLogName."'";
						$SelAdm	= $this->ExecuteQuery($SelAdm, "select");//echo '<pre>';print_r($SelAdm);exit;
					if(sizeof($SelAdm)==1)
					{ 
							if($SelAdm[0]['AdminStatus']==1)
							{ 
							//	$MemPass=OurTechEncryptionDecryption('Decrypt',$SelAdm[0]['admin_pass']);
								if($SelAdm[0]['admin_pass']==$SLogPass)
								{
									$_SESSION['PJ525A_Session']	= session_id();
									$_SESSION['PJ525A_Name']	= $SelAdm[0]['admin_name'];
									if($SelAdm[0]['AdminIdent']==1){
										$_SESSION['PJ525A_Type']	= 'MainAdmin';
									}
									else{
										$_SESSION['PJ525A_Type']	= 'SubAdmin';
									}	
									$_SESSION['PJ525A_Ident']	= $SelAdm[0]['AdminIdent'];//echo $_SESSION['PJ525A_Ident'];exit;
									$_SESSION['PJ525A_Time']	= strtotime("now");
									Redirect('index.php');
								}
								else{	
								$objSmarty->assign("ErrorMessages", "Your Password is InValid. Please Try Again.");			
								}
						}
						else
						{	
						$objSmarty->assign("ErrorMessages", "Your Account has been Blocked By Administrator");		
						}
					}
					else{	$objSmarty->assign("ErrorMessages",'Invalid User Name and Password.');		}	
													
					break;
				default:	
						$objSmarty->assign("ErrorMessage", "Your User Name and Password is InValid.");
					break;	
			}					
		}
		else{	
		$objSmarty->assign("ErrorMessage",implode('</p><p>',$ErrMsg));}	
	}
	
	/**************************		Function For Select Admin site and payment setting	Details	*******************************/
	function SelectAdminSettings($PageType)
	{
		global $objSmarty;
		switch($PageType)
		{
			case 'profile':
					$SelDet="select * from pj_siteadmin where AdminIdent!=''";
					$AdminDet=$this->ExecuteQuery($SelDet,"select");
					$objSmarty->assign("AdminDet",$AdminDet[0]);
				break;
			case 'paypalset':
					$SelDet="select * from pj_payment_settings where PayTypeId!=''";
					$AdminDet=$this->ExecuteQuery($SelDet,"select");
					$objSmarty->assign("AdminDet",$AdminDet[0]);
				break;
			case 'landing':
			 	$SelDet="select LandingImage from pj_sitesettings where AdminIdent!=''";
				$AdminDet=$this->ExecuteQuery($SelDet,"select");
				$objSmarty->assign("AdminDet",$AdminDet[0]);
				break;
			default:
					$SelDet="select * from pj_sitesettings where AdminIdent!=''";
					$AdminDet=$this->ExecuteQuery($SelDet,"select");
					$objSmarty->assign("AdminDet",$AdminDet[0]);
				break;
		}
	}
	/**************************		Function For Select  forget password Details	*******************************/
	function SendLoginDetailsToAdmin($SLogType)
	{	
		global $objSmarty,$ngconfig;$site_path=$ngconfig['SiteGlobalPath'];$FlagIns=true;
		switch($SLogType)
		{
			case 'MasterAdmin':	
				
			if(empty($_POST['Forget'])){
			
					$objSmarty->assign("SiteTitle",SiteMainTitle." - Forget Password"); ///site title
					$Err=array();extract($_POST);	
					$MailRegx= "/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/";
					if (empty($for_Mail)){
						$Err[] = 'Please enter the Email Address';$FlagIns=false;
					}
					elseif (!preg_match($MailRegx,$for_Mail)){
						$Err[]= 'Please enter the valid Email Address';$FlagIns=false;
					}
					if($FlagIns)
					{
						$SelMem = "SELECT * FROM `pj_siteadmin` where AdminIdent!='' and ProfileEMail='".$for_Mail."'";
						$MemDet=  $this->ExecuteQuery($SelMem,"select");
						
						//$MemPass=OurTechEncryptionDecryption('Decrypt',$MemDet[0]['admin_pass']);
						if(sizeof($MemDet)==1)
						{	
							$headers  = "MIME-Version: 1.0\n";
							$headers .= "Content-type: text/html; charset=iso-8859-1\n";
							$Subject = 	'Your Login Details For '.SiteMainTitle;
							$headers .= 'From: '.AdminProName.'<'.AdminMail.">\n";
							
						 	$MailCon='<table width="730" border="0" cellspacing="0" cellpadding="0" align="center">
							<tr><td style="font:12px Arial; color:#333333;text-align:justify;line-height:18px;">
								<h1 style="padding:0px;margin:0px;font:bold 15px/25px Arial; color:#d13d0f;line-height:25px;">
								Hi '.$MemDet[0]['ProfileName'].'</h1> Your Login Details For '.SiteMainTitle.' is shown below<br/>
									<table width="665" border="0" cellspacing="0" align="center" cellpadding="0"><tr>
										<td width="101" align="center" valign="top" style="padding-top:10px;"></td>
										<td width="564" valign="top"><table width="564" border="0" cellspacing="0" cellpadding="0">
											<tr height="25"><td width="120"><strong>User Name </strong></td>
												<td width="400">: '.$MemDet[0]['admin_name'].'</td></tr>
											<tr height="25"><td width="120"><strong>Email </strong></td>
												<td width="400">: '.$MemDet[0]['ProfileEMail'].'</td></tr>
											<tr height="25"><td width="120"><strong>Password </strong></td>
												<td width="400">: '.$MemDet[0]['admin_pass'].'</td></tr>					
										</table></td>
									</tr></table>
							</td></tr></table>';
							GeneralAdmin::WesoIshMailTemplate($MemDet[0]['ProfileEMail'],$Subject,$MailCon,$headers);
							//GeneralAdmin::GetMailTemplateDetailsForMail($MemDet[0]['ProfileEMail'],$MailCon,'ForgotPassword');
							Redirect('index.php?login=msucc');
						}
						else
						{	$objSmarty->assign('ErrorMessage','Entered mail-id not associate with any of the account.');	}
					}
					else
					{	$objSmarty->assign('ErrorMessage',join('<br/>',$Err));		}
				}	
				break;
			default:
			
				break;
		}						
	} 
	
	function SelectVisitorsStatistics(){
		global $objSmarty;
	
		$SelTot="SELECT COUNT(VId) as Total FROM pj_site_visitors_statistics where VId!=''";
		$TotDet=$this->ExecuteQuery($SelTot,"select");
		
		$SelDay="SELECT COUNT(VId) as DayTotal FROM pj_site_visitors_statistics GROUP BY DAY(VDate) order by DayTotal desc";
		$DayDet=$this->ExecuteQuery($SelDay,"select");
		$AvgTotDay=$TotDet[0]['Total']/sizeof($DayDet); 
		$objSmarty->assign("DayTotal",floor($AvgTotDay)); 
		
		$SelWeek="SELECT COUNT(VId) as WeekTotal FROM pj_site_visitors_statistics GROUP BY WEEK(VDate) order by WeekTotal desc";
		$WeekDet=$this->ExecuteQuery($SelWeek,"select");
		$AvgTotWeek=$TotDet[0]['Total']/sizeof($WeekDet);   
		$objSmarty->assign("WeekTotal",floor($AvgTotWeek));  
		
		$SelMonth="SELECT COUNT(VId) as MonTotal FROM pj_site_visitors_statistics GROUP BY MONTH(VDate) order by MonTotal desc";
		$MonthDet=$this->ExecuteQuery($SelMonth,"select");
		$AvgTotMon=$TotDet[0]['Total']/sizeof($MonthDet);  
		$objSmarty->assign("MonTotal",floor($AvgTotMon));  
		
		$SelYear="SELECT COUNT(VId) as YearTotal FROM pj_site_visitors_statistics GROUP BY YEAR(VDate) order by YearTotal desc";
		$YearDet=$this->ExecuteQuery($SelYear,"select");
		$AvgTotYear=$TotDet[0]['Total']/sizeof($YearDet);  
		$objSmarty->assign("YearTotal",floor($AvgTotYear)); 	
		
	}
}
?>