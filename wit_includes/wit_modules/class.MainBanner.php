<?php   #####	Class Function for Banner	####
class MainBanner extends MysqlFns
{
	
	////////////////////////// 				Function For Select Banner List				/////////////////////////
	
	function SelectBannerLists($ResVal,$PageVal)
	{
		global $objSmarty;$SelCon="";
		 $PageURL='mainbanner_mgmt.php?';
		$seaname=(isset($_GET['seaname'])) ? $_GET['seaname']:'';$showonly=(isset($_GET['showonly'])) ? $_GET['showonly']:'';
		$seatitle=(isset($_GET['seatitle']))?$_GET['seatitle']:''; 
		if($showonly!=""){
			$SelCon.=" and BL.BannerStatus='".$showonly."'";$PageURL.="showonly=".urlencode($showonly)."&";
		} 
		if($seatitle!=""){
			$SelCon.=" and BL.BannerTitle Like '".trim($seatitle)."%'";$PageURL.="seatitle=".urlencode($seatitle)."&";
		} 
		$SelFields=array('BL.*','PP.*');
		$SelQuery="Select count(BL.BannerID) from pj_banners_list BL left join pj_content_pages PP on PP.page_id =BL.PageId Where
				BL.BannerID!='' $SelCon order by BL.BannerID desc";		
		GeneralAdmin::SelectTableContentLists($ResVal,$SelQuery,$SelFields,$PageVal,$PageURL,15);
	}
	
	/*****************************		Function For Add and Edit Banner DSetails		*********************************/
	function AdminAddUpdatteBannerDetails($AdminAction,$con_id)
	{
		global $objSmarty,$ngconfig;extract($_POST);$ErrMsg=array();$Che_con='';
 		if($con_id)
		{	$Che_con=" and md5(BannerID)!='".$con_id."'";	} 
		 
		if($_POST['AdminAction']!='Update' || $_FILES['BannerImage']['name']!=''){
			if($_FILES['BannerImage']['name']=='') {
				$ErrMsg[]="Please Select Banner Image";
			}  
		}
 		if(count($ErrMsg)==0)
		{
			$CImgDet='';
 				$PhotoLoc=$ngconfig['ProImageLoad'];
				if($_FILES['BannerImage']['name']!='' && $_FILES['BannerImage']['error']==0)
				{	 
					$path_info=pathinfo($_FILES['BannerImage']['name']);$extension = strtolower($path_info['extension']);
					$BannerImage=date("Ymd").time().rand(5,10000).'.'.$path_info['extension'];
					$ResizeArr=array("300||100||m_".$BannerImage);
					$ImgSuc=GeneralAdmin::WesoUploadedFile('Image',$_FILES['BannerImage']['tmp_name'],$PhotoLoc,$BannerImage,
											$ResizeArr);
 //echo "<PRE>"; print_r($_FILES);exit;
			//$ImgSuc=GeneralAdmin::WesoUploadedFile('Image',$_FILES['BannerImage']['tmp_name'],$LoadLoc,$BannerImage,'');		
					if($ImgSuc==6) {	
						$SelQuery="SELECT `BannerImage` from  pj_banners_list where md5(BannerID)='".$con_id."'";
						$Result=$this->ExecuteQuery($SelQuery, "select");
						if($Result[0]['BannerImage']!=''){
							$FileDelete=array($PhotoLoc.$Result[0]['BannerImage']);
							GeneralAdmin::WesoFileFolderDelete($FileDelete);	
						}
						$BannerImage;
					}
					$AltImg=",BannerImage='".$BannerImage."' ";
				} 
			
			if($_POST['AdminAction']=='Update'){
  			   $AltBan = "Update pj_banners_list set PageId= '".$PageId."',BannerTitle='".$BannerTitle."' $AltImg Where
			   		 md5(BannerID)='".$con_id."'";
							
				$AltCheck=$this->ExecuteQuery($AltBan, "update"); 	
				if(!empty($AltCheck)){
					Redirect('mainbanner_mgmt.php?pro_msg=usucc');
				}
				else{
					$objSmarty->assign("ErrMessage", "IT IS ALREADY UPDATED PLEASE DO SOME MODIFICATIONS TO UPDATE"); 
				}
			}
			else{
				$BanIns="insert into pj_banners_list(PageId,BannerTitle,BannerImage) values('".$PageId."','".$BannerTitle."',
						'".$BannerImage."')";
				$this->ExecuteQuery($BanIns,"insert");
				Redirect('mainbanner_mgmt.php?pro_msg=asucc');
			}
		}
		else
		{	$objSmarty->assign("ErrMessage",implode('<br/>',$ErrMsg));$objSmarty->assign("ArrDet",$_POST);		}	
	} 
	
}
?>