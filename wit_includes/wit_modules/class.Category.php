<?php
class Category extends MysqlFns
{	
	/*****************************	Category List	**************************************************/
	/*****************************		Select  Category List	**************************************************/
	function SelectMainCatList()
	{///search is not completed,beacuse change work
		global $objSmarty;$SelCon=""; $PageURL=SiteMainPath.'category.html?'; 
		$search_text=(isset($_GET['search_text'])) ? $_GET['search_text']:'';$cat=(isset($_GET['cat'])) ? $_GET['cat']:'';
		
		if($search_text!=""){//search values
			$SelCon.=" and PC.CatName LIKE '".trim($search_text)."%'";$PageURL.="search_text=".urlencode($search_text)."&";
		}
		if($cat!=""){//search values
			$SelCon.=" and PC.CatName='".$cat."'";$PageURL.="cat=".urlencode($cat)."&";
		}
		$SelFields=array('PC.*');
		$SelQuery="SELECT count(PC.CatId) from pj_category_main PC  where PC.CatId!='' $SelCon";
		$CatLists=$this->ExecuteQuery($SelQuery,"select");
	
		GeneralAdmin::SelectTableContentLists('CatList',$SelQuery,$SelFields,'ClasPage',$PageURL,10);
	}
	/*****************************	Category && SubCategory List view(12-4-12)	**************************************************/
	function SelectCateJoinList($SelIds)
	{	
		global $objSmarty; $StrSep='';$StrText='';
		 $SelCat ="Select * from pj_category_sublevel Where SCatDelSta='1'and SCatStatus='1'and SCatId!='' and CatId IN (".$SelIds.")" ;
		 $SelRes= $this->ExecuteQuery($SelCat, "select");	
					foreach($SelRes as $CatDet){
						$StrText.='<li><a href=classified/categorty/'.$CatDet['SEOsubCat'].'>'.stripslashes($CatDet['SubCatName']).'</a></li>'; 	
					}		
		return $StrText;//
	}
	
	/*********************************************************************************************************************/	
	/*****************************************   	For Admin Panel Control 	*****************************************/
	/*********************************************************************************************************************/	
	
   	/*****************************		Select  Category List	**************************************************/
	function GetServiceCatList($ResVal,$PageVal)
	{
		global $objSmarty,$objLang;$SelCon="";$PageURL='category_mgmt.php?';$OrderCon="order by CAT.CatId desc";
		
		$seaname=(isset($_GET['seaname'])) ? $_GET['seaname']:'';$sortby=(isset($_GET['sortby'])) ? $_GET['sortby']:'';
		$stcoksort=(isset($_GET['stcoksort'])) ? $_GET['stcoksort']:'';
		if($seaname!=""){
			$SelCon.=" and CAT.CatName LIKE '".trim($seaname)."%'";$PageURL.="seaname=".urlencode($seaname)."&";
		}
		$SortArr=array('cat'=>'CAT.CatName');
		if(!empty($sortby)){
			$SortDet=explode('_',$sortby);
			if(array_key_exists($SortDet[0],$SortArr)){
				$OrderCon=" order by ".$SortArr[$SortDet[0]]." ".$SortDet[1]."";
			}
			$PageURL.="sortby=".urlencode($sortby)."&";
		}	
		$objSmarty->assign("PageURL",$PageURL);$CatStock=array();

		$SelFields=array('CAT.*');//	Fields To select Table listing	
		 $SelQuery="SELECT count(CAT.CatId) from ".$objLang->tableName('pj_category_main',true)." CAT where CAT.CatId!=''$SelCon $OrderCon ";
		$CatList=GeneralAdmin::SelectTableContentLists($ResVal,$SelQuery,$SelFields,$PageVal,$PageURL);
		
/* 		if(sizeof($CatList)>0){		
			$StckSort=SORT_ASC;			
			if($stcoksort=='stocks_desc'){		
				$StckSort=SORT_DESC;
			}
			

			for($i=0;$i<sizeof($CatList);$i++){
				$SelQu="SELECT sum(ProStocks) from pj_product_list where CatId='".$CatList[$i]['CatId']."'";	
				$AltCheck=$this->ExecuteQuery($SelQu, "select");
				$CatList[$i]['ProStocks']=$AltCheck[0]['sum(ProStocks)'];
			}
			function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
				$sort_col = array();
				foreach ($arr as $key=> $row) {
					$sort_col[$key] = $row[$col];
				}
			
				array_multisort($sort_col, $dir, $arr);
			}
			array_sort_by_column($CatList, 'ProStocks',$StckSort);
		}		
		$objSmarty->assign("$ResVal",$CatList);*/
	}
	
   	/*****************************		Add Update Category Name	**************************************************/
	function CategoryAddUpdate($AdminAction,$con_id)
	{ 
		global $objSmarty,$objLang;extract($_POST);$SelCon='';
		
		if(empty($CatName)){
			$ErrMsg[]="Enter The New Category-Name";
		}
		else{
			if(!empty($con_id)){
				$SelCon=" and md5(CatId)!='".$con_id."'";
			}
			$SelQuery="SELECT C.* from ".$objLang->tableName('pj_category_main',true)." C where CatName='".$CatName."' $SelCon";
			if(!GeneralAdmin::CheckDupRecord($SelQuery)){
				$ErrMsg[]="Category Name Already Exists";
			}	
		}	
		if(count($ErrMsg)==0)
		{  
			if($AdminAction=='Update'){
				$AltCon="Update ".$objLang->tableName('pj_category_main',true)." set CatName='".$CatName."' Where md5(CatId)='".$con_id."'";
  				$AltCheck=$this->ExecuteQuery($AltCon, "update");
				GeneralAdmin::MakeAltSEOUrlString($objLang->tableName('pj_category_main',true),'CatId','CatSEOName',$con_id,$CatName);
				if(!empty($AltCheck)){
 					Redirect('category_mgmt.php?pro_msg=usucc');
				}
				else{
					$objSmarty->assign("ErrMessage", "IT IS ALREADY UPDATED PLEASE DO SOME MODIFICATIONS TO UPDATE"); 
				}
			}
			else{	
				$InsCon="insert into ".$objLang->tableName('pj_category_main')." (CatName) values('".$CatName."')";
				$InsConLanguage="insert into ".$objLang->tableName('pj_category_main','french')." (CatName) values('".$CatName."')";
				$this->ExecuteQuery($InsCon,"insert"); $con_id=mysql_insert_id();
				$this->ExecuteQuery($InsConLanguage,"insert");
				
				GeneralAdmin::MakeAltSEOUrlString('pj_category_main','CatId','CatSEOName',$con_id,$CatName);
				GeneralAdmin::MakeAltSEOUrlString($objLang->tableName('pj_category_main','french'),'CatId','CatSEOName',$con_id,$CatName);
 				Redirect('category_mgmt.php?pro_msg=asucc');
			}			
		}
		else
		{	$objSmarty->assign("ErrMessage",implode('<br/>',$ErrMsg));;$objSmarty->assign("Arr",$_POST);		}
	}
	
   	/*****************************		Select Sub Category List	**************************************************/
	function GetSubCatList($ResVal,$PageVal,$CatId)
	{
		global $objSmarty,$objLang;$SelCon="";$PageURL='subcategory_mgmt.php?';$OrderCon="order by SCAT.SCatId desc";
		$SCatStock=array();
		$seaname=(isset($_GET['seaname'])) ? $_GET['seaname']:'';
		$seacat_id=(isset($_GET['seacat_id'])) ? $_GET['seacat_id']:'';$sortby=(isset($_GET['sortby']))?$_GET['sortby']:'';
		$stcoksort=(isset($_GET['stcoksort'])) ? $_GET['stcoksort']:'';
		if($seaname!=""){
			$SelCon.=" and SCAT.SubCatName LIKE '".trim($seaname)."%'";$PageURL.="seaname=".urlencode($seaname)."&";
		}
		if($seacat_id!=""){
			$SelCon.=" and SCAT.CatId='".trim($seacat_id)."'";$PageURL.="seacat_id=".urlencode($seacat_id)."&";
		}
		if(!empty($CatId)){
			$SelCon.=" and  md5(SCAT.CatId)='".$CatId."'";$PageURL.="CatId=".urlencode($CatId)."&";
		}
		if(!empty($stcoksort)){
			$PageURL.="stcoksort=".urlencode($stcoksort)."&";
		}
		$SortArr=array('cat'=>'CAT.CatName','subcat'=>'SCAT.SubCatName');
		if(!empty($sortby)){
			$SortDet=explode('_',$sortby);
			if(array_key_exists($SortDet[0],$SortArr)){
				$OrderCon=" order by ".$SortArr[$SortDet[0]]." ".$SortDet[1]."";
			}
			$PageURL.="sortby=".urlencode($sortby)."&";
		}
		$objSmarty->assign("PageURL",$PageURL);
		$SelFields=array('SCAT.*','CAT.CatName');//	Fields To select Table listing	
		$SelQuery="SELECT count(SCAT.SCatId) from ".$objLang->tableName('pj_category_sublevel',true)." SCAT left join pj_category_main CAT on SCAT.CatId=CAT.CatId 
				where SCAT.SCatId!='' $SelCon  $OrderCon";
		GeneralAdmin::SelectTableContentLists($ResVal,$SelQuery,$SelFields,$PageVal,$PageURL,15,'SubPageNav');

	}
	
   	/*****************************		Select Sub Category List 	**************************************************/
	function SubCategoryAddUpdate($AdminAction,$con_id,$vCatId)
	{ 
		global $objSmarty,$objLang;$ErrMsg=array();$SelCon='';extract($_POST);
		
		if(empty($SubCatName)){
			$ErrMsg[]="Enter the Sub-Category Name";
		}
		else{
			if(!empty($con_id)){
				$SelCon=" and md5(C.SCatId)!='".$con_id."'";
			}
				$SelQuery="SELECT C.* from ".$objLang->tableName('pj_category_sublevel',true)." C where C.SubCatName='".$SubCatName."' and CatId='".$vCatId."' $SelCon";
			if(!GeneralAdmin::CheckDupRecord($SelQuery)){
				$ErrMsg[]="SubCategory Name Already Exists";
			}	
		}	
		if(count($ErrMsg)==0)
		{  
			if($AdminAction=='Update')
			{   
				$AltCon="Update ".$objLang->tableName('pj_category_sublevel',true)." set SubCatName='".$SubCatName."',CatId='".$CatId."' 
							Where md5(SCatId)='".$con_id."'";
  				$AltCheck=$this->ExecuteQuery($AltCon, "update"); 	
				if(!empty($AltCheck)){
					GeneralAdmin::MakeAltSEOUrlString($objLang->tableName('pj_category_sublevel',true),'SCatId','SEOsubCat',$con_id,$SubCatName);
 					Redirect('subcategory_mgmt.php?pro_msg=usucc');
				}
				else{
					$objSmarty->assign("ErrMessage", "IT IS ALREADY UPDATED PLEASE DO SOME MODIFICATIONS TO UPDATE"); 
				}
			}
			else{	
				$InsCon="insert into ".$objLang->tableName('pj_category_sublevel')." (SubCatName,CatId) values('".$SubCatName."','".$CatId."')";
				$InsConLanguage="insert into ".$objLang->tableName('pj_category_sublevel','french')." (SubCatName,CatId) values('".$SubCatName."','".$CatId."')";
				$this->ExecuteQuery($InsCon,"insert"); $con_id=mysql_insert_id();	
				$this->ExecuteQuery($InsConLanguage,"insert"); $con_id=mysql_insert_id();
				
				GeneralAdmin::MakeAltSEOUrlString('pj_category_sublevel','SCatId','SEOsubCat',$con_id,$SubCatName);
				GeneralAdmin::MakeAltSEOUrlString($objLang->tableName('pj_category_sublevel','french'),'SCatId','SEOsubCat',$con_id,$SubCatName);
 				Redirect('subcategory_mgmt.php?pro_msg=asucc');
			}			
		}
		else
		{	$objSmarty->assign("ErrMessage",implode('<br/>',$ErrMsg));$objSmarty->assign("Arr",$_POST);		}
	}
}	
?>