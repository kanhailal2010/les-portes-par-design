<?php
class Inquiry extends MysqlFns
{	

	/**********************************************************************************************************************/
	/************************************	For Admin Panel		*******************************************************/
	/**********************************************************************************************************************/
		
	/*************************** 			Function For List Games Details		***************************/
	function GetInquiryList($ResVal,$PageVal)
	{
		global $objSmarty;$SelCon="";$PageURL='inquiry_mgmt_home.php?';$OrderCon='order by GL.Inq_id desc';
		$sortby=(isset($_GET['sortby'])) ? $_GET['sortby']:'';$seaamil=(isset($_GET['seaamil'])) ? $_GET['seaamil']:''; 
		$seaname=(isset($_GET['seaname'])) ? $_GET['seaname']:''; $seapro=(isset($_GET['seapro'])) ? $_GET['seapro']:''; 
		if($seaname!=""){
			$SelCon.=" and GL.Name LIKE '".trim($seaname)."%'";$PageURL.="seaname=".urlencode($seaname)."&";
		} 
		if($seapro!=""){
			$SelCon.=" and PS.ItemName LIKE '".trim($seapro)."%'";$PageURL.="seapro=".urlencode($seapro)."&";
		} 
		if($seaamil!=""){
			$SelCon.=" and GL.InqMail LIKE '".trim($seaamil)."%'";$PageURL.="seaamil=".urlencode($seaamil)."&";
		}  
		$objSmarty->assign("PageURL",$PageURL);
		$SelFields=array("GL.*","PS.*","PC.*");
 		$SelQuery="SELECT count(GL.Inq_id) from pj_inquiry_list GL left join pj_sell_list PS on PS.SellId=GL.SellId
				left join pj_category_main PC on PC.CatId=PS.CatId where GL.Inq_id!='' $SelCon $OrderCon ";
		GeneralAdmin::SelectTableContentLists($ResVal,$SelQuery,$SelFields,$PageVal,$PageURL);
		 
	}  
}
?>