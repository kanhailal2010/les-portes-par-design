<?php 	
	/*****************************			Site Config File				***********************************************/
	include "../wit_includes/wit_common.php";
		
	/*****************************			Include  File					***********************************************/
	include _MAINSITEPATH_."/wit_SEOURL_Function.php";
	
	/*****************************			Include Supported Class Files	********************************************/
	include_once $ngconfig['SiteClassPath']."class.MainBanner.php";
	include_once $ngconfig['SiteClassPath']."class.PEAR_RESIZE.php";
	include_once $ngconfig['SiteClassPath']."class.GeneralAdmin.php";

	/*****************************			Class Objects			***********************************************/
	$objAdmin	= new GeneralAdmin();$objMBanner= new MainBanner();
		
	/*************************** Declare Page Variables For Admin Control Functions	**************************************/
	$tablename="pj_banners_list";$UniqId="BannerID";$ResVal="BannerList";$m_sta="BannerStatus";$PageVal="BannerPage";
	$MgmtArr=array("BannerMgmt"=>array("$tablename","$UniqId","$m_sta"));
	/***************************	**************************************/		
	$pro_msg=(isset($_GET['pro_msg'])) ? $_GET['pro_msg']:'';$act_type=(isset($_GET['act_type'])) ? $_GET['act_type']:'';
	$mgtact=(isset($_GET['mgtact'])) ? $_GET['mgtact']:'';$con_id=(isset($_GET['con_id'])) ? $_GET['con_id']:'';
	$RedURL='mainbanner_mgmt.php?';$checkstas=(isset($_GET['checkstas'])) ? $_GET['checkstas']:'';

	switch($act_type)
	{
  		case 'addnew':

 				if($con_id!=''){
					CheckAdminMgmtPermission('Redirct','BothAdmin','Banner','ManageBanner');	 	 
 					$SelStatus=$objAdmin->SelectSingleTableRowDetails($tablename,$UniqId,'ArrDet',$con_id);	
					if(!$SelStatus) {
						Redirect('banner_mgmt.php?pro_msg=vfail');
					}
 					$objSmarty->assign("SiteTitle",SiteMainTitle." -  Banner Management - Edit Banner Details");
					$objSmarty->assign("PageTitle","Banner Management - Edit Banner Details");
				}		
				else{ 	
					CheckAdminMgmtPermission('Redirct','BothAdmin','Banner','AddBanner');	 
					$objSmarty->assign("SiteTitle",SiteMainTitle." -  Banner Management - Add New Banner Details");
					$objSmarty->assign("PageTitle","Banner Management - Add New Banner Details");				
				}	
 				if(!empty($_POST['AdminAction'])){	
					$objMBanner->AdminAddUpdatteBannerDetails($_POST['AdminAction'],$con_id); 
				}   
				$Sql1 = 'select * from pj_content_pages Where page_id!=""';
				$Result1= $objMysqlFns->ExecuteQuery($Sql1,'select');
				$objSmarty->assign('PageList',$Result1);  
 	
 				$objSmarty->assign("IncludeTpl", "mainbanner_mgmt_new.html");		
			break;
		default:
				CheckAdminMgmtPermission('Redirct','BothAdmin','Banner','ManageBanner');
				if(isset($_POST['Action_Type'])){ $objAdmin->AdminControlMgmt($tablename,$UniqId,$m_sta,$RedURL); }
 			 
				$objMBanner->SelectBannerLists($ResVal,$PageVal);	
				
				/******************  		Select Admin Control List	**********************/
				$MCtrlArray=array('Active'=>'Active','InActive'=>'InActive','Delete'=>'Delete');
				$objSmarty->assign("ControlList",$MCtrlArray);
				
				/************Error and Success Messages For Administrative Actions  *************************/
				if($mgtact!="") {	
					$ActArray=array("delsuc"=>"Deleted","actsuc"=>"Active","inasuc"=>"Inactive");
					if($checkstas!="fals"){
						$objSmarty->assign("SucMessage","Selected Banner(s) Has Been ".$ActArray[$mgtact]." Successfully");
					}
					else{
						$objSmarty->assign("ErrMessage","Selected Banner(s) Already in ".$ActArray[$mgtact]." Status");
					}
				}	
				if($pro_msg!='') { 
					if ( $pro_msg=="asucc") $objSmarty->assign("SucMessage", "New Banner Details Has Been Added Successfully!!");
					elseif ( $pro_msg=="usucc") $objSmarty->assign("SucMessage", "Banner Details Has Been Updated Successfully!!");	
				}
				
				$objSmarty->assign("SiteTitle",SiteMainTitle." - Banner Management");	//	For Page Main Title
				$objSmarty->assign("PageTitle"," Banner Management");	//	For Page Sub Title	
				$objSmarty->assign("IncludeTpl","mainbanner_mgmt.html"); // Assign Page For Smarty
			break;	
	}
	

	$objSmarty->assign("ProImageDisp",$ngconfig['ProImageDisp']);
 	/*****************************			Include Admin Menu and Ctrl Create File 	**********************************/
	include _MAINSITEPATH_."/wit_admincontrols.php";
	$MCtrlArray=array('ON'=>'ON','OFF'=>'OFF','Delete'=>'Delete');
	$objSmarty->assign("ControlList",$MCtrlArray);	
	#===========================================================================		
	$objSmarty->display("site_admin_main.html");
?>