<?php 	
	/*****************************			Site Config File				***********************************************/
	include "../wit_includes/wit_common.php";
	
	/*****************************			Include  File				***********************************************/
	include_once $ngconfig['SiteClassPath']."class.GeneralAdmin.php";
	include_once $ngconfig['SiteClassPath']."class.Gallery.php";
	include_once $ngconfig['SiteClassPath']."class.PEAR_RESIZE.php";
	include_once $ngconfig['SiteClassPath']."class.Location.php";
	
	/*****************************			Class Objects				***********************************************/
	$objAdmin	= new GeneralAdmin();$objPlace	= new Place();$objLocation	= new Location();
	
	/************************		Table Details Declartion	***************************************/
	$tablename="pj_galleryimage_list";$UniqId="ImageId";$ResVal="PlaceList";$m_sta="ImageStatus";$PageVal="PlacePage";
	
	/*****************************		Page Variables Declartion	***********************************************/
	$RedURL='gallery_mgmt.php?';$pro_msg=(isset($_GET['pro_msg']))?$_GET['pro_msg']:'';
	$act_type=(isset($_GET['act_type'])) ? $_GET['act_type']:'';$checkstas=(isset($_GET['checkstas'])) ? $_GET['checkstas']:'';
	$mgtact=(isset($_GET['mgtact'])) ? $_GET['mgtact']:'';$con_id=(isset($_GET['con_id'])) ? $_GET['con_id']:'';

	switch($act_type)
	{
	case 'addnew': 
				/*****************************		Admin & Subadmin Login Permisson Check 	*************************/
 				if($con_id!=''){
					CheckAdminMgmtPermission('Redirct','BothAdmin','Gallery','Manage Gallery');
 					$objPlace->SelectPlaceDetailsForAdmin($con_id);
					
					$objSmarty->assign("SiteTitle",SiteMainTitle." - Gallery Management - Edit Gallery Details");
					$objSmarty->assign("PageTitle","Gallery Management - Edit Gallery Details");
				}		
				else{
					CheckAdminMgmtPermission('Redirct','BothAdmin','Gallery','Add Gallery');
					$objSmarty->assign("SiteTitle",SiteMainTitle." - Gallery Management - Add Gallery Details");
					$objSmarty->assign("PageTitle","Gallery Management - Add Gallery Details");
				}
				if(isset($_POST['AdminAction'])=='Update'){
					$objPlace->AdminUpdatePlaceDetails($con_id); 
				} 
				$objSmarty->assign("PlaceSub",'PlacesList');											/// Country list 
 				$objSmarty->assign("IncludeTpl","gallery_mgmt_new.html");
			break;
		default:
				/*****************************		Admin & Subadmin Login Permisson Check 	*************************/
				CheckAdminMgmtPermission('Redirct','BothAdmin','Gallery','Manage Gallery');

				/******************  		Call Function For Delete,Active Inactive Table Details 	**********************/
				if(isset($_POST['Action_Type'])){
					$objAdmin->AdminControlMgmt($tablename,$UniqId,$m_sta,$RedURL);
				} 
				/******************  		Select Site Sub-Admin List	**********************/
				$objPlace->SelectSitePlaceLists($ResVal,$PageVal);
	
				/******************  		Admin Action Error and Success Messages	**********************/
				if($pro_msg!=''){
					if($pro_msg=="usucc"){
						$objSmarty->assign("SucMessage", "Gallery Details Has Been Updated Successfully!!");
					}else{
						$objSmarty->assign("SucMessage", "New Gallery Details Has Been Added Successfully!!");
					}
				}
				if($mgtact!="")	{	
				$ActArray=array("delsuc"=>"Deleted");
					if($checkstas!="fals"){
						$objSmarty->assign("SucMessage","Selected Gallery(s) Has Been ".$ActArray[$mgtact]." Successfully");
					}
					else{
						$objSmarty->assign("ErrMessage","Selected Gallery(s) Already in ".$ActArray[$mgtact]." Status");
					}
				}

				$objSmarty->assign("SiteTitle",SiteMainTitle." - Gallery Management");	//	For Page Main Title
				$objSmarty->assign("PageTitle"," Gallery Management");	//	For Page Sub Title
				$objSmarty->assign("IncludeTpl", "gallery_mgmt.html");	//	Assign Page For Smarty	
			break;			
	}
 	
	/*****************************			Include Admin Menu and Ctrl Create File 	**********************************/
	include _MAINSITEPATH_."/wit_admincontrols.php";

	/******************  	Admin Management Active,Delete etc Control Arrays List	 		**********************/
	$MCtrlArray=array('ON'=>'ON','OFF'=>'OFF','Delete'=>'Delete');
	$objSmarty->assign("ControlList",$MCtrlArray);
	
	$objSmarty->assign("TinyMCEPath",$ngconfig['TinyEditor']); ///ckc editor	
	$objSmarty->assign("GalleryImgDisp",$ngconfig['GalleryImgDisp']); 
	
	#===========================================================================		
	$objSmarty->display("site_admin_main.html");
?>