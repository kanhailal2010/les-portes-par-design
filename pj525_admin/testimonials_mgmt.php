<?php 	
	/*****************************			Site Config File				***********************************************/
	include "../wit_includes/wit_common.php";
		
	/*****************************			Include  File					***********************************************/
	include _MAINSITEPATH_."/wit_SEOURL_Function.php";
	
	/*****************************			Include Supported Class Files	********************************************/
	include_once $ngconfig['SiteClassPath']."class.GeneralAdmin.php";
	include_once $ngconfig['SiteClassPath']."class.Testimonials.php";
	
	/*****************************			Class Objects			*		**********************************************/
	$objAdmin	= new GeneralAdmin();$objTest	= new Testimonials();
		
	/*****************************		Database Variables Declartion	***********************************************/
	$tablename="tbl_testimonials";$UniqId="TestimonialsId";$ResVal="TestMonList";$m_sta="TestimonialsStatus";$PageVal="TesmPage";
	
	/*****************************		Page Variables Declartion	***********************************************/
	$pro_msg=(isset($_GET['pro_msg'])) ? $_GET['pro_msg']:'';$RedURL='testimonials_mgmt.php?';
	$act_type=(isset($_GET['act_type'])) ? $_GET['act_type']:'';$mgtact=(isset($_GET['mgtact'])) ? $_GET['mgtact']:'';
	$checkstas=(isset($_GET['checkstas'])) ? $_GET['checkstas']:'';
	
	switch($act_type)
	{
		case 'addnew':
				/*****************************		Admin & Subadmin Login Permisson Check 	*************************/
 				$con_id=(isset($_GET['con_id'])) ? $_GET['con_id']:'';
				if($con_id!=''){					
					$SelStatus=$objAdmin->SelectSingleTableRowDetails($tablename,$UniqId,'Arr',$con_id);	
					if(!$SelStatus){
						Redirect('testimonials_mgmt.php?pro_msg=vfail');
					}
				  CheckAdminMgmtPermission('Redirct','MainAdmin','Testimonials','ManageTestimonials');
					$objSmarty->assign("SiteTitle",SiteMainTitle." - Testimonials Management - Edit Testimonials Details");
					$objSmarty->assign("PageTitle","Testimonials Management - Edit Testimonials Details");
				}		
				if(!empty($_POST['AdminAction'])){
					$objTest->TestimonialsAddUpdate('testimonials_mgmt','content');
				}
	
			   $SelAds="SELECT * from pj_user_list where UserId!='' and UserSta='1'";
			   $UserList=$objMysqlFns->ExecuteQuery($SelAds,"select"); //echo '<pre>';print_r($FPageList);exit;
			   $objSmarty->assign('UserList',$UserList);
				$objSmarty->assign("TinyMCEPath",$ngconfig['TinyEditor']);
				CheckAdminMgmtPermission('Redirct','MainAdmin','Testimonials','AddTestimonials');
				$objSmarty->assign("SiteTitle",SiteMainTitle." - Testimonials Management - Add Testimonials Details");
				$objSmarty->assign("PageTitle","Testimonials Management - Add Testimonials Details");
				$objSmarty->assign("IncludeTpl", "testimonials_new.html");		
			break;
		default:
				/*****************************		Admin & Subadmin Login Permisson Check 	*************************/
				CheckAdminMgmtPermission('Redirct','MainAdmin','Testimonials','ManageTestimonials');
	
				/******************  		Call Function For Delete,Active Inactive Table Details 	**********************/
				if(isset($_POST['Action_Type'])){ 
					if($_POST['Action_Type']=='Approve' || $_POST['Action_Type']=='UnApprove'){
					$objAdmin->AdminControlMgmt($tablename,$UniqId,'STAppSta',$RedURL);
					}
					else{
					$objAdmin->AdminControlMgmt($tablename,$UniqId,$m_sta,$RedURL);
					}
				} 
				/******************  		Select Admin Posted  Static Page List	**********************/
				$objTest->GetTestimonialsLists($ResVal,$PageVal);
				
				/******************  		Admin Action Error and Success Messages	**********************/
				if(!empty($pro_msg)){
					if($pro_msg=="usucc"){
						$objSmarty->assign("SucMessage", "Testimonials Has Been Updated Successfully!!");}	
					elseif($pro_msg=="asucc"){
						$objSmarty->assign("SucMessage", "New Testimonials Details Has Been Added Successfully!!");
					}
				}	
				if($mgtact!="")	{	
				$ActArray=array("delsuc"=>"Delete","actsuc"=>"Active","inasuc"=>"InActive","actcon"=>"Approve","unappcon"=>"UnApprove");
					if($checkstas!="fals"){
						$objSmarty->assign("SucMessage","Selected Testimonials Has Been ".$ActArray[$mgtact]."d Successfully");
					}
					else{
						$objSmarty->assign("ErrMessage","Selected Testimonials Already in ".$ActArray[$mgtact]." Status");
					}
				}
				$objSmarty->assign("SiteTitle",SiteMainTitle." - Testimonials Management");	//	For Page Main Title
				$objSmarty->assign("PageTitle"," Testimonials Management");	//	For Page Sub Title
				$objSmarty->assign("IncludeTpl", "testimonials_mgmt.html");	//	Assign Page For Smarty	
			break;			
	}
	/*****************************			Include Admin Menu and Ctrl Create File 	**********************************/
	include _MAINSITEPATH_."/wit_admincontrols.php";
	$MCtrlArray=array('ON'=>'ON','OFF'=>'OFF','Delete'=>'Delete',);
	$objSmarty->assign("ControlList",$MCtrlArray);	
		
	#===========================================================================		
	$objSmarty->display("site_admin_main.html");
?>