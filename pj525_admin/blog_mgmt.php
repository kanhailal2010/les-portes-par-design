<?php 	
	/*****************************			Site Config File				***********************************************/
	include "../wit_includes/wit_common.php";
     
	/*****************************			Include  File				***********************************************/
	include_once $ngconfig['SiteClassPath']."class.GeneralAdmin.php";
	include_once $ngconfig['SiteClassPath']."class.PEAR_RESIZE.php";
	include_once $ngconfig['SiteClassPath']."class.Blog.php";
	/*****************************			Class Objects			***********************************************/
	$objAdmin = new GeneralAdmin(); $objBlog = new Blog();
	
	/*****************************			Include  File					***********************************************/
	include _MAINSITEPATH_."/wit_SEOURL_Function.php";

	
	/*****************************		Database Variables Declartion	***********************************************/
	$tablename="pj_blog"; $UniqId="BlogId";$ResVal="ProList"; $m_sta="BlogStatus";$PageVal="UserPage";
	
	/*****************************		Page Variables Declartion	***********************************************/
	$RedURL='blog_mgmt.php?';$act_type=(isset($_GET['act_type'])) ? $_GET['act_type']:'';
	$mgtact=(isset($_GET['mgtact'])) ? $_GET['mgtact']:'';$con_id=(isset($_GET['con_id'])) ? $_GET['con_id']:'';
	$checkstas=(isset($_GET['checkstas'])) ? $_GET['checkstas']:'';
	$pro_msg=(isset($_GET['pro_msg'])) ? $_GET['pro_msg']:'';$SelStatus=true;
	switch($act_type)
	{
		case 'comment':
					/******************  		Call Function For Delete,Active Inactive Table Details 	**********************/
				CheckAdminMgmtPermission('Redirct','BothAdmin','Blog','ManageBlog');
				$con_id=(isset($_GET['con_id'])) ? $_GET['con_id']:'';
				if(!empty($con_id)){
					$objBlog->SelectCommentLists($con_id); 
				}
				$objSmarty->assign("IncludeTpl", "blog_comment.html");
			break;
	 
		case 'addnew': 
				if($con_id!='') {
 					$SelStatus=$objAdmin->SelectSingleTableRowDetails($tablename,$UniqId,'Arr',$con_id);
 					 
					if(!$SelStatus){
						Redirect('blog_mgmt.php?pro_msg=vfail');
					}
					CheckAdminMgmtPermission('Redirct','BothAdmin','Blog','ManageBlog');
					$objSmarty->assign("SiteTitle",SiteMainTitle." -  Blog Management - Edit Blog Details");
					$objSmarty->assign("PageTitle","Blog Management - Edit Blog Details");
				}		
				else{
					CheckAdminMgmtPermission('Redirct','BothAdmin','Blog','AddBlog');
					$objSmarty->assign("SiteTitle",SiteMainTitle." -  Blog Management - Add Blog Details");
					$objSmarty->assign("PageTitle","Blog Management - Add Blog Details");
				}
 				if(!empty($_POST['AdminAction'])){
					$objBlog->AddUpdate();
				}
  				$objSmarty->assign("TinyMCEPath",$ngconfig['TinyEditor']);
				$objSmarty->assign("IncludeTpl", "blog_new.html");
			break;  
 		default:
			/******************  		Call Function For Delete,Active Inactive Table Details 	**********************/
			CheckAdminMgmtPermission('Redirct','BothAdmin','Blog','ManageBlog');
				
			if(isset($_POST['Action_Type'])){
				if($_POST['Action_Type']=='Set Us Featured' || $_POST['Action_Type']=='Unset Featured'){
					$objAdmin->AdminControlMgmt($tablename,$UniqId,'ProFeatureSta',$RedURL);
				}
				else{
	 				$objAdmin->AdminControlMgmt($tablename,$UniqId,$m_sta,$RedURL);
				}	
			}
			/******************  		Admin Action Error and Success Messages	*********************Oakwood197!*/
				if($pro_msg!=''){
					if($pro_msg=="usucc"){
						$objSmarty->assign("SucMessage", " Blog Details Has Been Updated Successfully!!");
					}else{
						$objSmarty->assign("SucMessage", "New  Blog Details Has Been Added Successfully!!");
					}
				}
			if($mgtact!=""){	
				$ActArray=array("delsuc"=>"Deleted","actsuc"=>"Actived","inasuc"=>"InActived");
				if($checkstas=="sucs"){
				   $objSmarty->assign("SucMessage","Selected  Blog(s) Details Has Been ".$ActArray[$mgtact]." Successfully");
				}
				else{
					$objSmarty->assign("ErrMessage","Selected  Blog(s) Already in ".$ActArray[$mgtact]." Status");
				}
			}
		
			$objBlog->GetBlogLists('ProList',"BlogPage");
			
			$objSmarty->assign("SiteTitle",SiteMainTitle." Blog Management");
			$objSmarty->assign("PageTitle"," Blog Management");
			$objSmarty->assign("IncludeTpl", "blog_mgmt.html");	
		break;
	}	
 	
     /*****************************			Include Admin Menu and Ctrl Create File 	**********************************/
	include _MAINSITEPATH_."/wit_admincontrols.php";
	
	/*****************************			Include Admin Menu and Ctrl Create File 	**********************************/
	if($act_type=='managefile' || $act_type=='manageimage'){
		$MCtrlArray=array('Delete'=>'Delete');
	}
	else{
		$MCtrlArray=array('ON'=>'ON','OFF'=>'OFF','Delete'=>'Delete');
	}
	$objSmarty->assign("ControlList",$MCtrlArray);
 	$objSmarty->assign("StoreImageDisp",$ngconfig['StoreImageDisp'] );
	#===========================================================================		
	$objSmarty->display("site_admin_main.html");
?>