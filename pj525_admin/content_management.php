<?php 	
	/*****************************			Site Config File				***********************************************/
	include "../wit_includes/wit_common.php";
		
	/*****************************			Include  File					***********************************************/
	include _MAINSITEPATH_."/wit_SEOURL_Function.php";
	
	/*****************************			Include Supported Class Files	********************************************/
	include_once $ngconfig['SiteClassPath']."class.GeneralAdmin.php";
	include_once $ngconfig['SiteClassPath']."class.Content.php";
	
	/*****************************			Class Objects			*		**********************************************/
	$objAdmin	= new GeneralAdmin();$objContent	= new Content();
		
	/*****************************		Database Variables Declartion	***********************************************/
	$tablename=$objLang->tableName("pj_content_pages",true);$UniqId="page_id";$ResVal="ContList";$m_sta="page_status";$PageVal="ContentPage";
	
	/*****************************		Page Variables Declartion	***********************************************/
	$pro_msg=(isset($_GET['pro_msg'])) ? $_GET['pro_msg']:'';$RedURL='content_management.php?';
	$act_type=(isset($_GET['act_type'])) ? $_GET['act_type']:'';$mgtact=(isset($_GET['mgtact'])) ? $_GET['mgtact']:'';
	$checkstas=(isset($_GET['checkstas'])) ? $_GET['checkstas']:'';
	
	switch($act_type)
	{
		case 'addnew':
				/*****************************		Admin & Subadmin Login Permisson Check 	*************************/
			
	
				$con_id=(isset($_GET['con_id'])) ? $_GET['con_id']:'';
				if($con_id!=''){					
					$SelStatus=$objAdmin->SelectSingleTableRowDetails($tablename,$UniqId,'Arr',$con_id);	
					if(!$SelStatus){
						Redirect('content_management.php?pro_msg=vfail');
					}
				  CheckAdminMgmtPermission('Redirct','MainAdmin','Content','ManageContent');
					$objSmarty->assign("SiteTitle",SiteMainTitle." - Content Management - Edit Content Details");
					$objSmarty->assign("PageTitle","Content Management - Edit Content Details");
				}		
				else{
					CheckAdminMgmtPermission('Redirct','MainAdmin','Content','AddContent');
					$objSmarty->assign("SiteTitle",SiteMainTitle." -  Content Management - Add Content Details");
					$objSmarty->assign("PageTitle","Content Management - Add Content Details");				
				}	
				if(!empty($_POST['AdminAction'])){
					$objContent->ContentAddUpdate('content_management','content');
				}
				
				$objSmarty->assign("TinyMCEPath",$ngconfig['TinyEditor']);
				$objSmarty->assign("RedirFileName", "content_management");
				$objSmarty->assign("PageButName", "Content");
				$objSmarty->assign("IncludeTpl", "content_new.html");		
			break;
		default:
				/*****************************		Admin & Subadmin Login Permisson Check 	*************************/
				CheckAdminMgmtPermission('Redirct','MainAdmin','Content','ManageContent');
	
				/******************  		Call Function For Delete,Active Inactive Table Details 	**********************/
				if(isset($_POST['Action_Type'])){
					$objAdmin->AdminControlMgmt($tablename,$UniqId,$m_sta,$RedURL);
				}
				/******************  		Select Admin Posted  Static Page List	**********************/
				$objContent->GetContentLists('content',$ResVal,$PageVal);
				
				/******************  		Admin Action Error and Success Messages	**********************/
				if(!empty($pro_msg)){
					if($pro_msg=="asucc")
						$objSmarty->assign("SucMessage", "New Content Details Has Been Added Successfully!!");
					elseif($pro_msg=="usucc")
						$objSmarty->assign("SucMessage", "Content Details Has Been Updated Successfully!!");	
				}	
				if($mgtact!="")	{	
					$ActArray=array("delsuc"=>"Delete","actsuc"=>"Active","inasuc"=>"InActive");
					if($checkstas!="fals"){
						$objSmarty->assign("SucMessage","Selected Content Has Been ".$ActArray[$mgtact]."d Successfully");
					}
					else{
						$objSmarty->assign("ErrMessage","Selected Content Already in ".$ActArray[$mgtact]." Status");
					}
				}
				$objSmarty->assign("SiteTitle",SiteMainTitle." - Content Management");	//	For Page Main Title
				$objSmarty->assign("PageTitle"," Content Management");	//	For Page Sub Title
				$objSmarty->assign("IncludeTpl", "content_management.html");	//	Assign Page For Smarty	
			break;			
	}
	/*****************************			Include Admin Menu and Ctrl Create File 	**********************************/
	include _MAINSITEPATH_."/wit_admincontrols.php";
	$MCtrlArray=array('ON'=>'ON','OFF'=>'OFF','Delete'=>'Delete');
	$objSmarty->assign("ControlList",$MCtrlArray);	
		
	#===========================================================================		
	$objSmarty->display("site_admin_main.html");
?>