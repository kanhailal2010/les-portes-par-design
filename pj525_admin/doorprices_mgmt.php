<?php 	
	/*****************************			Site Config File				***********************************************/
	include "../wit_includes/wit_common.php";
     
	/*****************************			Include  File				***********************************************/
	include_once $ngconfig['SiteClassPath']."class.GeneralAdmin.php";
	include_once $ngconfig['SiteClassPath']."class.PEAR_RESIZE.php";
	include_once $ngconfig['SiteClassPath']."class.Quotes.php";
	/*****************************			Class Objects			***********************************************/
	$objAdmin = new GeneralAdmin(); $objPro = new Quotes();
	
	/*****************************			Include  File					***********************************************/
	include _MAINSITEPATH_."/wit_SEOURL_Function.php";

 	/*****************************		Database Variables Declartion	***********************************************/
	$tablename="pj_door_prices"; $UniqId="DPriceId";$ResVal="ProList"; $m_sta="PStatus";$PageVal="UserPage";
	
	/*****************************		Page Variables Declartion	***********************************************/
	$RedURL='doorprices_mgmt.php?';$act_type=(isset($_GET['act_type'])) ? $_GET['act_type']:'';
	$mgtact=(isset($_GET['mgtact'])) ? $_GET['mgtact']:'';$con_id=(isset($_GET['con_id'])) ? $_GET['con_id']:'';
	$checkstas=(isset($_GET['checkstas'])) ? $_GET['checkstas']:'';
	$pro_msg=(isset($_GET['pro_msg'])) ? $_GET['pro_msg']:'';$SelStatus=true;
	switch($act_type)
	{
		case 'addnew': 
				if($con_id!='') {
 					$SelStatus=$objAdmin->SelectSingleTableRowDetails($tablename,$UniqId,'ArrD',$con_id); 
					if(!$SelStatus){
						Redirect('doorprices_mgmt.php?pro_msg=vfail');
					}
					CheckAdminMgmtPermission('Redirct','BothAdmin','Quote Generator','ManageDPrices');
					$objSmarty->assign("SiteTitle",SiteMainTitle." -  Door Price Management - Edit Door Price Details");
					$objSmarty->assign("PageTitle","Door Price Management - Edit Door Price Details");
				}		
				else{
					CheckAdminMgmtPermission('Redirct','BothAdmin','Quote Generator','AddDPrices');
					$objSmarty->assign("SiteTitle",SiteMainTitle." -  Door Price Management - Add DoorPrice Details");
					$objSmarty->assign("PageTitle","Door Price Management - Add Door Price Details");
				}
 				if(!empty($_POST['AdminAction'])){
					$objPro->AddUpdateDoorPriceDetails($con_id,admin);
				}
				$objPro->SelectFinishTypesForAdmin();
				$objPro->SelectProfileTypesForAdmin();	
  				$objSmarty->assign("TinyMCEPath",$ngconfig['TinyEditor']);
				$objSmarty->assign("IncludeTpl", "doorprices_new.html");
			break;  
 		default:
			/******************  		Call Function For Delete,Active Inactive Table Details 	**********************/
			CheckAdminMgmtPermission('Redirct','BothAdmin','Quote Generator','ManageDPrices');
				
			if(isset($_POST['Action_Type'])){
				if($_POST['Action_Type']=='Set Us Featured' || $_POST['Action_Type']=='Unset Featured'){
					$objAdmin->AdminControlMgmt($tablename,$UniqId,'ProFeatureSta',$RedURL);
				}
				else{
	 				$objAdmin->AdminControlMgmt($tablename,$UniqId,$m_sta,$RedURL);
				}	
			}
			/******************  		Admin Action Error and Success Messages	*********************Oakwood197!*/
				if($pro_msg!=''){
					if($pro_msg=="usucc"){
						$objSmarty->assign("SucMessage", " Door Price Details Has Been Updated Successfully!!");
					}else{
						$objSmarty->assign("SucMessage", "New  Door Price Details Has Been Added Successfully!!");
					}
				}
			if($mgtact!=""){	
				$ActArray=array("delsuc"=>"Deleted","actsuc"=>"Actived","inasuc"=>"InActived");
				if($checkstas=="sucs"){
				   $objSmarty->assign("SucMessage","Selected  Door Price(s) Details Has Been ".$ActArray[$mgtact]." Successfully");
				}
				else{
					$objSmarty->assign("ErrMessage","Selected  Door Price(s) Already in ".$ActArray[$mgtact]." Status");
				}
			}
		
			$objPro->GetDoorPriceLists('ProList',"DoorPricePage");
			
			$objSmarty->assign("SiteTitle",SiteMainTitle." Door Price Management");
			$objSmarty->assign("PageTitle"," Door Price Management");
			$objSmarty->assign("IncludeTpl", "doorprices_mgmt.html");	
		break;
	}	
 	
     /*****************************			Include Admin Menu and Ctrl Create File 	**********************************/
	include _MAINSITEPATH_."/wit_admincontrols.php";
	
	/*****************************			Include Admin Menu and Ctrl Create File 	**********************************/
	if($act_type=='managefile' || $act_type=='manageimage'){
		$MCtrlArray=array('Delete'=>'Delete');
	}
	else{
		$MCtrlArray=array('Delete'=>'Delete');
	}
	$objSmarty->assign("ControlList",$MCtrlArray); 
	#===========================================================================		
	$objSmarty->display("site_admin_main.html");
?>