<?php 	
	/*****************************			Site Config File				***********************************************/
	include "../wit_includes/wit_common.php";
	
	/*****************************			Include  Supported Class File	***********************************************/
	include_once $ngconfig['SiteClassPath']."class.Payment.php";
	include_once $ngconfig['SiteClassPath']."class.GeneralAdmin.php";
	
	/*****************************			Class Objects			***********************************************/
	$objAdmin	= new GeneralAdmin();$objCamp= new Payment();	

	/*****************************		Database Variables Declartion	***********************************************/
	$tablename="pj_item_payments";$UniqId="PayId";$ResVal="PayList";$m_sta="Paystatus";$PageVal="PayPage";
	
	/*****************************		Page Variables Declartion	***********************************************/
	$pro_msg=(isset($_GET['pro_msg'])) ? $_GET['pro_msg']:''; $seanames=(isset($_GET['seanames'])) ? $_GET['seanames']:'';
	$RedURL='order_list_home.php?'; $act_type=(isset($_GET['act_type'])) ? $_GET['act_type']:'';
	$mgtact=(isset($_GET['mgtact'])) ? $_GET['mgtact']:'';
	switch($act_type)
	{   
		case 'send':
					/******************  		Call Function For Delete,Active Inactive Table Details 	**********************/
				CheckAdminMgmtPermission('Redirct','BothAdmin','Payment','ManageProductPayment');
				$con_id=(isset($_GET['con_id'])) ? $_GET['con_id']:'';$pay_id=(isset($_GET['pay_id'])) ? $_GET['pay_id']:'';
				if(!empty($con_id)){
					$objCamp->SendOrdertoBarazin($con_id,$pay_id); 
				} 
			break;  
		case 'orders':
					/******************  		Call Function For Delete,Active Inactive Table Details 	**********************/
				CheckAdminMgmtPermission('Redirct','BothAdmin','Payment','ManageProductPayment');
				$con_id=(isset($_GET['con_id'])) ? $_GET['con_id']:'';
				if(!empty($con_id)){
					$objCamp->SelectProductOrderLists($ResVal,$PageVal,$con_id); 
				}
				$objSmarty->assign("IncludeTpl", "sell_order_list.html");
			break;
	 
		default:	
					/******************  		Call Function For Delete,Active Inactive Table Details 	**********************/
			CheckAdminMgmtPermission('Redirct','BothAdmin','Orders','ManageProductPayment');
			if(isset($_POST['Action_Type'])){
 				$objAdmin->AdminControlMgmt($tablename,$UniqId,$m_sta,$RedURL);
			}
			/******************  			Admin Management Active,Delete etc Control Arrays List	 		**********************/
			if($pro_msg=="ssucc"){ 
				$objSmarty->assign("SucMessage","Quote Details Has Been sent to BARAZIN Successfully");
			}
			/******************  			Admin Management Active,Delete etc Control Arrays List	 		**********************/
			if($mgtact!=""){	
				$ActArray=array('InProgress'=>'InProgress','Completed'=>'Completed','Paid'=>'Paid','Shipped'=>'Shipped','Delivered'=>'Delivered',"delsuc"=>"Deleted","actsuc"=>"Active","inasuc"=>"Inactive");
				$objSmarty->assign("SucMessage","Selected Payment(s) Details Has Been ".$ActArray[$mgtact]." Successfully");
			}
			$objCamp->GetUserPurchasedLists($ResVal,$PageVal);
			$objSmarty->assign("PageTitle","Orders / Payment");
			$objSmarty->assign("SiteTitle",SiteMainTitle." - Orders / Payment ");	//	For Page Main Title
			$objSmarty->assign("IncludeTpl", "sell_payment_mgmt.html");
		
		break;
	}
	/*****************************			Include Admin Menu and Ctrl Create File 	**********************************/

	include _MAINSITEPATH_."/wit_admincontrols.php";
	$MCtrlArray=array('InProgress'=>'InProgress','Completed'=>'Completed','Paid'=>'Paid','Shipped'=>'Shipped','Delivered'=>'Delivered','Delete'=>'Delete');
	$objSmarty->assign("ControlList",$MCtrlArray);
 	$objSmarty->assign("FinishImageDisp",$ngconfig['FinishImageDisp'] );
 	$objSmarty->assign("ShapeImageDisp",$ngconfig['ShapeImageDisp'] );
 	$objSmarty->assign("GlassImageDisp",$ngconfig['GlassImageDisp'] );
	#===========================================================================		
	$objSmarty->display("site_admin_main.html");
?>



