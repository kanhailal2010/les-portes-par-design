<?php 	
	/*****************************			Site Config File				***********************************************/
	include "../wit_includes/wit_common.php";
		
	/*****************************			Include  File					***********************************************/
	include _MAINSITEPATH_."/wit_SEOURL_Function.php";
	
	/*****************************			Include Supported Class Files	********************************************/
	include_once $ngconfig['SiteClassPath']."class.GeneralAdmin.php";
	include_once $ngconfig['SiteClassPath']."class.Category.php";
	
	/*****************************			Class Objects			*		**********************************************/
	$objAdmin	= new GeneralAdmin();$objCat	= new Category();

	/*************************** Declare Page Variables For Admin Control Functions	**************************************/
	$tablename="pj_category_main";$UniqId="CatId";$ResVal="CatList";$m_sta="CatStatus";$PageVal="PageNavigation";
	
	 $MgmtArr=array("CatMgmt"=>array("$tablename","$UniqId","$m_sta"));

	/***************************	**************************************/		
	$pro_msg=(isset($_GET['pro_msg'])) ? $_GET['pro_msg']:'';$act_type=(isset($_GET['act_type'])) ? $_GET['act_type']:'';
	$mgtact=(isset($_GET['mgtact'])) ? $_GET['mgtact']:'';$con_id=(isset($_GET['con_id'])) ? $_GET['con_id']:'';
	$RedURL='category_mgmt.php?';$checkstas=(isset($_GET['checkstas'])) ? $_GET['checkstas']:'';
	
	switch($act_type)
	{
  		case 'addnew':	
 				if($con_id!=''){ 	 
						CheckAdminMgmtPermission('Redirct','MainAdmin','Category','ManageCategory');
						
 					$SelStatus=$objAdmin->SelectSingleTableRowDetails($tablename,$UniqId,'Arr',$con_id);	
					
					if(!$SelStatus) { Redirect('category_mgmt.php?pro_msg=vfail');  }
					
 					$objSmarty->assign("SiteTitle",SiteMainTitle." -  Category Management - Edit Category Details");
					$objSmarty->assign("PageTitle","Category Management - Edit Category Details");
				}		
				else{  
					CheckAdminMgmtPermission('Redirct','MainAdmin','Category','AddCategory');
						
					$objSmarty->assign("SiteTitle",SiteMainTitle." -  Category Management - Add New Category Details");
					$objSmarty->assign("PageTitle","Category Management - Add New Category Details");				
				}	
 				if(!empty($_POST['AdminAction']))	
					$objCat->CategoryAddUpdate($AdminAction,$con_id);   
				$objSmarty->assign("IncludeTpl", "category_new.html");		
			break;
		default:
				CheckAdminMgmtPermission('Redirct','MainAdmin','Category','ManageCategory');
				if(isset($_POST['Action_Type'])){ 
					$objAdmin->AdminControlMgmt($tablename,$UniqId,$m_sta,$RedURL);
				}
				$objCat->GetServiceCatList($ResVal,$PageVal);	
				
				/******************  		Select Admin Control List	**********************/
				$MCtrlArray=array('Active'=>'Active','InActive'=>'InActive');
				$objSmarty->assign("ControlList",$MCtrlArray);
				
				/***********	Error and Success Messages For Administrative Actions  *********************************/
				if($mgtact!="") {	
					$ActArray=array("delsuc"=>"Deleted");
					if($checkstas=="sucs"){
						$objSmarty->assign("SucMessage","Selected Category(s) Has Been ".$ActArray[$mgtact]." Successfully");
					}
					/*else{
						$objSmarty->assign("ErrMessage","Selected Category(s) Already  In ".$ActArray[$mgtact]." Status");
					}*/
				}	
				if($pro_msg!="") {	
					if ($pro_msg=="asucc")
						$objSmarty->assign("SucMessage", "New Category Details Has Been Added Successfully!!");
					elseif($pro_msg=="usucc")
						$objSmarty->assign("SucMessage","Category Details Has Been Updated Successfully!!");	
				}	
				$objSmarty->assign("SiteTitle",SiteMainTitle." - Category Management"); //	Page Main Title
				$objSmarty->assign("PageTitle"," Category Management "); //	Page Sub Title
				$objSmarty->assign("IncludeTpl", "category_mgmt.html"); // Assign Page For Smarty	
			break;			
	}
	/*****************************			Include Admin Menu and Ctrl Create File 	**********************************/
	include _MAINSITEPATH_."/wit_admincontrols.php";
	$MCtrlArray=array('ON'=>'ON','OFF'=>'OFF','Delete'=>'Delete');
	$objSmarty->assign("ControlList",$MCtrlArray);	
		
	#===========================================================================		
	$objSmarty->display("site_admin_main.html");
?>