<?php 
	/*****************************			Site Config File				***********************************************/
	include "../wit_includes/wit_common.php";
	
	include_once $ngconfig['SiteClassPath']."class.GeneralAdmin.php";

   	/*****************************		Page Variables Declartion	***********************************************/
	$MgmtArr=array(
 		"AdminMgmt"=>array('tablename'=>"pj_siteadmin",'UniqId'=>"AdminIdent",'m_sta'=>"AdminStatus"),
		"GalleryMgmt"=>array('tablename'=>"pj_galleryimage_list",'UniqId'=>"ImageId",'m_sta'=>"ImageStatus"),
		"CateMgmt"=>array('tablename'=>"pj_category_main",'UniqId'=>"CatId",'m_sta'=>"CatStatus"),
		"ProductMgmt"=>array('tablename'=>"pj_sell_list",'UniqId'=>"SellId",'m_sta'=>"ItemStatus"), 
		"GlassMgmt"=>array('tablename'=>"pj_glass_list",'UniqId'=>"GlassId",'m_sta'=>"GlassStatus"),  
		"ProfileTypeMgmt"=>array('tablename'=>"pj_profile_type",'UniqId'=>"PTypeId",'m_sta'=>"PStatus"),
		"FinishTypeMgmt"=>array('tablename'=>"pj_finish_type",'UniqId'=>"FinishId",'m_sta'=>"FStatus"),  
		"ContentMgmt"=>array('tablename'=>"pj_content_pages",'UniqId'=>"page_id",'m_sta'=>"page_status"), 
		"BannerMgmt"=>array('tablename'=>"pj_banners_list",'UniqId'=>"BannerID",'m_sta'=>"BannerStatus"), 
		"BuyersMgmt"=>array('tablename'=>"pj_user_list",'UniqId'=>"UserId",'m_sta'=>"UserSta"), 
		"TestimonialsMgmt"=>array('tablename'=>"tbl_testimonials",'UniqId'=>"TestimonialsId",'m_sta'=>"TestimonialsStatus"), 
		"CountryMgmt"=>array('tablename'=>"pj_location_country",'UniqId'=>"country_id",'m_sta'=>"country_status"),  
		"QuotesMgmt"=>array('tablename'=>"pj_quotes",'UniqId'=>"QuoteId",'m_sta'=>"QStatus"), 
		"StateMgmt"=>array('tablename'=>"pj_location_state",'UniqId'=>"state_id",'m_sta'=>"StateSta"), 
		"BlogMgmt"=>array('tablename'=>"pj_blog",'UniqId'=>"BlogId",'m_sta'=>"BlogStatus"),
		"PackageMgmt"=>array('tablename'=>"pj_packages",'UniqId'=>"PackId",'m_sta'=>"PackSta"), 
 	); 
	
	// For updating tables havning other language
	$tableLang = array("GalleryMgmt","CateMgmt","ProductMgmt","ContentMgmt");
	
	global $objLang;
	$MgmtName=(isset($_POST['MgmtName'])) ? $_POST['MgmtName']:''; 
	$Seltype=(isset($_POST['Seltype'])) ? $_POST['Seltype']:'';$SelIds=(isset($_POST['SelIds'])) ? $_POST['SelIds']:'';
	$UserIds=$SelIds;$Action=(isset($_POST['Action']))?$_POST['Action']:'';	
	 
	if(!empty($Seltype) &&  !empty($Action)){
		/*******************************	Select Table Details for Admin status Change	******************************/
		$TableName=$MgmtArr[$Seltype]['tablename'];
		$Uid=$MgmtArr[$Seltype]['UniqId'];$status =$MgmtArr[$Seltype]['m_sta'];
		
		if(!empty($UserIds) && !empty($TableName) && !empty($Uid) && !empty($status)){
				$ActionCheck=true;
				$MgmtType=(isset($_SESSION['PJ525A_Mgmt']))?$_SESSION['PJ525A_Mgmt']:'';
				$PJ525A_Type=(isset($_SESSION['PJ525A_Type']))?$_SESSION['PJ525A_Type']:'';
				if($PJ525A_Type=='SubAdmin'){
					$SelAdm="Select * from pj_subadmin_list Where SubAdmId = '".$_SESSION['PJ525A_Ident']."' and 
						FIND_IN_SET('".$MgmtType."',SSelMgmts) and FIND_IN_SET('".$MgmtType.'@'.$Action."',SMgmtCtrls) ";
				$AdmDet	= $objMysqlFns->ExecuteQuery($SelAdm, "select"); 				
				if(sizeof($AdmDet)==0){
					$ActionCheck=false;
				}
			}
			if(!empty($Action)){ 
				if($Action=='ON'){	
					$Action='1';
				}
				else{
					$Action='0';
				}
				if(in_array($Seltype,$tableLang))
				{
					foreach($objLang->language_list() as $lang=>$langTitle)
					{
						$AltQry = "Update ".$objLang->tableName($TableName,$lang)." Set $status='".$Action."' Where (".$Uid.") IN (".$UserIds.")" ; 
						$AltChcek=GeneralAdmin::ExecuteQuery($AltQry, "update");
					}
				}
				$AltQry = "Update $TableName Set $status='".$Action."' Where (".$Uid.") IN (".$UserIds.")" ; 
				$AltChcek=GeneralAdmin::ExecuteQuery($AltQry, "update");
				if($Action=='1'){
					$Action='ON';
				}
				else{	
					$Action='OFF';
				}
				if(!empty($AltChcek)){
					echo '{"ajax_res":"true","status_check":"true","ajax_msg":"Selected '.$MgmtName.' Status Has Been changed to '.$Action.'"}';	
				}
				else{
					echo '{"ajax_res":"true","status_check":"false","ajax_msg":"No Changes in Selected '.$MgmtName.' Status"}';	
				}	
 			}	
		}
		else{
			echo '{"ajax_res":"false","ajax_msg":"Please Select valid action"}';return false;
		}
	 }
	 else{	echo '{"ajax_res":"false","ajax_msg":"Please Select valid action"}';	}
?> 