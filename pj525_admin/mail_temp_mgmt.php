<?php 	
	/*****************************			Site Config File				***********************************************/
	include "../wit_includes/wit_common.php";
		
	/*****************************			Include  File					***********************************************/
	include _MAINSITEPATH_."/wit_SEOURL_Function.php";
	
	/*****************************			Include Supported Class Files	********************************************/
	include_once $ngconfig['SiteClassPath']."class.GeneralAdmin.php";
	include_once $ngconfig['SiteClassPath']."class.SiteMailTemplate.php";
	
	/*****************************			Class Objects			*		**********************************************/
	$objAdmin	= new GeneralAdmin();$objSMail	= new SiteMailTemplate();
		
	/*****************************			Admin & Subadmin Login Permisson Check 	**********************************/
	CheckAdminLoginSession('Redirct','BothAdmin','MailTemplate');
	
 	/*****************************		Table Variables Declartion	***********************************************/
	$tablename="pj_mail_template";$m_id="MTempId";$ResVal="MTempList";$m_sta="MailNotify";$PageVal="PayRPage";
		
	/*****************************		Page Variables Declartion	***********************************************/
	$RedURL='mail_temp_mgmt.php?';$mgtact=(isset($_GET['mgtact'])) ? $_GET['mgtact']:'';
	$act_type=(isset($_GET['act_type'])) ? $_GET['act_type']:'';
				
	switch($act_type)
	{
		case 'addnew':
				$con_id=(isset($_GET['con_id'])) ? $_GET['con_id']:'';
				if($con_id!=''){	
					$SelStatus=$objAdmin->SelectSingleTableRowDetails($tablename,$m_id,$ResVal,$con_id);	
						
					if(!$SelStatus)
					{
						Redirect('mail_temp_mgmt.php?pro_msg=vfail');
					}
				}
				
				if(!empty($_POST['AdminAction'])){
					$objSMail->InsertUpdateMailTemplateDetails($con_id);
				}
	
				$objSmarty->assign("PageTitle","Manage Mail Template  - Mail Template Details");
	
				/*****************************		Admin & Subadmin Login Permisson Check 	*************************/
				CheckAdminMgmtPermission('Redirct','BothAdmin','Other','ManageMailTemplate');
				
				

				$objSmarty->assign("SiteTitle",SiteMainTitle." Edit Mail Template");			
				$objSmarty->assign("PageTitle","Edit Mail Template");	
				$objSmarty->assign("TinyMCEPath",$ngconfig['TinyEditor']);	
				$objSmarty->assign("IncludeTpl", "mail_temp_new.html");			
			break;
		default:
						/*****************************		Admin & Subadmin Login Permisson Check 	*************************/
				CheckAdminMgmtPermission('Redirct','BothAdmin','Other','ManageMailTemplate');
				if($mgtact=="usucc")
					$objSmarty->assign("SucMessage", "Mail Template Has Been Updated Successfully!!");
				$objSMail->SelectSiteMailTemplateLists($ResVal,$PageVal);
				
				$objSmarty->assign("SiteTitle",SiteMainTitle." - Manage Mail Template");			
				$objSmarty->assign("PageTitle","Manage Mail Template");
				$objSmarty->assign("IncludeTpl", "mail_temp_mgmt.html");
			break;			
	}	

 	
	/*****************************			Include Admin Menu and Ctrl Create File 	**********************************/
	include _MAINSITEPATH_."/wit_admincontrols.php";
	
	#===========================================================================		
	$objSmarty->display("site_admin_main.html");
?>

