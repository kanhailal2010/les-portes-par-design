<?php 	
	/*****************************			Site Config File			***********************************************/
	include "../wit_includes/wit_common.php";
	
	/*****************************			Include  File				***********************************************/
	include_once $ngconfig['SiteClassPath']."class.GeneralAdmin.php";
	include_once $ngconfig['SiteClassPath']."class.Admin.php";	
	include_once $ngconfig['SiteClassPath']."class.PEAR_RESIZE.php";
	/*****************************			Class Objects			***********************************************/
	$objGen	= new GeneralAdmin();$objAdmin			= new Admin();

	CheckAdminMgmtPermission('Redirct','BothAdmin','','');
	/*****************************		Page Variables Declartion	***********************************************/
	
	$pro_msg=(isset($_GET['pro_msg'])) ? $_GET['pro_msg']:'';
	$act_type=(isset($_GET['act_type'])) ? $_GET['act_type']:'';
				
	switch($act_type)
	{
		case 'username':
				if(isset($_POST['Submit']))
				{	
					$objAdmin->AdminProfileChangeSettings($act_type);
				}
				if($pro_msg=="succ")
					$objSmarty->assign("SucMessage", "Login Username has been updated successfully");	
					
				$objSmarty->assign("SiteTitle",SiteMainTitle." - Change Login Username");	
				$objSmarty->assign("PageTitle","Change Login Username");						
 				$objSmarty->assign("USettings", "style='color:#990d0d;'");	//	For SubMenu Select
			break;
		case 'video':
			if(isset($_POST['Submit']))
			{
				$objAdmin->AdminProfileChangeSettings($act_type);
			}
			$objAdmin->SelectAdminSettings($act_type);
			if($pro_msg=="succ")
				$objSmarty->assign("SucMessage", "Video Details has been updated successfully");	
				
			$objSmarty->assign("SiteTitle",SiteMainTitle." - Home Page Video");
			$objSmarty->assign("TinyMCEPath",$ngconfig['TinyEditor']);	
			$objSmarty->assign("PageTitle","Home Page Video");
			$objSmarty->assign("HomeVideo", "style='color:#990d0d;'");	//	For SubMenu Select
			
			break;
		case 'password':
			if(isset($_POST['Submit']))
			{
				$objAdmin->AdminProfileChangeSettings($act_type);
			}
			
			if($pro_msg=="succ")
				$objSmarty->assign("SucMessage", "Login Password Details has been updated successfully");	
			$objSmarty->assign("PSettings", "style='color:#990d0d;'");		
			$objSmarty->assign("SiteTitle",SiteMainTitle." - Change Login Password");	
			$objSmarty->assign("PageTitle","Change Login Password");
			$objSmarty->assign("PassSettings", "class='subcurrent'");	//	For SubMenu Select
			
			break;
		case 'profile':
			if(isset($_POST['Submit']))
			{
				$objAdmin->AdminProfileChangeSettings($act_type);
			}
			
			$objAdmin->SelectAdminSettings($act_type);
			
			if($pro_msg=="succ")
				$objSmarty->assign("SucMessage", "Profile Settings has been updated successfully");	
			$objSmarty->assign("LSettings", "style='color:#990d0d;'");		
			$objSmarty->assign("SiteTitle",SiteMainTitle." - Admin Profile Settings");		
			$objSmarty->assign("PageTitle","Admin Profile Settings");						
			$objSmarty->assign("ProfileSettings", "class='subcurrent'");	//	For SubMenu Select
				
			break;
		case 'paypalset':
		
			if(isset($_POST['Submit']))
			{
				$objAdmin->AdminProfileChangeSettings($act_type);
			}
			
			$objAdmin->SelectAdminSettings($act_type);
			
			if($pro_msg=="succ")
			$objSmarty->assign("SucMessage", "Payment Settings Details has been updated successfully");
			$objSmarty->assign("PSSettings", "style='color:#990d0d;'");		
			$objSmarty->assign("SiteTitle",SiteMainTitle." - Change Payment Settings");	
			$objSmarty->assign("PageTitle","Change Payment Settings");			
			$objSmarty->assign("PaypalSettings", "class='subcurrent'");	//	For SubMenu Select
			
			break;
		default:
			if(isset($_POST['Submit']))
			{
				$objAdmin->AdminProfileChangeSettings('settings');
			}
			$objAdmin->SelectAdminSettings($act_type);
			
			if($pro_msg=="succ")
					$objSmarty->assign("SucMessage", "Site Settings has been updated successfully");
			$objSmarty->assign("LSettings", "style='color:#990d0d;'");	
			$objSmarty->assign("SiteTitle",SiteMainTitle." - Change Site Settings");	
			$objSmarty->assign("PageTitle","Change Site Settings");						
			$objSmarty->assign("SiteSettings", "class='subcurrent'");	//	For SubMenu Select
			
			break;			
	}		

	/*****************************			For Header Main Menu Select 			******************************************/		
	$objSmarty->assign("Settings", "color:#00CC33; ");
	/***********************************			Assign DEsign File For Smarty	******************************************/	
	$objSmarty->assign("IncludeTpl", "admin_site_settings.html");
	
	/*****************************			Include Admin Menu and Ctrl Create File 	**********************************/
	include _MAINSITEPATH_."/wit_admincontrols.php";
		
	#===========================================================================		
	$objSmarty->display("site_admin_main.html");
?>