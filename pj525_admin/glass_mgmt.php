<?php 	
	/*****************************			Site Config File				***********************************************/
	include "../wit_includes/wit_common.php";
     
	/*****************************			Include  File				***********************************************/
	include_once $ngconfig['SiteClassPath']."class.GeneralAdmin.php";
	include_once $ngconfig['SiteClassPath']."class.PEAR_RESIZE.php";
	include_once $ngconfig['SiteClassPath']."class.Glass.php";
	/*****************************			Class Objects			***********************************************/
	$objAdmin = new GeneralAdmin(); $objPro = new Glass();
	
	/*****************************			Include  File					***********************************************/
	include _MAINSITEPATH_."/wit_SEOURL_Function.php";

 	/*****************************		Database Variables Declartion	***********************************************/
	$tablename="pj_glass_list"; $UniqId="GlassId";$ResVal="ProList"; $m_sta="GlassStatus";$PageVal="UserPage";
	
	/*****************************		Page Variables Declartion	***********************************************/
	$RedURL='glass_mgmt.php?';$act_type=(isset($_GET['act_type'])) ? $_GET['act_type']:'';
	$mgtact=(isset($_GET['mgtact'])) ? $_GET['mgtact']:'';$con_id=(isset($_GET['con_id'])) ? $_GET['con_id']:'';
	$checkstas=(isset($_GET['checkstas'])) ? $_GET['checkstas']:'';
	$pro_msg=(isset($_GET['pro_msg'])) ? $_GET['pro_msg']:'';$SelStatus=true;
	switch($act_type)
	{
		case 'addnew': 
				if($con_id!='') {
 					$SelStatus=$objAdmin->SelectSingleTableRowDetails($tablename,$UniqId,'Arr',$con_id); 
					if(!$SelStatus){
						Redirect('glass_mgmt.php?pro_msg=vfail');
					}
					CheckAdminMgmtPermission('Redirct','BothAdmin','Glass','ManageGlass');
					$objSmarty->assign("SiteTitle",SiteMainTitle." -  Glass Management - Edit Glass Details");
					$objSmarty->assign("PageTitle","Glass Management - Edit Glass Details");
				}		
				else{
					CheckAdminMgmtPermission('Redirct','BothAdmin','Glass','AddGlass');
					$objSmarty->assign("SiteTitle",SiteMainTitle." -  Glass Management - Add Glass Details");
					$objSmarty->assign("PageTitle","Glass Management - Add Glass Details");
				}
 				if(!empty($_POST['AdminAction'])){
					$objPro->AddUpdateGlassDetails($con_id,admin);
				} 
 				$objSmarty->assign("TinyMCEPath",$ngconfig['TinyEditor']);
				$objSmarty->assign("IncludeTpl", "glass_mgmt_new.html");
			break;  
 		default:
			/******************  		Call Function For Delete,Active Inactive Table Details 	**********************/
			CheckAdminMgmtPermission('Redirct','BothAdmin','Glass','ManageGlass');
				
			if(isset($_POST['Action_Type'])){
				if($_POST['Action_Type']=='Set Us Featured' || $_POST['Action_Type']=='Unset Featured'){
					$objAdmin->AdminControlMgmt($tablename,$UniqId,'ProFeatureSta',$RedURL);
				}
				else{
	 				$objAdmin->AdminControlMgmt($tablename,$UniqId,$m_sta,$RedURL);
				}	
			}
			/******************  		Admin Action Error and Success Messages	*********************Oakwood197!*/
				if($pro_msg!=''){
					if($pro_msg=="usucc"){
						$objSmarty->assign("SucMessage", " Glass Details Has Been Updated Successfully!!");
					}else{
						$objSmarty->assign("SucMessage", "New  Glass Details Has Been Added Successfully!!");
					}
				}
			if($mgtact!=""){	
				$ActArray=array("delsuc"=>"Deleted","actsuc"=>"Actived","inasuc"=>"InActived");
				if($checkstas=="sucs"){
				   $objSmarty->assign("SucMessage","Selected  Glass(s) Details Has Been ".$ActArray[$mgtact]." Successfully");
				}
				else{
					$objSmarty->assign("ErrMessage","Selected  Glass(s) Already in ".$ActArray[$mgtact]." Status");
				}
			}
		
			$objPro->GetGlassLists('ProList',"GlassPage");
			
			$objSmarty->assign("SiteTitle",SiteMainTitle." Glass Management");
			$objSmarty->assign("PageTitle"," Glass Management");
			$objSmarty->assign("IncludeTpl", "glass_mgmt.html");	
		break;
	}	
 	
     /*****************************			Include Admin Menu and Ctrl Create File 	**********************************/
	include _MAINSITEPATH_."/wit_admincontrols.php";
	
	/*****************************			Include Admin Menu and Ctrl Create File 	**********************************/
	if($act_type=='managefile' || $act_type=='manageimage'){
		$MCtrlArray=array('Delete'=>'Delete');
	}
	else{
		$MCtrlArray=array('ON'=>'ON','OFF'=>'OFF','Delete'=>'Delete');
	}
	$objSmarty->assign("ControlList",$MCtrlArray);
 	$objSmarty->assign("GlassImageDisp",$ngconfig['GlassImageDisp'] );
	#===========================================================================		
	$objSmarty->display("site_admin_main.html");
?>