<?php 	
	/*****************************			Site Config File			***********************************************/
	include "../wit_includes/wit_common.php";
		
	/*****************************			Include  File					***********************************************/
	include _MAINSITEPATH_."/wit_SEOURL_Function.php";
	
	/*****************************			Include Supported Class Files	********************************************/
	include_once $ngconfig['SiteClassPath']."class.GeneralAdmin.php";
	include_once $ngconfig['SiteClassPath']."class.Location.php";
	
 	/*****************************			Class Objects			***********************************************/
	$objAdmin	= new GeneralAdmin();$objLocation	= new Location(); 
		
	/*****************************			Admin & Subadmin Login Permisson Check 	**********************************/
	CheckAdminLoginSession('Redirct','BothAdmin','Location');
	
	/*****************************			Include Admin Menu and Ctrl Create File 	**********************************/
	include _MAINSITEPATH_."/wit_admincontrols.php";
 	
	/*****************************		Database Variables Declartion	***********************************************/
	$tablename="pj_location_state";$UniqId="state_id";$ResVal="StateList";$m_sta="StateSta";$PageVal="LocRPage";
 
 	/*****************************		Page Variables Declartion	***********************************************/
	$pro_msg=(isset($_GET['pro_msg'])) ? $_GET['pro_msg']:'';$mgtact=(isset($_GET['mgtact'])) ? $_GET['mgtact']:'';
	$RedURL='state_mgmt.php?';$act_type=(isset($_GET['act_type'])) ? $_GET['act_type']:'';
	
 	switch($act_type)
	{
		default:
				CheckAdminMgmtPermission('Redirct','BothAdmin','Location','ManageState');
				/******************  		Call Function For Delete,Active Inactive Table Details 	**********************/
				if(isset($_POST['Action_Type'])){
					$objAdmin->AdminControlMgmt($tablename,$UniqId,$m_sta,$RedURL);
				}
				
				/******************  		Select Admin Posted  City List	**********************/
				$objLocation->GetStateLists($ResVal,'LocRPage'); 
				
				/******************  		Admin Management Active,Delete etc Control Arrays List	 **********************/
				
				/******************  		Admin Action Error and Success Messages	**********************/
				if($pro_msg!="")
				{
					if($pro_msg=="asucc")
						$objSmarty->assign("SucMessage", "New State Details Has Been Added Successfully!!");
					elseif($pro_msg=="usucc")
						$objSmarty->assign("SucMessage", "State Details Has Been Updated Successfully!!");	
				}	
				if($mgtact!="")
				{	
					$ActArray=array("delsuc"=>"Deleted","actsuc"=>"Actived","inasuc"=>"InActived");
					$objSmarty->assign("SucMessage","Selected State Details Has Been ".$ActArray[$mgtact]." Successfully");
				}
				
				$objSmarty->assign("SiteTitle",SiteMainTitle." - State Management");	//	For Page Main Title
				$objSmarty->assign("PageTitle"," State Management");	//	For Page Sub Title
				$objSmarty->assign("IncludeTpl", "state_mgmt.html");	//	Assign Page For Smarty	
			break;			
	}
	
	/*****************************			Include Admin Menu and Ctrl Create File 	**********************************/
	include _MAINSITEPATH_."/wit_admincontrols.php";

	/******************  	Admin Management Active,Delete etc Control Arrays List	 		**********************/
	$MCtrlArray=array('ON'=>'ON','OFF'=>'OFF','Delete'=>'Delete');
	$objSmarty->assign("ControlList",$MCtrlArray);	
	
	
	#===========================================================================		
	$objSmarty->display("site_admin_main.html");
?>