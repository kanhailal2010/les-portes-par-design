jQuery(function() {
	jQuery(".tbl_navi tbody").tableDnD({
		onDrop: function(table, row) {
			var orders = jQuery.tableDnD.serialize();
			jQuery.post('order_navigation.php', { orders : orders });
		}
	}); 

	jQuery(".tbl_photo tbody").tableDnD({
		onDrop: function(table, row) {
			var orders = jQuery.tableDnD.serialize();
			jQuery.post('order_photos.php', { orders : orders });
		}
	}); 

});