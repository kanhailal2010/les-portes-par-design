<?php 	
	/*****************************			Site Config File				***********************************************/
	include "../wit_includes/wit_common.php";
     
	/*****************************			Include  File				***********************************************/
	include_once $ngconfig['SiteClassPath']."class.GeneralAdmin.php";
	include_once $ngconfig['SiteClassPath']."class.PEAR_RESIZE.php";
	include_once $ngconfig['SiteClassPath']."class.Product.php";
	/*****************************			Class Objects			***********************************************/
	$objAdmin = new GeneralAdmin(); $objPro = new Product();
	
	/*****************************			Include  File					***********************************************/
	include _MAINSITEPATH_."/wit_SEOURL_Function.php";

	
	/*****************************		Database Variables Declartion	***********************************************/
	$tablename=$objLang->tableName("pj_sell_list",true); $UniqId="SellId";$ResVal="ProList"; $m_sta="ItemStatus";$PageVal="UserPage";
	
	/*****************************		Page Variables Declartion	***********************************************/
	$RedURL='product_mgmt.php?';$act_type=(isset($_GET['act_type'])) ? $_GET['act_type']:'';
	$mgtact=(isset($_GET['mgtact'])) ? $_GET['mgtact']:'';$con_id=(isset($_GET['con_id'])) ? $_GET['con_id']:'';
	$checkstas=(isset($_GET['checkstas'])) ? $_GET['checkstas']:'';
	$pro_msg=(isset($_GET['pro_msg'])) ? $_GET['pro_msg']:'';$SelStatus=true;
	switch($act_type)
	{
		case 'addnew': 
				if($con_id!='') {
 					$SelStatus=$objAdmin->SelectSingleTableRowDetails($tablename,$UniqId,'Arr',$con_id); 
					if(!$SelStatus){
						Redirect('product_mgmt.php?pro_msg=vfail');
					}
					CheckAdminMgmtPermission('Redirct','BothAdmin','Product','ManageProduct');
					$objSmarty->assign("SiteTitle",SiteMainTitle." -  Product Management - Edit Product Details");
					$objSmarty->assign("PageTitle","Product Management - Edit Product Details");
				}		
				else{
					CheckAdminMgmtPermission('Redirct','BothAdmin','Product','AddProduct');
					$objSmarty->assign("SiteTitle",SiteMainTitle." -  Product Management - Add Product Details");
					$objSmarty->assign("PageTitle","Product Management - Add Product Details");
				}
 				if(!empty($_POST['AdminAction'])){
					$objPro->AddUpdateSellerItemDetails($con_id,admin);
				}
				$objPro->SelectImage($con_id);
				$objPro->SelectCategoryList();	
 				$objSmarty->assign("TinyMCEPath",$ngconfig['TinyEditor']);
				$objSmarty->assign("IncludeTpl", "product_management_new.html");
			break;  
 		default:
			/******************  		Call Function For Delete,Active Inactive Table Details 	**********************/
			CheckAdminMgmtPermission('Redirct','BothAdmin','Product','ManageProduct');
				
			if(isset($_POST['Action_Type'])){
				if($_POST['Action_Type']=='Set Us Featured' || $_POST['Action_Type']=='Unset Featured'){
					$objAdmin->AdminControlMgmt($tablename,$UniqId,'ProFeatureSta',$RedURL);
				}
				else{
	 				$objAdmin->AdminControlMgmt($tablename,$UniqId,$m_sta,$RedURL);
				}	
			}
			/******************  		Admin Action Error and Success Messages	*********************Oakwood197!*/
				if($pro_msg!=''){
					if($pro_msg=="usucc"){
						$objSmarty->assign("SucMessage", " Product Details Has Been Updated Successfully!!");
					}else{
						$objSmarty->assign("SucMessage", "New  Product Details Has Been Added Successfully!!");
					}
				}
			if($mgtact!=""){	
				$ActArray=array("delsuc"=>"Deleted","actsuc"=>"Actived","inasuc"=>"InActived");
				if($checkstas=="sucs"){
				   $objSmarty->assign("SucMessage","Selected  Product(s) Details Has Been ".$ActArray[$mgtact]." Successfully");
				}
				else{
					$objSmarty->assign("ErrMessage","Selected  Product(s) Already in ".$ActArray[$mgtact]." Status");
				}
			}
		
			$objPro->GetProductLists('ProList',"ProductPage");
			
			$objSmarty->assign("SiteTitle",SiteMainTitle." Product Management");
			$objSmarty->assign("PageTitle"," Product Management");
			$objSmarty->assign("IncludeTpl", "product_management.html");	
		break;
	}	
 	
     /*****************************			Include Admin Menu and Ctrl Create File 	**********************************/
	include _MAINSITEPATH_."/wit_admincontrols.php";
	
	/*****************************			Include Admin Menu and Ctrl Create File 	**********************************/
	if($act_type=='managefile' || $act_type=='manageimage'){
		$MCtrlArray=array('Delete'=>'Delete');
	}
	else{
		$MCtrlArray=array('ON'=>'ON','OFF'=>'OFF');
	}
	$objSmarty->assign("ControlList",$MCtrlArray);
 	$objSmarty->assign("StoreImageDisp",$ngconfig['StoreImageDisp'] );
	#===========================================================================		
	$objSmarty->display("site_admin_main.html");
?>