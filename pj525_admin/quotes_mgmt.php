<?php 	
	/*****************************			Site Config File				***********************************************/
	include "../wit_includes/wit_common.php";
		
	/*****************************			Include  File					***********************************************/
	include _MAINSITEPATH_."/wit_SEOURL_Function.php";
	
	/*****************************			Include Supported Class Files	********************************************/
	include_once $ngconfig['SiteClassPath']."class.GeneralAdmin.php";
	include_once $ngconfig['SiteClassPath']."class.Quotes.php";
	
	/*****************************			Class Objects			*		**********************************************/
	$objAdmin	= new GeneralAdmin();$objTest	= new Testimonials();
		
	/*****************************		Database Variables Declartion	***********************************************/
	$tablename="pj_quotes";$UniqId="QuoteId";$ResVal="QList";$m_sta="QStatus";$PageVal="QPage";
	
	/*****************************		Page Variables Declartion	***********************************************/
	$pro_msg=(isset($_GET['pro_msg'])) ? $_GET['pro_msg']:'';$RedURL='quotes_mgmt.php?';
	$act_type=(isset($_GET['act_type'])) ? $_GET['act_type']:'';$mgtact=(isset($_GET['mgtact'])) ? $_GET['mgtact']:'';
	$checkstas=(isset($_GET['checkstas'])) ? $_GET['checkstas']:'';
	
	switch($act_type)
	{
 		default:
				/*****************************		Admin & Subadmin Login Permisson Check 	*************************/
				CheckAdminMgmtPermission('Redirct','MainAdmin','Quotes','ManageQuotes');
	
				/******************  		Call Function For Delete,Active Inactive Table Details 	**********************/
				if(isset($_POST['Action_Type'])){  
					$objAdmin->AdminControlMgmt($tablename,$UniqId,$m_sta,$RedURL); 
				} 
				/******************  		Select Admin Posted  Static Page List	**********************/
				$objTest->GetQuotesLists($ResVal,$PageVal);
				
				/******************  		Admin Action Error and Success Messages	**********************/
				if(!empty($pro_msg)){
					if($pro_msg=="usucc"){
						$objSmarty->assign("SucMessage", "Quotes Has Been Updated Successfully!!");}	
				}	
				if($mgtact!="")	{	
				$ActArray=array("delsuc"=>"Delete","actsuc"=>"Active","inasuc"=>"InActive");
					if($checkstas!="fals"){
						$objSmarty->assign("SucMessage","Selected Quotes Has Been ".$ActArray[$mgtact]."d Successfully");
					}
					else{
						$objSmarty->assign("ErrMessage","Selected Quotes Already in ".$ActArray[$mgtact]." Status");
					}
				}
				$objSmarty->assign("SiteTitle",SiteMainTitle." - Quotes Management");	//	For Page Main Title
				$objSmarty->assign("PageTitle"," Quotes Management");	//	For Page Sub Title
				$objSmarty->assign("IncludeTpl", "quotes_mgmt.html");	//	Assign Page For Smarty	
			break;			
	}
	/*****************************			Include Admin Menu and Ctrl Create File 	**********************************/
	include _MAINSITEPATH_."/wit_admincontrols.php";
	$MCtrlArray=array('ON'=>'ON','OFF'=>'OFF','Delete'=>'Delete',);
	$objSmarty->assign("ControlList",$MCtrlArray);	
		
	#===========================================================================		
	$objSmarty->display("site_admin_main.html");
?>