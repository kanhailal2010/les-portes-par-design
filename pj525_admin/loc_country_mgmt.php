<?php 	
	/*****************************			Site Config File			***********************************************/
	include "../wit_includes/wit_common.php";
		
	/*****************************			Include  File					***********************************************/
	include _MAINSITEPATH_."/wit_SEOURL_Function.php";
	
	/*****************************			Include Supported Class Files	********************************************/
	include_once $ngconfig['SiteClassPath']."class.GeneralAdmin.php";
	include_once $ngconfig['SiteClassPath']."class.Location.php";
	
 	/*****************************			Class Objects			***********************************************/
	$objAdmin	= new GeneralAdmin();$objLocation	= new Location();
		
 	/*****************************		Database Variables Declartion	***********************************************/
	$tablename="pj_location_country";$UniqId="country_id";$ResVal="ContList";$m_sta="country_status";$PageVal="LocRPage";
 
 	/*****************************		Page Variables Declartion	***********************************************/
	$pro_msg=(isset($_GET['pro_msg'])) ? $_GET['pro_msg']:'';$mgtact=(isset($_GET['mgtact'])) ? $_GET['mgtact']:'';
	$RedURL='loc_country_mgmt.php?';$act_type=(isset($_GET['act_type'])) ? $_GET['act_type']:'';
	
 	switch($act_type)
	{
		case 'addnew':
				
				$con_id=(isset($_GET['con_id'])) ? $_GET['con_id']:'';
						
				if($con_id!='')
				{	CheckAdminMgmtPermission('Redirct','BothAdmin','Location','ManageCountry');			
					$SelStatus=$objAdmin->SelectSingleTableRowDetails($tablename,$UniqId,'Arr',$con_id);	
					if(!$SelStatus)
					{
						Redirect('location_mgmt.php?pro_msg=vfail');
					}
					
					$objSmarty->assign("SiteTitle",SiteMainTitle." -  Country Management - Edit Country Details");
					$objSmarty->assign("PageTitle","Country Management - Edit Country Details");
				}		
				else{
					/********************			Admin Login and Subadmin Permisson Check 	***********************/

					CheckAdminMgmtPermission('Redirct','BothAdmin','Location','AddCountry');
					$objSmarty->assign("SiteTitle",SiteMainTitle." -  Country Management - Add Country Details");
					$objSmarty->assign("PageTitle","Country Management - Add Country Details");				
				}	
	
				if(!empty($_POST['AdminAction'])){
					$objLocation->AddUpdateCountryDetails($_POST['AdminAction'],$con_id);
				}
				$objSmarty->assign("IncludeTpl", "loc_country_new.html");		
			break;
		case 'addnewstate':
				
				$tablename="pj_location_state";$UniqId="state_id";
				
				$con_id=(isset($_GET['con_id'])) ? $_GET['con_id']:'';
				$objLocation->SelectCountryList();		
				if($con_id!='')
				{	CheckAdminMgmtPermission('Redirct','BothAdmin','Location','ManageState');			
					$SelStatus=$objAdmin->SelectSingleTableRowDetails($tablename,$UniqId,'Arr',$con_id);	
					if(!$SelStatus)
					{
						Redirect('state_mgmt.php?pro_msg=vfail');
					}
	
					$objSmarty->assign("SiteTitle",SiteMainTitle." -  State Management - Edit State Details");
					$objSmarty->assign("PageTitle","State Management - Edit State Details");
				}		
				else{
					/********************			Admin Login and Subadmin Permisson Check 	***********************/

					CheckAdminMgmtPermission('Redirct','BothAdmin','Location','AddState');
					$objSmarty->assign("SiteTitle",SiteMainTitle." -  State Management - Add State Details");
					$objSmarty->assign("PageTitle","State Management - Add State Details");				
				}	
	
				if(!empty($_POST['AdminAction'])){
					$objLocation->AddUpdateStateDetails($_POST['AdminAction'],$con_id);
				}
				
				
				$objSmarty->assign("IncludeTpl", "loc_state_new.html");		
			break;
		default:
				CheckAdminMgmtPermission('Redirct','BothAdmin','Location','ManageCountry');
				/******************  		Call Function For Delete,Active Inactive Table Details 	**********************/
				if(isset($_POST['Action_Type'])){
					$objAdmin->AdminControlMgmt($tablename,$UniqId,$m_sta,$RedURL);
				}
				
				/******************  		Select Admin Posted  Country List	**********************/
				$objLocation->SelectSiteCountryLists($ResVal,'LocRPage'); 
				
				/******************  		Admin Management Active,Delete etc Control Arrays List	 **********************/
				
				/******************  		Admin Action Error and Success Messages	**********************/
				if($pro_msg!="")
				{
					if($pro_msg=="asucc")
						$objSmarty->assign("SucMessage", "New Country Details Has Been Added Successfully!!");
					elseif($pro_msg=="usucc")
						$objSmarty->assign("SucMessage", "Country Details Has Been Updated Successfully!!");	
				}	
				if($mgtact!="")
				{	
					$ActArray=array("delsuc"=>"Deleted","actsuc"=>"Actived","inasuc"=>"InActived");
					$objSmarty->assign("SucMessage","Selected Country Details Has Been ".$ActArray[$mgtact]." Successfully");
				}
				
				$objSmarty->assign("SiteTitle",SiteMainTitle." - Country Management");	//	For Page Main Title
				$objSmarty->assign("PageTitle"," Country Management");	//	For Page Sub Title
				$objSmarty->assign("IncludeTpl", "loc_country_mgmt.html");	//	Assign Page For Smarty	
			break;			
	}
	/*****************************			Include Admin Menu and Ctrl Create File 	**********************************/
	include _MAINSITEPATH_."/wit_admincontrols.php";
	
	/******************  		Assign Script File to Inner Pages For Smarty		**********************/
	$MCtrlArray=array('ON'=>'ON','OFF'=>'OFF','Delete'=>'Delete');
	$objSmarty->assign("ControlList",$MCtrlArray);	
	
	#===========================================================================		
	$objSmarty->display("site_admin_main.html");
?>