<?php 	
	/*****************************			Site Config File				***********************************************/
	include "../wit_includes/wit_common.php";
     
	/*****************************			Include  File				***********************************************/
	include_once $ngconfig['SiteClassPath']."class.GeneralAdmin.php";
	include_once $ngconfig['SiteClassPath']."class.PEAR_RESIZE.php";
	include_once $ngconfig['SiteClassPath']."class.Glass.php";
	/*****************************			Class Objects			***********************************************/
	$objAdmin = new GeneralAdmin(); $objPro = new Glass();
	
	/*****************************			Include  File					***********************************************/
	include _MAINSITEPATH_."/wit_SEOURL_Function.php";

 	/*****************************		Database Variables Declartion	***********************************************/
	$tablename="pj_finish_type"; $UniqId="FinishId";$ResVal="ProList"; $m_sta="FStatus";$PageVal="UserPage";
	
	/*****************************		Page Variables Declartion	***********************************************/
	$RedURL='finishtype_mgmt.php?';$act_type=(isset($_GET['act_type'])) ? $_GET['act_type']:'';
	$mgtact=(isset($_GET['mgtact'])) ? $_GET['mgtact']:'';$con_id=(isset($_GET['con_id'])) ? $_GET['con_id']:'';
	$checkstas=(isset($_GET['checkstas'])) ? $_GET['checkstas']:'';
	$pro_msg=(isset($_GET['pro_msg'])) ? $_GET['pro_msg']:'';$SelStatus=true;
	switch($act_type)
	{
		case 'addnew': 
				if($con_id!='') {
 					$SelStatus=$objAdmin->SelectSingleTableRowDetails($tablename,$UniqId,'Arr',$con_id); 
					if(!$SelStatus){
						Redirect('finishtype_mgmt.php?pro_msg=vfail');
					}
					CheckAdminMgmtPermission('Redirct','BothAdmin','Quote Generator','ManageFinishType');
					$objSmarty->assign("SiteTitle",SiteMainTitle." -  FinishType Management - Edit FinishType Details");
					$objSmarty->assign("PageTitle","FinishType Management - Edit FinishType Details");
				}		
				else{
					CheckAdminMgmtPermission('Redirct','BothAdmin','Quote Generator','AddFinishType');
					$objSmarty->assign("SiteTitle",SiteMainTitle." -  FinishType Management - Add FinishType Details");
					$objSmarty->assign("PageTitle","FinishType Management - Add FinishType Details");
				}
 				if(!empty($_POST['AdminAction'])){
					$objPro->AddUpdateFinishTypeDetails($con_id,admin);
				}
 				$objSmarty->assign("IncludeTpl", "finishtype_new.html");
			break;  
 		default:
			/******************  		Call Function For Delete,Active Inactive Table Details 	**********************/
			CheckAdminMgmtPermission('Redirct','BothAdmin','Quote Generator','ManageFinishType');
				
			if(isset($_POST['Action_Type'])){
				if($_POST['Action_Type']=='Set Us Featured' || $_POST['Action_Type']=='Unset Featured'){
					$objAdmin->AdminControlMgmt($tablename,$UniqId,'ProFeatureSta',$RedURL);
				}
				else{
	 				$objAdmin->AdminControlMgmt($tablename,$UniqId,$m_sta,$RedURL);
				}	
			}
			/******************  		Admin Action Error and Success Messages	*********************Oakwood197!*/
				if($pro_msg!=''){
					if($pro_msg=="usucc"){
						$objSmarty->assign("SucMessage", " FinishType Details Has Been Updated Successfully!!");
					}else{
						$objSmarty->assign("SucMessage", "New  FinishType Details Has Been Added Successfully!!");
					}
				}
			if($mgtact!=""){	
				$ActArray=array("delsuc"=>"Deleted","actsuc"=>"Actived","inasuc"=>"InActived");
				if($checkstas=="sucs"){
				   $objSmarty->assign("SucMessage","Selected  FinishType(s) Details Has Been ".$ActArray[$mgtact]." Successfully");
				}
				else{
					$objSmarty->assign("ErrMessage","Selected  FinishType(s) Already in ".$ActArray[$mgtact]." Status");
				}
			}
		
			$objPro->GetFinishTypeLists('ProList',"FinishTypePage");
			
			$objSmarty->assign("SiteTitle",SiteMainTitle." FinishType Management");
			$objSmarty->assign("PageTitle"," FinishType Management");
			$objSmarty->assign("IncludeTpl", "finishtype_mgmt.html");	
		break;
	}	
 	
     /*****************************			Include Admin Menu and Ctrl Create File 	**********************************/
	include _MAINSITEPATH_."/wit_admincontrols.php";
	
	/*****************************			Include Admin Menu and Ctrl Create File 	**********************************/
	if($act_type=='managefile' || $act_type=='manageimage'){
		$MCtrlArray=array('Delete'=>'Delete');
	}
	else{
		$MCtrlArray=array('ON'=>'ON','OFF'=>'OFF','Delete'=>'Delete');
	}
	$objSmarty->assign("ControlList",$MCtrlArray);
 	$objSmarty->assign("FinishImageDisp",$ngconfig['FinishImageDisp'] );
	#===========================================================================		
	$objSmarty->display("site_admin_main.html");
?>