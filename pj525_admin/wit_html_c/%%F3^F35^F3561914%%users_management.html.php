<?php /* Smarty version 2.6.3, created on 2013-05-17 09:08:41
         compiled from users_management.html */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'stripslashes', 'users_management.html', 8, false),array('modifier', 'date_format', 'users_management.html', 64, false),array('modifier', 'md5_enc', 'users_management.html', 88, false),)), $this); ?>
<script src="PageStyleScript/SiteAdminCommonFunction.js" type="text/javascript"></script>
<div id="page-heading"><h1><?php echo $this->_tpl_vars['PageTitle']; ?>
</h1></div>
<div class="body_head PadLR10">			
	<div class="InnDiv940 AdmFleft">	
		<div class="AdmFleft" style="width:960px;">	
		<form name="frm_search" method="get" action="">
					<div class="HeadFont12 AdmFleft PadRLB5">
					User Name&nbsp;:&nbsp;<input type="text" name="seaname" value="<?php echo ((is_array($_tmp=$_GET['seaname'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
" /></div>
					<div class="HeadFont12 AdmFleft PadRLB5">
		Email&nbsp;:&nbsp;<input type="text" name="seamail" value="<?php echo ((is_array($_tmp=$_GET['seamail'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
" /></div>
 					<div class="HeadFont12 AdmFleft PadRLB5">
		Phone Number&nbsp;:&nbsp;<input type="text" name="seaPhone" value="<?php echo ((is_array($_tmp=$_GET['seaPhone'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
" /></div>
 					<div class="HeadFont12 AdmFleft PadRLB5" style="padding-left:50px;">
						<input type="submit" name="seabut" value="Search" class="btn"/></div>	
					<div class="HeadFont12 AdmFleft PadRLB5">
<input type="button" name="seabut" value="Show All" class="btn" onClick="GoBackRedirct('users_management.php');" /></div>
 		</form>		
	</div>
		<div class="AdmFleft" style="width:800px;"><div class="NewMsgDiv">	
		<?php if ($this->_tpl_vars['SucMessage'] != '' || $this->_tpl_vars['ErrMessage'] != ''): ?>	
			<?php if ($this->_tpl_vars['ErrMessage'] != ''): ?>
				<div class="Msg_error">
					<div class="AdmFleft"><?php echo $this->_tpl_vars['ErrMessage']; ?>
</div>
					<div class="AdmFRight"><a class="close-msg"><img src="images/close-button.png" alt="" /></a></div>
				</div>
			<?php elseif ($this->_tpl_vars['SucMessage'] != ''): ?>
				<div class="Msg_success">
					<div class="AdmFleft"><?php echo $this->_tpl_vars['SucMessage']; ?>
</div>
					<div class="AdmFRight"><a class="close-msg"><img src="images/close-button.png" alt="" /></a></div>
				</div>
			<?php endif; ?>
			<div class="spacer"></div>
		<?php endif; ?>
		</div></div>
		<div style="width:160px; float:left; padding:8px 0;">
			<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "AdminControlMenu_New.html", 'smarty_include_vars' => array('title' => 'admin control list settings')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
		</div>
		<div class="clearspace"></div>
	</div>
	<div class="clearspace"></div>
	<div id="Errmsgval"></div>
	<form name="frm_list" method="post" action="">
		<input type="hidden" name="Action_Type" />
		<input type="hidden" name="ConSId"/>	
		<div class="clearspace"></div>
		 	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
		<tr>
			<th class="table-header-cornerL line-left">S.No</th>			
			<th class="table-header-repeat line-left"> User Name</th>
 			<th class="table-header-repeat line-left"> EMail</th>
			<th class="table-header-repeat line-left">Join Date</th>
			<th class="table-header-repeat line-left">Phone Number</th>	
			<th class="table-header-repeat line-left"> Status</th>
			<th class="table-header-cornerR line-left">Actions</th>
		</tr>
		<tfoot><tr><td colspan="7" align="right"><?php echo $this->_tpl_vars['UserPage']; ?>
</td></tr></tfoot>
		<tbody>
		<?php $this->assign('i', $this->_tpl_vars['PageStartNum']); ?>
		<?php if (count($_from = (array)$this->_tpl_vars['UserList'])):
    foreach ($_from as $this->_tpl_vars['TableDet']):
?>
		<tr <?php if ($this->_tpl_vars['i']%2 != '0'): ?> class="alternate-row"<?php endif; ?> id="order_<?php echo $this->_tpl_vars['TableDet']['UserId']; ?>
">
				<td width="7%"><input type="checkbox" name="ConId[]" value="<?php echo $this->_tpl_vars['TableDet']['UserId']; ?>
" /><?php echo $this->_tpl_vars['i']++; ?>
.</td>
				<td width="17%"><?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['UserName'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</td> 
				<td width="18%"><?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['UserEMail'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</td>
				<td width="15%"><?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['UJoinDate'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d  %b  %Y") : smarty_modifier_date_format($_tmp, "%d  %b  %Y")); ?>
</td>
				<td width="15%"><?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['UserPhone'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</td>
				<td width="10%"><?php if ($this->_tpl_vars['TableDet']['UserSta'] == 1): ?>
					<span id="InActive_<?php echo $this->_tpl_vars['TableDet']['UserId']; ?>
">
						<a href="javascript:;" onClick="GoToJSONAjaxFunction('OFF','<?php echo $this->_tpl_vars['TableDet']['UserId']; ?>
','BuyersMgmt','User(s)')">
						<img class="tik" src="images/on_btn.png" /></a>
					</span>
					<span id="Active_<?php echo $this->_tpl_vars['TableDet']['UserId']; ?>
" style="display:none;">
						<a href="javascript:;" onClick="GoToJSONAjaxFunction('ON','<?php echo $this->_tpl_vars['TableDet']['UserId']; ?>
','BuyersMgmt','User(s)')">
						<img class="tik" src="images/off_btn.png" /></a>
					</span>
					<?php else: ?>
					<span id="InActive_<?php echo $this->_tpl_vars['TableDet']['UserId']; ?>
" style="display:none;">
						<a href="javascript:;" onClick="GoToJSONAjaxFunction('OFF','<?php echo $this->_tpl_vars['TableDet']['UserId']; ?>
','BuyersMgmt','User(s)')">
						<img  border="0" src="images/on_btn.png" /></a>
					</span>
					<span id="Active_<?php echo $this->_tpl_vars['TableDet']['UserId']; ?>
">
						<a href="javascript:;" onClick="GoToJSONAjaxFunction('ON','<?php echo $this->_tpl_vars['TableDet']['UserId']; ?>
','BuyersMgmt','User(s)')">
						<img border="0" src="images/off_btn.png" /></a>
					</span>
					
					<?php endif; ?>
					</td>
				<td width="13%"><div class="gallery options-width">
					<a title="Edit" class="icon-1 info-tooltip" href="users_management.php?act_type=addnew&con_id=<?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['UserId'])) ? $this->_run_mod_handler('md5_enc', true, $_tmp) : smarty_modifier_md5_enc($_tmp)); ?>
"></a>
					<a title="Delete" class="icon-2 info-tooltip" href="javascript:;" onclick="AdminDeleteFunction('<?php echo $this->_tpl_vars['TableDet']['UserId']; ?>
');"></a>
					<div id="MyShow_<?php echo $this->_tpl_vars['TableDet']['UserId']; ?>
">
					<a title="Show" class="icon-3 info-tooltip" href="javascript:;"  onclick="GoToViewFunction('<?php echo $this->_tpl_vars['TableDet']['UserId']; ?>
','MyHide','MyShow');"></a>
				</div>					
				<div id="MyHide_<?php echo $this->_tpl_vars['TableDet']['UserId']; ?>
" style="display:none;">
					<a title="Hide" class="icon-3 info-tooltip" href="javascript:;" onclick="GoToHideFunction('<?php echo $this->_tpl_vars['TableDet']['UserId']; ?>
','MyShow','MyHide');"></a>
				</div>
				<?php if ($this->_tpl_vars['TableDet']['AutoRenewalStatus'] == 'Yes'): ?><a title="Send Renewal Alert" class="icon-1 info-tooltip"  href="admin_mail_manual.php?con_id=<?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['UserId'])) ? $this->_run_mod_handler('md5_enc', true, $_tmp) : smarty_modifier_md5_enc($_tmp)); ?>
" ></a><?php endif; ?>
				</div></td>	</tr>
             	<tr id="ResIdDiv_<?php echo $this->_tpl_vars['TableDet']['UserId']; ?>
" style="display:none;" class="ViewDet" ><td colspan="9">
					<div class="HeadYBord14">Details of '<?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['UserName'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
' - User</div>
					<div style="width:450px;float:left; padding-top:10px;" >							
						<div class="clearspace"></div> 
						<div class="CMDet">
							<div class="CMDet1">First Name</div>
							<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['UserFName'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
						</div>
						<div class="CMDet">
							<div class="CMDet1">Last Name</div>
							<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['UserLName'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
						</div>
						<div class="CMDet">
							<div class="CMDet1">User Name</div>
							<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['UserName'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
						</div>
						
						<div class="CMDet"> 	 	 	
							<div class="CMDet1">Email</div>
							<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['UserEMail'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
						</div>	
						<div class="CMDet"> 	 	 	
							<div class="CMDet1">PayPal Email</div>
							<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['UserPayPalMail'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
						</div> 
						<div class="CMDet">
							<div class="CMDet1">Phone Number</div>
							<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['UserPhone'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
						</div>
						<div class="CMDet">
							<div class="CMDet1">Address</div>
							<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['UserAdds'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
						</div>
						<div class="clearspace"></div> 
					</div>
					<div style="width:350px; float:left; padding-top:10px;">
						<div class="CMDet">
								<div class="CMDet1">City</div>
								<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['UserCity'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
							</div>	
						<div class="CMDet">
								<div class="CMDet1">State/Province</div>
								<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['state_name'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
							</div>	
						<div class="CMDet">
							<div class="CMDet1">Country</div>
							<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['country_name'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
						</div>
						<div class="CMDet">
							<div class="CMDet1">Zip Code/Postal Code</div>
							<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['UserZip'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
						</div>
 						<div class="CMDet">
							<div class="CMDet1">Join Date</div>
							<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['UJoinDate'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d  %b  %Y") : smarty_modifier_date_format($_tmp, "%d  %b  %Y")); ?>
</div>
						</div>	
						<div class="CMDet">
							<div class="CMDet1">Register IP</div>
							<div class="CMDet2">:&nbsp;<?php echo $this->_tpl_vars['TableDet']['URegIP']; ?>
</div>
						</div>
						<div class="CMDet">
							<div class="CMDet1">Status</div>
							<div class="CMDet2">:&nbsp;<?php if ($this->_tpl_vars['TableDet']['UserSta'] == '1'): ?>ON<?php else: ?>OFF<?php endif; ?></div>
						</div>
						 		
					</div>
				</td></tr>
				
		<?php endforeach; unset($_from); else: ?>
			<tr><td colspan="7" align="center" class="error_msg"><strong>No Users Found ..</strong></td></tr>					
		<?php endif; ?>						
		</tbody>
	</table>			
	<div class="clearspace"></div>

	</form>	
</div>
<?php echo '
<script type="text/javascript">
var FormName=\'frm_list\';
var FormObj=document.forms[FormName];
var MgmtName=\'User(s)\';
var Seltype=\'BuyersMgmt\';

function GoBackRedirct(RedirURL){
	window.location.href= RedirURL;
}
</script>
'; ?>