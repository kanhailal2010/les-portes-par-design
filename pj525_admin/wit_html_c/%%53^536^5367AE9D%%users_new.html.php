<?php /* Smarty version 2.6.3, created on 2013-05-17 09:08:51
         compiled from users_new.html */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'stripslashes', 'users_new.html', 88, false),)), $this); ?>
<link href="PageStyleScript/ui.css" rel="stylesheet"  />
<script src="PageStyleScript/SiteAdminValidateFunction.js" type="text/javascript"></script>
<script src="PageStyleScript/state.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['TinyMCEPath']; ?>
ckeditor/ckeditor.js"></script>
<?php echo '
<script type="text/javascript" src="PageStyleScript/ui.js"></script>
<script language="javascript">
jQuery(function() {
	jQuery(\'#UserDOB\').datepicker({changeYear:true,changeMonth:true,dateFormat:\'dd-MM-yy\'}); 
});
</script>
'; ?>

<?php echo '
<script type="text/javascript">
var SiteMainPath="{SiteMainPath}";
var AllowExtension= new Array();
function GoToFormValidate(ValiType)
{	
	ErrCss=\'txtboxerr\';var FlagIns=true; 
	FlagIns=FormValidateFunction(\'Text\',\'#UserFName\',\'Please enter first name\'); 
	if(FlagIns){
		FlagIns=FormValidateFunction(\'Text\',\'#UserLName\',\'Please enter last name\');
	}
	if(FlagIns){
		FlagIns=FormValidateFunction(\'Text\',\'#UserName\',\'Please enter user name\');
	}
	if(FlagIns){
		FlagIns=FormValidateFunction(\'Text\',\'#UserPass\',\'Please enter password\');
	}
	
	if(FlagIns){
		FlagIns=FormValidateFunction(\'Text\',\'#UserEMail\',\'Please enter email\');
	}
	
	if(FlagIns){
		FlagIns=FormValidateFunction(\'EMail\',\'#UserEMail\',\'Please enter valid email\');
	}
	if(FlagIns){
		FlagIns=FormValidateFunction(\'Text\',\'#UserPayPalMail\',\'Please enter paypal email\');
	}
	
	if(FlagIns){
		FlagIns=FormValidateFunction(\'EMail\',\'#UserPayPalMail\',\'Please enter valid paypal email\');
	}  
	if(FlagIns){
		FlagIns=FormValidateFunction(\'Text\',\'#UserAdds\',\'Please enter address\');
	} 
	/*if(FlagIns){
		FlagIns=FormValidateFunction(\'Text\',\'#UserPhone\',\'Please enter phone number\');
	}*/
	if(FlagIns){
		FlagIns=FormValidateFunction(\'Text\',\'#UserCity\',\'Please enter city\');
	}
	if(FlagIns){
		FlagIns=FormValidateFunction(\'Text\',\'#UserCountry\',\'Please select country\');
	}
	if(FlagIns){
		FlagIns=FormValidateFunction(\'Text\',\'#UserState\',\'Please select State/Province\');
	}
	if(FlagIns){	
		FlagIns=FormValidateFunction(\'Text\',\'#UserZip\',\'Please enter zipcode/postalcode\');
	}
	
	 
	if(FlagIns){
		document.forms[FormName].submit();
	}
	else{	return false;	}
}
</script>
'; ?>

<div id="page-heading"><h1><?php echo $this->_tpl_vars['PageTitle']; ?>
</h1></div>
<div class="clearspace"></div>
<form name="FormAddUpdate" method="post" action="" enctype="multipart/form-data"><div class="body_head">			
	<div class="TutProWid2"><div style="padding-left:50px;"><div style="width:600px;">
	<?php if ($this->_tpl_vars['SucMessage'] != '' || $this->_tpl_vars['ErrMessage'] != ''): ?>	
		<div class="FormDiv">							
			<div class="Msg_Div">
			<?php if ($this->_tpl_vars['ErrMessage'] != ''): ?><div class="Msg_error"><?php echo $this->_tpl_vars['ErrMessage']; ?>
</div>
			<?php elseif ($this->_tpl_vars['SucMessage'] != ''): ?><div class="Msg_success"><?php echo $this->_tpl_vars['SucMessage']; ?>
</div><?php endif; ?>
			</div>
		</div><div class="spacer"></div>
	<?php endif; ?> 
			<div class="FormDiv">
                <div class="FormDet1">First Name<samp style="color:#FF3300">*</samp></div>
                <div class="FormDet2">:</div>
                <div class="FormDet3">
<input type="text" name="UserFName" id="UserFName" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['Arr']['UserFName'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
" style="width:350px;" maxlength="30">
                <br /><span id="UserFNameMsg" class="error"></span>
            </div>
			</div>
			
			<div class="FormDiv">
                <div class="FormDet1">Last Name<samp style="color:#FF3300">*</samp></div>
                <div class="FormDet2">:</div>
                <div class="FormDet3">
<input type="text" name="UserLName" id="UserLName" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['Arr']['UserLName'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
" style="width:350px;" maxlength="30">
						<br /><span id="UserLNameMsg" class="error"></span>
						 
            </div>
			</div>
			
			<div class="FormDiv">
                <div class="FormDet1">User Name<samp style="color:#FF3300">*</samp></div>
                <div class="FormDet2">:</div>
                <div class="FormDet3">
<input type="text" name="UserName" id="UserName" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['Arr']['UserName'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
" style="width:350px;" maxlength="30">
						<br /><span id="UserNameMsg" class="error"></span>
						 
            </div>
			</div>
			
			<div class="FormDiv">
                <div class="FormDet1">Password<samp style="color:#FF3300">*</samp></div>
                <div class="FormDet2">:</div>
                <div class="FormDet3">
<input type="password" name="UserPass" id="UserPass" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['MemPass1'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
" style="width:350px;" maxlength="30">
						<br /><span id="UserPassMsg" class="error"></span>
						 
            </div>
			</div>
			
            <div class="FormDiv">
                <div class="FormDet1">Email<samp style="color:#FF3300">*</samp></div>
                <div class="FormDet2">:</div>
                <div class="FormDet3">
<input type="text" name="UserEMail" id="UserEMail" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['Arr']['UserEMail'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
" style="width:350px;" maxlength="150">
                <br /><span id="UserEMailMsg" class="error"></span>
                </div>
            </div>     
			   <div class="FormDiv">
                <div class="FormDet1"> PayPal Mail<samp style="color:#FF3300">*</samp></div>
                <div class="FormDet2">:</div>
                <div class="FormDet3">
<input type="text" name="UserPayPalMail" id="UserPayPalMail" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['Arr']['UserPayPalMail'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
" style="width:350px;" maxlength="150">
                <br /><span id="UserPayPalMailMsg" class="error"></span>
                </div>
            </div>    
			    
            <div class="FormDiv"> 	
                <div class="FormDet1">Address<samp style="color:#FF3300">*</samp></div>
                <div class="FormDet2">:</div>
                <div class="FormDet3">
<input type="text" name="UserAdds" id="UserAdds" style="width:350px;" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['Arr']['UserAdds'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
" maxlength="150">
                <br /><span id="UserAddsMsg" class="error"></span>
                </div>
            </div> 
 		
			<div class="FormDiv">
                <div class="FormDet1">Phone Number</div>
                <div class="FormDet2">:</div>
                <div class="FormDet3">
<input type="text" name="UserPhone" id="UserPhone" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['Arr']['UserPhone'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
" style="width:350px;" maxlength="15" >
<br>[Ex: (012) 345-6789]
                <br /><span id="UserPhoneMsg" class="error"></span>
                </div>
            </div>
			
			<!--end-->          
			<div class="FormDiv"> 	
                <div class="FormDet1">City<samp style="color:#FF3300">*</samp></div>
                <div class="FormDet2">:</div>
                <div class="FormDet3">
<input name="UserCity" id="UserCity" type="text" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['Arr']['UserCity'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
" maxlength="50"/>
                <br /><span id="UserCityMsg" class="error"></span>
                </div>
            </div>
			<div class="FormDiv"> 	
                <div class="FormDet1">Country<samp style="color:#FF3300">*</samp></div>
                <div class="FormDet2">:</div>
                <div class="FormDet3">
<select name="UserCountry" id="UserCountry" style="width:280px;" onChange="GoToPrintJSONAjaxDetails('State',this.value,'UserState');">
    <option value="" >Select Country</option>
    <?php if (count($_from = (array)$this->_tpl_vars['StateList'])):
    foreach ($_from as $this->_tpl_vars['DataDet']):
?> 
        <option <?php if ($this->_tpl_vars['Arr']['UserCountry'] == $this->_tpl_vars['DataDet']['country_id'] || ( $this->_tpl_vars['DataDet']['country_id'] == '219' && $this->_tpl_vars['Arr']['UserCountry'] == '' )): ?> selected="selected" <?php endif; ?> value="<?php echo $this->_tpl_vars['DataDet']['country_id']; ?>
" >
            <?php echo ((is_array($_tmp=$this->_tpl_vars['DataDet']['country_name'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
 
        </option >
    <?php endforeach; unset($_from); endif; ?> 
    </select>
                <br /><span id="UserCountryMsg" class="error"></span>
                </div>
            </div>
			<div class="FormDiv"> 	
                <div class="FormDet1">State/Province<samp style="color:#FF3300">*</samp></div>
                <div class="FormDet2">:</div>
                <div class="FormDet3">
                    <select name="UserState" id="UserState" style="width:280px;">
                        <option value="">Select State</option>
					 </select>
                	<br /><span id="UserStateMsg" class="error"></span>
                </div>
            </div>	
            <div class="FormDiv"> 	
                <div class="FormDet1">Zip Code/Postal Code<samp style="color:#FF3300">*</samp></div>
                <div class="FormDet2">:</div>
                <div class="FormDet3">
<input name="UserZip" id="UserZip" type="text" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['Arr']['UserZip'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
" maxlength="15" onkeypress="return AllowOnlyNumbers(event);"/>
<br /><span id="UserZipMsg" class="error"></span>
                </div>
            </div> 
		<div class="FormDiv">
			<div class="FormDet1">&nbsp;</div>
			<div class="FormDet2">&nbsp;</div>
			<div class="FormDet3">
<?php if ($_GET['con_id'] != ''): ?>
<input type="submit" name="AltPage" value="Update"class="btn" onclick="return GoToFormValidate('2');" />
<input type="hidden" name="AdminAction" value="Update"class="btn" />
<?php else: ?>
<input type="hidden" name="AdminAction" value="Add"class="btn" />
<input type="submit" name="AltPage" value="Add"class="btn" onclick="return GoToFormValidate('1');" />

<?php endif; ?>
&nbsp;<input type="button" name="goback" value="GoBack" class="btn" onClick="GoBack('users_management.php');" />
			</div>
		</div>
	</div></div></div>			
	<div class="clearspace"></div>
</div></form>

<script type="text/javascript">
var FormName='FormAddUpdate';
var FormObj=document.forms[FormName];
var MgmtName='Business';
</script>
<script type="text/javascript">	
<?php if ($this->_tpl_vars['Arr']['UserCountry'] != ''): ?>
	GoToPrintJSONAjaxDetails('State','<?php echo $this->_tpl_vars['Arr']['UserCountry']; ?>
','UserState','<?php echo $this->_tpl_vars['Arr']['UserState']; ?>
');
<?php else: ?>
	GoToPrintJSONAjaxDetails('State','219','UserState','<?php echo $this->_tpl_vars['Arr']['UserState']; ?>
');
<?php endif; ?>	
	
</script>