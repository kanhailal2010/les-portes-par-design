<?php /* Smarty version 2.6.3, created on 2013-05-06 04:55:14
         compiled from inquiry_mgmt.html */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'stripslashes', 'inquiry_mgmt.html', 8, false),array('modifier', 'html_entity_decode', 'inquiry_mgmt.html', 66, false),array('modifier', 'truncate', 'inquiry_mgmt.html', 66, false),array('modifier', 'date_format', 'inquiry_mgmt.html', 67, false),)), $this); ?>

<script src="PageStyleScript/SiteAdminCommonFunction.js" type="text/javascript"></script>
<div id="page-heading"><h1><?php echo $this->_tpl_vars['PageTitle']; ?>
</h1></div>
<div class="body_head PadLR10">			
	<div class="InnDiv940 AdmFleft">	
		<div class="AdmFleft" style="width:900px;"><form name="frm_search" method="get" action="">
            <div class="HeadFont12 AdmFleft PadRLB5">
        Name&nbsp;:&nbsp;<input type="text" name="seaname" value="<?php echo ((is_array($_tmp=$_GET['seaname'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
"/></div>
            <div class="HeadFont12 AdmFleft PadRLB5">
        Product Name&nbsp;:&nbsp;<input type="text" name="seapro" value="<?php echo ((is_array($_tmp=$_GET['seapro'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
"/></div>
            <div class="HeadFont12 AdmFleft PadRLB5">
        Email&nbsp;:&nbsp;<input type="text" name="seaamil" value="<?php echo ((is_array($_tmp=$_GET['seaamil'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
"/></div>
            <div class="HeadFont12 AdmFleft PadRLB5">
                <input type="submit" name="seabut" value="Search" class="btn"/></div>	
            <div class="HeadFont12 AdmFleft PadRLB5">
                <input type="button" name="seabut" value="Show All" class="btn" onClick="GoBackRedirct('inquiry_mgmt_home.php');" /></div>		
		</form></div>
		<div class="AdmFleft" style="width:800px;"><div class="NewMsgDiv">	
		<?php if ($this->_tpl_vars['SucMessage'] != '' || $this->_tpl_vars['ErrMessage'] != ''): ?>	
			<?php if ($this->_tpl_vars['ErrMessage'] != ''): ?>
				<div class="Msg_error">
					<div class="AdmFleft"><?php echo $this->_tpl_vars['ErrMessage']; ?>
</div>
					<div class="AdmFRight"><a class="close-msg"><img src="images/close-button.png" alt="" /></a></div>
				</div>
			<?php elseif ($this->_tpl_vars['SucMessage'] != ''): ?>
				<div class="Msg_success">
					<div class="AdmFleft"><?php echo $this->_tpl_vars['SucMessage']; ?>
</div>
					<div class="AdmFRight"><a class="close-msg"><img src="images/close-button.png" alt="" /></a></div>
				</div>
			<?php endif; ?>
			<div class="spacer"></div>
		<?php endif; ?>
		</div></div>
		<div style="width:160px; float:left; padding:8px 0;">
			<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "AdminControlMenu_New.html", 'smarty_include_vars' => array('title' => 'admin control list settings')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
		</div>
	<form name="frm_list" method="post" action="">
		<input type="hidden" name="Action_Type" />
		<input type="hidden" name="ConSId"/>	
		<input type="hidden" name="Seltype"/>	

		<div class="clearspace"></div>
		<!--	Admin Mgmt Permission Check	Start-->
			
		<!--	Admin Mgmt Permission Check	Start-->

		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
			<thead><tr>
				<th class="table-header-cornerL"><b>S.No</b></th>
				<th class="table-header-repeat line-left"><b>Name</b></th>
 				<th class="table-header-repeat line-left"><b>Product Name</b></th>
 				<th class="table-header-repeat line-left"><b>EMail</b></th>
				<th class="table-header-repeat line-left"><b>Inquiry</b></th>
				<th class="table-header-repeat line-left"><b>Date</b></th>	
				<th class="table-header-cornerR line-left"><b>Actions</b></th>						
			</tr></thead>
			<tfoot><tr><td colspan="7" align="right"><?php echo $this->_tpl_vars['PageNavi']; ?>
</td></tr></tfoot>
			<tbody>
			<?php $this->assign('i', $this->_tpl_vars['PageStartNum']); ?>
			<?php if (count($_from = (array)$this->_tpl_vars['InqList'])):
    foreach ($_from as $this->_tpl_vars['ConDet']):
?>
			<tr  bgcolor="<?php if ($this->_tpl_vars['i']%2 != '0'): ?>#FFFFFF<?php else: ?>#F3F3F3<?php endif; ?>"> 
				<td width="6%"><input type="checkbox" name="ConId[]" id="ConId[]" value="<?php echo $this->_tpl_vars['ConDet']['Inq_id']; ?>
"/><?php echo $this->_tpl_vars['i']++; ?>
.</td>
				<td width="15%"><?php echo ((is_array($_tmp=$this->_tpl_vars['ConDet']['Name'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</td>
				<td width="20%"><?php echo ((is_array($_tmp=$this->_tpl_vars['ConDet']['ItemName'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</td>
				<td width="19%"><?php echo ((is_array($_tmp=$this->_tpl_vars['ConDet']['InqMail'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</td>
				<td width="20%"><?php echo ((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['ConDet']['InqMsg'])) ? $this->_run_mod_handler('html_entity_decode', true, $_tmp) : html_entity_decode($_tmp)))) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)))) ? $this->_run_mod_handler('truncate', true, $_tmp, 50) : smarty_modifier_truncate($_tmp, 50)); ?>
</td>
				<td width="10%"><?php echo ((is_array($_tmp=$this->_tpl_vars['ConDet']['InqDate'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%b %d, %Y") : smarty_modifier_date_format($_tmp, "%b %d, %Y")); ?>
</td>
                <td width="10%">
				<div class="gallery options-width">
					<a title="Delete" class="icon-2 info-tooltip" href="javascript:;" onclick="AdminDeleteFunction('<?php echo $this->_tpl_vars['ConDet']['Inq_id']; ?>
');"></a> 
						
					<div id="MyShow_<?php echo $this->_tpl_vars['ConDet']['Inq_id']; ?>
">
					<a title="Show" class="icon-3 info-tooltip" href="javascript:;"  onclick="GoToViewFunction('<?php echo $this->_tpl_vars['ConDet']['Inq_id']; ?>
','MyHide','MyShow');"></a>
				</div>					
					<div id="MyHide_<?php echo $this->_tpl_vars['ConDet']['Inq_id']; ?>
" style="display:none;">
						<a title="Hide" class="icon-3 info-tooltip" href="javascript:;" onclick="GoToHideFunction('<?php echo $this->_tpl_vars['ConDet']['Inq_id']; ?>
','MyShow','MyHide');"></a>
					</div>  
				</div>
				<tr id="ResIdDiv_<?php echo $this->_tpl_vars['ConDet']['Inq_id']; ?>
" style="display:none;" class="ViewDet" >	
 					<td colspan="7">
					<div class="HeadYBord14">'<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['ConDet']['ItemName'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)))) ? $this->_run_mod_handler('html_entity_decode', true, $_tmp) : html_entity_decode($_tmp)); ?>
' - Details</div>
			<div class="clearspace"></div>
						<div style="width:940px;float:left; padding-top:10px;" >
						<div style="width:550px; float:left; padding-top:10px;" >
							<div class="CMDet">
							<div class="CMDet1">Product Name </div>
							<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['ConDet']['ItemName'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)))) ? $this->_run_mod_handler('html_entity_decode', true, $_tmp) : html_entity_decode($_tmp)); ?>
</div></div>
							<div class="clearspace"></div>
							<div class="CMDet">
							<div class="CMDet1">Category</div>
							<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['ConDet']['CatName'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)))) ? $this->_run_mod_handler('html_entity_decode', true, $_tmp) : html_entity_decode($_tmp)); ?>
</div></div>
							<div class="clearspace"></div>
							<div class="CMDet">
							<div class="CMDet1">Name </div>
							<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['ConDet']['Name'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)))) ? $this->_run_mod_handler('html_entity_decode', true, $_tmp) : html_entity_decode($_tmp)); ?>
</div></div>
							<div class="clearspace"></div>
							<div class="CMDet">
								<div class="CMDet1">EMail</div>
								<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['ConDet']['InqMail'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
							</div><div class="clearspace"></div> 
							<div class="CMDet">
								<div class="CMDet1">Date</div>
								<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['ConDet']['InqDate'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%b %d, %Y") : smarty_modifier_date_format($_tmp, "%b %d, %Y")); ?>
</div>
							</div><div class="clearspace"></div>
							<div class="CMDet">
								<div class="CMDet1">Request Price </div>
								<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['ConDet']['InqMsg'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)))) ? $this->_run_mod_handler('html_entity_decode', true, $_tmp) : html_entity_decode($_tmp)); ?>
</div>
							</div> 
							</div>
						<div style="width:350px; float:left; padding-top:10px;">
							<?php if ($this->_tpl_vars['ConDet']['SellId'] == '1'): ?>
								<div class="CMDet">
									<div class="CMDet1">Finish Type</div>
									<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['ConDet']['FinishType'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
								</div>
								<div class="CMDet">
									<div class="CMDet1">Hinge Selections</div>
									<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['ConDet']['Hinge'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
								</div>
								<div class="CMDet">
									<div class="CMDet1">Glass</div>
									<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['ConDet']['Glass'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
								</div>
								<div class="CMDet">
									<div class="CMDet1">Config Bevel</div>
									<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['ConDet']['ConfigBevel'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
								</div>
								<div class="CMDet">
									<div class="CMDet1">Caming</div>
									<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['ConDet']['Caming'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
								</div>
								<div class="CMDet">
									<div class="CMDet1">Dimension</div>
									<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['ConDet']['Dimension'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
								</div>
								<div class="CMDet">
									<div class="CMDet1">Color Number</div>
									<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['ConDet']['ColorNumber'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
								</div>
								<div class="CMDet">
									<div class="CMDet1">Finish</div>
									<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['ConDet']['Finish'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
								</div>
								<div class="CMDet">
									<div class="CMDet1">Quantity</div>
									<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['ConDet']['Qty'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
								</div>
								<div class="CMDet">
									<div class="CMDet1">Hole size (A)</div>
									<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['ConDet']['HoleA'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
								</div>
								<div class="CMDet">
									<div class="CMDet1">Length (B)</div>
									<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['ConDet']['LengthB'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
								</div>
								<div class="CMDet">
									<div class="CMDet1">Width (C)</div>
									<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['ConDet']['WidthC'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
								</div>
								<div class="CMDet">
									<div class="CMDet1">Hinge(HL-HR)</div>
									<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['ConDet']['Hinges'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
								</div>
								<!--<div class="CMDet">
									<div class="CMDet1">Square-Foot(Unit)</div>
									<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['ConDet']['SqftUnit'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
								</div>
								<div class="CMDet">
									<div class="CMDet1">Square-Foot(Total)</div>
									<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['ConDet']['SqftTot'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
								</div>-->
							<?php endif; ?> 
							<?php if ($this->_tpl_vars['ConDet']['SellId'] == '3'): ?>
								<div class="CMDet">
									<div class="CMDet1">Qty</div>
									<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['ConDet']['Qty'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
								</div>
								<div class="CMDet">
									<div class="CMDet1">Type</div>
									<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['ConDet']['Type'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
								</div>
							<?php endif; ?> 
							<?php if ($this->_tpl_vars['ConDet']['SellId'] == '2'): ?>
								<div class="CMDet">
									<div class="CMDet1">Finish</div>
									<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['ConDet']['Finish'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
								</div>
							<?php endif; ?> 
							<?php if ($this->_tpl_vars['ConDet']['SellId'] == '2' || $this->_tpl_vars['ConDet']['SellId'] == '3'): ?>
								<div class="CMDet">
									<div class="CMDet1">X</div>
									<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['ConDet']['X'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
								</div>
							<?php endif; ?> 
							<?php if ($this->_tpl_vars['ConDet']['SellId'] == '2'): ?>
								<div class="CMDet">
									<div class="CMDet1">Y</div>
									<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['ConDet']['Y'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
								</div>
							<?php endif; ?> 
							<?php if ($this->_tpl_vars['ConDet']['SellId'] == '2' || $this->_tpl_vars['ConDet']['SellId'] == '3'): ?>
								<div class="CMDet">
									<div class="CMDet1">Z</div>
									<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['ConDet']['Z'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
								</div>
							<?php endif; ?> 
							<?php if ($this->_tpl_vars['ConDet']['SellId'] == '2'): ?>
								<div class="CMDet">
									<div class="CMDet1">e</div>
									<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['ConDet']['e'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
								</div>
							<?php endif; ?> 
							<?php if ($this->_tpl_vars['ConDet']['SellId'] == '3'): ?>
								<div class="CMDet">
									<div class="CMDet1">Light</div>
									<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['ConDet']['Light'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
								</div>
							<?php endif; ?>
						</div>
				<div class="clearspace"></div>						
			</div>
				   </td></tr></td>
			</tr>	
			<?php endforeach; unset($_from); else: ?>
				<tr><td colspan="7" align="center" class="Msg_info"><strong>No Results Found ..</strong></td></tr>					
			<?php endif; ?>				
			</tbody>
		</table>			
		<div class="clearspace"></div>
	</form>	
	</div>
</div>
<?php echo '
<script type="text/javascript">
var FormName=\'frm_list\';
var FormObj=document.forms[FormName];
var MgmtName=\'Inquiry(s)\';
var Seltype=\'CategoryMgmt\';
</script>
'; ?>