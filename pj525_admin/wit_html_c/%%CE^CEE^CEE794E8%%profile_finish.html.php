<?php /* Smarty version 2.6.3, created on 2013-08-19 07:31:11
         compiled from profile_finish.html */ ?>
<div id="page-heading"><h1> Profile Finish Management </h1></div>
<div class="body_head PadLR10">
	<div class="InnDiv940 AdmFleft">
                <?php if ($this->_tpl_vars['SucMessage'] != '' || $this->_tpl_vars['ErrMessage'] != ''): ?>	
                    <div class="AdmFleft SeaTotBox">							
                        <div class="Msg_Div">
                        <?php if ($this->_tpl_vars['ErrMessage'] != ''): ?><div class="Msg_error"><?php echo $this->_tpl_vars['ErrMessage']; ?>
</div>
                        <?php elseif ($this->_tpl_vars['SucMessage'] != ''): ?><div class="Msg_success"><?php echo $this->_tpl_vars['SucMessage']; ?>
</div><?php endif; ?>
                        </div>
                    </div><div class="spacer"></div>
                <?php endif; ?>
		<form action="profile_finish.php?act_type=<?php echo $this->_tpl_vars['act_type']; ?>
" method="post" enctype="multipart/form-data">
		<input type="hidden" name="finish_id" value="<?php echo $this->_tpl_vars['edit']['profile_finish_id']; ?>
">
		
			
		<div class="FormDiv">
				<div class="FormDet1">Profile Finish Title</div>
				<div class="FormDet2">:</div>
				<div class="FormDet3">
				<input type="text" name="dptitle" value="<?php echo $this->_tpl_vars['edit']['profile_finish_title']; ?>
">
				</div>
		</div>
		<div class="clearspace"></div>
			
			
		<div class="FormDiv">
				<div class="FormDet1">Profile Finish Code</div>
				<div class="FormDet2">:</div>
				<div class="FormDet3">
				<input type="text" name="dpcode" value="<?php echo $this->_tpl_vars['edit']['profile_finish_code']; ?>
">
				</div>
		</div>
		<div class="clearspace"></div>
		
			
		<div class="FormDiv">
				<div class="FormDet1">Profile Finish Factor</div>
				<div class="FormDet2">:</div>
				<div class="FormDet3">
				<input type="text" name="dpfactor" value="<?php echo $this->_tpl_vars['edit']['profile_finish_factor']; ?>
">
				</div>
		</div>
		<div class="clearspace"></div>
		
			
		<div class="FormDiv">
				<div class="FormDet1">Profile Finish Image</div>
				<div class="FormDet2">:</div>
				<div class="FormDet3">
					<?php if ($this->_tpl_vars['edit']['profile_finish_image'] != ''): ?>
						<img src="<?php echo $this->_tpl_vars['SiteHttpPath'];   echo PROFILE_FINISH_FOLDER.'thumbnail/';  echo $this->_tpl_vars['edit']['profile_finish_image']; ?>
" width="150">
					<?php endif; ?>
					<input type="file" class="btn" name="dpimage"  value="">
				</div>
		</div>
		<div class="clearspace"></div>
		
			
		<div class="FormDiv">
				<div class="FormDet1">Profile Finish Factor</div>
				<div class="FormDet2">:</div>
				<div class="FormDet3">
					<input class="btn" type="submit" name="submit" value="submit">
					<button class="btn" onclick="window.location='profile_finish.php'; return false;"> Cancel </button>
				</div>
		</div>
		<div class="clearspace"></div>
			
			</form>
			</div> <!-- .InnDiv940 AdmFleft -->
			<hr>
			<table id="product-table" width="100%" border="0" cellspacing="0" cellpadding="0">
				<thead>
				<tr>
					<th class="table-header-cornerL"> Profile Finish Name</th>
					<th class="table-header-repeat line-left"> Profile Finish Factor</th>
					<th class="table-header-repeat line-left"> Profile Finish Image</th>
					<th class="table-header-cornerR line-left">Action</th>
				</tr>
				</thead>
				<tbody>
				
				<?php if (count($_from = (array)$this->_tpl_vars['profile_finish'])):
    foreach ($_from as $this->_tpl_vars['f']):
?>
					<tr>
						<td><?php echo $this->_tpl_vars['f']['profile_finish_title']; ?>
</td>
						<td><?php echo $this->_tpl_vars['f']['profile_finish_factor']; ?>
</td>
						<td><img src="<?php echo $this->_tpl_vars['SiteHttpPath'];   echo PROFILE_FINISH_FOLDER.'thumbnail/';  echo $this->_tpl_vars['f']['profile_finish_image']; ?>
" height="80"></td>
						<td>
							<div class="gallery options-width">
								<a class="icon-1 info-tooltip" href="profile_finish.php?act_type=edit&finish_id=<?php echo $this->_tpl_vars['f']['profile_finish_id']; ?>
"></a>
								<a class="icon-2 info-tooltip" href="profile_finish.php?act_type=delete&finish_id=<?php echo $this->_tpl_vars['f']['profile_finish_id']; ?>
" onclick="return confirm('Please Confirm Deletion !!')"></a>
							</div>
						</td>
					</tr>
				<?php endforeach; unset($_from); endif; ?>	
				</tbody>
			</table>
</div>