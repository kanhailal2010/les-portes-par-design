<?php /* Smarty version 2.6.3, created on 2013-05-18 04:11:18
         compiled from state_mgmt.html */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'stripslashes', 'state_mgmt.html', 9, false),array('modifier', 'md5_enc', 'state_mgmt.html', 88, false),)), $this); ?>
<script src="PageStyleScript/SiteAdminCommonFunction.js" type="text/javascript"></script>
<div id="page-heading"><h1><?php echo $this->_tpl_vars['PageTitle']; ?>
</h1></div>
<div class="body_head PadLR10">			
		<div class="InnDiv940 AdmFleft">	
			<div class="AdmFleft">
			<form name="frm_search" method="get">
				<div class="PadRLB5 AdmFleft">
	 	<div class="HeadFont12 AdmFleft PadRLB5">
State Name&nbsp;:&nbsp;<input type="text" name="seaname" id="seaname" value="<?php echo ((is_array($_tmp=$_GET['seaname'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
"  /></div>		
					<div class="HeadFont12 AdmFleft PadRLB5">
						<input type="submit" name="seabut" value="Search" class="btn"/></div>
					<div class="HeadFont12 AdmFleft PadRLB5">
<input type="button" name="seabut" value="Show All" class="btn" onClick="GoBackRedirct('state_mgmt.php');" /></div>	
				</div>
			</form>
			</div>
			<div class="AdmFleft" style="width:800px;"><div class="NewMsgDiv">	
		<?php if ($this->_tpl_vars['SucMessage'] != '' || $this->_tpl_vars['ErrMessage'] != ''): ?>	
			<?php if ($this->_tpl_vars['ErrMessage'] != ''): ?>
				<div class="Msg_error">
					<div class="AdmFleft"><?php echo $this->_tpl_vars['ErrMessage']; ?>
</div>
					<div class="AdmFRight"><a class="close-msg"><img src="images/close-button.png" alt="" /></a></div>
				</div>
			<?php elseif ($this->_tpl_vars['SucMessage'] != ''): ?>
				<div class="Msg_success">
					<div class="AdmFleft"><?php echo $this->_tpl_vars['SucMessage']; ?>
</div>
					<div class="AdmFRight"><a class="close-msg"><img src="images/close-button.png" alt="" /></a></div>
				</div>
			<?php endif; ?>
			<div class="spacer"></div>
		<?php endif; ?>
		</div></div>
		<div style="width:160px; float:left; padding:8px 0;">
			<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "AdminControlMenu_New.html", 'smarty_include_vars' => array('title' => 'admin control list settings')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
		</div>
		<div class="clearspace"></div>
		</div>
	<div id="Errmsgval"></div>
			<form name="frm_list" method="post">
		<input type="hidden" name="Action_Type" >
		<input type="hidden" name="ConSId"/>
		<div class="clearspace"></div>	
		<!--	Admin Mgmt Permission Check	Start-->
        
		<!--	Admin Mgmt Permission Check	Start-->		
		 	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
		<tr>
				<th class="table-header-cornerL" ><b>S.No</b></th>
				<th  class="table-header-repeat line-left" >State Name</b>	</th>
				<th  class="table-header-repeat line-left">Country Name</b>	</th>
				<th  class="table-header-repeat line-left"><b>Status</b></th>
				<th class="table-header-cornerR"><b>Actions</b></th>						
		</tr>
		<tfoot><tr><td colspan="5" align="right"><?php echo $this->_tpl_vars['LocRPage']; ?>
</td></tr></tfoot>
		<tbody>
			<?php $this->assign('i', $this->_tpl_vars['PageStartNum']); ?>
			<?php if (count($_from = (array)$this->_tpl_vars['StateList'])):
    foreach ($_from as $this->_tpl_vars['DataDet']):
?> 
			<tr  bgcolor="<?php if ($this->_tpl_vars['i']%2 != '0'): ?>#FFFFFF<?php else: ?>#F3F3F3<?php endif; ?>"> 
				<td width="10%">
				<input type="checkbox" name="ConId[]" value="<?php echo $this->_tpl_vars['DataDet']['state_id']; ?>
" /><?php echo $this->_tpl_vars['i']++; ?>
.</td>
				<td width="35%"><?php echo ((is_array($_tmp=$this->_tpl_vars['DataDet']['state_name'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</td>
				<td width="35%"><?php echo ((is_array($_tmp=$this->_tpl_vars['DataDet']['country_name'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</td>
				<td width="7%">					
					<?php if ($this->_tpl_vars['DataDet']['StateSta'] == 1): ?>
					
					<span id="InActive_<?php echo $this->_tpl_vars['DataDet']['state_id']; ?>
">
						<a href="javascript:;" onClick="GoToJSONAjaxFunction('OFF','<?php echo $this->_tpl_vars['DataDet']['state_id']; ?>
','StateMgmt','State(s)')">
						<img class="tik" src="images/on_btn.png" /></a>
					</span>
					<span id="Active_<?php echo $this->_tpl_vars['DataDet']['state_id']; ?>
" style="display:none;">
						<a href="javascript:;" onClick="GoToJSONAjaxFunction('ON','<?php echo $this->_tpl_vars['DataDet']['state_id']; ?>
','StateMgmt','State(s)')">
						<img class="tik" src="images/off_btn.png" /></a>
					</span>
					<?php else: ?>
					<span id="InActive_<?php echo $this->_tpl_vars['DataDet']['state_id']; ?>
" style="display:none;">
						<a href="javascript:;" onClick="GoToJSONAjaxFunction('OFF','<?php echo $this->_tpl_vars['DataDet']['state_id']; ?>
','StateMgmt','State(s)')">
						<img  border="0" src="images/on_btn.png" /></a>
					</span>
					<span id="Active_<?php echo $this->_tpl_vars['DataDet']['state_id']; ?>
">
						<a href="javascript:;" onClick="GoToJSONAjaxFunction('ON','<?php echo $this->_tpl_vars['DataDet']['state_id']; ?>
','StateMgmt','State(s)')">
						<img border="0" src="images/off_btn.png" /></a>
					</span>
					
					<?php endif; ?>
					</td>					
				<td width="10%">
		<div class="gallery options-width">
		<a title="Edit" class="icon-1 info-tooltip" href="loc_country_mgmt.php?act_type=addnewstate&con_id=<?php echo ((is_array($_tmp=$this->_tpl_vars['DataDet']['state_id'])) ? $this->_run_mod_handler('md5_enc', true, $_tmp) : smarty_modifier_md5_enc($_tmp)); ?>
"></a>
		<a title="Delete" class="icon-2 info-tooltip" href="javascript:;" onclick="AdminDeleteFunction('<?php echo $this->_tpl_vars['DataDet']['state_id']; ?>
');"></a>	</div>	
				</td>	
												
			</tr>
			<?php endforeach; unset($_from); else: ?>
				<tr><td colspan="5" align="center" class="error_msg"><strong>No States Found...</strong></td></tr>	
			<?php endif; ?>						
			</tbody>
		</table>			
		<div class="clearspace"></div>
	</form>
</div>
<?php echo '
<script type="text/javascript">
var FormName=\'frm_list\';
var FormObj=document.forms[FormName];
var MgmtName=\'State(s)\';
var Seltype=\'StateMgmt\';
</script>
'; ?>