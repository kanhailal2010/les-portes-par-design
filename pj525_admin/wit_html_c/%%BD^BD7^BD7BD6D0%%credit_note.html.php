<?php /* Smarty version 2.6.3, created on 2013-05-18 04:14:50
         compiled from credit_note.html */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'stripslashes', 'credit_note.html', 8, false),array('modifier', 'md5', 'credit_note.html', 69, false),)), $this); ?>
<script src="PageStyleScript/SiteAdminCommonFunction.js" type="text/javascript"></script>
<div id="page-heading"><h1><?php echo $this->_tpl_vars['PageTitle']; ?>
</h1></div>
<div class="body_head PadLR10">			
	<div class="InnDiv940 AdmFleft">	
		<div class="AdmFleft" style="width:960px;">		<form name="frm_search" method="get" action="">
				<div class="PadRLB5 AdmFleft">
				<div class="HeadFont12 AdmFleft PadRLB5">
User Name&nbsp;:&nbsp;<input type="text" name="usersea" value="<?php echo ((is_array($_tmp=$_GET['usersea'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
"/></div>
				<div class="HeadFont12 AdmFleft PadRLB5">
Invoice Number&nbsp;:&nbsp;<input type="text" name="pronamesea" value="<?php echo ((is_array($_tmp=$_GET['pronamesea'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
"/></div> 
					 
					<div class="HeadFont12 AdmFleft PadRLB5">
						<input type="submit" name="seabut" value="Search" class="btn"/></div>	
					<div class="HeadFont12 AdmFleft PadRLB5">
<input type="button" name="seabut" value="Show All" class="btn" onClick="GoBackRedirct('credit_note.php');" /></div>		
				</div>
			</form>		</div>
		<div class="AdmFleft" style="width:800px;"><div class="NewMsgDiv">	
		<?php if ($this->_tpl_vars['SucMessage'] != '' || $this->_tpl_vars['ErrMessage'] != ''): ?>	
			<?php if ($this->_tpl_vars['ErrMessage'] != ''): ?>
				<div class="Msg_error">
					<div class="AdmFleft"><?php echo $this->_tpl_vars['ErrMessage']; ?>
</div>
					<div class="AdmFRight"><a class="close-msg"><img src="images/close-button.png" alt="" /></a></div>
				</div>
			<?php elseif ($this->_tpl_vars['SucMessage'] != ''): ?>
				<div class="Msg_success">
					<div class="AdmFleft"><?php echo $this->_tpl_vars['SucMessage']; ?>
</div>
					<div class="AdmFRight"><a class="close-msg"><img src="images/close-button.png" alt="" /></a></div>
				</div>
			<?php endif; ?>
			<div class="spacer"></div>
		<?php endif; ?>
		</div></div>
		<div style="width:160px; float:left; padding:8px 0;">
			<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "AdminControlMenu_New.html", 'smarty_include_vars' => array('title' => 'admin control list settings')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
		</div>
		<div class="clearspace"></div>
	</div>
	<div class="clearspace"></div>
	<div id="Errmsgval"></div>
<form name="frm_list" method="post" action="">
		<input type="hidden" name="Action_Type" />
		<input type="hidden" name="ConSId"/>	
	<div class="clearspace"></div>
 	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
		<tr>
			<th class="table-header-cornerL"><b>S.No</b></th>
			<th class="table-header-repeat line-left"><b>&nbsp;User Name</b></th>
			<th class="table-header-repeat line-left"><b>&nbsp;Invoice Number</b></th>
			<th class="table-header-repeat line-left"><b>&nbsp;Credit Note</b></th>  
			<th class="table-header-repeat line-left"><b>Amount</b></th>
			<th class="table-header-repeat line-left"><b>Taxes Applied</b></th>
			<th class="table-header-cornerR line-left">Actions</th>
		</tr>
		<tfoot><tr><td colspan="7" align="right"><?php echo $this->_tpl_vars['CreditNotePage']; ?>
</td></tr></tfoot>
		<tbody>
		<?php $this->assign('i', $this->_tpl_vars['PageStartNum']); ?>
			<?php if (count($_from = (array)$this->_tpl_vars['ProList'])):
    foreach ($_from as $this->_tpl_vars['TableDet']):
?>
		<tr <?php if ($this->_tpl_vars['i']%2 != '0'): ?> class="alternate-row" id="order_<?php echo $this->_tpl_vars['TableDet']['CNId']; ?>
"<?php endif; ?>>
		<td width="7%"><input type="checkbox" name="ConId[]" value="<?php echo $this->_tpl_vars['TableDet']['CNId']; ?>
" /><?php echo $this->_tpl_vars['i']++; ?>
.</td>
					<td width="15%"><?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['UserName'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</td>
					<td width="12%"><?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['InvoiceNum'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</td>
                    <td width="23%"><?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['CreditNote'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</td>  
                    <td width="8%">$<?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['PayAmt'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</td> 
                    <td width="20%">GST-<?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['GSTTax'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
%, QST-<?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['QSTTax'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
%,Sales-<?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['SalesTax'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
%</td>   
					 
				<td width="10%">
				<div class="gallery options-width">
					<!--<a title="Edit Credit Note" class="icon-1 info-tooltip" href="credit_note.php?act_type=addnew&edit=1&con_id=<?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['CNId'])) ? $this->_run_mod_handler('md5', true, $_tmp) : md5($_tmp)); ?>
"></a>-->
 				<div id="MyShow_<?php echo $this->_tpl_vars['TableDet']['CNId']; ?>
">
					<a title="Show" class="icon-3 info-tooltip" href="javascript:;" onclick="GoToViewFunction('<?php echo $this->_tpl_vars['TableDet']['CNId']; ?>
','MyHide','MyShow');"></a>
				</div>					
				<div id="MyHide_<?php echo $this->_tpl_vars['TableDet']['CNId']; ?>
" style="display:none;">
					<a title="Hide" class="icon-3 info-tooltip" href="javascript:;" onclick="GoToHideFunction('<?php echo $this->_tpl_vars['TableDet']['CNId']; ?>
','MyShow','MyHide');"></a>
				</div>  
					<a title="Delete" class="icon-2 info-tooltip" href="javascript:;" onclick="AdminDeleteFunction('<?php echo $this->_tpl_vars['TableDet']['CNId']; ?>
');"></a>
				</div>							
				</td>	
			<tr  id="ResIdDiv_<?php echo $this->_tpl_vars['TableDet']['CNId']; ?>
" style="display:none;" class="ViewDet" >					
			<td colspan="7">
			<div class="HeadYBord14">Details of '<?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['InvoiceNum'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
'</div>
			<div class="clearspace"></div>
						<div style="width:940px;float:left; padding-top:10px;" >
							<div style="width:550px; float:left; padding-top:10px;">
						<div class="CMDet"> 	 	 	
								<div class="CMDet1">User Name</div>
								<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['UserName'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
							</div>
							<div class="CMDet"> 
								<div class="CMDet1">Invoice Number</div>
								<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['InvoiceNum'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
							</div>  
							<div class="CMDet"> 
								<div class="CMDet1">Credit Note</div>
								<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['CreditNote'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
							</div>  
							<div class="CMDet"> 
								<div class="CMDet1">Amount</div>
								<div class="CMDet2">:&nbsp;$<?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['PayAmt'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
							</div>  
							<div class="CMDet"> 
								<div class="CMDet1">Taxes Applied</div>
								<div class="CMDet2">:&nbsp;GST-<?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['GSTTax'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
%, QST-<?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['QSTTax'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
%,Sales-<?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['SalesTax'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
%</div>
							</div>   
							</div>
							<div style="width:350px; float:left; padding-top:10px;"> 
							</div>
				<div class="clearspace"></div>						
			</div>
			   </td></tr>
			</tr>
		
		<?php endforeach; unset($_from); else: ?>
			<tr><td colspan="7" align="center" class="error_msg"><strong>No Payments Found ..</strong></td></tr>					
		<?php endif; ?>						
		<tr>
		<td width="7%"></td>
					<td width="15%"></td>
					<td width="12%"></td>
                    <td width="23%"><b>Total</b></td>  
                    <td width="8%"><b>$<?php echo ((is_array($_tmp=$this->_tpl_vars['Total'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</b></td> 
                    <td width="20%"></td>   
					 
				<td width="10%"> 			
				</td>	
			 
			</tr>
		</tbody>
	</table>			
	<div class="clearspace"></div>
</form>	
</div>
<?php echo '
<script type="text/javascript">
var FormName=\'frm_list\';
var FormObj=document.forms[FormName];
var MgmtName=\'Credit(s)\';
var Seltype=\'ProductMgmt\';

</script>
'; ?>