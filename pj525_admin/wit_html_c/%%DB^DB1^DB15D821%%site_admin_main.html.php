<?php /* Smarty version 2.6.3, created on 2013-08-12 15:23:16
         compiled from site_admin_main.html */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $this->_tpl_vars['SiteTitle']; ?>
</title>
<link rel="stylesheet" href="theme_css/screen.css" type="text/css" media="screen" title="default" />
<link href="PageStyleScript/adminstyles_1.css" rel="stylesheet" type="text/css" />
<link href="PageStyleScript/form_style.css" rel="stylesheet" type="text/css" />

<!--[if IE]>
<link rel="stylesheet" media="all" type="text/css" href="theme_css/pro_dropline_ie.css" />
<![endif]-->

<!--  jquery core -->
<script src="PageStyleScript/jquery-1.7.1.min.js" type="text/javascript"></script>

<?php echo '
<!-- Custom jquery scripts -->
<script src="theme_scripts/custom_jquery.js" type="text/javascript"></script>
 
<!-- Tooltips -->
<script src="theme_scripts/jquery.tooltip.js" type="text/javascript"></script>
<script src="theme_scripts/jquery.dimensions.js" type="text/javascript"></script>
<script src="PageStyleScript/Admin_status_Change.js"></script>
<script type="text/javascript">
$(function() {
	$(\'a.info-tooltip \').tooltip({
		track: true,
		delay: 0,
		fixPNG: true, 
		showURL: false,
		showBody: " - ",
		top: -35,
		left: 5
	});
});
</script> 
'; ?>

<?php echo $this->_tpl_vars['SupportScript']; ?>


</head>
<body> 
<!-- Start: page-top-outer -->
<div id="page-top-outer">    

<!-- Start: page-top -->
<div id="page-top">

	<!-- start logo -->
	<div id="logo">
	<a href="index.php"><div style="margin-top:-52px;"><img src="images/logo.png" width="240" height="100" alt="" /></div></a>
	</div>
	<!-- end logo -->
	<div style="float:right;width:370px;"><ul id="headernav">
		<li><a href="index.php" style="padding-top:7px">Welcome <?php echo $_SESSION['PJ525A_Name']; ?>
</a></li>
		<li><a href="<?php if ($_SESSION['PJ525A_Type'] == 'MainAdmin'): ?>admin_site_settings.php?act_type=settings<?php else: ?>admin_mgmt.php<?php endif; ?>"  style="padding-top:7px; <?php echo $this->_tpl_vars['Settings']; ?>
">My Account</a>
		<ul>
				<?php if ($_SESSION['PJ525A_Type'] == 'MainAdmin'): ?>
				<li><a href="admin_site_settings.php?act_type=settings">Site Settings</a></li>
				<?php endif; ?>
				<li><a href="index.php" style="padding-bottom:10px;">Visitor Statistics</a></li>
				<li><a href="admin_site_settings.php?act_type=paypalset">Payment Settings</a></li>  
		</ul>
		</li>
		<li><a href="logout.php"><img src="theme_images/shared/nav/nav_logout.gif" alt="" /></a></li>
		<li style="padding-top:6px;"> <?php echo $this->_tpl_vars['languageSelection']; ?>
 </li>
	</ul></div>
 	<div class="clear"></div>

</div>
<!-- End: page-top -->

</div>
<!-- End: page-top-outer -->
	
<div class="clear">&nbsp;</div>
 
<!--  start nav-outer-repeat................................................................................................. START -->
<div class="nav-outer-repeat"> 
<!--  start nav-outer -->
<div class="nav-outer"> 
	<!--  start nav -->
	<div class="nav"><div class="table">
		<?php echo $this->_tpl_vars['MenuText']; ?>
 
		<div class="clear"></div>
	</div><div class="clear"></div></div>
	<!--  start nav -->
</div>
<div class="clear"></div>
<!--  start nav-outer -->
</div>
<!--  start nav-outer-repeat................................................... END -->

 <div class="clear"></div>
 
<!-- start content-outer ........................................................................................................................START -->
<div id="content-outer">
<!-- start content -->
<div id="content">
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['IncludeTpl'], 'smarty_include_vars' => array('Title' => 'Admin Include Templates')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	<!--  start page-heading -->
	<!-- end page-heading -->
	<div class="clear">&nbsp;</div>
</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer........................................................END -->

<div class="clear">&nbsp;</div>
    
<!-- start footer -->         
<div id="footer">
	<!--  start footer-left -->
	<div id="footer-left">&#169;&nbsp;<?php echo $this->_tpl_vars['site_copyright']; ?>
</div>
	<!--  end footer-left -->
	<div class="clear">&nbsp;</div>
</div>
<!-- end footer -->
 
</body>
</html>