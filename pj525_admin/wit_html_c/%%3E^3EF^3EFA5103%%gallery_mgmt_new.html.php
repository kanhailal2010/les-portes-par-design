<?php /* Smarty version 2.6.3, created on 2013-05-30 08:13:34
         compiled from gallery_mgmt_new.html */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'gallery_mgmt_new.html', 4, false),array('modifier', 'stripslashes', 'gallery_mgmt_new.html', 59, false),)), $this); ?>
<script src="PageStyleScript/SiteAdminValidateFunction.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['TinyMCEPath']; ?>
ckeditor/ckeditor.js"></script>
<script type="text/javascript"> 
var ImageList='<?php echo count($this->_tpl_vars['ImgList']); ?>
';
</script>
<?php echo '
<script type="text/javascript">
var SiteMainPath="{SiteMainPath}";
var AllowExtension= new Array();
function GoToFormValidate(ValiType)
{	
	ErrCss=\'txtboxerr\';var FlagIns=true;
	
	
	FlagIns=FormValidateFunction(\'Text\',\'#Title\',\'Please enter Title\'); 
	if(FlagIns){
		FlagIns=FormValidateFunction(\'Text\',\'#URL\',\'Please enter URL\');
	} 
	if(FlagIns)
	{	
		if(jQuery(\'#GalleryImage\').val()== \'\' && ValiType !=\'2\') {	
			FlagIns=FormValidateFunction(\'Text\',\'#GalleryImage\',\'Please Select Image\');
		}  
		if(jQuery(\'#GalleryImage\').val()!= \'\' && ValiType !=\'2\'){ ErrCss=\'txtboxerr\';
			if(!jQuery(\'#GalleryImage\').val().substr(-4).toLowerCase().match(\'.jpg|jpeg|.gif|.png|.bmp\')){
			jQuery(\'#GalleryImageMsg\').show();
			jQuery(\'#GalleryImageMsg\').html(\'jpeg/jpg/gif/png/bmp formats only allowed\');
			jQuery(\'#GalleryImage\').attr(\'class\',\'error_element\');
			jQuery(\'#GalleryImage\').focus();return false;
			}		
		}
 	} 
 	if(FlagIns) {	
		document.forms[FormName].submit();
	}
	else{	return false;	}
}
</script>
'; ?>

<div id="page-heading"><h1><?php echo $this->_tpl_vars['PageTitle']; ?>
</h1></div>
<div class="clearspace"></div>
<form name="FormAddUpdate" method="post" action="" enctype="multipart/form-data">
 <input type="hidden" id="id" value="1">
<div class="body_head">			
	<div class="TutProWid2">
	<div style="padding-left:50px;"><div style="width:600px;">
	<?php if ($this->_tpl_vars['SucMessage'] != '' || $this->_tpl_vars['ErrMessage'] != ''): ?>	
		<div class="FormDiv">							
			<div class="Msg_Div">
			<?php if ($this->_tpl_vars['ErrMessage'] != ''): ?><div class="Msg_error"><?php echo $this->_tpl_vars['ErrMessage']; ?>
</div>
			<?php elseif ($this->_tpl_vars['SucMessage'] != ''): ?><div class="Msg_success"><?php echo $this->_tpl_vars['SucMessage']; ?>
</div><?php endif; ?>
			</div>
		</div><div class="spacer"></div>
	<?php endif; ?>
  			<div class="FormDiv">
				<div class="FormDet1">Title</div>
				<div class="FormDet2">:</div>
				<div class="FormDet3">
				<input type="text" name="Title" id="Title" class="txtbox1" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['Arr']['Title'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
" />
				<br /><span id="TitleMsg" class="error"></span>
				</div>
			</div>  
 			<div class="FormDiv">
				<div class="FormDet1">URL</div>
				<div class="FormDet2">:</div>
				<div class="FormDet3">
				<input type="text" name="URL" id="URL" class="txtbox1" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['Arr']['URL'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
" />
				<br>[Ex: http://www.google.co.in/]
				<br /><span id="URLMsg" class="error"></span>
				</div>
			</div> 
		<div class="FormDiv">
                        <div class="FormDet1">Image</div>
                        <div class="FormDet2">:</div>
                        <div class="FormDet3">
							<input name="GalleryImage" id="GalleryImage" class="compound_file" type="file"/> 
							<br /><span id="GalleryImageMsg" class="error"></span>
						</div>
			<?php if ($this->_tpl_vars['Arr']['GalleryImage'] != ''): ?>
			<div class="FormDiv">
                        <div class="FormDet1">&nbsp;</div>
                        <div class="FormDet2">&nbsp;</div>
                        <div class="FormDet3">
							<img border='0' src="<?php echo $this->_tpl_vars['GalleryImgDisp']; ?>
t_<?php echo $this->_tpl_vars['Arr']['GalleryImage']; ?>
" alt="<?php echo $this->_tpl_vars['GalleryImgDisp']; ?>
t_<?php echo $this->_tpl_vars['Arr']['GalleryImage']; ?>
"/>
						</div>	
				<?php endif; ?>
		<div class="FormDiv">
			<div class="FormDet1">&nbsp;</div>
			<div class="FormDet2">&nbsp;</div>
			<div class="FormDet3">
<?php if ($_GET['con_id'] != ''): ?>
<input type="button" name="AltPage" value="Update"class="btn" onclick="GoToFormValidate('2');" />
<input type="hidden" name="AdminAction" value="Update"class="btn" />
<?php else: ?>
<input type="hidden" name="AdminAction" value="Add"class="btn" />
<input type="button" name="AltPage" value="Add"class="btn" onclick="GoToFormValidate('1');" />
<?php endif; ?>
&nbsp;<input type="button" name="goback" value="GoBack" class="btn" onClick="GoBack('place_mgmt.php');" />
			</div>
		</div>
	</div></div></div>			
	<div class="clearspace"></div>
</div></form>	
<?php echo '
<script type="text/javascript">
var FormName=\'FormAddUpdate\';
var FormObj=document.forms[FormName];
var MgmtName=\'Tour(s)\';
</script>
'; ?>

<?php echo '
<script type="text/javascript">
var FormName=\'FormAddUpdate\';
var FormObj=document.forms[FormName];
var MgmtName=\'Content\';

function RemoveUploadedImages(SelId){
	if(confirm(\'Are you sure you want to delete this image?\')){
		jQuery(\'#ProImgSucc\').html(\'<div class="Msg_SuccDiv"><img src="images/ajax-loader.gif" border="0"/></div><div class="spacer"></div>\');
		var AjacRes=jQuery.ajax({
			url: \'admin_json_ajax.php\',
			type: "POST",
			dataType:\'json\',
			data: {SelType:\'ProImg\',SelId:SelId},
			error:function (xhr){
				alert(\'Server Error\');
			},
			success: function(JSonRes){
				if(JSonRes.ajax_res==\'true\'){
					jQuery(\'#ProImgSucc\').html(\'<div class="Msg_SuccDiv"><div class="Msg_success">\'+JSonRes.ajax_msg+\'</div></div><div class="spacer"></div>\');
					jQuery(\'#Img_\'+SelId).hide();		
				}
				else{	
					jQuery(\'#ProImgSucc\').html(\'<div class="Msg_SuccDiv"><div class="Msg_error">\'+JSonRes.ajax_msg+\'</div></div><div class="spacer"></div>\');						
				}
			}
		});
	}	
}
</script>
'; ?>
 