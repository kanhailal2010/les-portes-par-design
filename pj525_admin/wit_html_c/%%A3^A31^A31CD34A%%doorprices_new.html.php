<?php /* Smarty version 2.6.3, created on 2013-05-06 11:35:55
         compiled from doorprices_new.html */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'doorprices_new.html', 5, false),array('modifier', 'stripslashes', 'doorprices_new.html', 75, false),)), $this); ?>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['TinyMCEPath']; ?>
ckeditor/ckeditor.js"></script>
<script src="PageStyleScript/SiteAdminCommonFunction.js" type="text/javascript"></script>
<script src="PageStyleScript/SiteAdminValidateFunction.js" type="text/javascript"></script>
<script type="text/javascript"> 
var ImageList='<?php echo count($this->_tpl_vars['ImgList']); ?>
'; 
</script>
<?php echo '
<script type="text/javascript">
var SiteMainPath="{SiteMainPath}";
var AllowExtension= new Array();;var ErrCss=\'txtboxerr\';
function GoToFormValidate(ValiType)
{	
	var FlagIns=true;var ErrCss=\'txtboxerr\';
	
	FlagIns=FormValidateFunction(\'Text\',\'#PTypeId\',\'Please select profile type\'); 
	if(FlagIns){
		FlagIns=FormValidateFunction(\'Text\',\'#FinishId\',\'Please select finish\');
	} 
	if(FlagIns){
		FlagIns=FormValidateFunction(\'Text\',\'#FinishId\',\'Please select finish\');
	} 
	if(FlagIns){
		FlagIns=FormValidateFunction(\'Text\',\'#Sqfeet\',\'Please enter square feet\');
	} 
	//if(FlagIns){
//		FlagIns=FormValidateFunction(\'Text\',\'#GlassGroup\',\'Please enter glass group\');
//	}
	if(FlagIns){
		FlagIns=FormValidateFunction(\'Text\',\'#DoorPrice\',\'Please enter door price\');
	}
	if(FlagIns){
		FlagIns=FormValidateFunction(\'Text\',\'#DLPercent\',\'Please enter Lesportes percentage\');
	}  
	if(FlagIns){
		FlagIns=FormValidateFunction(\'Text\',\'#DBPercent\',\'Please enter Barazin percentage\');
	}  
	/*if(FlagIns){
		FlagIns=FormValidateFunction(\'Text\',\'#BarazinPrice\',\'Please enter barazin price\');
	} */ 
	if(FlagIns){	
		document.forms[FormName].submit();
	}
}  
</script>
'; ?>

<div id="page-heading"><h1><?php echo $this->_tpl_vars['PageTitle']; ?>
</h1></div>
<div class="clearspace"></div>
	<form name="add_pro" method="post">
	<input type="hidden" id="id" value="1">
	<div class="body_head"><div class="TutProWid2">
		<div style="padding-left:50px;"><div style="width:600px; float:left;">
		<div class="AdmFleft" style="width:800px;"><div class="NewMsgDiv">	
		<?php if ($this->_tpl_vars['SucMessage'] != '' || $this->_tpl_vars['ErrMessage'] != ''): ?>	
			<?php if ($this->_tpl_vars['ErrMessage'] != ''): ?>
				<div class="Msg_error">
					<div class="AdmFleft"><?php echo $this->_tpl_vars['ErrMessage']; ?>
</div>
					<div class="AdmFRight"><a class="close-msg"><img src="images/close-button.png" alt="" /></a></div>
				</div>
			<?php elseif ($this->_tpl_vars['SucMessage'] != ''): ?>
				<div class="Msg_success">
					<div class="AdmFleft"><?php echo $this->_tpl_vars['SucMessage']; ?>
</div>
					<div class="AdmFRight"><a class="close-msg"><img src="images/close-button.png" alt="" /></a></div>
				</div>
			<?php endif; ?>
			<div class="spacer"></div>
		<?php endif; ?>
		</div></div>
 			<div class="FormDiv">
				<div class="FormDet1">Profile Type</div>
				<div class="FormDet2">:</div>
				<div class="FormDet3">
					<select name="PTypeId" id="PTypeId">
					<option value="">Select Profile Type</option>
					 <?php if (count($_from = (array)$this->_tpl_vars['PTList'])):
    foreach ($_from as $this->_tpl_vars['CatDet']):
?> 
					 <option <?php if ($this->_tpl_vars['ArrD']['PTypeId'] == $this->_tpl_vars['CatDet']['PTypeId']): ?>selected="selected"<?php endif; ?> value="<?php echo $this->_tpl_vars['CatDet']['PTypeId']; ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['CatDet']['ProfileNumber'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</option> 
					 <?php endforeach; unset($_from); endif; ?>
					</select> 
					<br /><span id="PTypeIdMsg" class="error"></span>
				</div>
			</div> 
			<div class="FormDiv">
				<div class="FormDet1">Finish</div>
				<div class="FormDet2">:</div>
				<div class="FormDet3">
					<select name="FinishId" id="FinishId">
					<option value="">Select Finish</option>
					 <?php if (count($_from = (array)$this->_tpl_vars['FinList'])):
    foreach ($_from as $this->_tpl_vars['CatDet']):
?> 
					 <option <?php if ($this->_tpl_vars['ArrD']['FinishId'] == $this->_tpl_vars['CatDet']['FinishId']): ?>selected="selected"<?php endif; ?> value="<?php echo $this->_tpl_vars['CatDet']['FinishId']; ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['CatDet']['FinishType'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</option> 
					 <?php endforeach; unset($_from); endif; ?>
					</select>
 				<br /><span id="FinishIdMsg" class="error"></span>
				</div>
			</div>
		<div class="clearspace"></div></div><div class="clearspace"></div></div>
		<div style="padding-left:50px;"><div style="width:600px; float:left;">  
 			<div class="FormDiv">
				<div class="FormDet1">Glass Group</div>
				<div class="FormDet2">:</div>
				<div class="FormDet3">
				<input type="text" name="GlassGroup" id="GlassGroup" class="txtbox1" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['ArrD']['GlassGroup'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
" onkeypress="return AllowOnlyNumbers(event);" />
				<br /><span id="GlassGroupMsg" class="error"></span>
				</div>
			</div>  
 			<div class="FormDiv">
				<div class="FormDet1">Minimum sqft</div>
				<div class="FormDet2">:</div>
				<div class="FormDet3">
				<input type="text" name="Sqfeet" id="Sqfeet" class="txtbox1" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['ArrD']['Sqfeet'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
" onkeypress="return AllowOnlyNumbers(event);" />
				<br /><span id="SqfeetMsg" class="error"></span>
				</div>
			</div>  
 			<div class="FormDiv">
				<div class="FormDet1">Door Price (Suggested Price)</div>
				<div class="FormDet2">:</div>
				<div class="FormDet3">$
				<input type="text" name="DoorPrice" id="DoorPrice" class="txtbox1" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['ArrD']['DoorPrice'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
" onkeypress="return AllowOnlyCurrency(event);" />
				<br /><span id="DoorPriceMsg" class="error"></span>
				</div>
			</div> 
			<div class="FormDiv">
				<div class="FormDet1">Lesportes Percentage</div>
				<div class="FormDet2">:</div>
				<div class="FormDet3">
				<input type="text" name="DLPercent" id="DLPercent" style="width:45px;" class="txtbox1" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['ArrD']['DLPercent'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
" />&nbsp;%
				<br /><span id="DLPercentMsg" class="error"></span>
				</div>
			</div> 
			<div class="FormDiv">
				<div class="FormDet1">BARAZIN Percentage</div>
				<div class="FormDet2">:</div>
				<div class="FormDet3">
				<input type="text" name="DBPercent" id="DBPercent" style="width:45px;" class="txtbox1" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['ArrD']['DBPercent'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
" />&nbsp;%
				<br /><span id="DBPercentMsg" class="error"></span>
				</div>
			</div>
  			<!--<div class="FormDiv">
				<div class="FormDet1">Barazin Price</div>
				<div class="FormDet2">:</div>
				<div class="FormDet3">$
				<input type="text" name="BarazinPrice" id="BarazinPrice" class="txtbox1" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['ArrD']['BarazinPrice'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
" onkeypress="return AllowOnlyCurrency(event);"/>
				<br /><span id="BarazinPriceMsg" class="error"></span>
				</div>
			</div>  -->
 			<div class="FormDiv">
				<div class="FormDet1">&nbsp;</div>
				<div class="FormDet2">&nbsp;</div>
				<div class="FormDet3">
	<?php if ($_GET['con_id'] != ''): ?>
		<input type="button" name="Update" value="Update"class="btn" onclick="return GoToFormValidate('2');" />
		<input type="hidden" name="AdminAction" value="Update"class="btn" />
	<?php else: ?>
		<input type="hidden" name="AdminAction" value="Add"class="btn" />
		<input type="button" name="Add" value="Add"class="btn" onclick="return GoToFormValidate('1');" />
	<?php endif; ?>
	&nbsp;<input type="button" name="goback" value="GoBack" class="btn" onClick="GoBack('doorprices_mgmt.php');">
				</div>
			</div>
		<div class="clearspace"></div></div><div class="clearspace"></div></div>
	</div><div class="clearspace"></div></div>
	</form>  
<?php echo ' 
<script type="text/javascript">
var FormName=\'add_pro\';
var FormObj=document.forms[FormName];
var MgmtName=\'Product\';  
</script>
'; ?>