<?php /* Smarty version 2.6.3, created on 2013-08-21 09:41:18
         compiled from mail_temp_new.html */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'stripslashes', 'mail_temp_new.html', 42, false),)), $this); ?>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['TinyMCEPath']; ?>
ckeditor/ckeditor.js"></script>
<?php echo '
<script src="PageStyleScript/SiteAdminValidateFunction.js" type="text/javascript"></script>
<script type="text/javascript">
var AllowExtension= new Array();
function GoToFormValidate(ValiType)
{	
	ErrCss=\'txtboxerr\';var FlagIns=true;
	FlagIns=FormValidateFunction(\'Text\',\'#TempTitle\',\'Please Enter Title\');
	if(FlagIns)
	{	
		FlagIns=FormValidateFunction(\'Text\',\'#MailSub\',\'Please Enter Subject\');
	}
	if(FlagIns){	
		FlagIns=FormValidateFunction(\'TinyMce\',\'MailCotent\',\'Please Enter Content\');
	}
	if(FlagIns)
	{	
		document.forms[FormName].submit();
	}
	else{	return false;	}
}
</script>
'; ?>


<div id="page-heading"><h1><?php echo $this->_tpl_vars['PageTitle']; ?>
</h1></div>
<div class="clearspace"></div>
<form name="post_from" method="post" action=""><div class="body_head">			
		<div class="TutProWid2"><div style="padding-left:50px;"><div style="width:600px;">
        <?php if ($this->_tpl_vars['SucMessage'] != '' || $this->_tpl_vars['ErrMessage'] != ''): ?>	
            <div class="AdmFleft SeaTotBox">							
                <div class="Msg_Div">
                <?php if ($this->_tpl_vars['ErrMessage'] != ''): ?><div class="Msg_error"><?php echo $this->_tpl_vars['ErrMessage']; ?>
</div>
                <?php elseif ($this->_tpl_vars['SucMessage'] != ''): ?><div class="Msg_success"><?php echo $this->_tpl_vars['SucMessage']; ?>
</div><?php endif; ?>
                </div>
            </div><div class="spacer"></div>
        <?php endif; ?>
            <div class="FormDiv">
                <div class="FormDet1">Title</div>
                <div class="FormDet2">:</div>
                <div class="FormDet3">	
<input type="text" name="TempTitle" id="TempTitle" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['MTempList']['TempTitle'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
" style="width:350px;" readonly="" />		<br /><span id="TempTitleMsg" class="error"></span>
                </div>
            </div>
            <div class="FormDiv">
                <div class="FormDet1">Subject</div>
                <div class="FormDet2">:</div>
                <div class="FormDet3">
<input type="text" name="MailSub" id="MailSub" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['MTempList']['MailSub'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
" style="width:350px;"/>													
                    <br /><span id="MailSubMsg" class="error"></span>
                </div>
            </div>
		<div class="clearspace"></div></div><div class="clearspace"></div></div>
		<div class="FormDiv">
			<div class="FormDet1" style="width:27%">Mail Content</div>
			<div class="FormDet2">:</div>
			<div class="FormDet3">
<textarea name="MailCotent" id="MailCotent" rows="5" class="ckeditor"><?php echo ((is_array($_tmp=$this->_tpl_vars['MTempList']['MailCotent'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</textarea>
			<br /><span id="MailCotentMsg" class="error"></span>
			</div>
		</div>
		<div style="padding-left:50px;"><div style="width:600px; float:left;">	
            <div class="FormDiv">
                <div class="FormDet1">Notify</div>
                <div class="FormDet2">:</div>
                <div class="FormDet3">
<input type="radio" name="MailNotify" value="1" checked="checked"/>On
<input type="radio" name="MailNotify" value="0"<?php if ($this->_tpl_vars['MTempList']['MailNotify'] == '0'): ?> checked="checked"<?php endif; ?>/>Off<br />
                    <span id="msg4" class="error"></span>
                </div>
            </div>
            <div class="FormDiv">
                <div class="FormDet1">&nbsp;</div>
                <div class="FormDet2">&nbsp;</div>
                <div class="FormDet3">
    <?php if ($_GET['con_id'] != ''): ?>
        <input type="button" name="AltPage" value="Update"class="btn" onclick="GoToFormValidate();" />
        <input type="hidden" name="AdminAction" value="Update"class="btn" />
    <?php else: ?>
        <input type="hidden" name="AdminAction" value="Add"class="btn" />
        <input type="button" name="AltPage" value="Add"class="btn" onclick="GoToFormValidate();" />
    <?php endif; ?>
&nbsp;<input type="button" name="goback" value="GoBack Mail Template Home" class="btn"  onClick="GoBack('mail_temp_mgmt.php');"/>
                
                </div>
            </div><div class="spacer"></div>
               
		</div></div></div>
 	</div></form>
<?php echo '
<script type="text/javascript">
var FormName=\'post_from\';
var FormObj=document.forms[FormName];
var MgmtName=\'Mail\';
</script>
'; ?>