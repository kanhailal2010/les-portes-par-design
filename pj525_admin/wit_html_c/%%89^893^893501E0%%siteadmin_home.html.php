<?php /* Smarty version 2.6.3, created on 2013-04-27 13:38:07
         compiled from siteadmin_home.html */ ?>
<link href="PageStyleScript/chart.css" rel="stylesheet" type="text/css" media="all"> 
<link href="PageStyleScript/datepicker.css" rel="stylesheet" type="text/css" media="all">  
<link href="PageStyleScript/tipsy.css" rel="stylesheet" type="text/css" media="all">  
<link href="PageStyleScript/visualize.css" rel="stylesheet" type="text/css" media="all"> 	
<!--[if IE]>
		<link href="css/ie.css" rel="stylesheet" type="text/css" media="all">
		<script type="text/javascript" src="js/excanvas.js"></script>
<![endif]-->
<script type="text/javascript" src="PageStyleScript/jquery-ui.js"></script>
<script type="text/javascript" src="PageStyleScript/chart.js"></script>
<script type="text/javascript" src="PageStyleScript/jquery.img.preload.js"></script> 
<script type="text/javascript" src="PageStyleScript/visualize.js"></script> 
<script type="text/javascript" src="PageStyleScript/tipsy.js"></script>
<div class="mid_body AdmFleft">
	<div class="AdmFleft">
		<div class="con_head">
			<div class="con_head1"></div>
			<div class="con_head2"><div class="HeadFont14">Admin Panel</div></div>
			<div class="con_head3"></div>
		</div>
		<div class="clearspace"></div>		
		<div class="body_head PadA10">
			<div class="HeadFont14">Welcome to Secure Administration Suite</div>
			 <div class="inner1"> 

            <div class="onecolumn">
                <div class="header1">
                    <span>Visitor Statistics</span>
                    <div class="switch" style="width:200px">
                    <table width="200px" cellpadding="0" cellspacing="0">
                    <tbody>
						<tr>
						<td>
				<input type="button" id="chart_bar" name="chart_bar" class="left_switch active" value="Bar" style="width:50px"/>
						</td>
						
						<td>
				<input type="button" id="chart_area" name="chart_area" class="middle_switch" value="Area" style="width:50px"/>
						</td>
						<td>
				<input type="button" id="chart_pie" name="chart_pie" class="middle_switch" value="Pie" style="width:50px"/>
						</td>
						<td>
				<input type="button" id="chart_line" name="chart_line" class="right_switch" value="Line" style="width:50px"/>
						</td>
						
						</tr>
                    </tbody>
                    </table>
                    </div>
		</div>
                <br class="clear"/>
                 <div class="content">
                    <div id="chart_wrapper" class="chart_wrapper"></div>
		      <table id="graph_data" class="data" rel="bar" cellpadding="0" cellspacing="0" width="100%">
        		<caption>Visitor Statistics</caption>
			<thead>
				<tr>
					<th>Yearly</th>
					<th>Monthly</th>
					<th>Weekly</th>
					<th>Daily</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><?php echo $this->_tpl_vars['YearTotal']; ?>
</td>
					<td><?php echo $this->_tpl_vars['MonTotal']; ?>
</td>
					<td><?php echo $this->_tpl_vars['WeekTotal']; ?>
</td>
					<td><?php echo $this->_tpl_vars['DayTotal']; ?>
</td>
 				</tr>
  			 </tbody>
		  </table>
                  <div id="chart_wrapper" class="chart_wrapper"></div>
                 </div>
            </div>
 			</div>
            
			<div class="clearspace"></div>
		</div>			
	</div>
	<div class="clearspace"></div>	
</div>