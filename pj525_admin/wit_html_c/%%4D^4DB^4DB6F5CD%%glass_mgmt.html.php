<?php /* Smarty version 2.6.3, created on 2013-05-03 08:50:41
         compiled from glass_mgmt.html */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'stripslashes', 'glass_mgmt.html', 8, false),array('modifier', 'md5', 'glass_mgmt.html', 90, false),array('modifier', 'date_format', 'glass_mgmt.html', 130, false),)), $this); ?>
<script src="PageStyleScript/SiteAdminCommonFunction.js" type="text/javascript"></script>
<div id="page-heading"><h1><?php echo $this->_tpl_vars['PageTitle']; ?>
</h1></div>
<div class="body_head PadLR10">			
	<div class="InnDiv940 AdmFleft">	
		<div class="AdmFleft" style="width:960px;">		<form name="frm_search" method="get" action="">
				<div class="PadRLB5 AdmFleft">
				<div class="HeadFont12 AdmFleft PadRLB5">
Glass Name&nbsp;:&nbsp;<input type="text" name="pronamesea" value="<?php echo ((is_array($_tmp=$_GET['pronamesea'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
"/></div>
				<div class="HeadFont12 AdmFleft PadRLB5">
Code&nbsp;:&nbsp;<input type="text" name="code" value="<?php echo ((is_array($_tmp=$_GET['code'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
"/></div>
					 
					<div class="HeadFont12 AdmFleft PadRLB5">
						<input type="submit" name="seabut" value="Search" class="btn"/></div>	
					<div class="HeadFont12 AdmFleft PadRLB5">
<input type="button" name="seabut" value="Show All" class="btn" onClick="GoBackRedirct('glass_mgmt.php');" /></div>		
				</div>
			</form>		</div>
		<div class="AdmFleft" style="width:800px;"><div class="NewMsgDiv">	
		<?php if ($this->_tpl_vars['SucMessage'] != '' || $this->_tpl_vars['ErrMessage'] != ''): ?>	
			<?php if ($this->_tpl_vars['ErrMessage'] != ''): ?>
				<div class="Msg_error">
					<div class="AdmFleft"><?php echo $this->_tpl_vars['ErrMessage']; ?>
</div>
					<div class="AdmFRight"><a class="close-msg"><img src="images/close-button.png" alt="" /></a></div>
				</div>
			<?php elseif ($this->_tpl_vars['SucMessage'] != ''): ?>
				<div class="Msg_success">
					<div class="AdmFleft"><?php echo $this->_tpl_vars['SucMessage']; ?>
</div>
					<div class="AdmFRight"><a class="close-msg"><img src="images/close-button.png" alt="" /></a></div>
				</div>
			<?php endif; ?>
			<div class="spacer"></div>
		<?php endif; ?>
		</div></div>
		<div style="width:160px; float:left; padding:8px 0;">
			<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "AdminControlMenu_New.html", 'smarty_include_vars' => array('title' => 'admin control list settings')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
		</div>
		<div class="clearspace"></div>
	</div>
	<div class="clearspace"></div>
	<div id="Errmsgval"></div>
<form name="frm_list" method="post" action="">
		<input type="hidden" name="Action_Type" />
		<input type="hidden" name="ConSId"/>	
	<div class="clearspace"></div>
 	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
		<tr>
			<th class="table-header-cornerL"><b>S.No</b></th>
			<th class="table-header-repeat line-left"><b>&nbsp;Glass Name</b></th>
			<th class="table-header-repeat line-left"><b>&nbsp;Code</b></th> 
			<th class="table-header-repeat line-left"><b>&nbsp;Glass Group</b></th>
			<th class="table-header-repeat line-left"><b>&nbsp;Thickness</b></th>
			<th class="table-header-repeat line-left"><b>&nbsp;Minimum sqft</b></th>
			<th class="table-header-repeat line-left"><b>Status</b></th>
			<th class="table-header-cornerR line-left">Actions</th>
		</tr>
		<tfoot><tr><td colspan="8" align="right"><?php echo $this->_tpl_vars['GlassPage']; ?>
</td></tr></tfoot>
		<tbody>
		<?php $this->assign('i', $this->_tpl_vars['PageStartNum']); ?>
			<?php if (count($_from = (array)$this->_tpl_vars['ProList'])):
    foreach ($_from as $this->_tpl_vars['TableDet']):
?>
		<tr <?php if ($this->_tpl_vars['i']%2 != '0'): ?> class="alternate-row" id="order_<?php echo $this->_tpl_vars['TableDet']['GlassId']; ?>
"<?php endif; ?>>
		<td width="8%"><input type="checkbox" name="ConId[]" value="<?php echo $this->_tpl_vars['TableDet']['GlassId']; ?>
" /><?php echo $this->_tpl_vars['i']++; ?>
.</td>
					<td width="23%"><?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['GlassName'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</td>
                    <td width="12%"><?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['GlassCode'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</td>  
                    <td width="12%"><?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['GlassGroup'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</td>  
                    <td width="11%"><?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['Thickness'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</td> 
                    <td width="11%"><?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['Sqfeet'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</td> 
					<td width="10%">
						<?php if ($this->_tpl_vars['TableDet']['GlassStatus'] == 1): ?>
					<span id="InActive_<?php echo $this->_tpl_vars['TableDet']['GlassId']; ?>
">
						<a href="javascript:;" onClick="GoToJSONAjaxFunction('OFF','<?php echo $this->_tpl_vars['TableDet']['GlassId']; ?>
','GlassMgmt','Glass(s)')">
						<img class="tik" src="images/on_btn.png" /></a>
					</span>
					<span id="Active_<?php echo $this->_tpl_vars['TableDet']['GlassId']; ?>
" style="display:none;">
						<a href="javascript:;" onClick="GoToJSONAjaxFunction('ON','<?php echo $this->_tpl_vars['TableDet']['GlassId']; ?>
','GlassMgmt','Glass(s)')">
						<img class="tik" src="images/off_btn.png" /></a>
					</span>
					<?php else: ?>
					<span id="InActive_<?php echo $this->_tpl_vars['TableDet']['GlassId']; ?>
" style="display:none;">
						<a href="javascript:;" onClick="GoToJSONAjaxFunction('OFF','<?php echo $this->_tpl_vars['TableDet']['GlassId']; ?>
','GlassMgmt','Glass(s)')">
						<img  border="0" src="images/on_btn.png" /></a>
					</span>
					<span id="Active_<?php echo $this->_tpl_vars['TableDet']['GlassId']; ?>
">
						<a href="javascript:;" onClick="GoToJSONAjaxFunction('ON','<?php echo $this->_tpl_vars['TableDet']['GlassId']; ?>
','GlassMgmt','Glass(s)')">
						<img border="0" src="images/off_btn.png" /></a>
					</span> 
					<?php endif; ?>
					</td>
				<td width="13%">
				<div class="gallery options-width">
					<a title="Edit" class="icon-1 info-tooltip" href="glass_mgmt.php?act_type=addnew&edit=1&con_id=<?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['GlassId'])) ? $this->_run_mod_handler('md5', true, $_tmp) : md5($_tmp)); ?>
"></a>
					<a title="Delete" class="icon-2 info-tooltip" href="javascript:;" 
					onclick="AdminDeleteFunction('<?php echo $this->_tpl_vars['TableDet']['GlassId']; ?>
');"></a>
					
					<div id="MyShow_<?php echo $this->_tpl_vars['TableDet']['GlassId']; ?>
">
					<a title="Show" class="icon-3 info-tooltip" href="javascript:;"  onclick="GoToViewFunction('<?php echo $this->_tpl_vars['TableDet']['GlassId']; ?>
','MyHide','MyShow');"></a>
				</div>					
				<div id="MyHide_<?php echo $this->_tpl_vars['TableDet']['GlassId']; ?>
" style="display:none;">
					<a title="Hide" class="icon-3 info-tooltip" href="javascript:;" onclick="GoToHideFunction('<?php echo $this->_tpl_vars['TableDet']['GlassId']; ?>
','MyShow','MyHide');"></a>
				</div>  
				</div>							
				</td>	
			<tr  id="ResIdDiv_<?php echo $this->_tpl_vars['TableDet']['GlassId']; ?>
" style="display:none;" class="ViewDet" >					
			<td colspan="8">
			<div class="HeadYBord14">Details of '<?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['ItemName'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
' Glass</div>
			<div class="clearspace"></div>
						<div style="width:940px;float:left; padding-top:10px;" >
							<div style="width:500px; float:left; padding-top:10px;">
						<div class="CMDet"> 	 	 	
								<div class="CMDet1">Glass Code</div>
								<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['GlassCode'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
							</div>
							<div class="CMDet">
								<div class="CMDet1">Glass Name</div>
								<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['GlassName'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
							</div> 
							<div class="CMDet">
								<div class="CMDet1">Thickness</div>
								<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['Thickness'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
							</div>
							<div class="CMDet">
								<div class="CMDet1">Minimum sqft</div>
								<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['Sqfeet'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
							</div>
							<div class="CMDet">
								<div class="CMDet1">Glass Group</div>
								<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['GlassGroup'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
							</div> 
							<div class="CMDet">
								<div class="CMDet1">Post Date</div>
								<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['GlassPostDate'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d,%b %Y") : smarty_modifier_date_format($_tmp, "%d,%b %Y")); ?>
</div>
							</div>
  							<div class="CMDet">
								<div class="CMDet1">Status</div>
								<div class="CMDet2">:&nbsp;<?php if ($this->_tpl_vars['TableDet']['GlassStatus'] == '1'): ?>ON<?php else: ?>OFF<?php endif; ?></div>
							</div> 
							</div>
							<div style="width:400px; float:left; padding-top:10px;">
							<div class="CMDet">
								<div class="CMDet1">Lesportes Regular Price</div>
								<div class="CMDet2">:&nbsp;$<?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['LRegularPrice'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
							</div>
							<div class="CMDet">
								<div class="CMDet1">Lesportes Tempered Price</div>
								<div class="CMDet2">:&nbsp;$<?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['LTemperedPrice'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
							</div>
							<div class="CMDet">
								<div class="CMDet1">Lesportes Percentage</div>
								<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['LPercent'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
%</div>
							</div>
							<div class="CMDet">
								<div class="CMDet1">Barazin Percentage</div>
								<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['BPercent'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
%</div>
							</div>
 							<div class="CMDet">
								<div class="CMDet1">Glass Image</div>
								<div class="CMDet2">:&nbsp;<?php if ($this->_tpl_vars['TableDet']['GlassImage'] != ''): ?>	<img border='0' src="<?php echo $this->_tpl_vars['GlassImageDisp']; ?>
m_<?php echo $this->_tpl_vars['TableDet']['GlassImage']; ?>
"/><?php else: ?>No Image <?php endif; ?></div>
							</div>
							</div>
				<div class="clearspace"></div>						
			</div>
			   </td></tr>
			</tr>
		
		<?php endforeach; unset($_from); else: ?>
			<tr><td colspan="8" align="center" class="error_msg"><strong>No Glass Found ..</strong></td></tr>					
		<?php endif; ?>						
		</tbody>
	</table>			
	<div class="clearspace"></div>
</form>	
</div>
<?php echo '
<script type="text/javascript">
var FormName=\'frm_list\';
var FormObj=document.forms[FormName];
var MgmtName=\'Glass(s)\';
var Seltype=\'GlassMgmt\';

</script>
'; ?>