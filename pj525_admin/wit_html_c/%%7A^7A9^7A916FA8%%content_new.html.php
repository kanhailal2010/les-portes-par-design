<?php /* Smarty version 2.6.3, created on 2013-05-04 05:17:43
         compiled from content_new.html */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'stripslashes', 'content_new.html', 57, false),)), $this); ?>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['TinyMCEPath']; ?>
ckeditor/ckeditor.js"></script>
<script src="PageStyleScript/SiteAdminValidateFunction.js" type="text/javascript"></script>
<?php echo '
<script type="text/javascript">
var SiteMainPath="{SiteMainPath}";
var AllowExtension= new Array();
function GoToFormValidate(ValiType)
{	
	var FlagIns=true;ErrCss=\'txtboxerr\';
	FlagIns=FormValidateFunction(\'Text\',\'#page_title\',\'Please enter title\');
	if(FlagIns)
	{	
		FlagIns=FormValidateFunction(\'TinyMce\',\'page_content\',\'Please enter content description\');
	}
	if(FlagIns)
	{	
		FlagIns=FormValidateFunction(\'Text\',\'#CMetaKey\',\'Please enter meta key\');
	}
	if(FlagIns)
	{	
		FlagIns=FormValidateFunction(\'Text\',\'#CMetaDesc\',\'Please enter meta  description\');
	}
	if(FlagIns)
	{	
		document.forms[FormName].submit();
	}
	else{	return false;	}
}
 
</script>
'; ?>

<div id="page-heading"><h1><?php echo $this->_tpl_vars['PageTitle']; ?>
</h1></div>
<div class="clearspace"></div>
<form name="FormAddUpdate" method="post" action=""><div class="body_head">			
	<div class="TutProWid2"><div style="padding-left:50px;"><div style="width:600px;">
	<?php if ($this->_tpl_vars['SucMessage'] != '' || $this->_tpl_vars['ErrMessage'] != ''): ?>	
		<div class="FormDiv">							
			<div class="Msg_Div">
			<?php if ($this->_tpl_vars['ErrMessage'] != ''): ?><div class="Msg_error"><?php echo $this->_tpl_vars['ErrMessage']; ?>
</div>
			<?php elseif ($this->_tpl_vars['SucMessage'] != ''): ?><div class="Msg_success"><?php echo $this->_tpl_vars['SucMessage']; ?>
</div><?php endif; ?>
			</div>
		</div><div class="spacer"></div>
	<?php endif; ?>							
		<div class="FormDiv">
			<div class="FormDet1">Title</div>
			<div class="FormDet2">:</div>
			<div class="FormDet3">
<input type="text" name="page_title" id="page_title"  class="txtbox1" value="<?php echo $this->_tpl_vars['Arr']['page_title']; ?>
" style="width:350px;" maxlength="100"/>
	<br /><span id="page_titleMsg" class="error"></span>
			</div>
		</div>
		<div class="clearspace"></div></div><div class="clearspace"></div></div>
		<div class="FormDiv">
			<div class="FormDet1" style="width:27%">Description </div>
			<div class="FormDet2">:</div>
			<div class="FormDet3">
<textarea name="page_content" id="page_content" class="ckeditor" rows="12" cols="40"><?php echo ((is_array($_tmp=$this->_tpl_vars['Arr']['page_content'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</textarea>
			<br /><span id="page_contentMsg" class="error"></span> 	
			</div>
		</div>	
		<div style="padding-left:50px;"><div style="width:600px; float:left;">					
		<div class="FormDiv"> 	
			<div class="FormDet1">Meta Key</div>
			<div class="FormDet2">:</div>
			<div class="FormDet3">
<textarea name="CMetaKey" id="CMetaKey" rows="10" cols="45"><?php echo ((is_array($_tmp=$this->_tpl_vars['Arr']['CMetaKey'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</textarea>
			<br /><span id="CMetaKeyMsg" class="error"></span>
			</div>
		</div>
		<div class="FormDiv"> 	
			<div class="FormDet1">Meta  Description</div>
			<div class="FormDet2">:</div>
			<div class="FormDet3">
<textarea name="CMetaDesc" id="CMetaDesc" rows="10" cols="45"><?php echo ((is_array($_tmp=$this->_tpl_vars['Arr']['CMetaDesc'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</textarea>
			<br/><span id="CMetaDescMsg" class="error"></span>
			</div>
		</div>
		<div class="FormDiv">
			<div class="FormDet1">&nbsp;</div>
			<div class="FormDet2">&nbsp;</div>
			<div class="FormDet3">
<?php if ($_GET['con_id'] != ''): ?>
<input type="submit" name="AltPage" value="Update <?php echo $this->_tpl_vars['PageButName']; ?>
"class="btn" onclick="return GoToFormValidate();" />
<input type="hidden" name="AdminAction" value="Update"class="btn" />
<?php else: ?>
<input type="hidden" name="AdminAction" value="Add"class="btn" />
<input type="submit" name="AltPage" value="Add <?php echo $this->_tpl_vars['PageButName']; ?>
"class="btn" onclick="return GoToFormValidate();" />
<?php endif; ?>
&nbsp;<input type="button" name="goback" value="GoBack <?php echo $this->_tpl_vars['PageButName']; ?>
" class="btn" onClick="GoBack('<?php echo $this->_tpl_vars['RedirFileName']; ?>
.php');" />
			</div>
		</div>
	</div></div></div>			
	<div class="clearspace"></div>
</div></form>	
<?php echo '
<script type="text/javascript">
var FormName=\'FormAddUpdate\';
var FormObj=document.forms[FormName];
var MgmtName=\'Content\';
</script>
'; ?>