<?php /* Smarty version 2.6.3, created on 2013-08-17 12:15:02
         compiled from profile_insert.html */ ?>
<div id="page-heading"><h1> Profile Insert Management </h1></div>
<div class="body_head PadLR10">
	<div class="InnDiv940 AdmFleft">
                <?php if ($this->_tpl_vars['SucMessage'] != '' || $this->_tpl_vars['ErrMessage'] != ''): ?>	
                    <div class="AdmFleft SeaTotBox">							
                        <div class="Msg_Div">
                        <?php if ($this->_tpl_vars['ErrMessage'] != ''): ?><div class="Msg_error"><?php echo $this->_tpl_vars['ErrMessage']; ?>
</div>
                        <?php elseif ($this->_tpl_vars['SucMessage'] != ''): ?><div class="Msg_success"><?php echo $this->_tpl_vars['SucMessage']; ?>
</div><?php endif; ?>
                        </div>
                    </div><div class="spacer"></div>
                <?php endif; ?>
		<form action="profile_insert.php?act_type=<?php echo $this->_tpl_vars['act_type']; ?>
" method="post" enctype="multipart/form-data">
		<input type="hidden" name="insert_id" value="<?php echo $this->_tpl_vars['edit']['insert_id']; ?>
">
		<div class="FormDiv">
				<div class="FormDet1">Profile Insert Title</div>
				<div class="FormDet2">:</div>
				<div class="FormDet3">
				<input type="text" name="dptitle" value="<?php echo $this->_tpl_vars['edit']['insert_title']; ?>
">
				</div>
		</div>
		<div class="clearspace"></div>
			
		<div class="FormDiv">
				<div class="FormDet1">Profile Insert Code</div>
				<div class="FormDet2">:</div>
				<div class="FormDet3">
				<input type="text" name="dpcode" value="<?php echo $this->_tpl_vars['edit']['insert_code']; ?>
">
				</div>
		</div>
		<div class="clearspace"></div>
			
		<div class="FormDiv">
				<div class="FormDet1">Profile Insert Factor</div>
				<div class="FormDet2">:</div>
				<div class="FormDet3">
				<input type="text" name="dpfactor" value="<?php echo $this->_tpl_vars['edit']['insert_factor']; ?>
">
				</div>
		</div>
		<div class="clearspace"></div>
			
		<div class="FormDiv">
				<div class="FormDet1">Insert Available with </div>
				<div class="FormDet2">:</div>
				<div class="FormDet3">  <?php echo $this->_tpl_vars['profile_dropdown']; ?>
	</div>
		</div>
		<div class="clearspace"></div>
			
		<div class="FormDiv">
				<div class="FormDet1">Profile Insert Image</div>
				<div class="FormDet2">:</div>
				<div class="FormDet3">
				<?php if ($this->_tpl_vars['edit']['insert_image'] != ''): ?>
							<img src="<?php echo $this->_tpl_vars['SiteHttpPath'];   echo INSERT_FOLDER.'thumbnail/';  echo $this->_tpl_vars['edit']['insert_image']; ?>
" width="150">
						<?php endif; ?>
						<input class="btn" type="file" name="dpimage"  value="">
				</div>
			</div>
			<div class="clearspace"></div>
			
			<div class="FormDiv">
				<div class="FormDet1">&nbsp; </div>
				<div class="FormDet2">&nbsp; </div>
				<div class="FormDet3">
				<input type="submit" class="btn" name="submit" value="submit">
				<button class="btn" onclick="window.location='profile_insert.php'; return false;"> Cancel </button>
				</div>
			</div>
			<div class="clearspace"></div>
			
			
			</form>
			</div> <!-- .InnDiv940 AdmFleft -->
			<hr>
			<table id="product-table" width="100%" border="0" cellspacing="0" cellpadding="0">
				<thead>
				<tr>
					<th class="table-header-cornerL"> Profile Insert Name</th>
					<th class="table-header-repeat line-left"> Profile Insert Factor</th>
					<th class="table-header-repeat line-left"> Profile Insert Image</th>
					<th class="table-header-cornerR line-left">Action</th>
				</tr>
				</thead>
				<tbody>
				
				<?php if (count($_from = (array)$this->_tpl_vars['profile_insert'])):
    foreach ($_from as $this->_tpl_vars['f']):
?>
					<tr>
						<td>
						<a href="profile_sub_insert.php?insert_id=<?php echo $this->_tpl_vars['f']['insert_id']; ?>
"> <?php echo $this->_tpl_vars['f']['insert_title']; ?>
 </a>
						<br/>
						<!-- <small>Available With: <?php  echo $f['insert_title'];  ?></small> -->
						 <small>
							Available With: 
							<?php   
								$avw = $this->get_template_vars(f); //var_dump($avw);
								if($avw['insert_available_with']!='[""]')
								{
									$avw = objectToArray( json_decode($avw['insert_available_with']));
									if($avw[0]=='') unset($avw[0]);
									echo implode(',',$avw);
								}
								else
								echo "All";
							 ?>
						 </small> 
						</td>
						<td><?php echo $this->_tpl_vars['f']['insert_factor']; ?>
</td>
						<td><img src="<?php echo $this->_tpl_vars['SiteHttpPath'];   echo INSERT_FOLDER.'thumbnail/';  echo $this->_tpl_vars['f']['insert_image']; ?>
" height="80"></td>
						<td>
							<div class="gallery options-width">
								<a class="icon-1 info-tooltip" href="profile_insert.php?act_type=edit&insert_id=<?php echo $this->_tpl_vars['f']['insert_id']; ?>
"></a>
								<a class="icon-2 info-tooltip" href="profile_insert.php?act_type=delete&insert_id=<?php echo $this->_tpl_vars['f']['insert_id']; ?>
" onclick="return confirm('Please Confirm Deletion !!')"></a>
							</div>
						</td>
					</tr>
				<?php endforeach; unset($_from); endif; ?>	
				</tbody>
			</table>
</div>