<?php /* Smarty version 2.6.3, created on 2013-05-05 16:54:56
         compiled from mail_temp_mgmt.html */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'stripslashes', 'mail_temp_mgmt.html', 39, false),array('modifier', 'html_entity_decode', 'mail_temp_mgmt.html', 40, false),array('modifier', 'truncate', 'mail_temp_mgmt.html', 40, false),array('modifier', 'md5_enc', 'mail_temp_mgmt.html', 43, false),)), $this); ?>
<script src="PageStyleScript/SiteAdminCommonFunction.js" type="text/javascript"></script>
<div id="page-heading"><h1><?php echo $this->_tpl_vars['PageTitle']; ?>
</h1></div>
<div class="body_head PadLR10">	
<div class="AdmFleft" style="width:800px;"><div class="NewMsgDiv">	
		<?php if ($this->_tpl_vars['SucMessage'] != '' || $this->_tpl_vars['ErrMessage'] != ''): ?>	
			<?php if ($this->_tpl_vars['ErrMessage'] != ''): ?>
				<div class="Msg_error">
					<div class="AdmFleft"><?php echo $this->_tpl_vars['ErrMessage']; ?>
</div>
					<div class="AdmFRight"><a class="close-msg"><img src="images/close-button.png" alt="" /></a></div>
				</div>
			<?php elseif ($this->_tpl_vars['SucMessage'] != ''): ?>
				<div class="Msg_success">
					<div class="AdmFleft"><?php echo $this->_tpl_vars['SucMessage']; ?>
</div>
					<div class="AdmFRight"><a class="close-msg"><img src="images/close-button.png" alt="" /></a></div>
				</div>
			<?php endif; ?>
			<div class="spacer"></div>
		<?php endif; ?>
		</div></div>		
	<div class="InnDiv940 AdmFleft">			
		<div class="clearspace"></div>
		<form name="frm_list" method="post">
		<input type="hidden" name="Action_Type" >
		<input type="hidden" name="ConSId"/>
			<div class="clearspace"></div>
			<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
				<thead><tr bgcolor="#D6D6D6">
					<th class="table-header-cornerL"><b>S.No</b></th>
					<th class="table-header-repeat line-left"><b>Title</b></th>
					<th class="table-header-repeat line-left"><b>Subject</b></th>
					<th class="table-header-cornerR line-left"><b>Actions</b></th>						
				</tr></thead>
				<tfoot><tr><td colspan="4" align="right"><?php echo $this->_tpl_vars['PayRPage']; ?>
</td></tr></tfoot>
				<tbody>
				<?php $this->assign('i', $this->_tpl_vars['PageStartNum']); ?>
				<?php if (count($_from = (array)$this->_tpl_vars['MTempList'])):
    foreach ($_from as $this->_tpl_vars['ConDet']):
?>
					<tr bgcolor="<?php if ($this->_tpl_vars['i']%2 != '0'): ?>#FFFFFF<?php else: ?>#F3F3F3<?php endif; ?>"> 
						<td width="10%"><?php echo $this->_tpl_vars['i']++; ?>
.</td>
						<td width="30%"><?php echo ((is_array($_tmp=$this->_tpl_vars['ConDet']['TempTitle'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</td>
						<td width="40%"><?php echo ((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['ConDet']['MailSub'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)))) ? $this->_run_mod_handler('html_entity_decode', true, $_tmp) : html_entity_decode($_tmp)))) ? $this->_run_mod_handler('truncate', true, $_tmp, '30') : smarty_modifier_truncate($_tmp, '30')); ?>
</td>
						<td width="20%">
						<div class="gallery options-width">
							<a title="Edit" class="icon-1 info-tooltip" href="mail_temp_mgmt.php?act_type=addnew&con_id=<?php echo ((is_array($_tmp=$this->_tpl_vars['ConDet']['MTempId'])) ? $this->_run_mod_handler('md5_enc', true, $_tmp) : smarty_modifier_md5_enc($_tmp)); ?>
"></a>
							
							<div id="MyShow_<?php echo $this->_tpl_vars['ConDet']['MTempId']; ?>
">
					<a title="Show" class="icon-3 info-tooltip" href="javascript:;"  onclick="GoToViewFunction('<?php echo $this->_tpl_vars['ConDet']['MTempId']; ?>
','MyHide','MyShow');"></a>
				</div>					
				<div id="MyHide_<?php echo $this->_tpl_vars['ConDet']['MTempId']; ?>
" style="display:none;">
					<a title="Hide" class="icon-3 info-tooltip" href="javascript:;" onclick="GoToHideFunction('<?php echo $this->_tpl_vars['ConDet']['MTempId']; ?>
','MyShow','MyHide');"></a>
				</div>
							
							
						</div>
						</td>
					</tr>
					<tr id="ResIdDiv_<?php echo $this->_tpl_vars['ConDet']['MTempId']; ?>
" style="display:none;" class="ViewDet" ><td colspan="4">
						<div class="HeadYBord14">Details of '<?php echo ((is_array($_tmp=$this->_tpl_vars['ConDet']['TempTitle'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
' - Mail Template</div>
						<div class="clearspace"></div>
						<div class="CMDet">
							<div class="CMDet1">Subject</div>
							<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['ConDet']['MailSub'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
						</div>
						<div class="CMDet">
							<div class="CMDet1">Content</div>
							<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['ConDet']['MailCotent'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)))) ? $this->_run_mod_handler('html_entity_decode', true, $_tmp) : html_entity_decode($_tmp)); ?>
</div>
						</div>	
						<div class="CMDet">
							<div class="CMDet1">Mail Notify</div>
							<div class="CMDet2">:&nbsp;<?php if ($this->_tpl_vars['ConDet']['MailNotify'] == 1): ?>On<?php else: ?>Off<?php endif; ?></div>
						</div>					
					</td></tr>
				<?php endforeach; unset($_from); else: ?>
				<tr><td colspan="4" align="center" class="Msg_info"><strong>No Results Found ..</strong></td></tr>					
				<?php endif; ?>						
				</tbody>
			</table>			
			<div class="clearspace"></div>
		</form>
	</div>
</div>
<?php echo '
<script type="text/javascript">
var FormName=\'frm_list\';
var FormObj=document.forms[FormName];
var MgmtName=\'User(s)\';
</script>
'; ?>