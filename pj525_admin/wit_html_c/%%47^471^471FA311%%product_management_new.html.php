<?php /* Smarty version 2.6.3, created on 2013-05-04 04:48:47
         compiled from product_management_new.html */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'product_management_new.html', 5, false),array('modifier', 'stripslashes', 'product_management_new.html', 75, false),)), $this); ?>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['TinyMCEPath']; ?>
ckeditor/ckeditor.js"></script>
<script src="PageStyleScript/SiteAdminCommonFunction.js" type="text/javascript"></script>
<script src="PageStyleScript/SiteAdminValidateFunction.js" type="text/javascript"></script>
<script type="text/javascript"> 
var ImageList='<?php echo count($this->_tpl_vars['ImgList']); ?>
'; 
</script>
<?php echo '
<script type="text/javascript">
var SiteMainPath="{SiteMainPath}";
var AllowExtension= new Array();;var ErrCss=\'txtboxerr\';
function GoToFormValidate(ValiType)
{	
	var FlagIns=true;var ErrCss=\'txtboxerr\';
	
	FlagIns=FormValidateFunction(\'Text\',\'#ItemName\',\'Please enter product name\'); 
	if(FlagIns){
		FlagIns=FormValidateFunction(\'Text\',\'#CatId\',\'Please select category\');
	} 
	if(FlagIns){
		FlagIns=FormValidateFunction(\'TinyMce\',\'ItemDesc\',\'Please enter description\');
	}
	if(FlagIns){
		FlagIns=FormValidateFunction(\'TinyMce\',\'Specification\',\'Please enter specification\');
	}  
	if(FlagIns)
	{	
		if(ImageList==0){ 
			if(jQuery(\'#ItemImage\').val()== \'\' && ValiType !=\'2\')
			{	
				FlagIns=FormValidateFunction(\'Text\',\'#ItemImage\',\'Please Select Image\');
			}  
			if(jQuery(\'#ItemImage\').val()!= \'\' && ValiType !=\'2\'){ ErrCss=\'txtboxerr\';
				if(!jQuery(\'#ItemImage\').val().substr(-4).toLowerCase().match(\'.jpg|jpeg|.gif|.png|.bmp\')){
				jQuery(\'#ItemImageMsg\').show();
				jQuery(\'#ItemImageMsg\').html(\'jpeg/jpg/gif/png/bmp formats only allowed\');
				jQuery(\'#ItemImage\').attr(\'class\',\'error_element\');
				jQuery(\'#ItemImage\').focus();return false;
				}		
			}
		} 
	} 
	if(FlagIns){	
		document.forms[FormName].submit();
	}
}  
</script>
'; ?>

<div id="page-heading"><h1><?php echo $this->_tpl_vars['PageTitle']; ?>
</h1></div>
<div class="clearspace"></div>
	<form name="add_pro" method="post" enctype="multipart/form-data">
	<input type="hidden" id="id" value="1">
	<div class="body_head"><div class="TutProWid2">
		<div style="padding-left:50px;"><div style="width:600px; float:left;">
		<div class="AdmFleft" style="width:800px;"><div class="NewMsgDiv">	
		<?php if ($this->_tpl_vars['SucMessage'] != '' || $this->_tpl_vars['ErrMessage'] != ''): ?>	
			<?php if ($this->_tpl_vars['ErrMessage'] != ''): ?>
				<div class="Msg_error">
					<div class="AdmFleft"><?php echo $this->_tpl_vars['ErrMessage']; ?>
</div>
					<div class="AdmFRight"><a class="close-msg"><img src="images/close-button.png" alt="" /></a></div>
				</div>
			<?php elseif ($this->_tpl_vars['SucMessage'] != ''): ?>
				<div class="Msg_success">
					<div class="AdmFleft"><?php echo $this->_tpl_vars['SucMessage']; ?>
</div>
					<div class="AdmFRight"><a class="close-msg"><img src="images/close-button.png" alt="" /></a></div>
				</div>
			<?php endif; ?>
			<div class="spacer"></div>
		<?php endif; ?>
		</div></div>
		<?php if ($this->_tpl_vars['Arr']['SellId'] != '1' && $this->_tpl_vars['Arr']['SellId'] != '2' && $this->_tpl_vars['Arr']['SellId'] != '3'): ?>
 			<div class="FormDiv">
				<div class="FormDet1">Product Name</div>
				<div class="FormDet2">:</div>
				<div class="FormDet3">
				<input type="text" name="ItemName" id="ItemName" class="txtbox1" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['Arr']['ItemName'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
" />
				<br /><span id="ItemNameMsg" class="error"></span>
				</div>
			</div> 
		<?php else: ?>
 			<div class="FormDiv">
				<div class="FormDet1">Product Name</div>
				<div class="FormDet2">:</div>
				<div class="FormDet3"><?php echo ((is_array($_tmp=$this->_tpl_vars['Arr']['ItemName'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
 </div>
				<input type="hidden" name="ItemName" id="ItemName" class="txtbox1" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['Arr']['ItemName'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
" />
			</div> 
		<?php endif; ?>
			<div class="FormDiv">
				<div class="FormDet1">Category</div>
				<div class="FormDet2">:</div>
				<div class="FormDet3">
					<select name="CatId" id="CatId">
					<option value="">Select Category</option>
					 <?php if (count($_from = (array)$this->_tpl_vars['CatList'])):
    foreach ($_from as $this->_tpl_vars['CatDet']):
?> 
					 <option <?php if ($this->_tpl_vars['Arr']['CatId'] == $this->_tpl_vars['CatDet']['CatId']): ?>selected="selected"<?php endif; ?> value="<?php echo $this->_tpl_vars['CatDet']['CatId']; ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['CatDet']['CatName'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</option> 
					 <?php endforeach; unset($_from); endif; ?>
					</select>
 				<br /><span id="CatIdMsg" class="error"></span>
				</div>
			</div>
		<div class="clearspace"></div></div><div class="clearspace"></div></div>
		<div class="FormDiv">
			<div class="FormDet1" style="width:27%">Description </div>
			<div class="FormDet2">:</div>
			<div class="FormDet3">
<textarea name="ItemDesc" id="ItemDesc" class="ckeditor" ><?php echo ((is_array($_tmp=$this->_tpl_vars['Arr']['ItemDesc'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</textarea>
			<br /><span id="ItemDescMsg" class="error"></span>
			</div>
		</div>
		<div class="FormDiv">
			<div class="FormDet1" style="width:27%">Specification</div>
			<div class="FormDet2">:</div>
			<div class="FormDet3">
<textarea name="Specification" id="Specification" class="ckeditor" ><?php echo ((is_array($_tmp=$this->_tpl_vars['Arr']['Specification'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</textarea>
			<br /><span id="SpecificationMsg" class="error"></span>
			</div>
		</div>
		<div style="padding-left:50px;"><div style="width:600px; float:left;"> 
 			<div class="FormDiv">
				<div class="FormDet1">Product Images</div>
				<div class="FormDet2">:</div>
				<div class="FormDet3"><div class="left" style="width:220px;">
					<input name="ItemImage[]" id="ItemImage" class="compound_file" type="file"/>  
					<br /><a href="#" class="left" onClick="addFormField(); return false;">Add</a> 
					</div>
						 &nbsp; 
						<div class="clear"></div><span id="ItemImageMsg" class="error"></span>
						 <div id="MoreFiles"></div><div id="ItemImageSucc"></div>
						 <?php if (count($_from = (array)$this->_tpl_vars['ImgList'])):
    foreach ($_from as $this->_tpl_vars['ImgDet']):
?>
						<div style="padding-right:15px; float:left; height:100px;" id="Img_<?php echo $this->_tpl_vars['ImgDet']['ImageId']; ?>
">
							<div style="padding-right:5px;float:left;">
								<img border='0' src="<?php echo $this->_tpl_vars['StoreImageDisp']; ?>
t_<?php echo $this->_tpl_vars['ImgDet']['ItemImage']; ?>
"/>
							</div>
							<div style="padding-right:5px;float:left;">
								<a href="javascript:;" onclick="RemoveUploadedImages('<?php echo $this->_tpl_vars['ImgDet']['ImageId']; ?>
');">
							<img border='0' src='images/Delete_img.png'/></a>
							</div>
						</div>                            
					<?php endforeach; unset($_from); endif; ?>
					</div>
					<div class="spacer"></div>
				</div> 
 			<div class="FormDiv">
				<div class="FormDet1">&nbsp;</div>
				<div class="FormDet2">&nbsp;</div>
				<div class="FormDet3">
	<?php if ($_GET['con_id'] != ''): ?>
	<input type="button" name="Update" value="Update Product"class="btn" onclick="return GoToFormValidate('2');" />
	<input type="hidden" name="AdminAction" value="Update"class="btn" />
	<?php else: ?>
	<input type="hidden" name="AdminAction" value="Add"class="btn" />
	<input type="button" name="Add" value="Add Product"class="btn" onclick="return GoToFormValidate('1');" />
	<?php endif; ?>
	&nbsp;<input type="button" name="goback" value="GoBack" class="btn" onClick="GoBack('product_mgmt.php');">
				</div>
			</div>
		<div class="clearspace"></div></div><div class="clearspace"></div></div>
	</div><div class="clearspace"></div></div>
	</form>  
<?php echo ' 
<script type="text/javascript">
var FormName=\'add_pro\';
var FormObj=document.forms[FormName];
var MgmtName=\'Product\'; 

function RemoveUploadedImages(SelId){//alert(SelId);

	if(confirm(\'Are you sure you want to delete this image?\')){
		jQuery(\'#ItemImageSucc\').html(\'<div class="Msg_SuccDiv"><img src="images/ajax-loader.gif" border="0"/></div><div class="spacer"></div>\');
		var AjacRes=jQuery.ajax({
			url: \'admin_json_ajax.php\',
			type: "POST",
			dataType:\'json\',
			data: {SelType:\'PhotoImg\',SelId:SelId},
			error:function (xhr){
				alert(\'Server Error\');
			},
			success: function(JSonRes){//alert(JSonRes);
				if(JSonRes.ajax_res==\'true\'){
					jQuery(\'#ItemImageSucc\').html(\'<div class="Msg_SuccDiv"><div class="Msg_success">\'+JSonRes.ajax_msg+\'</div></div><div class="spacer"></div>\');
					jQuery(\'#Img_\'+SelId).hide();		
				}
				else{	
					jQuery(\'#ItemImageSucc\').html(\'<div class="Msg_SuccDiv"><div class="Msg_error">\'+JSonRes.ajax_msg+\'</div></div><div class="spacer"></div>\');						
				}
			}
		});
	}	
}
</script>
'; ?>