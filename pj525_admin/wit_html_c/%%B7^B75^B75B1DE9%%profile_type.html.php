<?php /* Smarty version 2.6.3, created on 2013-08-21 14:40:32
         compiled from profile_type.html */ ?>
<div id="page-heading"><h1> Profile Type Management </h1></div>
<div class="body_head PadLR10">
	<div class="InnDiv940 AdmFleft">
                <?php if ($this->_tpl_vars['SucMessage'] != '' || $this->_tpl_vars['ErrMessage'] != ''): ?>	
                    <div class="AdmFleft SeaTotBox">							
                        <div class="Msg_Div">
                        <?php if ($this->_tpl_vars['ErrMessage'] != ''): ?><div class="Msg_error"><?php echo $this->_tpl_vars['ErrMessage']; ?>
</div>
                        <?php elseif ($this->_tpl_vars['SucMessage'] != ''): ?><div class="Msg_success"><?php echo $this->_tpl_vars['SucMessage']; ?>
</div><?php endif; ?>
                        </div>
                    </div><div class="spacer"></div>
                <?php endif; ?>
		<form action="profile_type.php?act_type=<?php echo $this->_tpl_vars['act_type']; ?>
" method="post" enctype="multipart/form-data">
		<input type="hidden" name="profile_id" value="<?php echo $this->_tpl_vars['edit']['profile_id']; ?>
">
		<div class="FormDiv">
				<div class="FormDet1">Profile Type Title</div>
				<div class="FormDet2">:</div>
				<div class="FormDet3">
				<input type="text" name="dptitle" value="<?php echo $this->_tpl_vars['edit']['name']; ?>
">
				</div>
		</div>
		<div class="clearspace"></div>
			
		<div class="FormDiv">
				<div class="FormDet1">Profile Type Code</div>
				<div class="FormDet2">:</div>
				<div class="FormDet3">
				<input type="text" name="dpcode" value="<?php echo $this->_tpl_vars['edit']['code']; ?>
">
				</div>
		</div>
		<div class="clearspace"></div>
			
		<div class="FormDiv">
				<div class="FormDet1">Profile Type Factor </div>
				<div class="FormDet2">:</div>
				<div class="FormDet3"> 
					<table width="97%">
						<tr> 
							<td width="25%"> Natural </td> 
							<td width="5%"> : </td> 
							<td width="70%"> <input type="text" name="nfactor" value="<?php echo $this->_tpl_vars['edit']['nfactor']; ?>
"> </td>
						</tr>
						<tr> 
							<td width="25%"> Chrome </td> 
							<td width="5%"> : </td> 
							<td width="70%"> <input type="text" name="cfactor" value="<?php echo $this->_tpl_vars['edit']['cfactor']; ?>
"> </td>
						</tr>
						<tr> 
							<td width="25%"> Stainless </td>
							<td width="5%"> : </td> 
							<td width="70%"> <input type="text" name="sfactor" value="<?php echo $this->_tpl_vars['edit']['sfactor']; ?>
"> </td>
						</tr>
						<tr> 
							<td width="25%"> Oil Rubbed Bronze </td> 
							<td width="5%"> : </td> 
							<td width="70%"> <input type="text" name="ofactor" value="<?php echo $this->_tpl_vars['edit']['ofactor']; ?>
"> </td>
						</tr>
						<tr> 
							<td width="25%"> Black </td> 
							<td width="5%"> : </td> 
							<td width="70%"> <input type="text" name="bfactor" value="<?php echo $this->_tpl_vars['edit']['bfactor']; ?>
"> </td>
						</tr>
						<tr> 
							<td width="25%"> White </td> 
							<td width="5%"> : </td> 
							<td width="70%"> <input type="text" name="wfactor" value="<?php echo $this->_tpl_vars['edit']['wfactor']; ?>
"> </td>
						</tr>
					</table>
				</div>
		</div>
		<div class="clearspace"></div>
		
		<div class="FormDiv">
				<div class="FormDet1">Profile Type Image</div>
				<div class="FormDet2">:</div>
				<div class="FormDet3">
					<?php if ($this->_tpl_vars['edit']['image'] != ''): ?>
						<img src="<?php echo $this->_tpl_vars['SiteHttpPath'];   echo DOOR_FOLDER.'thumbnail/';  echo $this->_tpl_vars['edit']['image']; ?>
" width="150">
					<?php endif; ?>
					<input type="file" class="btn" name="dpimage"  value="">
				</div>
		</div>
		<div class="clearspace"></div>
		
		<div class="FormDiv">
				<div class="FormDet1">&nbsp; </div>
				<div class="FormDet2">&nbsp; </div>
				<div class="FormDet3">
					<input type="submit" class="btn" name="submit" value="submit">
					<button class="btn" onclick="window.location='profile_type.php'; return false;"> Cancel </button>
				</div>
		</div>
		<div class="clearspace"></div>
			
			
			</form>
			</div> <!-- .InnDiv940 AdmFleft -->
			<hr>
			<table id="product-table" width="100%" border="0" cellspacing="0" cellpadding="0">
				<thead>
				<tr>
					<th class="table-header-cornerL"> Profile Type Name</th>
					<th class="table-header-repeat line-left"> Profile Type Factor</th>
					<th class="table-header-repeat line-left"> Profile Type Image</th>
					<th class="table-header-cornerR line-left">Action</th>
				</tr>
				</thead>
				<tbody>
				
				<?php if (count($_from = (array)$this->_tpl_vars['profile_type'])):
    foreach ($_from as $this->_tpl_vars['f']):
?>
					<tr>
						<td><?php echo $this->_tpl_vars['f']['name']; ?>
</td>
						<td>
							<table width="96%">
								<tr> <td> Natural </td> <td> : </td> <td> <?php echo $this->_tpl_vars['f']['nfactor']; ?>
 </td> </tr>
								<tr> <td> Chrome </td> <td> : </td> <td> <?php echo $this->_tpl_vars['f']['cfactor']; ?>
 </td> </tr>
								<tr> <td> Stainless </td> <td> : </td> <td> <?php echo $this->_tpl_vars['f']['sfactor']; ?>
 </td> </tr>
								<tr> <td> Oil Rubbed Bronze </td> <td> : </td> <td> <?php echo $this->_tpl_vars['f']['ofactor']; ?>
 </td> </tr>
								<tr> <td> Black </td> <td> : </td> <td> <?php echo $this->_tpl_vars['f']['bfactor']; ?>
 </td> </tr>
								<tr> <td> White </td> <td> : </td> <td> <?php echo $this->_tpl_vars['f']['wfactor']; ?>
 </td> </tr>
							</table>
						</td>
						<td><img src="<?php echo $this->_tpl_vars['SiteHttpPath'];   echo DOOR_FOLDER.'thumbnail/';  echo $this->_tpl_vars['f']['image']; ?>
" height="160"></td>
						<td>
							<div class="gallery options-width">
								<a class="icon-1 info-tooltip" href="profile_type.php?act_type=edit&profile_id=<?php echo $this->_tpl_vars['f']['profile_id']; ?>
"></a>
								<a class="icon-2 info-tooltip" href="profile_type.php?act_type=delete&profile_id=<?php echo $this->_tpl_vars['f']['profile_id']; ?>
" onclick="return confirm('Please Confirm Deletion !!')"></a>
							</div>
						</td>
					</tr>
				<?php endforeach; unset($_from); endif; ?>	
				</tbody>
			</table>
</div>