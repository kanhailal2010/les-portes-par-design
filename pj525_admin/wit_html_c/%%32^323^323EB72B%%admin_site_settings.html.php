<?php /* Smarty version 2.6.3, created on 2013-08-20 14:36:17
         compiled from admin_site_settings.html */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'stripslashes', 'admin_site_settings.html', 124, false),)), $this); ?>
<!--<div id="page-heading"><h1><?php echo $this->_tpl_vars['PageTitle']; ?>
</h1></div>-->
<link href="PageStyleScript/settings_page.css" rel="stylesheet" type="text/css" />
<div class="PMainL200" >
	<div class="PMenuTop"><h5> Settings </h5></div>
 	<div class="AMenuList"><ul>
		<?php if ($_SESSION['PJ525A_Type'] == 'MainAdmin'): ?>
		<li id="Menu_settings"><a href="admin_site_settings.php?act_type=settings" <?php echo $this->_tpl_vars['LSettings']; ?>
>Site Settings</a></li>
		<?php endif; ?>	
		<li id="Menu_paypalset"><a href="admin_site_settings.php?act_type=paypalset" <?php echo $this->_tpl_vars['PSSettings']; ?>
>Payment Settings</a></li>		
		<li id="Menu_username"><a href="admin_site_settings.php?act_type=username" <?php echo $this->_tpl_vars['USettings']; ?>
>Change Username</a></li>
		<li id="Menu_password"><a href="admin_site_settings.php?act_type=password" <?php echo $this->_tpl_vars['PSettings']; ?>
>Change Password</a></li> 
		<li><a href="logout.php">Logout</a></li>
	</ul></div>
</div>	
<div class="PMainR730">
	<h3><?php echo $this->_tpl_vars['PageTitle']; ?>
</h3>
	<div class="AdmFleft" style="width:700px;" align="center" >	
		<?php if ($this->_tpl_vars['SucMessage'] != '' || $this->_tpl_vars['ErrMessage'] != ''): ?>	
			<?php if ($this->_tpl_vars['ErrMessage'] != ''): ?>
				<div class="Msg_error" style="width:420px;">
					<div class="AdmFleft"><?php echo $this->_tpl_vars['ErrMessage']; ?>
</div>
				</div>
			<?php elseif ($this->_tpl_vars['SucMessage'] != ''): ?>
				<div class="Msg_success" style="width:350px;" >
					<div class="AdmFleft"><?php echo $this->_tpl_vars['SucMessage']; ?>
</div>
			</div>
			<?php endif; ?>
			<div class="spacer"></div>
		<?php endif; ?>
		</div><div class="clearspace"></div>
	
<?php if ($_GET['act_type'] == 'username'): ?>	
<?php echo '
<script type="text/javascript">
var ErrCss=\'txtboxerr\';var FlagIns=true;
function GoToFormValidate(ValiType)
{	
	FlagIns=FormValidateFunction(\'Text\',\'#TxtUername\',\'Please enter Current Username\',ErrCss);
	if(FlagIns)
	{	
		FlagIns=FormValidateFunction(\'Text\',\'#TxtNUername\',\'Please enter New Username\',ErrCss);
	}	
	if(FlagIns)
	{	
		FlagIns=FormValidateFunction(\'Text\',\'#TxtRUername\',\'Please enter Retype Username\',ErrCss);
	}
	if(FlagIns)
	{	
		if(jQuery(\'#TxtRUername\').val() != jQuery(\'#TxtNUername\').val()){
			jQuery(\'#TxtRUernameMsg\').show();
			jQuery(\'#TxtRUernameMsg\').html(\'New Username and Retype Username must be same\');
			jQuery(\'#TxtRUername\').attr(\'class\',\'txtboxerr\');
			jQuery(\'#TxtRUername\').focus();
			 return false;
		}else{
			jQuery(\'#TxtRUernameMsg\').hide();
			jQuery(\'#TxtRUernameMsg\').html(\'\');
			jQuery(\'#TxtRUername\').attr(\'class\',\'txtbox1\');
		}	
	}
	if(FlagIns){	
		return true;
	}
	else{	return false;	}
}
</script>	
'; ?>

	<div style="padding:10px;"><div style="padding:10px; width:600px;"><form name="change_pwd" method="post">
		<div class="FormDiv">
			<div class="FormDet1">Current UserName</div>
			<div class="FormDet2">:</div>
			<div class="FormDet3">
<input type="text" name="TxtUername" id="TxtUername" class="txtbox1"  maxlength="15" style="width:175px;" />
					  <br /><span id="TxtUernameMsg" class="error"></span>
			</div>
		</div>
		<div class="FormDiv">
			<div class="FormDet1">New UserName</div>
			<div class="FormDet2">:</div>
			<div class="FormDet3">
<input type="text" name="TxtNUername" id="TxtNUername"  class="txtbox1" maxlength="15" style="width:175px;" />
				  <br /><span id="TxtNUernameMsg" class="error"></span>
			</div>
		</div>
		<div class="FormDiv">
			<div class="FormDet1">Retype UserName</div>
			<div class="FormDet2">:</div>
			<div class="FormDet3">
<input type="text" name="TxtRUername" id="TxtRUername"  class="txtbox1" maxlength="15" style="width:175px;" />
				  <br /><span id="TxtRUernameMsg" class="error"></span>
			</div>
		</div>
		<div class="FormDiv">
			<div class="FormDet1">&nbsp;</div>
			<div class="FormDet2">&nbsp;</div>
			<div class="FormDet3">
<input type="submit" name="Submit" value="Update"class="btn" onClick="return GoToFormValidate();">
			</div>
		</div>
	</form></div></div>	
<?php elseif ($_GET['act_type'] == 'video'): ?>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['TinyMCEPath']; ?>
ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="PageStyleScript/tiny_specfic_textarea.js"></script>
	<?php echo '
	<script language="javascript">
	var ErrCss=\'txtboxerr\';var FlagIns=true;
	function GoToFormValidate(ValiType)
	{	
		FlagIns=FormValidateFunction(\'TinyMce\',\'VideoDesc\',\'Please Enter Description\',ErrCss);
		if(FlagIns){	
			
		}
		else{	return false;	}
	}
	</script>
	'; ?>

	<div style="padding:10px;"><div style="padding:10px; width:600px;"><form name="change_pwd" method="post">
		<div class="FormDiv">
			<div class="FormDet1">&nbsp;</div>
			<div class="FormDet2">&nbsp;</div>
			<div class="FormDet3" style="width:100%" >
	<br /><span style="font: bold 12px Verdana, Arial, Helvetica, sans-serif;
color: black;">Note : Please Set embed video Object script width="499" and height="335"</span><br /><br />
<textarea name="VideoDesc" id="VideoDesc" class="ckeditor" rows="12" cols="40"><?php echo ((is_array($_tmp=$this->_tpl_vars['AdminDet']['VideoDesc'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</textarea>
							
							<br /><span id="VideoDescMsg" class="error"></span>
			</div>
		</div>
<!--		<div class="FormDiv">
			<div class="FormDet1">&nbsp;</div>
			<div class="FormDet2">&nbsp;</div>
			<div class="FormDet3">
	<?php echo ((is_array($_tmp=$this->_tpl_vars['AdminDet']['VideoDesc'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>

			</div>
		</div>-->

		<div class="FormDiv">
			<div class="FormDet1">&nbsp;</div>
			<div class="FormDet2">&nbsp;</div>
			<div class="FormDet3">
			<input type="submit" name="Submit" value="Update"class="btn" onClick="return GoToFormValidate();">
			</div>
		</div>
	</form></div></div>				
<?php elseif ($_GET['act_type'] == 'profile'): ?>	
<?php echo '
<script type="text/javascript">
var ErrCss=\'txtboxerr\';var FlagIns=true;
function GoToFormValidate(ValiType)
{	
	FlagIns=FormValidateFunction(\'Text\',\'#ProfileName\',\'Please Enter Profile Username\',ErrCss);
	if(FlagIns)
	{	
		FlagIns=FormValidateFunction(\'Text\',\'#ProfileEMail\',\'Please Enter Profile Email\',ErrCss);
	}
	if(FlagIns)
	{	
		FlagIns=FormValidateFunction(\'EMail\',\'#ProfileEMail\',\'Please Enter Valid Profile Email Address\',ErrCss);
	}
	if(FlagIns)
	{	
		document.forms[FormName].submit();
	}
	else{
		return false;
	}
}
</script>	
'; ?>

	<div style="padding:10px;"><div style="padding:10px; width:600px;"><form name="change_pwd" method="post">
		<div class="FormDiv">
			<div class="FormDet1">Profile Name</div>
			<div class="FormDet2">:</div>
			<div class="FormDet3">
<input type="text" name="ProfileName" id="ProfileName"  class="txtbox1" maxlength="30" style="width:275px;" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['AdminDet']['ProfileName'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
"/>
					  <br /><span id="ProfileNameMsg" class="error"></span>
			</div>
		</div>
		<div class="FormDiv">
			<div class="FormDet1">Email [ For Password Recovery]</div>
			<div class="FormDet2">:</div>
			<div class="FormDet3">
<input type="text" name="ProfileEMail" id="ProfileEMail" class="txtbox1"  maxlength="100" style="width:275px;" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['AdminDet']['ProfileEMail'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
"/>
						  <br /><span id="ProfileEMailMsg" class="error"></span>
			</div>
		</div>
		<div class="FormDiv">
			<div class="FormDet1">Confirm Password</div>
			<div class="FormDet2">:</div>
			<div class="FormDet3">
	<input type="password" name="txtConPwd" id="txtConPwd"  class="txtbox1" maxlength="15" style="width:175px;" />
					  <br /><span id="txtConPwdMsg" class="error"></span>
			</div>
		</div>
		
		<div class="FormDiv">
			<div class="FormDet1">&nbsp;</div>
			<div class="FormDet2">&nbsp;</div>
			<div class="FormDet3">
<input type="submit" name="Submit" value="Update"class="btn" onClick="return GoToFormValidate();">
			</div>
		</div>
	</form></div></div>

<?php elseif ($_GET['act_type'] == 'password'):  echo '
<script type="text/javascript">
var ErrCss=\'txtboxerr\';var FlagIns=true;
function GoToFormValidate(ValiType)
{	
	FlagIns=FormValidateFunction(\'Text\',\'#txtCurPwd\',\'Please enter Your Current Password\',ErrCss);
	if(FlagIns)
	{	
		FlagIns=FormValidateFunction(\'Text\',\'#txtNewPwd\',\'Please enter Your New Password\',ErrCss);
	}	
	if(FlagIns)
	{	
		FlagIns=FormValidateFunction(\'Text\',\'#txtConPwd\',\'Please enter Confirm Password\',ErrCss);
	}
	if(FlagIns)
	{	
		if(jQuery(\'#txtNewPwd\').val() != jQuery(\'#txtConPwd\').val()){
			jQuery(\'#txtConPwdMsg\').show();
			jQuery(\'#txtConPwdMsg\').html(\'New Password and Confirm Password must be same\');
			jQuery(\'#txtConPwd\').attr(\'class\',\'txtboxerr\');
			jQuery(\'#txtConPwd\').focus();
			 return false;
		}else{
			jQuery(\'#txtConPwdMsg\').hide();
			jQuery(\'#txtConPwdMsg\').html(\'\');
			jQuery(\'#txtConPwd\').attr(\'class\',\'txtbox1\');
		}	
	}
	if(FlagIns)
	{	
		document.forms[FormName].submit();
	}
	else{	return false;	}
}
</script>
'; ?>

	<div style="padding:10px;"><div style="padding:10px; width:600px;"><form name="change_pwd" method="post">
		<div class="FormDiv">
			<div class="FormDet1">Current Password</div>
			<div class="FormDet2">:</div>
			<div class="FormDet3">
	<input type="password" name="txtCurPwd" id="txtCurPwd" class="txtbox1"  maxlength="15" style="width:175px;" />
					  <br /><span id="txtCurPwdMsg" class="error"></span>
			</div>
		</div>
		<div class="FormDiv">
			<div class="FormDet1">New Password</div>
			<div class="FormDet2">:</div>
			<div class="FormDet3">
	<input type="password" name="txtNewPwd" id="txtNewPwd"  class="txtbox1" maxlength="15" style="width:175px;" />
					  <br /><span id="txtNewPwdMsg" class="error"></span>
			</div>
		</div>
		<div class="FormDiv">
			<div class="FormDet1">Confirm Password</div>
			<div class="FormDet2">:</div>
			<div class="FormDet3">
	<input type="password" name="txtConPwd" id="txtConPwd"  class="txtbox1" maxlength="15" style="width:175px;" />
					  <br /><span id="txtConPwdMsg" class="error"></span>
			</div>
		</div>
		<div class="FormDiv">
			<div class="FormDet1">&nbsp;</div>
			<div class="FormDet2">&nbsp;</div>
			<div class="FormDet3">
			<input type="submit" name="Submit" value="Update"class="btn" onClick="return GoToFormValidate();">
			</div>
		</div>
	</form></div></div>	  
<?php elseif ($_GET['act_type'] == 'paypalset'):  echo '
<script language="javascript">
var ErrCss=\'txtboxerr\';var FlagIns=true;
function GoToFormValidate(ValiType)
{	
	FlagIns=FormValidateFunction(\'Text\',\'#PayPalMail\',\'Please enter paypal mail\',ErrCss);
	
	if(FlagIns){	
		FlagIns=FormValidateFunction(\'EMail\',\'#PayPalMail\',\'Please enter valid paypal email\',ErrCss);
	}
	if(FlagIns){	
		FlagIns=FormValidateFunction(\'Text\',\'#API_Username\',\'Please enter API username\',ErrCss);
	}	
	if(FlagIns){	
		FlagIns=FormValidateFunction(\'Text\',\'#API_Password\',\'Please enter API password\',ErrCss);
	}	
	if(FlagIns){	
		FlagIns=FormValidateFunction(\'Text\',\'#API_Signature\',\'Please enter API signature\',ErrCss);
	}	
	if(FlagIns)
	{	
		document.forms[FormName].submit();
	}
	else{	return false;	}
}
</script>
'; ?>

	<div style="padding:10px;"><div style="padding:10px; width:600px;"><form name="change_pwd" method="post">
		<div class="FormDiv">
			<div class="FormDet1">Paypal Mail</div>
			<div class="FormDet2">:</div>
			<div class="FormDet3">
<input type="text" name="PayPalMail" id="PayPalMail" class="txtbox1" value="<?php echo $this->_tpl_vars['AdminDet']['PayPalMail']; ?>
" style=" width:300px;" />
						<br /><span id="PayPalMailMsg" class="error"></span>
			</div>
		</div>
		<div class="FormDiv">
			<div class="FormDet1">API Username</div>
			<div class="FormDet2">:</div>
			<div class="FormDet3">
<input type="text" name="API_Username" id="API_Username" class="txtbox1" value="<?php echo $this->_tpl_vars['AdminDet']['API_Username']; ?>
" style=" width:300px;" />
						<br /><span id="API_UsernameMsg" class="error"></span>
			</div>
		</div>
		<div class="FormDiv">
			<div class="FormDet1">API Password</div>
			<div class="FormDet2">:</div>
			<div class="FormDet3">
<input type="text" name="API_Password" id="API_Password" class="txtbox1" value="<?php echo $this->_tpl_vars['AdminDet']['API_Password']; ?>
" style=" width:300px;" />
						<br /><span id="API_PasswordMsg" class="error"></span>
			</div>
		</div>
		<div class="FormDiv">
			<div class="FormDet1">API Signature</div>
			<div class="FormDet2">:</div>
			<div class="FormDet3">
<input type="text" name="API_Signature" id="API_Signature" class="txtbox1" value="<?php echo $this->_tpl_vars['AdminDet']['API_Signature']; ?>
" style=" width:305px;" />
						<br /><span id="API_SignatureMsg" class="error"></span>
			</div>
		</div>
		<div class="FormDiv">
			<div class="FormDet1">Server Type</div>
			<div class="FormDet2">:</div>
			<div class="FormDet3">
						<select name="ServerType" id="ServerType" class="txtbox1" style=" width:303px;">
							<option <?php if ($this->_tpl_vars['AdminDet']['ServerType'] == '0'): ?> selected="selected"<?php endif; ?> value="0">Test Server</option>
							<option <?php if ($this->_tpl_vars['AdminDet']['ServerType'] == '1'): ?> selected="selected"<?php endif; ?> value="1">Live Server</option>
						</select>
						<br /><span id="ServerTypeMsg" class="error"></span>
			</div>
		</div>
		<div class="FormDiv">
			<div class="FormDet1">&nbsp;</div>
			<div class="FormDet2">&nbsp;</div>
			<div class="FormDet3">
<input type="submit" name="Submit" value="Update" class="btn" onClick="return GoToFormValidate();">
			</div>
		</div>
	</form></div></div>  
<?php else:  echo '
<script type="text/javascript">
var ErrCss=\'txtboxerr\';var FlagIns=true;
function GoToFormValidate(ValiType)
{	
	FlagIns=FormValidateFunction(\'Text\',\'#site_name\',\'Please enter site title \',ErrCss);
	if(FlagIns)
	{	
		FlagIns=FormValidateFunction(\'Text\',\'#site_copyright\',\'Please enter site copyright\',ErrCss);
	}
	if(FlagIns)
	{	
		FlagIns=FormValidateFunction(\'Text\',\'#OurSiteFooter\',\'Please enter admin site name \',ErrCss);
	}	
	if(FlagIns)
	{	
		FlagIns=FormValidateFunction(\'Text\',\'#admin_mail\',\'Please enter admin email\',ErrCss);
	}	
	if(FlagIns)
	{	
		FlagIns=FormValidateFunction(\'EMail\',\'#admin_mail\',\'Please enter valid admin email\',ErrCss);
	}
	if(FlagIns)
	{	
		FlagIns=FormValidateFunction(\'Text\',\'#contact_mail\',\'Please enter contact email\',ErrCss);
	}
	if(FlagIns)
	{	
		FlagIns=FormValidateFunction(\'EMail\',\'#contact_mail\',\'Please enter valid contact email\',ErrCss);
	}
	if(FlagIns)
	{	
		FlagIns=FormValidateFunction(\'Text\',\'#BarazinMail\',\'Please enter barazin email\',ErrCss);
	}
	if(FlagIns)
	{	
		FlagIns=FormValidateFunction(\'EMail\',\'#BarazinMail\',\'Please enter valid barazin email\',ErrCss);
	}
	if(FlagIns)
	{	
		FlagIns=FormValidateFunction(\'Text\',\'#BasePrice\',\'Please enter Door base price\',ErrCss);
	}
	if(FlagIns)
	{	
		FlagIns=FormValidateFunction(\'Text\',\'#price_level_factor\',\'Please enter Price Level Factor\',ErrCss);
	}
	if(FlagIns)
	{	
		FlagIns=FormValidateFunction(\'Text\',\'#ExtraHingesPrice\',\'Please enter price for Extra hinges holes\',ErrCss);
	}
	if(FlagIns)
	{	
		FlagIns=FormValidateFunction(\'Text\',\'#HolesPrice\',\'Please enter price for holes\',ErrCss);
	}	
	if(FlagIns)
	{	
		FlagIns=FormValidateFunction(\'Text\',\'#GDHolePrice\',\'Please enter price for glass drilling holes\',ErrCss);
	}
	if(FlagIns)
	{	
		FlagIns=FormValidateFunction(\'Text\',\'#ShipCost\',\'Please enter price for Shipping\',ErrCss);
	}	
	if(FlagIns){
		FlagIns=FormValidateFunction(\'Text\',\'#GMapAddress\',\'Please enter Google Map Address\',ErrCss);
	}	
	if(FlagIns){
		FlagIns=FormValidateFunction(\'Text\',\'#admin_phone\',\'Please enter Phone Number\',ErrCss);
	}
	if(FlagIns)
	{	
		FlagIns=FormValidateFunction(\'Text\',\'#meta_key\',\'Please enter meta keyword\',ErrCss);
	}
	if(FlagIns)
	{	
		FlagIns=FormValidateFunction(\'Text\',\'#meta_desc\',\'Please enter meta description\',ErrCss);
	}		
	if(FlagIns)
	{	
		
	}
	else{	return false;	}
}
</script>
'; ?>

	<div style="padding:10px;"><div style="padding:10px; width:600px;"><form name="change_pwd" method="post">

		<div class="FormDiv">
			<div class="FormDet1">Site Title</div>
			<div class="FormDet2">:</div>
			<div class="FormDet3">
<input type="text" name="site_name" id="site_name"  class="txtbox1" value="<?php echo $this->_tpl_vars['AdminDet']['site_name']; ?>
" style=" width:300px;" />
				<br /><span id="site_nameMsg" class="error"></span>
			</div>
		</div>
		<div class="FormDiv">
			<div class="FormDet1">Site CopyRight</div>
			<div class="FormDet2">:</div>
			<div class="FormDet3">
<input type="text" name="site_copyright" id="site_copyright"  class="txtbox1" value="<?php echo $this->_tpl_vars['AdminDet']['site_copyright']; ?>
" style=" width:300px;" /><br />[Note : Please enter text only without copyright symbol in Site CopyRight field]
				<br /><span id="site_copyrightMsg" class="error"></span>
			</div>
		</div>	
		<div class="FormDiv">
			<div class="FormDet1">Admin Site Name </div>
			<div class="FormDet2">:</div>
			<div class="FormDet3">
<input type="text" name="OurSiteFooter" id="OurSiteFooter"  class="txtbox1" value="<?php echo $this->_tpl_vars['AdminDet']['OurSiteFooter']; ?>
" style=" width:300px;" />
				<br /><span id="OurSiteFooterMsg" class="error"></span>
			</div>
		</div>
		<div class="FormDiv">
			<div class="FormDet1">Admin Email</div>
			<div class="FormDet2">:</div>
			<div class="FormDet3">
<input type="text" name="admin_mail" id="admin_mail"  class="txtbox1" value="<?php echo $this->_tpl_vars['AdminDet']['admin_mail']; ?>
" style=" width:300px;" />
				<br /><span id="admin_mailMsg" class="error"></span>
			</div>
		</div>
		<div class="FormDiv">
			<div class="FormDet1">Contact Mail</div>
			<div class="FormDet2">:</div>
			<div class="FormDet3">
<input type="text" name="contact_mail" id="contact_mail" class="txtbox1" value="<?php echo $this->_tpl_vars['AdminDet']['contact_mail']; ?>
" style=" width:300px;" />
				<br /><span id="contact_mailMsg" class="error"></span>
			</div>
		</div>	
		<div class="FormDiv">
			<div class="FormDet1">BARAZIN Mail</div>
			<div class="FormDet2">:</div>
			<div class="FormDet3">
<input type="text" name="BarazinMail" id="BarazinMail" class="txtbox1" value="<?php echo $this->_tpl_vars['AdminDet']['BarazinMail']; ?>
" style=" width:300px;" />
				<br /><span id="BarazinMailMsg" class="error"></span>
			</div>
		</div>	
		<div class="FormDiv">
			<div class="FormDet1">Door Base Price</div>
			<div class="FormDet2">:</div>
			<div class="FormDet3">$
<input type="text" name="BasePrice" id="BasePrice" class="txtbox1" value="<?php echo $this->_tpl_vars['AdminDet']['BasePrice']; ?>
" style=" width:300px;" />
				<br /><span id="BasePriceMsg" class="error"></span>
			</div>
		</div>	
		<div class="FormDiv">
			<div class="FormDet1">Price Level Factor</div>
			<div class="FormDet2">:</div>
			<div class="FormDet3">
<input type="text" name="price_level_factor" id="price_level_factor" class="txtbox1" value="<?php echo $this->_tpl_vars['AdminDet']['price_level_factor']; ?>
" style=" width:300px;" />
				<br /><span id="price_level_factorMsg" class="error"></span>
			</div>
		</div>		
		<div class="FormDiv">
			<div class="FormDet1">Price for Extra Hinges Holes</div>
			<div class="FormDet2">:</div>
			<div class="FormDet3">$
<input type="text" name="ExtraHingesPrice" id="ExtraHingesPrice" class="txtbox1" value="<?php echo $this->_tpl_vars['AdminDet']['ExtraHingesPrice']; ?>
" style=" width:300px;" />
				<br /><span id="ExtraHingesPriceMsg" class="error"></span>
			</div>
		</div>	
		<div class="FormDiv">
			<div class="FormDet1">Price for Holes</div>
			<div class="FormDet2">:</div>
			<div class="FormDet3">$
<input type="text" name="HolesPrice" id="HolesPrice" class="txtbox1" value="<?php echo $this->_tpl_vars['AdminDet']['HolesPrice']; ?>
" style=" width:300px;" />
				<br /><span id="HolesPriceMsg" class="error"></span>
			</div>
		</div>	  	
		<div class="FormDiv">
			<div class="FormDet1">Glass Drilling Hole Price</div>
			<div class="FormDet2">:</div>
			<div class="FormDet3">$
<input type="text" name="GDHolePrice" id="GDHolePrice" class="txtbox1" value="<?php echo $this->_tpl_vars['AdminDet']['GDHolePrice']; ?>
" style=" width:300px;" />
				<br /><span id="GDHolePriceMsg" class="error"></span>
			</div>
		</div>	  
		<div class="FormDiv">
			<div class="FormDet1">Price for Shipping</div>
			<div class="FormDet2">:</div>
			<div class="FormDet3">$
<input type="text" name="ShipCost" id="ShipCost" class="txtbox1" value="<?php echo $this->_tpl_vars['AdminDet']['ShipCost']; ?>
" style=" width:300px;" />
				<br /><span id="ShipCostMsg" class="error"></span>
			</div>
		</div>	
		 	
		<div class="FormDiv">
			<div class="FormDet1">Google Map Address</div>
			<div class="FormDet2">:</div>
			<div class="FormDet3">
<textarea name="GMapAddress" id="GMapAddress"  class="txtbox1" style="width:400px; height:130px;"><?php echo ((is_array($_tmp=$this->_tpl_vars['AdminDet']['GMapAddress'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</textarea>
				<br /><span id="GMapAddressMsg" class="error"></span>
			</div>
		</div>
		<!--<div class="FormDiv">
			<div class="FormDet1">Phone Number</div>
			<div class="FormDet2">:</div>
			<div class="FormDet3">
<input type="text" name="admin_phone" id="admin_phone"  class="txtbox1" value="<?php echo $this->_tpl_vars['AdminDet']['admin_phone']; ?>
" style=" width:300px;" />
				<br /><span id="admin_phoneMsg" class="error"></span>
			</div>
		</div>-->
		<div class="FormDiv">
			<div class="FormDet1">Meta Keyword</div>
			<div class="FormDet2">:</div>
			<div class="FormDet3">
<textarea name="meta_key" id="meta_key"  class="txtbox1" style="width:400px; height:130px;"><?php echo ((is_array($_tmp=$this->_tpl_vars['AdminDet']['meta_key'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</textarea>
				<br /><span id="meta_keyMsg" class="error"></span>
			</div>
		</div>		
		<div class="FormDiv">
			<div class="FormDet1">Meta Description</div>
			<div class="FormDet2">:</div>
			<div class="FormDet3">
<textarea name="meta_desc" id="meta_desc"  class="txtbox1" style="width:400px; height:130px;"><?php echo ((is_array($_tmp=$this->_tpl_vars['AdminDet']['meta_desc'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</textarea>
				<br /><span id="meta_descMsg" class="error"></span>
			</div>
		</div>
		<div class="FormDiv">
			<div class="FormDet1">&nbsp;</div>
			<div class="FormDet2">&nbsp;</div>
			<div class="FormDet3">
				<input type="submit" name="Submit" value="Update"class="btn" onClick="return GoToFormValidate();">
			</div>
		</div>
	</form></div></div>
<?php endif; ?>
<script src="PageStyleScript/SiteAdminValidateFunction.js" type="text/javascript"></script>
</div>