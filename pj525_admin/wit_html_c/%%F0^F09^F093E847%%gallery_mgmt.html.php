<?php /* Smarty version 2.6.3, created on 2013-05-09 03:30:20
         compiled from gallery_mgmt.html */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'stripslashes', 'gallery_mgmt.html', 26, false),array('modifier', 'md5', 'gallery_mgmt.html', 103, false),)), $this); ?>
<script type="text/javascript" src="PageStyleScript/jquery_002.js"></script>
<script type="text/javascript" src="PageStyleScript/jquery_003.js"></script>
<script type="text/javascript" src="PageStyleScript/jquery.js"></script>
<script src="PageStyleScript/SiteAdminCommonFunction.js"></script>
<link rel="stylesheet" type="text/css" href="PageStyleScript/jquery.css"> 
<?php echo '
<script type="text/javascript">
	jQuery().ready(function() {
		function format(mail) {
			return mail.name + "&lt;" + mail.to + " &gt;";
		}
		jQuery("#ImageId").autocomplete("get_auto_list.php?get_type=place", {
			width: 150,autoFill: false,
			selectFirst: false,
			matchContains: true,
			highlightItem: false,
		});		
	});	
</script>
'; ?>
 
<div id="page-heading"><h1><?php echo $this->_tpl_vars['PageTitle']; ?>
</h1></div>
<div class="body_head PadLR10">			
	<div class="InnDiv940 AdmFleft">	
		<div class="AdmFleft" style="width:960px;"><form name="frm_search" method="get" action="">
 				<div class="HeadFont12 AdmFleft PadRLB5">
Title&nbsp;:&nbsp;<input type="text" name="pronamesea" value="<?php echo ((is_array($_tmp=$_GET['pronamesea'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
"/></div>
				<div class="HeadFont12 AdmFleft PadRLB5">
URL&nbsp;:&nbsp;<input type="text" name="code" value="<?php echo ((is_array($_tmp=$_GET['code'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
"/></div> 
			<div class="HeadFont12 AdmFleft PadRLB5">
				<input type="submit" name="seabut" value="Search" class="btn"/></div>	
			<div class="HeadFont12 AdmFleft PadRLB5">
<input type="button" name="seabut" value="Show All" class="btn" onClick="GoBackRedirct('gallery_mgmt.php');" />
			</div>
			<div class="spacer"></div>
 		</form></div>
		<div class="AdmFleft" style="width:800px;"><div class="NewMsgDiv">	
		<?php if ($this->_tpl_vars['SucMessage'] != '' || $this->_tpl_vars['ErrMessage'] != ''): ?>	
			<?php if ($this->_tpl_vars['ErrMessage'] != ''): ?>
				<div class="Msg_error">
					<div class="AdmFleft"><?php echo $this->_tpl_vars['ErrMessage']; ?>
</div>
					<div class="AdmFRight"><a class="close-msg"><img src="images/close-button.png" alt="" /></a></div>
				</div>
			<?php elseif ($this->_tpl_vars['SucMessage'] != ''): ?>
				<div class="Msg_success">
					<div class="AdmFleft"><?php echo $this->_tpl_vars['SucMessage']; ?>
</div>
					<div class="AdmFRight"><a class="close-msg"><img src="images/close-button.png" alt="" /></a></div>
				</div>
			<?php endif; ?>
			<div class="spacer"></div>
		<?php endif; ?>
		</div></div>
		<div style="width:160px; float:left; padding:8px 0;">
			<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "AdminControlMenu_New.html", 'smarty_include_vars' => array('title' => 'admin control list settings')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
		</div>
		<div class="clearspace"></div>
	</div>
	<div id="Errmsgval"></div>
	<form name="frm_list" method="post" action="">
		<input type="hidden" name="Action_Type" />
		<input type="hidden" name="ConSId"/>	
		<div class="clearspace"></div>
		 	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
		<thead><tr>
			<th class="table-header-cornerL">S.No</th>  
			<th class="table-header-repeat line-left">Title</th> 
			<th class="table-header-repeat line-left">URL</th> 
			<th class="table-header-repeat line-left">Image</th> 
			<th class="table-header-repeat line-left">Status</th>
			<th class="table-header-cornerR line-left">Actions</th>
		</tr></thead>
		<tfoot><tr><td colspan="8" align="right"><?php echo $this->_tpl_vars['PlacePage']; ?>
</td></tr></tfoot>
		<tbody>
		<?php $this->assign('i', $this->_tpl_vars['PageStartNum']); ?>
		<?php if (count($_from = (array)$this->_tpl_vars['PlaceList'])):
    foreach ($_from as $this->_tpl_vars['TableDet']):
?>
		<tr <?php if ($this->_tpl_vars['i']%2 != '0'): ?> class="alternate-row"<?php endif; ?> id="order_<?php echo $this->_tpl_vars['TableDet']['ImageId']; ?>
">
			<td width="8%"><input type="checkbox" name="ConId[]" value="<?php echo $this->_tpl_vars['TableDet']['ImageId']; ?>
" /><?php echo $this->_tpl_vars['i']++; ?>
.</td>  
			<td width="22%"><?php echo $this->_tpl_vars['TableDet']['Title']; ?>
</td>  
			<td width="20%"><?php echo $this->_tpl_vars['TableDet']['URL']; ?>
</td> 
			<td width="20%"><img border='0' src="<?php echo $this->_tpl_vars['GalleryImgDisp']; ?>
t_<?php echo $this->_tpl_vars['TableDet']['GalleryImage']; ?>
"/></td>  
			<td width="13%">			
		<?php if ($this->_tpl_vars['TableDet']['ImageStatus'] == '1'): ?>
					<span id="InActive_<?php echo $this->_tpl_vars['TableDet']['ImageId']; ?>
">
						<a href="javascript:;" onClick="GoToJSONAjaxFunction('OFF','<?php echo $this->_tpl_vars['TableDet']['ImageId']; ?>
','GalleryMgmt','Gallery(s)')">
						<img class="tik" src="images/on_btn.png" /></a>
					</span>	
					<span id="Active_<?php echo $this->_tpl_vars['TableDet']['ImageId']; ?>
" style="display:none;">
						<a href="javascript:;" onClick="GoToJSONAjaxFunction('ON','<?php echo $this->_tpl_vars['TableDet']['ImageId']; ?>
','GalleryMgmt','Gallery(s)')">
						<img class="tik" src="images/off_btn.png" /></a>
					</span>	
				<?php else: ?>
					<span id="InActive_<?php echo $this->_tpl_vars['TableDet']['ImageId']; ?>
" style="display:none;">
						<a href="javascript:;" onClick="GoToJSONAjaxFunction('OFF','<?php echo $this->_tpl_vars['TableDet']['ImageId']; ?>
','GalleryMgmt','Gallery(s)')">
						<img  border="0" src="images/on_btn.png" /></a>
					</span>	
					<span id="Active_<?php echo $this->_tpl_vars['TableDet']['ImageId']; ?>
">
						<a href="javascript:;" onClick="GoToJSONAjaxFunction('ON','<?php echo $this->_tpl_vars['TableDet']['ImageId']; ?>
','GalleryMgmt','Gallery(s)')">
						<img border="0" src="images/off_btn.png" /></a>
					</span>				
				<?php endif; ?>
			</td>
			<td width="17%">
			<div class="gallery options-width">
				<a title="Edit" class="icon-1 info-tooltip" href="gallery_mgmt.php?act_type=addnew&con_id=<?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['ImageId'])) ? $this->_run_mod_handler('md5', true, $_tmp) : md5($_tmp)); ?>
"></a>			
					
				<a title="Delete" class="icon-2 info-tooltip" href="javascript:;" onclick="AdminDeleteFunction('<?php echo $this->_tpl_vars['TableDet']['ImageId']; ?>
');"></a>
 				<!--<a title="View" class="icon-3 info-tooltip" href="#User_View_<?php echo $this->_tpl_vars['i']; ?>
" rel="prettyPhoto[inline]"></a>-->
				<div id="MyShow_<?php echo $this->_tpl_vars['TableDet']['ImageId']; ?>
">
					<a title="View" class="icon-3 info-tooltip" href="javascript:;"  onclick="GoToViewFunction('<?php echo $this->_tpl_vars['TableDet']['ImageId']; ?>
','MyHide','MyShow');"></a>
				</div>					
				<div id="MyHide_<?php echo $this->_tpl_vars['TableDet']['ImageId']; ?>
" style="display:none;">
					<a title="View" class="icon-3 info-tooltip" href="javascript:;" onclick="GoToHideFunction('<?php echo $this->_tpl_vars['TableDet']['ImageId']; ?>
','MyShow','MyHide');"></a>
				</div>
			</div>	
			
			<tr  id="ResIdDiv_<?php echo $this->_tpl_vars['TableDet']['ImageId']; ?>
" style="display:none;" class="ViewDet" >
			<td colspan="8">
				<div class="HeadYBord14">Image Details</div>
				<div style="width:450px;float:left; padding-top:10px;" >
					<div class="clearspace"></div> 
					<div class="CMDet"> 	 	 	
						<div class="CMDet1">Title</div>
						<div class="CMDet2">:&nbsp;	<?php echo $this->_tpl_vars['TableDet']['Title']; ?>
</div>
					</div>
					<div class="CMDet"> 	 	 	
						<div class="CMDet1">URL</div>
						<div class="CMDet2">:&nbsp;	<?php echo $this->_tpl_vars['TableDet']['URL']; ?>
</div>
					</div>
					<div class="CMDet"> 	 	 	
						<div class="CMDet1">Image</div>
						<div class="CMDet2">:&nbsp;	<img border='0' src="<?php echo $this->_tpl_vars['GalleryImgDisp']; ?>
l_<?php echo $this->_tpl_vars['TableDet']['GalleryImage']; ?>
"/></div>
					</div>
  				</div>
 				<div class="clearspace"></div>						
			</td></tr>
 			</td>						
		</tr>	
		<?php endforeach; unset($_from); else: ?>
			<tr><td colspan="8" align="center" class="error_msg"><strong>No Results Found ..</strong></td></tr>					
		<?php endif; ?>
		</tbody>
	</table>			
	<div class="clearspace"></div>

	</form>	 
</div>
<script type="text/javascript">
var FormName='frm_list';
var FormObj=document.forms[FormName];
var MgmtName='Gallery(s)';
var Seltype='GalleryMgmt';
</script>