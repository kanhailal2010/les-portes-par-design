<?php /* Smarty version 2.6.3, created on 2013-05-04 05:15:02
         compiled from content_management.html */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'stripslashes', 'content_management.html', 8, false),array('modifier', 'html_entity_decode', 'content_management.html', 56, false),array('modifier', 'md5_enc', 'content_management.html', 83, false),)), $this); ?>
<script src="PageStyleScript/SiteAdminCommonFunction.js" type="text/javascript"></script>
<div id="page-heading"><h1><?php echo $this->_tpl_vars['PageTitle']; ?>
</h1></div>
<div class="body_head PadLR10">			
	<div class="InnDiv940 AdmFleft">	
		<div class="AdmFleft" style="width:960px;">			<form name="frm_search" method="get" action="">
			<div class="PadRLB5 AdmFleft">
				<div class="HeadFont12 AdmFleft PadRLB5">
					Title&nbsp;:&nbsp;<input type="text" name="seaname" value="<?php echo ((is_array($_tmp=$_GET['seaname'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
" /></div>
				<div class="HeadFont12 AdmFleft PadRLB5">
					<input type="submit" name="seabut" value="Search" class="btn"/></div>	
				<div class="HeadFont12 AdmFleft PadRLB5">
					<input type="button" name="seabut" value="Show All" class="btn" onClick="GoBackRedirct('content_management.php');" /></div>		
			</div>
		</form>		</div>
		<div class="AdmFleft" style="width:800px;"><div class="NewMsgDiv">	
		<?php if ($this->_tpl_vars['SucMessage'] != '' || $this->_tpl_vars['ErrMessage'] != ''): ?>	
			<?php if ($this->_tpl_vars['ErrMessage'] != ''): ?>
				<div class="Msg_error">
					<div class="AdmFleft"><?php echo $this->_tpl_vars['ErrMessage']; ?>
</div>
					<div class="AdmFRight"><a class="close-msg"><img src="images/close-button.png" alt="" /></a></div>
				</div>
			<?php elseif ($this->_tpl_vars['SucMessage'] != ''): ?>
				<div class="Msg_success">
					<div class="AdmFleft"><?php echo $this->_tpl_vars['SucMessage']; ?>
</div>
					<div class="AdmFRight"><a class="close-msg"><img src="images/close-button.png" alt="" /></a></div>
				</div>
			<?php endif; ?>
			<div class="spacer"></div>
		<?php endif; ?>
		</div></div>
		<div style="width:160px; float:left; padding:8px 0;">
			<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "AdminControlMenu_New.html", 'smarty_include_vars' => array('title' => 'admin control list settings')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
		</div>
		<div class="clearspace"></div>
	</div>
	<div class="clearspace"></div>
	<div id="Errmsgval"></div>
<form name="frm_Video" method="post" action="">
	<input type="hidden" name="Action_Type" />
	<input type="hidden" name="ConSId"/>	
	<div class="clearspace"></div>
 	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
		<tr>
			<th class="table-header-cornerL">S.No</th>
			<th class="table-header-repeat line-left">Title</th>
			<th class="table-header-repeat line-left">Status</th>
			<th class="table-header-cornerR line-left">Actions</th>
		</tr>
		<tfoot><tr><td colspan="4" align="right"><?php echo $this->_tpl_vars['ContentPage']; ?>
</td></tr></tfoot>
		<tbody>
		<?php $this->assign('i', $this->_tpl_vars['PageStartNum']); ?>
		<?php if (count($_from = (array)$this->_tpl_vars['ContList'])):
    foreach ($_from as $this->_tpl_vars['ConDet']):
?>
		<tr <?php if ($this->_tpl_vars['i']%2 != '0'): ?> class="alternate-row"<?php endif; ?>>
		
				<td width="10%"><input type="checkbox" name="ConId[]" value="<?php echo $this->_tpl_vars['ConDet']['page_id']; ?>
" /><?php echo $this->_tpl_vars['i']++; ?>
.</td>
				<td width="70%"><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['ConDet']['page_title'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)))) ? $this->_run_mod_handler('html_entity_decode', true, $_tmp) : html_entity_decode($_tmp)); ?>

				</td>
				<td width="7%">					
					<?php if ($this->_tpl_vars['ConDet']['page_status'] == '1'): ?>
					
					<span id="InActive_<?php echo $this->_tpl_vars['ConDet']['page_id']; ?>
">
						<a href="javascript:;" onClick="GoToJSONAjaxFunction('OFF','<?php echo $this->_tpl_vars['ConDet']['page_id']; ?>
','ContentMgmt','Content(s)')">
						<img class="tik" src="images/on_btn.png" /></a>
					</span>
					<span id="Active_<?php echo $this->_tpl_vars['ConDet']['page_id']; ?>
" style="display:none;">
						<a href="javascript:;" onClick="GoToJSONAjaxFunction('ON','<?php echo $this->_tpl_vars['ConDet']['page_id']; ?>
','ContentMgmt','Content(s)')">
						<img class="tik" src="images/off_btn.png" /></a>
					</span>
					<?php else: ?>
					<span id="InActive_<?php echo $this->_tpl_vars['ConDet']['page_id']; ?>
" style="display:none;">
						<a href="javascript:;" onClick="GoToJSONAjaxFunction('OFF','<?php echo $this->_tpl_vars['ConDet']['page_id']; ?>
','ContentMgmt','Content(s)')">
						<img  border="0" src="images/on_btn.png" /></a>
					</span>
					<span id="Active_<?php echo $this->_tpl_vars['ConDet']['page_id']; ?>
">
						<a href="javascript:;" onClick="GoToJSONAjaxFunction('ON','<?php echo $this->_tpl_vars['ConDet']['page_id']; ?>
','ContentMgmt','Content(s)')">
						<img border="0" src="images/off_btn.png" /></a>
					</span>
					
					<?php endif; ?>
				</td>
				<td width="13%">
				<div class="gallery options-width">
					<a title="Edit" class="icon-1 info-tooltip" href="content_management.php?act_type=addnew&con_id=<?php echo ((is_array($_tmp=$this->_tpl_vars['ConDet']['page_id'])) ? $this->_run_mod_handler('md5_enc', true, $_tmp) : smarty_modifier_md5_enc($_tmp)); ?>
"></a>
					<a title="Delete" class="icon-2 info-tooltip" href="javascript:;" onclick="AdminDeleteFunction('<?php echo $this->_tpl_vars['ConDet']['page_id']; ?>
');"></a>
					
					<div id="MyShow_<?php echo $this->_tpl_vars['ConDet']['page_id']; ?>
">
					<a title="Show" class="icon-3 info-tooltip" href="javascript:;"  onclick="GoToViewFunction('<?php echo $this->_tpl_vars['ConDet']['page_id']; ?>
','MyHide','MyShow');"></a>
				</div>					
				<div id="MyHide_<?php echo $this->_tpl_vars['ConDet']['page_id']; ?>
" style="display:none;">
					<a title="Hide" class="icon-3 info-tooltip" href="javascript:;" onclick="GoToHideFunction('<?php echo $this->_tpl_vars['ConDet']['page_id']; ?>
','MyShow','MyHide');"></a>
				</div>
					
				</div>							
				
				</td>						
			</tr>	
			<tr  id="ResIdDiv_<?php echo $this->_tpl_vars['ConDet']['page_id']; ?>
" style="display:none;" class="ViewDet" >
			<td colspan="4">
			<div class="HeadYBord14">Details of '<?php echo ((is_array($_tmp=$this->_tpl_vars['ConDet']['page_title'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
' - Content </div>
							<div class="clearspace"></div>
						<div class="CMDet">
							<div class="CMDet1">Title</div>
							<div class="CMDet2"> : &nbsp;<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['ConDet']['page_title'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)))) ? $this->_run_mod_handler('html_entity_decode', true, $_tmp) : html_entity_decode($_tmp)); ?>
</div>
						</div>
						<div class="clearspace"></div>
						<div class="CMDet">
						<div class="CMDet1">Description </div>
						<div class="CMDet2"> : &nbsp;<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['ConDet']['page_content'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)))) ? $this->_run_mod_handler('html_entity_decode', true, $_tmp) : html_entity_decode($_tmp)); ?>
</div></div>
						<div class="clearspace"></div>
						<div class="CMDet">
							<div class="CMDet1">Meta Key </div>
							<div class="CMDet2">: &nbsp;<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['ConDet']['CMetaKey'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)))) ? $this->_run_mod_handler('html_entity_decode', true, $_tmp) : html_entity_decode($_tmp)); ?>
</div>
						</div><div class="clearspace"></div>
						<div class="CMDet">
							<div class="CMDet1">Meta  Description </div>
							<div class="CMDet2"> : &nbsp;<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['ConDet']['CMetaDesc'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)))) ? $this->_run_mod_handler('html_entity_decode', true, $_tmp) : html_entity_decode($_tmp)); ?>
</div>
						</div>	
						
			   </td>
			</tr>
		
		<?php endforeach; unset($_from); else: ?>
			<tr><td colspan="4" align="center" class="error_msg"><strong>No Content Pages</strong></td></tr>					
		<?php endif; ?>						
		</tbody>
	</table>			
	<div class="clearspace"></div>
</form>	
</div>
<?php echo '
<script type="text/javascript">
var FormName=\'frm_Video\';
var FormObj=document.forms[FormName];
var MgmtName=\'Content(s)\';
var Seltype=\'ContentMgmt\';

function GoBackRedirct(RedirURL){
	window.location.href= RedirURL;
}
</script>
'; ?>