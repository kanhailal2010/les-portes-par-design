<?php /* Smarty version 2.6.3, created on 2013-05-04 05:17:37
         compiled from static_pages.html */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'stripslashes', 'static_pages.html', 10, false),array('modifier', 'md5_enc', 'static_pages.html', 59, false),array('modifier', 'html_entity_decode', 'static_pages.html', 74, false),)), $this); ?>
<link rel="stylesheet" href="PageStyleMenu/prettyPhoto.css" type="text/css" media="screen" charset="utf-8" />
<script src="PageStyleMenu/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
<script src="PageStyleScript/SiteAdminCommonFunction.js" type="text/javascript"></script>
<div id="page-heading"><h1><?php echo $this->_tpl_vars['PageTitle']; ?>
</h1></div>
<div class="body_head PadLR10">			
	<div class="InnDiv940 AdmFleft">	
		<div class="AdmFleft" style="width:960px;"><form name="frm_search" method="get" action="">
			<div class="PadRLB5 AdmFleft">
				<div class="HeadFont12 AdmFleft PadRLB5">
Title&nbsp;:&nbsp;<input type="text" name="seaname" value="<?php echo ((is_array($_tmp=$_GET['seaname'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
"/></div>
				<div class="HeadFont12 AdmFleft PadRLB5">
					<input type="submit" name="seabut" value="Search" class="btn"/></div>	
				<div class="HeadFont12 AdmFleft PadRLB5">
					<input type="button" name="seabut" value="Show All" class="btn" onClick="GoBackRedirct('static_pages.php');" /></div>		
			</div>
		</form>	</div>
		<div class="AdmFleft" style="width:800px;"><div class="NewMsgDiv">	
		<?php if ($this->_tpl_vars['SucMessage'] != '' || $this->_tpl_vars['ErrMessage'] != ''): ?>	
			<?php if ($this->_tpl_vars['ErrMessage'] != ''): ?>
				<div class="Msg_error">
					<div class="AdmFleft"><?php echo $this->_tpl_vars['ErrMessage']; ?>
</div>
					<div class="AdmFRight"><a class="close-msg"><img src="images/close-button.png" alt="" /></a></div>
				</div>
			<?php elseif ($this->_tpl_vars['SucMessage'] != ''): ?>
				<div class="Msg_success">
					<div class="AdmFleft"><?php echo $this->_tpl_vars['SucMessage']; ?>
</div>
					<div class="AdmFRight"><a class="close-msg"><img src="images/close-button.png" alt="" /></a></div>
				</div>
			<?php endif; ?>
			<div class="spacer"></div>
		<?php endif; ?>
		</div></div>
		<div class="clearspace"></div>
	</div>
	<div class="clearspace"></div>
	<div id="Errmsgval"></div>
<form name="frm_list" method="post" action="">
	<input type="hidden" name="Action_Type" />
	<input type="hidden" name="ConSId"/>	
	<div class="clearspace"></div>
	<!--	Admin Mgmt Permission Check	Start-->
		
	<!--	Admin Mgmt Permission Check	Start-->
 	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
		<tr>
			<th class="table-header-cornerL">S.No</th>
			<th class="table-header-repeat line-left">Title</th>
			<th class="table-header-cornerR line-left">Actions</th>
		</tr>
		<tfoot><tr><td colspan="3" align="right"><?php echo $this->_tpl_vars['ContentPage']; ?>
</td></tr></tfoot>
		<tbody>
		<?php $this->assign('i', $this->_tpl_vars['PageStartNum']); ?>
		<?php if (count($_from = (array)$this->_tpl_vars['ContList'])):
    foreach ($_from as $this->_tpl_vars['ConDet']):
?>
		<tr <?php if ($this->_tpl_vars['i']%2 != '0'): ?> class="alternate-row"<?php endif; ?>>
			<td width="10%"><?php echo $this->_tpl_vars['i']++; ?>
.</td>
			<td width="80%"><?php echo ((is_array($_tmp=$this->_tpl_vars['ConDet']['page_title'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</td>
           <td width="10%">				
					<div class="gallery options-width">
					<a class="icon-1 info-tooltip" title="Edit" href="static_pages.php?act_type=addnew&amp;con_id=<?php echo ((is_array($_tmp=$this->_tpl_vars['ConDet']['page_id'])) ? $this->_run_mod_handler('md5_enc', true, $_tmp) : smarty_modifier_md5_enc($_tmp)); ?>
"></a>
					
					<div id="MyShow_<?php echo $this->_tpl_vars['ConDet']['page_id']; ?>
">
					<a title="Show" class="icon-3 info-tooltip" href="javascript:;"  onclick="GoToViewFunction('<?php echo $this->_tpl_vars['ConDet']['page_id']; ?>
','MyHide','MyShow');"></a>
				</div>					
				<div id="MyHide_<?php echo $this->_tpl_vars['ConDet']['page_id']; ?>
" style="display:none;">
					<a title="Hide" class="icon-3 info-tooltip" href="javascript:;" onclick="GoToHideFunction('<?php echo $this->_tpl_vars['ConDet']['page_id']; ?>
','MyShow','MyHide');"></a>
				</div>
					
				</div>
                   	<tr  id="ResIdDiv_<?php echo $this->_tpl_vars['ConDet']['page_id']; ?>
" style="display:none;" class="ViewDet" >
			<td colspan="3">
			         <div class="HeadYBord14"><?php echo ((is_array($_tmp=$this->_tpl_vars['ConDet']['page_title'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
 - Details</div>
					 <div class="CMDet">
							<div class="CMDet1">Title</div>
							<div class="CMDet2"> : &nbsp;<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['ConDet']['page_title'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)))) ? $this->_run_mod_handler('html_entity_decode', true, $_tmp) : html_entity_decode($_tmp)); ?>
</div>
						</div>
						<div class="clearspace"></div>
					<div class="CMDet">
					<div class="CMDet1">Description :</div>
					<div class="CMDet2"><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['ConDet']['page_content'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)))) ? $this->_run_mod_handler('html_entity_decode', true, $_tmp) : html_entity_decode($_tmp)); ?>
</div><div class="clearspace"></div></div>
					<div class="CMDet">
						<div class="CMDet1">Meta Key</div>
						<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['ConDet']['CMetaKey'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
					</div><div class="clearspace"></div>
					<div class="CMDet">
						<div class="CMDet1">Meta  Description</div>
						<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['ConDet']['CMetaDesc'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
					</div><div class="clearspace"></div>						
                       </td></tr>					
		</tr>	
		<?php endforeach; unset($_from); else: ?>
			<tr><td colspan="3" align="center" class="error_msg"><strong>No Static Pages</strong></td></tr>					
		<?php endif; ?>						
		</tbody>
	</table>			
	<div class="clearspace"></div>
</form>	
</div>
<?php echo '
<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery(".gallery a[rel^=\'prettyPhoto\']").prettyPhoto({theme:\'facebook\'});
});
var FormName=\'frm_list\';
var FormObj=document.forms[FormName];
var MgmtName=\'Static Page(s)\';
</script>
'; ?>