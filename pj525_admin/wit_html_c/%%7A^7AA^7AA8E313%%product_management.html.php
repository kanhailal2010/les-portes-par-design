<?php /* Smarty version 2.6.3, created on 2013-05-03 08:57:21
         compiled from product_management.html */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'stripslashes', 'product_management.html', 8, false),array('modifier', 'html_entity_decode', 'product_management.html', 62, false),array('modifier', 'truncate', 'product_management.html', 62, false),array('modifier', 'md5', 'product_management.html', 87, false),array('modifier', 'date_format', 'product_management.html', 116, false),array('function', 'SmartyPlugIn', 'product_management.html', 134, false),)), $this); ?>
<script src="PageStyleScript/SiteAdminCommonFunction.js" type="text/javascript"></script>
<div id="page-heading"><h1><?php echo $this->_tpl_vars['PageTitle']; ?>
</h1></div>
<div class="body_head PadLR10">			
	<div class="InnDiv940 AdmFleft">	
		<div class="AdmFleft" style="width:960px;">		<form name="frm_search" method="get" action="">
				<div class="PadRLB5 AdmFleft">
				<div class="HeadFont12 AdmFleft PadRLB5">
Product Name&nbsp;:&nbsp;<input type="text" name="pronamesea" value="<?php echo ((is_array($_tmp=$_GET['pronamesea'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
"/></div>
				<div class="HeadFont12 AdmFleft PadRLB5">
Category&nbsp;:&nbsp;<input type="text" name="catsea" value="<?php echo ((is_array($_tmp=$_GET['catsea'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
"/></div>
					 
					<div class="HeadFont12 AdmFleft PadRLB5">
						<input type="submit" name="seabut" value="Search" class="btn"/></div>	
					<div class="HeadFont12 AdmFleft PadRLB5">
<input type="button" name="seabut" value="Show All" class="btn" onClick="GoBackRedirct('product_mgmt.php');" /></div>		
				</div>
			</form>		</div>
		<div class="AdmFleft" style="width:800px;"><div class="NewMsgDiv">	
		<?php if ($this->_tpl_vars['SucMessage'] != '' || $this->_tpl_vars['ErrMessage'] != ''): ?>	
			<?php if ($this->_tpl_vars['ErrMessage'] != ''): ?>
				<div class="Msg_error">
					<div class="AdmFleft"><?php echo $this->_tpl_vars['ErrMessage']; ?>
</div>
					<div class="AdmFRight"><a class="close-msg"><img src="images/close-button.png" alt="" /></a></div>
				</div>
			<?php elseif ($this->_tpl_vars['SucMessage'] != ''): ?>
				<div class="Msg_success">
					<div class="AdmFleft"><?php echo $this->_tpl_vars['SucMessage']; ?>
</div>
					<div class="AdmFRight"><a class="close-msg"><img src="images/close-button.png" alt="" /></a></div>
				</div>
			<?php endif; ?>
			<div class="spacer"></div>
		<?php endif; ?>
		</div></div>
		<div style="width:160px; float:left; padding:8px 0;">
			<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "AdminControlMenu_New.html", 'smarty_include_vars' => array('title' => 'admin control list settings')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
		</div>
		<div class="clearspace"></div>
	</div>
	<div class="clearspace"></div>
	<div id="Errmsgval"></div>
<form name="frm_list" method="post" action="">
		<input type="hidden" name="Action_Type" />
		<input type="hidden" name="ConSId"/>	
	<div class="clearspace"></div>
 	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
		<tr>
			<th class="table-header-cornerL"><b>S.No</b></th>
			<th class="table-header-repeat line-left"><b>&nbsp;Product Name</b></th>
			<th class="table-header-repeat line-left"><b>&nbsp;Category</b></th> 
			<th class="table-header-repeat line-left"><b>&nbsp;Description</b></th>
			<th class="table-header-repeat line-left"><b>Status</b></th>
			<th class="table-header-cornerR line-left">Actions</th>
		</tr>
		<tfoot><tr><td colspan="7" align="right"><?php echo $this->_tpl_vars['ProductPage']; ?>
</td></tr></tfoot>
		<tbody>
		<?php $this->assign('i', $this->_tpl_vars['PageStartNum']); ?>
			<?php if (count($_from = (array)$this->_tpl_vars['ProList'])):
    foreach ($_from as $this->_tpl_vars['TableDet']):
?>
		<tr <?php if ($this->_tpl_vars['i']%2 != '0'): ?> class="alternate-row" id="order_<?php echo $this->_tpl_vars['TableDet']['SellId']; ?>
"<?php endif; ?>>
		<td width="10%"><input type="checkbox" name="ConId[]" value="<?php echo $this->_tpl_vars['TableDet']['SellId']; ?>
" /><?php echo $this->_tpl_vars['i']++; ?>
.</td>
					<td width="25%"><?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['ItemName'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</td>
                    <td width="15%"><?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['CatName'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</td>  
                    <td width="27%"><?php echo ((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['TableDet']['ItemDesc'])) ? $this->_run_mod_handler('html_entity_decode', true, $_tmp) : html_entity_decode($_tmp)))) ? $this->_run_mod_handler('truncate', true, $_tmp, 50) : smarty_modifier_truncate($_tmp, 50)))) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</td> 
					<td width="10%">
						<?php if ($this->_tpl_vars['TableDet']['ItemStatus'] == 1): ?>
					<span id="InActive_<?php echo $this->_tpl_vars['TableDet']['SellId']; ?>
">
						<a href="javascript:;" onClick="GoToJSONAjaxFunction('OFF','<?php echo $this->_tpl_vars['TableDet']['SellId']; ?>
','ProductMgmt','Product(s)')">
						<img class="tik" src="images/on_btn.png" /></a>
					</span>
					<span id="Active_<?php echo $this->_tpl_vars['TableDet']['SellId']; ?>
" style="display:none;">
						<a href="javascript:;" onClick="GoToJSONAjaxFunction('ON','<?php echo $this->_tpl_vars['TableDet']['SellId']; ?>
','ProductMgmt','Product(s)')">
						<img class="tik" src="images/off_btn.png" /></a>
					</span>
					<?php else: ?>
					<span id="InActive_<?php echo $this->_tpl_vars['TableDet']['SellId']; ?>
" style="display:none;">
						<a href="javascript:;" onClick="GoToJSONAjaxFunction('OFF','<?php echo $this->_tpl_vars['TableDet']['SellId']; ?>
','ProductMgmt','Product(s)')">
						<img  border="0" src="images/on_btn.png" /></a>
					</span>
					<span id="Active_<?php echo $this->_tpl_vars['TableDet']['SellId']; ?>
">
						<a href="javascript:;" onClick="GoToJSONAjaxFunction('ON','<?php echo $this->_tpl_vars['TableDet']['SellId']; ?>
','ProductMgmt','Product(s)')">
						<img border="0" src="images/off_btn.png" /></a>
					</span>
					
					<?php endif; ?>
					</td>
				<td width="13%">
				<div class="gallery options-width">
					<a title="Edit" class="icon-1 info-tooltip" href="product_mgmt.php?act_type=addnew&edit=1&con_id=<?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['SellId'])) ? $this->_run_mod_handler('md5', true, $_tmp) : md5($_tmp)); ?>
"></a>
					<?php if ($this->_tpl_vars['TableDet']['SellId'] != '1' && $this->_tpl_vars['TableDet']['SellId'] != '2' && $this->_tpl_vars['TableDet']['SellId'] != '3'): ?>
					<a title="Delete" class="icon-2 info-tooltip" href="javascript:;" 
					onclick="AdminDeleteFunction('<?php echo $this->_tpl_vars['TableDet']['SellId']; ?>
');"></a>
					<?php endif; ?>
					<div id="MyShow_<?php echo $this->_tpl_vars['TableDet']['SellId']; ?>
">
					<a title="Show" class="icon-3 info-tooltip" href="javascript:;"  onclick="GoToViewFunction('<?php echo $this->_tpl_vars['TableDet']['SellId']; ?>
','MyHide','MyShow');"></a>
				</div>					
				<div id="MyHide_<?php echo $this->_tpl_vars['TableDet']['SellId']; ?>
" style="display:none;">
					<a title="Hide" class="icon-3 info-tooltip" href="javascript:;" onclick="GoToHideFunction('<?php echo $this->_tpl_vars['TableDet']['SellId']; ?>
','MyShow','MyHide');"></a>
				</div>  
				</div>							
				</td>	
			<tr  id="ResIdDiv_<?php echo $this->_tpl_vars['TableDet']['SellId']; ?>
" style="display:none;" class="ViewDet" >					
			<td colspan="7">
			<div class="HeadYBord14">Details of '<?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['ItemName'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
' Product</div>
			<div class="clearspace"></div>
						<div style="width:940px;float:left; padding-top:10px;" >
							<div style="width:550px; float:left; padding-top:10px;">
						<div class="CMDet"> 	 	 	
								<div class="CMDet1">Product Name</div>
								<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['ItemName'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
							</div>
							<div class="CMDet">
								<div class="CMDet1">Category</div>
								<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['CatName'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
							</div>  
							<div class="CMDet">
								<div class="CMDet1">Post Date</div>
								<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['TableDet']['ItemPostDate'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d,%b %Y") : smarty_modifier_date_format($_tmp, "%d,%b %Y")); ?>
</div>
							</div>
  							<div class="CMDet">
								<div class="CMDet1">Status</div>
								<div class="CMDet2">:&nbsp;<?php if ($this->_tpl_vars['TableDet']['ItemStatus'] == '1'): ?>ON<?php else: ?>OFF<?php endif; ?></div>
							</div> 
							<div class="CMDet">
								<div class="CMDet1">Description</div>
								<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['TableDet']['ItemDesc'])) ? $this->_run_mod_handler('html_entity_decode', true, $_tmp) : html_entity_decode($_tmp)))) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
							</div>
							<div class="CMDet">
								<div class="CMDet1">Specification</div>
								<div class="CMDet2">:&nbsp;<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['TableDet']['Specification'])) ? $this->_run_mod_handler('html_entity_decode', true, $_tmp) : html_entity_decode($_tmp)))) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</div>
							</div>
							</div>
							<div style="width:350px; float:left; padding-top:10px;">
 							<div class="CMDet">
								<div class="CMDet1">Product Image</div>
								<div class="CMDet2">:&nbsp;<?php echo SmartyPlugIn(array('GetType' => 'Proimages','InpVal' => $this->_tpl_vars['TableDet']['SellId']), $this);?>
</div>
							</div>
							</div>
				<div class="clearspace"></div>						
			</div>
			   </td></tr>
			</tr>
		
		<?php endforeach; unset($_from); else: ?>
			<tr><td colspan="7" align="center" class="error_msg"><strong>No Product Found ..</strong></td></tr>					
		<?php endif; ?>						
		</tbody>
	</table>			
	<div class="clearspace"></div>
</form>	
</div>
<?php echo '
<script type="text/javascript">
var FormName=\'frm_list\';
var FormObj=document.forms[FormName];
var MgmtName=\'Product(s)\';
var Seltype=\'ProductMgmt\';

</script>
'; ?>