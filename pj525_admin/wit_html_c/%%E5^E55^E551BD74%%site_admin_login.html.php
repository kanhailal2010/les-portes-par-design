<?php /* Smarty version 2.6.3, created on 2013-04-27 13:38:03
         compiled from site_admin_login.html */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><?php echo $this->_tpl_vars['SiteTitle']; ?>
</title>
<link rel="stylesheet" href="theme_css/screen.css" type="text/css" media="screen" title="default" />
<link rel="stylesheet" href="theme_css/form_style.css" type="text/css" media="screen" title="default" />
<!--  jquery core -->
<script src="theme_scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
	var SiteMainPath='<?php echo $this->_tpl_vars['SiteMainPath']; ?>
';
	var SiteMainTitle='<?php echo $this->_tpl_vars['SiteMainTitle']; ?>
';

</script>
<!-- Custom jquery scripts -->
<script src="theme_scripts/custom_jquery.js" type="text/javascript"></script>
<script src="PageStyleScript/SiteAdminValidateFunction.js" type="text/javascript"></script>
 <?php echo '
<script >
var ErrCss=\'txtboxerrnew\';var FlagIns=true;var BErrCss=\'login-inp\';
function GoToFormValidate(ValiType)
{	
	FlagIns=FormValidateFunction(\'Text\',\'#SLogName\',\'Please Enter User Name\',ErrCss);
	if(FlagIns)
	{	
		FlagIns=FormValidateFunction(\'Text\',\'#SLogPass\',\'Please Enter Password\',ErrCss);
	}	
	if(FlagIns)
	{	
		return true;
	}
	else{	return false;		}
}
function GoToForgetValidate(ValiType)
{	
		FlagIns=FormValidateFunction(\'Text\',\'#for_Mail\',\'Please Enter Email address\',ErrCss);
		if(FlagIns)
		{	
			FlagIns=FormValidateFunction(\'EMail\',\'#for_Mail\',\'Please Enter valid Email address\',ErrCss);
		}
	if(FlagIns)
	{	
		return true;	
	}
	else{	return false;		}
}
</script>
'; ?>

</head>
<body id="login-bg"> 
 
<!-- Start: login-holder -->
<div id="login-holder">

	<!-- start logo -->
	<div id="logo-login" style=" padding-top:-100px;">
		<a href="index.php"><div style="margin-top:-65px;"><img src="images/logo.png" width="240" height="100" alt="" /></div></a>
	</div>
	<!-- end logo -->
 	<div class="clear"></div>
 	<!--  start loginbox ................................................................................. -->
	<div id="loginbox">
 	<!--  start login-inner -->
	<div id="login-inner"><form name="LogForm" method="post" action="">	
	
		<div style="width:300px; margin:0; padding:0;">
		<?php if ($this->_tpl_vars['SucMessages'] != '' || $this->_tpl_vars['ErrorMessages'] != ''): ?>	
			<div class="MsgDiv">	
				<?php if ($this->_tpl_vars['ErrorMessages'] != ''): ?><div class="Msg_error"><?php echo $this->_tpl_vars['ErrorMessages']; ?>
</div>
				<?php elseif ($this->_tpl_vars['SucMessages'] != ''): ?><div class="Msg_success"><?php echo $this->_tpl_vars['SucMessages']; ?>
</div><?php endif; ?>
				<div class="clearxy"></div>
			</div><div class="clearxy"></div>					
		<?php endif; ?>
			<div class="FormDiv">
				<div class="FormDet1" style="width:26%;">User Name</div>
				<div class="FormDet2" style="width:4%;">:</div>
				<div class="FormDet4" style="width:70%; float:left;">					
<input type="text" name="SLogName" id="SLogName" class="login-inp" maxlength="30" tabindex="2"/>
					<div class="clearxy"></div><span id="SLogNameMsg" class="error"></span>
				</div>
			</div>
			<div class="FormDiv">
				<div class="FormDet1" style="width:27%;">Password</div>
				<div class="FormDet2" style="width:4%;">:</div>
				<div class="FormDet4" style="width:50%; float:left;">
<input type="password" name="SLogPass" id="SLogPass"  class="login-inp" maxlength="30" tabindex="3" />
					<div class="clearxy"></div><span id="SLogPassMsg" class="error"></span>
				</div>
			</div><div class="clearxy"></div>
			<div class="AdmFleft" style="width:190px; padding-top:2px; padding-left:120px;">
				<div class="AdmFRight">
	<input type="hidden"  value="MasterAdmin"  name="ALogType" />			
<input type="submit" class="submit-login" value=""  name="SLogType" onclick="return GoToFormValidate('1');"/>
				</div>
			</div><div class="clearxy"></div>
		</div>
	</form></div>
 	<!--  end login-inner -->
	<div class="clear"></div>

	<a href="admin_req_pass.php" class="forgot-pwd">Forgot Password?</a>
 </div>
 <!--  end loginbox -->
 
	<!--  start forgotbox ................................................................................... -->

	<div id="forgotbox">
		<div id="forgotbox-text">Please send us your email and we'll send your password.</div>
		<!--  start forgot-inner -->
		<div id="forgot-inner">
		<form name="ForgetForm" method="post" action="">	
		
		<?php if ($this->_tpl_vars['SucMessage'] != '' || $this->_tpl_vars['ErrorMessage'] != ''): ?>	
			<div class="MsgDiv">	
				<?php if ($this->_tpl_vars['ErrorMessage'] != ''): ?><div class="Msg_error"><?php echo $this->_tpl_vars['ErrorMessage']; ?>
</div>
				<?php elseif ($this->_tpl_vars['SucMessage'] != ''): ?><div class="Msg_success"><?php echo $this->_tpl_vars['SucMessage']; ?>
</div><?php endif; ?>
				<div class="clearxy"></div>
			</div><div class="clearxy"></div>					
		<?php endif; ?>
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
			<th>Email address:</th>
			<td><input type="text" value=""  name="for_Mail" id="for_Mail" class="login-inp" /></td>
		</tr>
		<tr>
			<th> </th>
			<td><span id="for_MailMsg" class="error"></span>
			<input type="hidden"  value="MasterAdmin"  name="SLogType" />
			<input type="submit"  name="Forget" value="" class="submit-login" onclick="return GoToForgetValidate('1');" /></td>
		</tr>
		</table>
		</form>
		</div>
		<!--  end forgot-inner -->
		<div class="clear"></div>
		<a href="" class="back-login">Back to login</a>
	</div>

	<!--  end forgotbox -->

</div>
<!-- End: login-holder -->
</body>
</html>
<?php echo '
<script type="text/javascript">
var FormName=\'LogForm\';
var FormObj=document.forms[FormName];
</script>
<script type="text/javascript">
var FormName1=\'ForgetForm\';
var FormObj1=document.forms[FormName1];
</script>
'; ?>

<script>
<?php if ($this->_tpl_vars['ErrorMessage'] != '' || $this->_tpl_vars['SucMessage'] != ''): ?>
<?php echo '
	jQuery(document).ready(function() {
		jQuery(\'.forgot-pwd\').trigger(\'click\'); 
	});

'; ?>

<?php endif; ?>
<?php if ($this->_tpl_vars['ErrorMessages'] != ''): ?>
<?php echo '
	jQuery(document).ready(function() {
		jQuery(\'.back-login\').trigger(\'click\'); 
	});

'; ?>

<?php endif; ?>
</script>