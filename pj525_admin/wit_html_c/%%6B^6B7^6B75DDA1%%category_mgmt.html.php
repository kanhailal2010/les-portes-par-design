<?php /* Smarty version 2.6.3, created on 2013-05-03 08:50:47
         compiled from category_mgmt.html */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'stripslashes', 'category_mgmt.html', 11, false),array('modifier', 'md5_enc', 'category_mgmt.html', 90, false),)), $this); ?>
<link rel="stylesheet" href="PageStyleMenu/prettyPhoto.css" type="text/css" media="screen" charset="utf-8" />
<script src="PageStyleMenu/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
<script src="PageStyleScript/SiteAdminCommonFunction.js" type="text/javascript"></script>
<div id="page-heading"><h1><?php echo $this->_tpl_vars['PageTitle']; ?>
</h1></div>
<div class="body_head PadLR10">	
	<div class="InnDiv940 AdmFleft">	
			<div class="AdmFleft">
			<form name="frm_search" method="get" action="">
				<div class="PadRLB5 AdmFleft">
					<div class="HeadFont12 AdmFleft PadRLB5">
Category Name&nbsp;:&nbsp;<input type="text" name="seaname" value="<?php echo ((is_array($_tmp=$_GET['seaname'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
"/></div>
					<div class="HeadFont12 AdmFleft PadRLB5">
						<input type="submit" name="seabut" value="Search" class="btn"/></div>	
					<div class="HeadFont12 AdmFleft PadRLB5">
						<input type="button" name="seabut" value="Show All" class="btn" onClick="GoBackRedirct('category_mgmt.php');" /></div>		
				</div>
			</form>	
			</div> 
	<div class="clearspace"></div>
			<div class="AdmFleft" style="width:800px;"><div class="NewMsgDiv">	
		<?php if ($this->_tpl_vars['SucMessage'] != '' || $this->_tpl_vars['ErrMessage'] != ''): ?>	
			<?php if ($this->_tpl_vars['ErrMessage'] != ''): ?>
				<div class="Msg_error">
					<div class="AdmFleft"><?php echo $this->_tpl_vars['ErrMessage']; ?>
</div>
					<div class="AdmFRight"><a class="close-msg"><img src="images/close-button.png" alt="" /></a></div>
				</div>
			<?php elseif ($this->_tpl_vars['SucMessage'] != ''): ?>
				<div class="Msg_success">
					<div class="AdmFleft"><?php echo $this->_tpl_vars['SucMessage']; ?>
</div>
					<div class="AdmFRight"><a class="close-msg"><img src="images/close-button.png" alt="" /></a></div>
				</div>
			<?php endif; ?>
			<div class="spacer"></div>
		<?php endif; ?>
		</div></div>
	<div style="width:160px; float:left; padding:8px 0;">
			<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "AdminControlMenu_New.html", 'smarty_include_vars' => array('title' => 'admin control list settings')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
		</div>
		<div class="clearspace"></div>
			</div>	
<form name="frm_list" method="post" action="">
		<input type="hidden" name="Action_Type" />
		<input type="hidden" name="ConSId"/>	
		<input type="hidden" name="Seltype"/>	

		<div class="clearspace"></div>
		<!--	Admin Mgmt Permission Check	Start-->
			
		<!--	Admin Mgmt Permission Check	Start-->
		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
			<thead><tr>
				<th class="table-header-cornerL"><b>S.No</b></th>
				<th class="table-header-repeat line-left"><b>Category Name</b></th>
 				<th class="table-header-repeat line-left"><b>Status</b></th>
				<th class="table-header-cornerR line-left"><b>Action</b></th>						
			</tr></thead>
			<tfoot><tr><td colspan="4" align="right"><?php echo $this->_tpl_vars['PageNavigation']; ?>
</td></tr></tfoot>
			<tbody>
			<?php $this->assign('i', $this->_tpl_vars['PageStartNum']); ?>
			<?php if (count($_from = (array)$this->_tpl_vars['CatList'])):
    foreach ($_from as $this->_tpl_vars['ConDet']):
?>
			<tr  bgcolor="<?php if ($this->_tpl_vars['i']%2 != '0'): ?>#FFFFFF<?php else: ?>#F3F3F3<?php endif; ?>"> 
				<td width="10%"><input type="checkbox" name="ConId[]" id="ConId[]" value="<?php echo $this->_tpl_vars['ConDet']['CatId']; ?>
"/><?php echo $this->_tpl_vars['i']++; ?>
.</td>
				<td width="60%"><?php echo ((is_array($_tmp=$this->_tpl_vars['ConDet']['CatName'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
</td>
				      <td width="15%">					
					<?php if ($this->_tpl_vars['ConDet']['CatStatus'] == '1'): ?>
					
					<span id="InActive_<?php echo $this->_tpl_vars['ConDet']['CatId']; ?>
">
						<a href="javascript:;" onClick="GoToJSONAjaxFunction('OFF','<?php echo $this->_tpl_vars['ConDet']['CatId']; ?>
','CateMgmt','Category(s)')">
						<img class="tik" src="images/on_btn.png" /></a>
					</span>
					<span id="Active_<?php echo $this->_tpl_vars['ConDet']['CatId']; ?>
" style="display:none;">
						<a href="javascript:;" onClick="GoToJSONAjaxFunction('ON','<?php echo $this->_tpl_vars['ConDet']['CatId']; ?>
','CateMgmt','Category(s)')">
						<img class="tik" src="images/off_btn.png" /></a>
					</span>
					<?php else: ?>
					<span id="InActive_<?php echo $this->_tpl_vars['ConDet']['CatId']; ?>
" style="display:none;">
						<a href="javascript:;" onClick="GoToJSONAjaxFunction('OFF','<?php echo $this->_tpl_vars['ConDet']['CatId']; ?>
','CateMgmt','Category(s)')">
						<img  border="0" src="images/on_btn.png" /></a>
					</span>
					<span id="Active_<?php echo $this->_tpl_vars['ConDet']['CatId']; ?>
">
						<a href="javascript:;" onClick="GoToJSONAjaxFunction('ON','<?php echo $this->_tpl_vars['ConDet']['CatId']; ?>
','CateMgmt','Category(s)')">
						<img border="0" src="images/off_btn.png" /></a>
					</span>
					
					<?php endif; ?>
					</td>
					
				</td><td width="15%">
				<div class="gallery options-width">
					<a title="Edit" class="icon-1 info-tooltip" href="category_mgmt.php?act_type=addnew&con_id=<?php echo ((is_array($_tmp=$this->_tpl_vars['ConDet']['CatId'])) ? $this->_run_mod_handler('md5_enc', true, $_tmp) : smarty_modifier_md5_enc($_tmp)); ?>
"></a>
					<a title="Delete" class="icon-2 info-tooltip" href="javascript:;" onclick="AdminDeleteFunction('<?php echo $this->_tpl_vars['ConDet']['CatId']; ?>
');"></a>
				</div></td>
					
			</tr>	
			<?php endforeach; unset($_from); else: ?>
				<tr><td colspan="4" align="center" class="error_msg"><strong>No Results Found ..</strong></td></tr>					
			<?php endif; ?>				
			</tbody>
		</table>			
		<div class="clearspace"></div>
	</form>
	</div>
</div>

<?php echo '
<script type="text/javascript">
var FormName=\'frm_list\';
var FormObj=document.forms[FormName];
var Seltype=\'CateMgmt\';
var MgmtName=\'Category(s)\';

</script>
'; ?>