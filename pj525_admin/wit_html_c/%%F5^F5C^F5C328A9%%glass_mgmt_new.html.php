<?php /* Smarty version 2.6.3, created on 2013-05-13 01:08:44
         compiled from glass_mgmt_new.html */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'glass_mgmt_new.html', 5, false),array('modifier', 'stripslashes', 'glass_mgmt_new.html', 89, false),)), $this); ?>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['TinyMCEPath']; ?>
ckeditor/ckeditor.js"></script>
<script src="PageStyleScript/SiteAdminCommonFunction.js" type="text/javascript"></script>
<script src="PageStyleScript/SiteAdminValidateFunction.js" type="text/javascript"></script>
<script type="text/javascript"> 
var ImageList='<?php echo count($this->_tpl_vars['ImgList']); ?>
'; 
</script>
<?php echo '
<script type="text/javascript">
var SiteMainPath="{SiteMainPath}";
var AllowExtension= new Array();;var ErrCss=\'txtboxerr\';
function GoToFormValidate(ValiType)
{	
	var FlagIns=true;var ErrCss=\'txtboxerr\';
	
	FlagIns=FormValidateFunction(\'Text\',\'#GlassCode\',\'Please enter Glass Code\'); 
	if(FlagIns){
		FlagIns=FormValidateFunction(\'Text\',\'#GlassName\',\'Please enter Glass Name\');
	} 
	if(FlagIns){
		FlagIns=FormValidateFunction(\'Text\',\'#GlassGroup\',\'Please enter Glass Group\');
	}
	if(FlagIns){
		FlagIns=FormValidateFunction(\'Text\',\'#Thickness\',\'Please select Thickness\');
	}
	if(FlagIns){
		FlagIns=FormValidateFunction(\'Text\',\'#Sqfeet\',\'Please enter square feet\');
	} 
	if(FlagIns){
		FlagIns=FormValidateFunction(\'Text\',\'#LRegularPrice\',\'Please enter Lesportes Regular Price\');
	}  
	if(FlagIns){
		FlagIns=FormValidateFunction(\'Text\',\'#LTemperedPrice\',\'Please enter Lesportes Tempered Price\');
	}  
	if(FlagIns){
		FlagIns=FormValidateFunction(\'Text\',\'#LPercent\',\'Please enter Lesportes percentage\');
	}  
	if(FlagIns){
		FlagIns=FormValidateFunction(\'Text\',\'#BPercent\',\'Please enter Barazin percentage\');
	}  
	if(FlagIns)
	{	
		if(ImageList==0){ 
			if(jQuery(\'#GlassImage\').val()== \'\' && ValiType !=\'2\')
			{	
				FlagIns=FormValidateFunction(\'Text\',\'#GlassImage\',\'Please Select Image\');
			}  
			if(jQuery(\'#GlassImage\').val()!= \'\' && ValiType !=\'2\'){ ErrCss=\'txtboxerr\';
				if(!jQuery(\'#GlassImage\').val().substr(-4).toLowerCase().match(\'.jpg|jpeg|.gif|.png|.bmp\')){
				jQuery(\'#GlassImageMsg\').show();
				jQuery(\'#GlassImageMsg\').html(\'jpeg/jpg/gif/png/bmp formats only allowed\');
				jQuery(\'#GlassImage\').attr(\'class\',\'error_element\');
				jQuery(\'#GlassImage\').focus();return false;
				}		
			}
		} 
	} 
	if(FlagIns){	
		document.forms[FormName].submit();
	}
}  
</script>
'; ?>

<div id="page-heading"><h1><?php echo $this->_tpl_vars['PageTitle']; ?>
</h1></div>
<div class="clearspace"></div>
	<form name="add_pro" method="post" enctype="multipart/form-data">
	<input type="hidden" id="id" value="1">
	<div class="body_head"><div class="TutProWid2">
		<div style="padding-left:50px;"><div style="width:600px; float:left;">
		<div class="AdmFleft" style="width:800px;"><div class="NewMsgDiv">	
		<?php if ($this->_tpl_vars['SucMessage'] != '' || $this->_tpl_vars['ErrMessage'] != ''): ?>	
			<?php if ($this->_tpl_vars['ErrMessage'] != ''): ?>
				<div class="Msg_error">
					<div class="AdmFleft"><?php echo $this->_tpl_vars['ErrMessage']; ?>
</div>
					<div class="AdmFRight"><a class="close-msg"><img src="images/close-button.png" alt="" /></a></div>
				</div>
			<?php elseif ($this->_tpl_vars['SucMessage'] != ''): ?>
				<div class="Msg_success">
					<div class="AdmFleft"><?php echo $this->_tpl_vars['SucMessage']; ?>
</div>
					<div class="AdmFRight"><a class="close-msg"><img src="images/close-button.png" alt="" /></a></div>
				</div>
			<?php endif; ?>
			<div class="spacer"></div>
		<?php endif; ?>
		</div></div>
 			<div class="FormDiv">
				<div class="FormDet1">Glass Code</div>
				<div class="FormDet2">:</div>
				<div class="FormDet3">
				<input type="text" name="GlassCode" id="GlassCode" class="txtbox1" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['Arr']['GlassCode'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
" />
				<br /><span id="GlassCodeMsg" class="error"></span>
				</div>
			</div> 
 			<div class="FormDiv">
				<div class="FormDet1">Glass Name</div>
				<div class="FormDet2">:</div>
				<div class="FormDet3">
				<input type="text" name="GlassName" id="GlassName" class="txtbox1" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['Arr']['GlassName'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
" />
				<br /><span id="GlassNameMsg" class="error"></span>
				</div>
			</div> 
 			<div class="FormDiv">
				<div class="FormDet1">Glass Group</div>
				<div class="FormDet2">:</div>
				<div class="FormDet3">
				<input type="text" name="GlassGroup" id="GlassGroup" class="txtbox1" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['Arr']['GlassGroup'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
" onkeypress="return AllowOnlyNumbers(event);" />
				<br /><span id="GlassGroupMsg" class="error"></span>
				</div>
			</div> 
			<div class="FormDiv">
				<div class="FormDet1">Thickness</div>
				<div class="FormDet2">:</div>
				<div class="FormDet3">
					<select name="Thickness" id="Thickness">
					<option value="">Select Thickness</option> 
						 <option <?php if ($this->_tpl_vars['Arr']['Thickness'] == '1 mm'): ?>selected="selected"<?php endif; ?> value="1 mm">1 mm</option> 
						 <option <?php if ($this->_tpl_vars['Arr']['Thickness'] == '2 mm'): ?>selected="selected"<?php endif; ?> value="2 mm">2 mm</option> 
						 <option <?php if ($this->_tpl_vars['Arr']['Thickness'] == '3 mm'): ?>selected="selected"<?php endif; ?> value="3 mm">3 mm</option> 
						 <option <?php if ($this->_tpl_vars['Arr']['Thickness'] == '4 mm'): ?>selected="selected"<?php endif; ?> value="4 mm">4 mm</option> 
						 <option <?php if ($this->_tpl_vars['Arr']['Thickness'] == '5 mm'): ?>selected="selected"<?php endif; ?> value="5 mm">5 mm</option> 
						 <option <?php if ($this->_tpl_vars['Arr']['Thickness'] == '6 mm'): ?>selected="selected"<?php endif; ?> value="6 mm">6 mm</option>  
					</select>
 				<br /><span id="ThicknessMsg" class="error"></span>
				</div>
			</div>
 			<div class="FormDiv">
				<div class="FormDet1">Minimum sqft</div>
				<div class="FormDet2">:</div>
				<div class="FormDet3">
				<input type="text" name="Sqfeet" id="Sqfeet" class="txtbox1" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['Arr']['Sqfeet'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
" />
				<br /><span id="SqfeetMsg" class="error"></span>
				</div>
			</div> 
 			<div class="FormDiv">
				<div class="FormDet1">Lesportes Regular Price</div>
				<div class="FormDet2">:</div>
				<div class="FormDet3">$
				<input type="text" name="LRegularPrice" id="LRegularPrice" class="txtbox1" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['Arr']['LRegularPrice'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
" onkeypress="return AllowOnlyCurrency(event);" />
				<br /><span id="LRegularPriceMsg" class="error"></span>
				</div>
			</div> 
 			<div class="FormDiv">
				<div class="FormDet1">Lesportes Tempered Price</div>
				<div class="FormDet2">:</div>
				<div class="FormDet3">$
				<input type="text" name="LTemperedPrice" id="LTemperedPrice" class="txtbox1" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['Arr']['LTemperedPrice'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
" onkeypress="return AllowOnlyCurrency(event);" />
				<br /><span id="LTemperedPriceMsg" class="error"></span>
				</div>
			</div> 
			<div class="FormDiv">
				<div class="FormDet1">Lesportes Percentage</div>
				<div class="FormDet2">:</div>
				<div class="FormDet3">
				<input type="text" name="LPercent" id="LPercent" style="width:45px;" class="txtbox1" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['Arr']['LPercent'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
" />&nbsp;%
				<br /><span id="LPercentMsg" class="error"></span>
				</div>
			</div> 
			<div class="FormDiv">
				<div class="FormDet1">BARAZIN Percentage</div>
				<div class="FormDet2">:</div>
				<div class="FormDet3">
				<input type="text" name="BPercent" id="BPercent" style="width:45px;" class="txtbox1" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['Arr']['BPercent'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
" />&nbsp;%
				<br /><span id="BPercentMsg" class="error"></span>
				</div>
			</div>
 			<!--<div class="FormDiv">
				<div class="FormDet1">Barazin Regular Price</div>
				<div class="FormDet2">:</div>
				<div class="FormDet3">$
				<input type="text" name="BRegularPrice" id="BRegularPrice" class="txtbox1" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['Arr']['BRegularPrice'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
" />
				<br /><span id="BRegularPriceMsg" class="error"></span>
				</div>
			</div> 
 			<div class="FormDiv">
				<div class="FormDet1">Barazin Tempered Price</div>
				<div class="FormDet2">:</div>
				<div class="FormDet3">$
				<input type="text" name="BTemperedPrice" id="BTemperedPrice" class="txtbox1" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['Arr']['BTemperedPrice'])) ? $this->_run_mod_handler('stripslashes', true, $_tmp) : stripslashes($_tmp)); ?>
" />
				<br /><span id="BTemperedPriceMsg" class="error"></span>
				</div>
			</div> -->
		<div class="clearspace"></div></div><div class="clearspace"></div></div> 
		<div style="padding-left:50px;"><div style="width:600px; float:left;"> 
 			<div class="FormDiv">
				<div class="FormDet1">Glass Image</div>
				<div class="FormDet2">:</div>
				<div class="FormDet3"><div class="left" style="width:220px;">
					<input name="GlassImage" id="GlassImage" class="compound_file" type="file"/>  
					</div>
						 &nbsp; 
						<div class="clear"></div><span id="GlassImageMsg" class="error"></span>
						<div class="clear"></div>
						<?php if ($this->_tpl_vars['Arr']['GlassImage'] != ''): ?>	<img border='0' src="<?php echo $this->_tpl_vars['GlassImageDisp']; ?>
t_<?php echo $this->_tpl_vars['Arr']['GlassImage']; ?>
"/> <?php endif; ?>
					</div>
					<div class="spacer"></div>
				</div> 
 			<div class="FormDiv">
				<div class="FormDet1">&nbsp;</div>
				<div class="FormDet2">&nbsp;</div>
				<div class="FormDet3">
	<?php if ($_GET['con_id'] != ''): ?>
		<input type="button" name="Update" value="Update Glass"class="btn" onclick="return GoToFormValidate('2');" />
		<input type="hidden" name="AdminAction" value="Update"class="btn" />
	<?php else: ?>
		<input type="hidden" name="AdminAction" value="Add"class="btn" />
		<input type="button" name="Add" value="Add Glass"class="btn" onclick="return GoToFormValidate('1');" />
	<?php endif; ?>
	&nbsp;<input type="button" name="goback" value="GoBack" class="btn" onClick="GoBack('glass_mgmt.php');">
				</div>
			</div>
		<div class="clearspace"></div></div><div class="clearspace"></div></div>
	</div><div class="clearspace"></div></div>
	</form>  
<?php echo ' 
<script type="text/javascript">
var FormName=\'add_pro\';
var FormObj=document.forms[FormName];
var MgmtName=\'Product\';  
</script>
'; ?>