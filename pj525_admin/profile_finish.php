<?php 	
	/*****************************			Site Config File			***********************************************/
	include "../wit_includes/wit_common.php";

	/*****************************		Include Supporing Class File	***********************************************/
	//include_once $ngconfig['SiteClassPath']."class.SiteUsers.php";
	include $ngconfig['SiteClassPath']."class.GeneralAdmin.php";
 	include_once $ngconfig['SiteClassPath']."class.Door.php"; 
	
	/*****************************			Class Objects			***********************************************/
	$objDoor=new Door(); //$ObjSiteUser=new SiteUsers();  
  	
	/*****************************		Page Variables Declaration 			**********************************/
		
	$pro_msg=(isset($_GET['pro_msg'])) ? $_GET['pro_msg']:'';$act_type=(isset($_GET['act_type'])) ? $_GET['act_type']:'';
	$mgtact=(isset($_GET['mgtact'])) ? $_GET['mgtact']:'';$con_id=(isset($_GET['con_id'])) ? $_GET['con_id']:'';
	$RedURL='profile_finish.php?pro_msg=';$checkstas=(isset($_GET['checkstas'])) ? $_GET['checkstas']:'';
	$finish_id=(isset($_REQUEST['finish_id'])) ? $_REQUEST['finish_id']:'';
	
	$objSmarty->assign('act_type','save');
	
	
	CheckAdminMgmtPermission('Redirct','MainAdmin','Configurator','profileFinish');
				
	switch($act_type)
	{
		case 'save':
				$objDoor->save_update_profile_finish();
				Redirect($RedURL.'asucc');
			break;
  		case 'edit':
					$objSmarty->assign('act_type','update');
					$objSmarty->assign('edit',$objDoor->fetch_finish($finish_id));
			break;
		case 'update':
				$objDoor->save_update_profile_finish($finish_id);
				Redirect($RedURL.'usucc');
			break;
		case 'delete':
				$objDoor->delete_profile_finish($finish_id);
				Redirect($RedURL.'dsucc');
			break;
		default:
				
				/***********	Error and Success Messages For Administrative Actions  *********************************/
				if($mgtact!="") {	
					$ActArray=array("delsuc"=>"Deleted");
					if($checkstas=="sucs"){
						$objSmarty->assign("SucMessage","Selected Category(s) Has Been ".$ActArray[$mgtact]." Successfully");
					}
					/*else{
						$objSmarty->assign("ErrMessage","Selected Category(s) Already  In ".$ActArray[$mgtact]." Status");
					}*/
				}	

				
				if($pro_msg!="") {	
					if ($pro_msg=="asucc")
						$msg = "New Profile Finish Has Been Added Successfully!!";
					elseif($pro_msg=="usucc")
						$msg = "Profile Finish Has Been Updated Successfully!!";
					else if ($pro_msg=="dsucc")
						$msg = "Profile Finish Has Been Deleted Successfully!!";
					
					$objSmarty->assign("SucMessage",$msg);	
				}
				
			break;			
	}
	

			$objSmarty->assign('profile_finish',$objDoor->profile_finish_factor());
			$objSmarty->assign("SiteTitle",SiteMainTitle." - Profile Finish Management"); //	Page Main Title
			$objSmarty->assign("PageTitle"," Profile Finish Management "); //	Page Sub Title
			$objSmarty->assign("IncludeTpl", "profile_finish.html");
			
	/*****************************			Include Admin Menu and Ctrl Create File 	**********************************/
	include _MAINSITEPATH_."/wit_admincontrols.php";
	$MCtrlArray=array('ON'=>'ON','OFF'=>'OFF','Delete'=>'Delete');
	$objSmarty->assign("ControlList",$MCtrlArray);	
		
	#===========================================================================		
	$objSmarty->display("site_admin_main.html");		
?>