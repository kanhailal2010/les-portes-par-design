<?php 	
	/*****************************			Site Config File				***********************************************/
	include "../wit_includes/wit_common.php";
     
	/*****************************			Include  File				***********************************************/
	include_once $ngconfig['SiteClassPath']."class.GeneralAdmin.php";
	include_once $ngconfig['SiteClassPath']."class.PEAR_RESIZE.php";
	include_once $ngconfig['SiteClassPath']."class.Glass.php";
	/*****************************			Class Objects			***********************************************/
	$objAdmin = new GeneralAdmin(); $objPro = new Glass();
	
	/*****************************			Include  File					***********************************************/
	include _MAINSITEPATH_."/wit_SEOURL_Function.php";

 	/*****************************		Database Variables Declartion	***********************************************/
	$tablename="pj_profile_type"; $UniqId="PTypeId";$ResVal="ProList"; $m_sta="PStatus";$PageVal="UserPage";
	
	/*****************************		Page Variables Declartion	***********************************************/
	$RedURL='profiletype_mgmt.php?';$act_type=(isset($_GET['act_type'])) ? $_GET['act_type']:'';
	$mgtact=(isset($_GET['mgtact'])) ? $_GET['mgtact']:'';$con_id=(isset($_GET['con_id'])) ? $_GET['con_id']:'';
	$checkstas=(isset($_GET['checkstas'])) ? $_GET['checkstas']:'';
	$pro_msg=(isset($_GET['pro_msg'])) ? $_GET['pro_msg']:'';$SelStatus=true;
	switch($act_type)
	{
		case 'addnew': 
				if($con_id!='') {
 					$SelStatus=$objAdmin->SelectSingleTableRowDetails($tablename,$UniqId,'Arr',$con_id); 
					if(!$SelStatus){
						Redirect('profiletype_mgmt.php?pro_msg=vfail');
					}
					CheckAdminMgmtPermission('Redirct','BothAdmin','Quote Generator','ManageProfileType');
					$objSmarty->assign("SiteTitle",SiteMainTitle." -  ProfileType Management - Edit ProfileType Details");
					$objSmarty->assign("PageTitle","ProfileType Management - Edit ProfileType Details");
				}		
				else{
					CheckAdminMgmtPermission('Redirct','BothAdmin','Quote Generator','AddProfileType');
					$objSmarty->assign("SiteTitle",SiteMainTitle." -  ProfileType Management - Add ProfileType Details");
					$objSmarty->assign("PageTitle","ProfileType Management - Add ProfileType Details");
				}
 				if(!empty($_POST['AdminAction'])){
					$objPro->AddUpdateProfiletypeDetails($con_id,admin);
				}
  				$objSmarty->assign("TinyMCEPath",$ngconfig['TinyEditor']);
				$objSmarty->assign("IncludeTpl", "profiletype_new.html");
			break;  
 		default:
			/******************  		Call Function For Delete,Active Inactive Table Details 	**********************/
			CheckAdminMgmtPermission('Redirct','BothAdmin','Quote Generator','ManageProfileType');
				
			if(isset($_POST['Action_Type'])){
				if($_POST['Action_Type']=='Set Us Featured' || $_POST['Action_Type']=='Unset Featured'){
					$objAdmin->AdminControlMgmt($tablename,$UniqId,'ProFeatureSta',$RedURL);
				}
				else{
	 				$objAdmin->AdminControlMgmt($tablename,$UniqId,$m_sta,$RedURL);
				}	
			}
			/******************  		Admin Action Error and Success Messages	*********************Oakwood197!*/
				if($pro_msg!=''){
					if($pro_msg=="usucc"){
						$objSmarty->assign("SucMessage", " ProfileType Details Has Been Updated Successfully!!");
					}else{
						$objSmarty->assign("SucMessage", "New  ProfileType Details Has Been Added Successfully!!");
					}
				}
			if($mgtact!=""){	
				$ActArray=array("delsuc"=>"Deleted","actsuc"=>"Actived","inasuc"=>"InActived");
				if($checkstas=="sucs"){
				   $objSmarty->assign("SucMessage","Selected  ProfileType(s) Details Has Been ".$ActArray[$mgtact]." Successfully");
				}
				else{
					$objSmarty->assign("ErrMessage","Selected  ProfileType(s) Already in ".$ActArray[$mgtact]." Status");
				}
			}
		
			$objPro->GetProfiletypeLists('ProList',"ProfileTypePage");
			
			$objSmarty->assign("SiteTitle",SiteMainTitle." ProfileType Management");
			$objSmarty->assign("PageTitle"," ProfileType Management");
			$objSmarty->assign("IncludeTpl", "profiletype_mgmt.html");	
		break;
	}	
 	
     /*****************************			Include Admin Menu and Ctrl Create File 	**********************************/
	include _MAINSITEPATH_."/wit_admincontrols.php";
	
	/*****************************			Include Admin Menu and Ctrl Create File 	**********************************/
	if($act_type=='managefile' || $act_type=='manageimage'){
		$MCtrlArray=array('Delete'=>'Delete');
	}
	else{
		$MCtrlArray=array('ON'=>'ON','OFF'=>'OFF','Delete'=>'Delete');
	}
	$objSmarty->assign("ControlList",$MCtrlArray);
 	$objSmarty->assign("ShapeImageDisp",$ngconfig['ShapeImageDisp'] );
	#===========================================================================		
	$objSmarty->display("site_admin_main.html");
?>