<?php 	
	/*****************************			Site Config File				***********************************************/
	include "../wit_includes/wit_common.php";
	
	/*****************************			Include  File				***********************************************/
	include_once $ngconfig['SiteClassPath']."class.GeneralAdmin.php";	
	include_once $ngconfig['SiteClassPath']."class.SiteUsers.php";
	include_once $ngconfig['SiteClassPath']."class.PEAR_RESIZE.php";
	include_once $ngconfig['SiteClassPath']."class.AdminUser.php";
	/*****************************			Class Objects				***********************************************/
	$objAdmin	= new GeneralAdmin();$objAdminUsers	= new AdminUsers();
	
	/************************		Table Details Declartion	***************************************/
	$tablename="pj_user_list";$UniqId="UserId";$ResVal="UserList";$m_sta="UserSta";$PageVal="UserPage";
	
	/*****************************		Page Variables Declartion	***********************************************/
	$RedURL='users_management.php?';$pro_msg=(isset($_GET['pro_msg']))?$_GET['pro_msg']:'';
	$act_type=(isset($_GET['act_type'])) ? $_GET['act_type']:'';$checkstas=(isset($_GET['checkstas'])) ? $_GET['checkstas']:'';
	$mgtact=(isset($_GET['mgtact'])) ? $_GET['mgtact']:'';$con_id=(isset($_GET['con_id'])) ? $_GET['con_id']:'';
	$pro_msg=(isset($_GET['pro_msg'])) ? $_GET['pro_msg']:'';
	switch($act_type)
	{
		case 'addnew':
		
	            $objAdminUsers->SelectConList();
				if($con_id!=''){
					CheckAdminMgmtPermission('Redirct','MainAdmin','User','ManageUser');
									
					$SelStatus=$objAdmin->SelectSingleTableRowDetails($tablename,$UniqId,'Arr',$con_id);
					if(!$SelStatus){
						Redirect('users_management.php?pro_msg=vfail');
					}
					$selqry="select UserPass from pj_user_list where md5(UserId)='".$con_id."'";
				$MemDet=$objMysqlFns->ExecuteQuery($selqry,"select");
				$MemPass=OurTechEncryptionDecryption('Decrypt',$MemDet[0]['UserPass']);	
				$objSmarty->assign("MemPass1",$MemPass);
					$objSmarty->assign("SiteTitle",SiteMainTitle." -  User  Management - Edit User Details");
					$objSmarty->assign("PageTitle","User Management - Edit User Details");
				}		
				else{
				CheckAdminMgmtPermission('Redirct','MainAdmin','User','AddUser');
					$objSmarty->assign("SiteTitle",SiteMainTitle." -  User  Management - Add User Details");
					$objSmarty->assign("PageTitle","User Management - Add User Details");
				}
				
				if(!empty($_POST['AdminAction'])){
					$objAdminUsers->AdminAddUpdateBuyersSellers($con_id,$RedURL,'User');
				}
				$objSmarty->assign("TinyMCEPath",$ngconfig['TinyEditor']);
 				$objSmarty->assign("IncludeTpl", "users_new.html");
			break;
		default:
				/*****************************		Admin & Subadmin Login Permisson Check 	*************************/
			CheckAdminMgmtPermission('Redirct','MainAdmin','User','ManageUser');
				
				/******************  		Call Function For Delete,Active Inactive Table Details 	**********************/
				if(isset($_POST['Action_Type'])){ 
					if($_POST['Action_Type']=='UnVerified' || $_POST['Action_Type']=='Verified'){
					$objAdmin->AdminControlMgmt($tablename,$UniqId,'UVerifySta',$RedURL);
					}
					else{
					$objAdmin->AdminControlMgmt($tablename,$UniqId,$m_sta,$RedURL);
					}
  				} 
				/******************  		Select Site Sub-Admin List	**********************/
				$objAdminUsers->SelectBuyersSellersLists($ResVal,$PageVal,'User');
					
				/******************  		Admin Action Error and Success Messages	**********************/
				if($pro_msg!=''){
					if($pro_msg=="usucc"){
						$objSmarty->assign("SucMessage", "User Details Has Been Updated Successfully!!");
					}
					elseif($pro_msg=="asucc"){
						$objSmarty->assign("SucMessage", "New User Details Has Been Added Successfully!!");
					}
				}
				if($mgtact!="")	{	
					$ActArray=array("delsuc"=>"Deleted","actcon"=>"Verified","cancelsuc"=>"UnVerified",
							"pendsuc"=>"Pending");
					if($checkstas=="sucs"){
						$objSmarty->assign("SucMessage","Selected User(s) Has Been ".$ActArray[$mgtact]." Successfully");
					}
					else{
						$objSmarty->assign("ErrMessage","Selected User(s) Already in ".$ActArray[$mgtact]." Status");
					}
				}
				$objSmarty->assign("SiteTitle",SiteMainTitle." - User Management");	//	For Page Main Title
				$objSmarty->assign("PageTitle"," User Management");	//	For Page Sub Title
				$objSmarty->assign("IncludeTpl", "users_management.html");	//	Assign Page For Smarty	
			break;			
	}
			$objSmarty->assign("UserImgDisp",$ngconfig['UserImgDisp']);	//	Assign Page For Smarty	
			
 		/*****************************			Include Admin Menu and Ctrl Create File 	**********************************/
	include _MAINSITEPATH_."/wit_admincontrols.php";
	$MCtrlArray=array('ON'=>'ON','OFF'=>'OFF','Delete'=>'Delete');
	$objSmarty->assign("ControlList",$MCtrlArray);
$objSmarty->assign("UserImageDisp",$ngconfig['UserImageDisp']);	//	Assign Page For Smarty	
	#===========================================================================		
	$objSmarty->display("site_admin_main.html");
?>