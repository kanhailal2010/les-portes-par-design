<?php 	
	/*****************************			Site Config File				***********************************************/
	include "../wit_includes/wit_common.php";
     
	/*****************************			Include  File				***********************************************/
	include_once $ngconfig['SiteClassPath']."class.GeneralAdmin.php";
	include_once $ngconfig['SiteClassPath']."class.PEAR_RESIZE.php";
	include_once $ngconfig['SiteClassPath']."class.Quotes.php";
	/*****************************			Class Objects			***********************************************/
	$objAdmin = new GeneralAdmin(); $objPro = new Quotes();
	
	/*****************************			Include  File					***********************************************/
	include _MAINSITEPATH_."/wit_SEOURL_Function.php"; 
	
	/*****************************		Page Variables Declartion	***********************************************/
	$RedURL='financial_report.php?';$act_type=(isset($_GET['act_type'])) ? $_GET['act_type']:'';
	$mgtact=(isset($_GET['mgtact'])) ? $_GET['mgtact']:'';$con_id=(isset($_GET['con_id'])) ? $_GET['con_id']:'';
	$checkstas=(isset($_GET['checkstas'])) ? $_GET['checkstas']:'';
	$pro_msg=(isset($_GET['pro_msg'])) ? $_GET['pro_msg']:'';$SelStatus=true;
	switch($act_type)
	{	
		case 'purchase':
			/******************  		Call Function For Delete,Active Inactive Table Details 	**********************/
			CheckAdminMgmtPermission('Redirct','BothAdmin','Financial Report','ManagePurchase');
				
			if(isset($_POST['Action_Type'])){ 
	 			$objAdmin->AdminControlMgmt('pj_barazin_purchase_orders','BarPurId',$m_sta,$RedURL); 
			} 
			if($mgtact!=""){	
				$ActArray=array("delsuc"=>"Deleted","actsuc"=>"Actived","inasuc"=>"InActived");
				if($checkstas=="sucs"){
				   $objSmarty->assign("SucMessage","Selected  Purchase Report(s) Details Has Been ".$ActArray[$mgtact]." Successfully");
				}
				else{
					$objSmarty->assign("ErrMessage","Selected  Purchase Report(s) Already in ".$ActArray[$mgtact]." Status");
				}
			}
		
			$objPro->GetPurchaseOrderLists('ProList',"QuotesPage");
			
			$objSmarty->assign("SiteTitle",SiteMainTitle." Purchase Report Management");
			$objSmarty->assign("PageTitle"," Purchase Report Management");
			$objSmarty->assign("IncludeTpl", "purchase_orders.html");	
		break;
		case 'profit':
			/******************  		Call Function For Delete,Active Inactive Table Details 	**********************/
			CheckAdminMgmtPermission('Redirct','BothAdmin','Financial Report','ManageProfit');
				
			if(isset($_POST['Action_Type'])){ 
	 			$objAdmin->AdminControlMgmt('pj_barazin_purchase_orders','BarPurId',$m_sta,$RedURL); 
			} 
			if($mgtact!=""){	
				$ActArray=array("delsuc"=>"Deleted","actsuc"=>"Actived","inasuc"=>"InActived");
				if($checkstas=="sucs"){
				   $objSmarty->assign("SucMessage","Selected  Profit Report(s) Details Has Been ".$ActArray[$mgtact]." Successfully");
				}
				else{
					$objSmarty->assign("ErrMessage","Selected  Profit Report(s) Already in ".$ActArray[$mgtact]." Status");
				}
			}
		
			$objPro->GetProfitOrderLists('ProList',"QuotesPage");
			
			$objSmarty->assign("SiteTitle",SiteMainTitle." Profit Report Management");
			$objSmarty->assign("PageTitle"," Profit Report Management");
			$objSmarty->assign("IncludeTpl", "profit_orders.html");	
		break;
  		default:
			/******************  		Call Function For Delete,Active Inactive Table Details 	**********************/
			CheckAdminMgmtPermission('Redirct','BothAdmin','Financial Report','ManageSales');
				
			if(isset($_POST['Action_Type'])){ 
	 			$objAdmin->AdminControlMgmt('pj_item_payments','PayId',$m_sta,$RedURL); 
			} 
			if($mgtact!=""){	
				$ActArray=array("delsuc"=>"Deleted","actsuc"=>"Actived","inasuc"=>"InActived");
				if($checkstas=="sucs"){
				   $objSmarty->assign("SucMessage","Selected  Sales Report(s) Details Has Been ".$ActArray[$mgtact]." Successfully");
				}
				else{
					$objSmarty->assign("ErrMessage","Selected  Sales Report(s) Already in ".$ActArray[$mgtact]." Status");
				}
			}
		
			$objPro->GetSalesOrderLists('ProList',"QuotesPage");
			
			$objSmarty->assign("SiteTitle",SiteMainTitle." Sales Report Management");
			$objSmarty->assign("PageTitle"," Sales Report Management");
			$objSmarty->assign("IncludeTpl", "sales_orders.html");	
		break;
	}	
 	
     /*****************************			Include Admin Menu and Ctrl Create File 	**********************************/
	include _MAINSITEPATH_."/wit_admincontrols.php";
	
	/*****************************			Include Admin Menu and Ctrl Create File 	**********************************/
	if($act_type=='managefile' || $act_type=='manageimage'){
		$MCtrlArray=array('Delete'=>'Delete');
	}
	else{
		$MCtrlArray=array('Delete'=>'Delete');
	}
	$objSmarty->assign("ControlList",$MCtrlArray);
 	$objSmarty->assign("FinishImageDisp",$ngconfig['FinishImageDisp'] );
 	$objSmarty->assign("ShapeImageDisp",$ngconfig['ShapeImageDisp'] );
 	$objSmarty->assign("GlassImageDisp",$ngconfig['GlassImageDisp'] );
	#===========================================================================		
	$objSmarty->display("site_admin_main.html");
?>