<?php 	
	/*****************************			Site Config File				***********************************************/
	include "../wit_includes/wit_common.php";
		
	/*****************************			Include  File					***********************************************/
	include _MAINSITEPATH_."/wit_SEOURL_Function.php";
	
	/*****************************			Include Supported Class Files	********************************************/
	include_once $ngconfig['SiteClassPath']."class.GeneralAdmin.php";
	include_once $ngconfig['SiteClassPath']."class.Content.php";
	
	/*****************************			Class Objects			*		**********************************************/
	$objAdmin	= new GeneralAdmin();$objContent	= new Content();
				
	/*****************************		Database Variables Declartion	***********************************************/
	$tablename="pj_content_pages";$UniqId="page_id";$ResVal="ContList";$m_sta="page_status";$PageVal="ContentPage";
	$tablename = $objLang->tableName('pj_content_pages',true);
	
	/*****************************		Page Variables Declartion	***********************************************/
	$pro_msg=(isset($_GET['pro_msg'])) ? $_GET['pro_msg']:'';$mgtact=(isset($_GET['mgtact'])) ? $_GET['mgtact']:'';	
	$RedURL='static_pages.php?';$act_type=(isset($_GET['act_type'])) ? $_GET['act_type']:'';
	$checkstas=(isset($_GET['checkstas'])) ? $_GET['checkstas']:'';$con_id=(isset($_GET['con_id'])) ? $_GET['con_id']:'';
	
	switch($act_type)
	{
		case 'addnew':		
				/*****************************			Admin & Subadmin Login Permisson Check 	**************************/
				CheckAdminMgmtPermission('Redirct','BothAdmin','Content','AddContent');
				if($con_id!=''){
				$SelStatus=$objAdmin->SelectSingleTableRowDetails($tablename,$UniqId,'Arr',$con_id);	
				if(!$SelStatus){
					Redirect('static_pages.php?pro_msg=vfail');
				}
				CheckAdminMgmtPermission('Redirct','BothAdmin','Content','ManageStatic');
				$objSmarty->assign("SiteTitle",SiteMainTitle." - Static Page Management - Edit Static Page Details");
				$objSmarty->assign("PageTitle","Static Page Management - Edit Static Page Details");
				}else{
				CheckAdminMgmtPermission('Redirct','BothAdmin','Content','AddStatic');
				$objSmarty->assign("SiteTitle",SiteMainTitle." - Static Page Management - Add New Static Page Details");
				$objSmarty->assign("PageTitle","Static Page Management - Add New Static Page Details");
				}
				if(!empty($_POST['AdminAction'])){
					$objContent->ContentAddUpdate('static_pages','static');
				}
				
				$objSmarty->assign("TinyMCEPath",$ngconfig['TinyEditor']);
				$objSmarty->assign("RedirFileName", "static_pages");
				$objSmarty->assign("PageButName", "Static Page");
				$objSmarty->assign("IncludeTpl", "content_new.html");		
			break;
		default:
				/*****************************			Admin & Subadmin Login Permisson Check 	**************************/
				CheckAdminMgmtPermission('Redirct','BothAdmin','Content','ManageStatic');
		
				/******************  		Select Admin Posted  Content List	**********************/
				$objContent->GetContentLists('static',$ResVal,$PageVal);
				if(!empty($pro_msg)){
					if($pro_msg=="asucc")
						$objSmarty->assign("SucMessage", "New Static Page Details Has Been Added Successfully!!");
					elseif($pro_msg=="usucc"){
						$objSmarty->assign("SucMessage", "Static Page Details Has Been Updated Successfully!!");	}
				}
				
				if($mgtact!=""){
					$ActArray=array("delsuc"=>"Delete","actsuc"=>"Active","inasuc"=>"InActive");
					if($checkstas!="fals"){
						$objSmarty->assign("SucMessage","Selected Static Page Has Been ".$ActArray[$mgtact]."d Successfully");
					}
					else{
						$objSmarty->assign("ErrMessage","Selected Static Page  Already in ".$ActArray[$mgtact]." Status");
					}
				}
				
				/******************  		Assign the smarty variables					**********************/
				
				$objSmarty->assign("SiteTitle",SiteMainTitle." - Static Pages Management");	//	For Page Main Title
				$objSmarty->assign("PageTitle"," Static Pages Management");	//	For Page Sub Title
				$objSmarty->assign("IncludeTpl", "static_pages.html");	//	Assign Page For Smarty	
			break;			
	}
	
	/*****************************			Include Admin Menu and Ctrl Create File 	**********************************/
	include _MAINSITEPATH_."/wit_admincontrols.php";	
		
	#===========================================================================		
	$objSmarty->display("site_admin_main.html");
?>