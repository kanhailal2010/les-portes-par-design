<?php /* Smarty version 2.6.3, created on 2011-07-21 07:01:08
         compiled from index.html */ ?>
<link rel="stylesheet" href="js/style.css" type="text/css" media="screen" />
<link rel="stylesheet" href="js/squeeze_style.css" type="text/css" media="screen" />
<script type="text/javascript" src="js/SiteValidateFunction.js"></script>
<script type="text/javascript" src="js/jquery-1.5.2.min.js"></script>
<?php echo '

<script language="javascript">
	var ErrCss=\'txtboxerr\';var FlagIns=true;
	function GoToFormValidate()
	{	
		FlagIns=FormValidateFunction(\'Text\',\'#Name\',\'Please Enter the Name\',ErrCss);
		if(FlagIns)
		{	
			FlagIns=FormValidateFunction(\'Text\',\'#Message\',\'Please Enter your Message\',ErrCss);
		}	
		if(FlagIns)
		{	
			FlagIns=FormValidateFunction(\'Text\',\'#Image\',\'Please Select the Image\',ErrCss);
		}	
		if(FlagIns)
		{	
			FlagIns=FormValidateFunction(\'Text\',\'#Brokerage_Firm\',\'Please Select the Firm\',ErrCss);
		}	
		if(FlagIns)
		{	
			
		}
		else{	return false;	}
	}
</script>
'; ?>

<div class="form_width1 margin_auto">
		<?php if ($this->_tpl_vars['ErrorMessage'] != ''): ?>
			<div class="notification hideit failure" style="width:350px;"><p><?php echo $this->_tpl_vars['ErrorMessage']; ?>
</p></div>
		<?php elseif ($this->_tpl_vars['SucMessage'] != ''): ?>
			<div class="notification hideit success" style="width:350px;"><p><?php echo $this->_tpl_vars['SucMessage']; ?>
</p></div>
		<?php endif; ?>
		
	<form method="post" action="" name="post_form" enctype="multipart/form-data">
	<input type="hidden" name="make_pdf" value="">
		<div class="FormDiv">
		<div class="left FormDet1">Name</div>
		<div class="left FormDet3">
		<input name="Name" value="" id="Name"type="text" class="compound_width"/>
		<br/><span id="NameMsg" class="error"></span>
		</div>
		<div class="clear height20"></div>
		</div>
		<div class="FormDiv">
		<div class="left FormDet1">Message</div>
		<div class="left FormDet3">
		<input name="Message" value="" id="Message" type="text" class="compound_width"/>
		<br/><span id="MessageMsg" class="error"></span>
		</div>
		<div class="clear height20"></div>
		</div>
		
		<div class="FormDiv">
		<div class="left FormDet1">Image</div>
		<div class="left FormDet3">
		<input type="file" name="Image" id="Image" class="txtbox1"/>
		<br/><span id="ImageMsg" class="error"></span>
		</div>
		<div class="clear height20"></div>
		</div>
				
		<div class="FormDiv">
		<div class="left FormDet1">Brokerage Firm</div>
		<div class="left FormDet3">
		
		<select id="Brokerage_Firm" name="Brokerage_Firm" style="width:150px;">
		<option value="">Select Firm</option>
		<?php if (count($_from = (array)$this->_tpl_vars['Firmlist'])):
    foreach ($_from as $this->_tpl_vars['Firm']):
?>
			<option <?php if ($this->_tpl_vars['Firm']['ID'] == 'Brokerage_Firm'): ?> selected="selected" <?php endif; ?> value="<?php echo $this->_tpl_vars['Firm']['ID']; ?>
"><?php echo $this->_tpl_vars['Firm']['Name_of_Firm']; ?>
</option>
		<?php endforeach; unset($_from); endif; ?>
		</select>
		<br/><span id="Brokerage_FirmMsg" class="error"></span>
		</div>
		<div class="clear height20"></div>
		</div>	
		<div align=""><input type="submit" name="submit"  value="submit" onclick="return GoToFormValidate();"/></div>
	</form>
</div>

